<?php
class CongtrinhsFilesController extends AppController {

	var $name = 'CongtrinhsFiles';

	 var $components = array( 'Session', 'Common', 'Auth', 'Search.Prg' );

	function beforeFilter()
	{
		parent::beforeFilter();
	}

	public function them($loai, $id_congtrinh, $id = null)
	{
		if(!is_numeric($id_congtrinh))exit;
		$this->set('loai', $loai);
		$this->set('id', $id);
		if(!empty($this->data)){

			// DIULT: KIEM TRA VA THEM THEO DOI HOP DONG
			if(isset($this->data['CongtrinhsKqtte']) && ($this->data['CongtrinhsKqtte']['checkbox_TDHD'])){
				if(strlen($this->data['CongtrinhsKqtte']['ngay']) > 0)
				{
					$this->loadModel('CongtrinhsKqtte');
					$save_tdhd['CongtrinhsKqtte']['ghichu']       = $this->Common->string2date($this->data['CongtrinhsKqtte']['ghichu']);
					$save_tdhd['CongtrinhsKqtte']['noidung']      = $this->Common->html($this->data['CongtrinhsFile']['mota']);
					$save_tdhd['CongtrinhsKqtte']['congtrinh_id'] =  $id_congtrinh;
					$save_tdhd['CongtrinhsKqtte']['ngay']         = $this->Common->string2date($this->data['CongtrinhsKqtte']['ngay']);
					$this->CongtrinhsKqtte->create();
					$this->CongtrinhsKqtte->saveAll($save_tdhd);
				}
				else
					$this->Session->setFlash('Vui lòng chọn ngày', 'default', array('class' => 'message_error'));
			}	// DIULT: KET THUC KIEM TRA VA THEM THEO DOI HOP DONG

			$save['CongtrinhsFile'] = $this->data['CongtrinhsFile'];
			$save['CongtrinhsFile']['congtrinh_id'] = $id_congtrinh;

			// namnb 16/08/2014: thêm chuyển công trình cho upload file
			if (isset($save['CongtrinhsFile']['tencongtrinhid']) && is_numeric($save['CongtrinhsFile']['tencongtrinhid']) && $save['CongtrinhsFile']['tencongtrinhid'] > 0)
				$save['CongtrinhsFile']['congtrinh_id'] = $id_congtrinh = $save['CongtrinhsFile']['tencongtrinhid'];
			elseif (isset($save['CongtrinhsFile']['loai']) && $loai != $save['CongtrinhsFile']['loai'])
				// do nothing
				$save['CongtrinhsFile']['modified'] = date('Y-m-d H:i:s', strtotime($save['CongtrinhsFile']['modified']));
			else
				unset($save['CongtrinhsFile']['modified']);

			if (isset($save['CongtrinhsFile']['loai']))
				$loai = $save['CongtrinhsFile']['loai'];
			else
				$save['CongtrinhsFile']['loai'] = $loai;

			// TH click SUA
			if( isset($id) && is_numeric($id) )
				$save['CongtrinhsFile']['id'] = $id;

			$save['File'] = $this->move_file_to_app_upload($this->data, 'congtrinhs_files' );
			if( !isset( $save['File'][0]['name']) )
				unset($save['File']);

			$this->CongtrinhsFile->create();
			if ($this->CongtrinhsFile->saveAll($save)){
				// gởi thông báo tất cả nếu là knxulyhientruong
				if ($loai == 'knxulyhientruong')
					$this->CongtrinhsFile->save_giaoviec_ytuong($save) ;
				$this->Session->setFlash('Lưu thành công', 'default', array('class' => 'message_success'));
			}
			else
				$this->Session->setFlash('Bị lỗi hệ thống', 'default', array('class' => 'message_error'));

			$url = $_SERVER['HTTP_HOST'].'/congtrinhs/view/'. $id_congtrinh.'/'. $loai;
			echo $url; die;

		}elseif( is_numeric($id) ){
			$tmp = $this->CongtrinhsFile->find('first', array(
				'conditions' => array(
					'CongtrinhsFile.id' => $id
				),
				'contain' => array(
					'File'
				)
			));
			$tmp['CongtrinhsFile'] = $this->Common->html_decode($tmp['CongtrinhsFile']);
			$this->data = $tmp;
		}
		$this->layout = 'ajax';
	}

	//diult them ngay 28/11/2013 them upload file của cong trinh
	function lists($loai, $congtrinh_id)
	{
		$cond = array(
				'CongtrinhsFile'.'.congtrinh_id' => $congtrinh_id,
				'CongtrinhsFile'.'.trangthai' => 4,
				'CongtrinhsFile'.'.loai' => $loai
		);

		$limit = 20;

		$this->paginate = array(
			'fields' => array('CongtrinhsFile'.'.*', 'DATE_FORMAT('.'CongtrinhsFile'.'.created, "%d/%m/%Y %H:%i") as created'),
			'contain' => array(
				'Nguoitao',
				'File',
			),
			'conditions' => $cond,
			'order' => 'CongtrinhsFile.modified desc',
			'limit' => $limit,
		);
		
		$this->set('datas', $this->paginate());
		$this->set('congtrinh_id', $congtrinh_id);
		$this->set('loai', $loai);
		$this->layout = 'ajax';
	}
	//diult them ngay 28/11/2013 them upload file của cong trinh

	// Attach Files
	function attach_file ($congtrinh_id = '')
	{

		$this->set( 'congtrinh_id', $congtrinh_id );

		$this->set( 'danh_sach_file',$this->requestAction('/congtrinhs_files/get_file_thay_doi_loai/all/' . $congtrinh_id, array('return')));

	}

	function get_file_thay_doi_loai( $loai = 'all', $congtrinh_id = '' )
	{

		if($loai =='all')
		{
			//$loai = array('banve','bienban','khaosat','phaply','congtrinhsbct','bantinh','kiemdinhthinghiem','giaodich',
			//	'congtrinhsbaocaotuan','congvanden','congvandi','congvan','nghiemthu');
		}
		$this->set( 'loai', $loai ); // dùng cái này cho phân trang thì vẫn chuyển sang upload tương ứng

		if( !empty( $_FILES ) )
		{
			// UPLOAD FILE VAO HE THONG

			$this->loadModel('Uploader');

			$post = $_POST;

				if( $this->Session->check( 'ID_CongtrinhsFile_AttachFile' ) )
				{

				// sử dụng lại Session

					$tmp['File'][0] = $_FILES['file'];
					$save = $this->move_file_to_app_upload( $tmp, 'congtrinhs_files' );

					if( isset($save[0]['name']) ){

						$save = $save[0];
						$save['item_id'] = $this->Session->read( 'ID_CongtrinhsFile_AttachFile' );

						$this->Uploader->create();
						if( $this->Uploader->save($save) )
						{
							$this->Session->setFlash('Lưu thành công', 'default', array('class' => 'message_success'));
						}else{
							$this->Session->setFlash('Uploader: Hệ thống lưu bị trục trặc. Vui lòng thử lại', 'default', array('class' => 'message_error'));
						}

					}

				}else{

				// Giữ lại Session của id của congtrinhs_files
					$save = array();
					$save['CongtrinhsFile'] = $this->Common->html( $post['data']['Uploader'] );
					$save['CongtrinhsFile']['mota'] = $save['CongtrinhsFile']['noidung'];
					$save['CongtrinhsFile']['loai'] = $save['CongtrinhsFile']['model_loai'];

					$tmp['File'][0] = $_FILES['file'];
					$save['File'] = $this->move_file_to_app_upload( $tmp, 'congtrinhs_files' );

					$this->loadModel('CongtrinhsFile');
					if( isset($save['File'][0]) )
					{
						$this->CongtrinhsFile->create();
						if( $this->CongtrinhsFile->saveAll($save) )
						{
							$this->Uploader->updateCounter();
							$this->Session->setFlash('Lưu thành công', 'default', array('class' => 'message_success'));

							$this->Session->write( 'ID_CongtrinhsFile_AttachFile', $this->CongtrinhsFile->id );

						}else{

							$this->Session->setFlash('Hệ thống lưu bị trục trặc. Vui lòng thử lại', 'default', array('class' => 'message_error'));
						}
					}
				}

			die;

		}else{
				if( $this->Session->check( 'ID_CongtrinhsFile_AttachFile' ) )
				{
					$this->Session->delete( 'ID_CongtrinhsFile_AttachFile' );
				}

			// --- -------- Lấy công trình mặc định

			if( !is_numeric($congtrinh_id) )
			{
				$congtrinh_id = 246;
			}
			$this->loadModel('Congtrinh');

			$tmp = $this->Congtrinh->find('first', array(
				'conditions' => array('Congtrinh.id' => $congtrinh_id),
				'fields' => array('id', 'tieude'),
				'contain' => false
			));

			$this->set('congtrinh', $this->Common->html_decode( $tmp['Congtrinh'] ) );

			// end -------- Lấy công trình mặc định

			// Lấy thông tin file
			$this->loadModel('Uploader');

			$cond = array(
				'Uploader.trangthai' => 4,
				//'Uploader.nguoitao' => $this->Auth->user('id')
			);

			$contain = array(
				'CongtrinhsFile' => array( 'id', 'loai' )
			);

			if( $loai == 'all' )
			{
				//

			}elseif( $loai == 'trangcanhan' )
			{

				$cond['Uploader.controller'] = 'users';
				$contain = false;

			}else{

				$cond['CongtrinhsFile.loai'] = $loai;

			}
			$cond['CongtrinhsFile.congtrinh_id'] = $congtrinh_id;

			$this->paginate = array(
				'fields' => array('id', 'name', 'file', 'dungluong', 'created', 'controller'),
				'conditions' => $cond,
				'contain' => $contain,
				'order' => 'Uploader.id desc',
				'limit' => 8
			);
			//pr($this->paginate('Uploader' ))	;
			$this->set( 'datas', $this->paginate('Uploader' ) );
		}

	}

	//diult them ngay 28/11/2013 them upload file của cong trinh
	// Attach Files
	//diult them để cho cau cao lanh
	function chon_file_view( $id, $file_view = 1 )
	{
		if( !$this->request->is('ajax') || !is_numeric($id))exit;

		$save['id'] = $id;
		$save['view'] = $file_view;
		//$save['congtrinh_id'] = $congtrinh_id;

		if( $this->CongtrinhsFile->save($save, false) )
		{
			$this->CongtrinhsFile->updateCounter('CongtrinhsFile');
			echo 'ok';
		}

		exit;
	}

	public function them_caolanh($id_congtrinh, $loai, $id = null)
	{
		if(!is_numeric($id_congtrinh))exit;

		$this->set('loai', $loai);

		$this->set('id', $id);

		if(!empty($this->data))
		{
			$save['CongtrinhsFile'] = $this->Common->html($this->data['CongtrinhsFile']);
			$save['CongtrinhsFile']['congtrinh_id'] = $id_congtrinh;
			$save['CongtrinhsFile']['loai'] = $loai;
			$save['CongtrinhsFile']['view'] = 1;

			$thongbao = '';

			if( isset($save['CongtrinhsFile']['noinhan']) )
			{
				$save['CongtrinhsFile']['mota'] = $save['CongtrinhsFile']['mota'].'<br><br><b>Nơi nhận: </b>'.$save['CongtrinhsFile']['noinhan'];
			}

			if( isset($id) && is_numeric($id) )
			{
				$save['CongtrinhsFile']['id'] = $id;
			}
			if($loai =='congvandi' || $loai =='congvanden' || $loai =='congvan' )
			{
				if( strlen( $save['CongtrinhsFile']['tieude'] ) > 0 && strlen( $save['CongtrinhsFile']['mota'] ) > 0 && strlen( $save['CongtrinhsFile']['maso'] ) > 0 && strlen( $save['CongtrinhsFile']['noinhan'] ) > 0)
				{
					$save['File'] = $this->move_file_to_app_upload($this->data, 'congtrinhs_files' );

					if( isset($id) || isset($save['File'][0]) ){

						// VI REDIRECT NEN KO THE CHECK VALIDATE DUOC THONG CAM, HIC
						$this->CongtrinhsFile->create();
						if( $this->CongtrinhsFile->saveAll($save) )
						{
							$this->Session->setFlash('Lưu'.$thongbao.' thành công', 'default', array('class' => 'message_success'));
							$this->redirect('/congtrinhs_files/list_caolanh/'. $id_congtrinh.'/'. $loai);
							//$this->requestAction('/congtrinhs/view/'. $id_congtrinh.'/'. $loai);
						}

					}else{
						$this->Session->setFlash('Vui lòng chọn file trước khi lưu.', 'default', array('class' => 'message_error'));
					}
				}else{
					$this->Session->setFlash('Vui lòng nhập thông tin file upload <i>(tiêu đề, mô tả, số công văn, nơi nhận...)</i>.', 'default', array('class' => 'message_error'));
				}
			}
			else{
				if( strlen( $save['CongtrinhsFile']['tieude'] ) > 0 && strlen( $save['CongtrinhsFile']['mota'] ) > 0)
				{
					$save['File'] = $this->move_file_to_app_upload($this->data, 'congtrinhs_files' );

					if( isset($id) || isset($save['File'][0]) ){

						// VI REDIRECT NEN KO THE CHECK VALIDATE DUOC THONG CAM, HIC
						$this->CongtrinhsFile->create();
						if( $this->CongtrinhsFile->saveAll($save) )
						{
							$this->Session->setFlash('Lưu'.$thongbao.' thành công', 'default', array('class' => 'message_success'));

							$this->redirect('/congtrinhs_files/list_caolanh/'. $id_congtrinh.'/'. $loai);
							//pr("aaaaaaa");exit();
							//$this->requestAction('/congtrinhs/view/'. $id_congtrinh.'/'. $loai);
						}

					}else{
						$this->Session->setFlash('Vui lòng chọn file trước khi lưu.', 'default', array('class' => 'message_error'));
					}
				}else{
					$this->Session->setFlash('Vui lòng nhập thông tin file upload <i>(tiêu đề, mô tả, ...)</i>.', 'default', array('class' => 'message_error'));
				}
			}

		}elseif( is_numeric($id) )
		{
			$tmp = $this->CongtrinhsFile->find('first', array(
				'conditions' => array(
					'CongtrinhsFile.id' => $id
				),
				'contain' => array(
					'File'
				)
			));
			$pieces = explode("<br><br><b>Nơi nhận: </b>", $tmp['CongtrinhsFile']['mota']);
			$tmp['CongtrinhsFile']['mota'] = $pieces[0];
			if(count($pieces) > 1)
			{
				$tmp['CongtrinhsFile']['noinhan'] = $pieces[1];
			}

			$tmp['CongtrinhsFile'] = $this->Common->html_decode($tmp['CongtrinhsFile']);

			//pr($tmp);
			$this->data = $tmp;
		}
		$this->layout = 'ajax';
	}

	// diult ket thuc code cau cao lanh

	function list_caolanh($congtrinh_id, $loai,$tam = null)
	{
		if(!is_numeric($congtrinh_id))exit;

		$cond = array(
				'CongtrinhsFile'.'.congtrinh_id' => $congtrinh_id,
				'CongtrinhsFile'.'.trangthai' => 4,
				'CongtrinhsFile'.'.view' => 1,
				'CongtrinhsFile'.'.loai' => $loai
		);

		$limit = 20;
		$max_limit = 100;
		$timkiem = 0;
		if( isset($this->data['Search']))
		{
		/*if( isset($this->data['Search']) && strlen($this->data['Search']['q']) > 0 )
		{*/
			//$cond = $this->Common->fulltext($this->data['Search']);

			$cond['CongtrinhsFile.loai'] = $loai;

			if(strlen($this->data['Search']['tieude']) > 0)
			{
				$cond[] = $this->Common->fulltext('CongtrinhsFile.tieude', $this->data['Search']['tieude'], $cond );
			}
			if (strlen($this->data['Search']['mota']) > 0) {
				$cond[] = $this->Common->fulltext('CongtrinhsFile.mota', $this->data['Search']['mota'], $cond );
			}
			if(isset($this->data['Search']['maso']) && strlen($this->data['Search']['maso']) > 0)
			{
				if($this->data['Search']['maso'] =='0')
				{
					$cond['CongtrinhsFile.maso'] = 'CongtrinhsFile.maso IS NULL';
				}else{
					$cond[] = $this->Common->fulltext('CongtrinhsFile.maso', $this->data['Search']['maso'], $cond );
				}

			}

			if($this->data['Search']['date_tmp_tu'] !='')
			{
				$cond['CongtrinhsFile.created >='] = $this->Common->string2date($this->data['Search']['date_tmp_tu']);
			}
			if($this->data['Search']['date_tmp_den'] !='')
			{
				//$cond['CongtrinhsFile.created <='] =  $this->Common->string2date($this->data['Search']['date_tmp_den']);
				$date= $this->Common->string2date($this->data['Search']['date_tmp_den']);
				$date = strtotime(date("Y-m-d", strtotime($date)) . " +1 days");
				$endDate = date('Y-m-d', $date);
				$cond['CongtrinhsFile.created <='] = $endDate;
			}
			//pr($cond);exit();
			if(strlen($this->data['Search']['nguoitao']) > 0)
			{
				$nguoitao =$this->data['Search']['nguoitao'];
				$this->loadModel( 'User' );
				$nguoitao = $this->User->find('list', array(
					'fields' => array('id', 'id'),
					'conditions' => array(
						'User.username LIKE' => '%' . $nguoitao . '%',
						//'User.trangthai' => 4
					),
					'contain' => false
				));
				$kiemtra_nguoitao = true;
				if( empty( $nguoitao ) )
				{
					$kiemtra_nguoitao = false;
				}else{

					$this->Session->write('main_search_nguoitao',$nguoitao);
				}
				if( isset( $nguoitao ) && $kiemtra_nguoitao )
				{
					$cond['CongtrinhsFile.nguoitao'] = array_values($nguoitao);
				}
			}
			if(strlen($this->data['Search']['tenfile']) > 0)
			{
				$timkiem = 1;
				$cond1['Uploader.name LIKE'] = '%'.$this->data['Search']['tenfile'].'%';
				$cond1['Uploader.trangthai'] = 4;
				$cond1['Uploader.controller'] = 'congtrinhs_files';
			}
			$max_limit = $limit = 1000;
		}
		if($timkiem ==0)
		{
			$this->paginate = array(
			'fields' => array( 'CongtrinhsFile'.'.*','DATE_FORMAT('.'CongtrinhsFile'.'.created, "%d/%m/%Y %H:%i") as created'),
			'contain' => array(
				'Nguoitao',
				'File',
			),
			'conditions' => $cond,
			'order' => 'CongtrinhsFile.created desc',
			'limit' => $limit,
			'maxLimit' => $max_limit,
			);
			//pr($this->paginate());exit();
		}else{
			$this->loadModel('Uploader')	;
			$kiemfile = $this->Uploader->find('list', array(
					'fields' =>array('item_id','item_id'),
					'conditions' => $cond1,
					'contain' =>false,
				));
			if( isset( $kiemfile ) && count($kiemfile) )
			{
				$cond['CongtrinhsFile.id'] = array_values($kiemfile);
			}
			$this->paginate = array(
			'fields' => array( 'CongtrinhsFile'.'.*','DATE_FORMAT('.'CongtrinhsFile'.'.created, "%d/%m/%Y %H:%i") as created'),
			'contain' => array(
				'Nguoitao','File',
			),
			'conditions' => $cond,
			'order' => 'CongtrinhsFile.modified desc',
			'limit' => $limit,
			'maxLimit' => $max_limit,
			);
			//pr($this->paginate());
		}
		//pr($this->paginate());
		$this->set('datas', $this->paginate());
		$this->set('congtrinh_id', $congtrinh_id);
		$this->set('loai', $loai);
		$this->layout = 'ajax';
	}
	//diult them ngay 28/11/2013 them upload file của cong trinh
}

