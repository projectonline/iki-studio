<?php
class LichhopsController extends AppController {

    var $name = 'Lichhops';
    var $components = array( 'Common', 'Cmfile', 'Auth' );

    function beforeFilter( )
    {
        // goi den before filter cha
        parent::beforeFilter();
    }

    function index($fromday = null, $today = null)
    {
        $ts = $this->_week_range($fromday, $today);
        $this->set('thubay', substr($ts[0],0,10));

        $datas = $this->Lichhop->find('all', array(
            'fields' => array( 'id','tieude', 'thanhphan', 'vitri', 'noidung', 'thoigian', 'phongban_id', 'nguoitao', 'modified' ),
            'conditions' => array(
                'Lichhop.trangthai' => 4,
                'Lichhop.thoigian >=' => $ts[0],
                'Lichhop.thoigian <=' => $ts[1]
            ),
            'contain' => array(
                'Nguoitao', 'File'
            ),
            'order' => 'Lichhop.thoigian asc'
        ));

        $tmp = array();
        foreach($datas as $data)
        {
            $key_ngay = substr($data['Lichhop']['thoigian'],0,10);

            // kiểm tra xem là buổi sáng hay chiều
            $am_pm = 0;
            if(date('a',strtotime($data['Lichhop']['thoigian'])) == 'pm')$am_pm = 1;

            $data['Lichhop']['Nguoitao'] = $data['Nguoitao'];
            $data['Lichhop']['File'] = $data['File'];
            $tmp[$key_ngay][$am_pm][$data['Lichhop']['phongban_id']][] = $data['Lichhop'];
        }
        $datas = $tmp;
        $this->set('datas', $datas);

        $phongbans = $this->Lichhop->Phongban->find('list',array(
            'conditions' => array(
                'code1' => 'usr',
                'code2' => 'grp',
                'trangthai' => 4
            ),
            'fields' => array('id', 'name'),
            'order' => 'name asc'
        ));
        //unset($phongbans[521], $phongbans[522], $phongbans[315], $phongbans[520], $phongbans[979], $phongbans[626]);
        $this->set('phongbans', $phongbans);


        $this->set('day_weeks', array('','Thứ hai','Thứ ba','Thứ tư','Thứ năm','Thứ sáu','Thứ bảy'));

        // LẤY 4 TUẦN CỦA THÁNG ĐANG XEM
        $tuans = array();
        $this->get_tuan($fromday);
        $this->set('tuan_dangxem',$ts);
    }

    function them()
    {


        if(!empty($this->data))
        {
            $save['Lichhop'] = $this->data['Lichhop'];
            $save['Lichhop']['thoigian'] = $this->Common->string2date($save['Lichhop']['ngay_tmp']).' '.$save['Lichhop']['ngay']['hour'].':'.$save['Lichhop']['ngay']['min'].':00';

            $save['File'] = $this->move_file_to_app_upload($this->data);
            if( isset($save['File'][0]) )
            {
                $save['File'] = $save['File'][0];
            }else{
                unset($save['File']);
            }

            $this->Lichhop->create();
            if($this->Lichhop->saveAll($save))
            {
                // $this->Session->setFlash('Lưu lịch họp thành công', 'default', array('class' => 'message_success'));

                // SAVE GIAO VIEC
                if( isset($this->data['Nguoinhanloinhan']) )
                {
                    $save_giaoviec = array(
                        'Giaoviec' => array(
                            'id_object_send' => $this->Lichhop->id,// id của lời nhắn (trả lời) kèm giao việc, dùng kết hợp với model_connect
                            'noidung' => "<b>Lịch họp: </b><br> <b>Thời gian</b>: <span style='color:red'>".$save['Lichhop']['thoigian'].'</span><br> <b>Vị trí</b>: '.$save['Lichhop']['vitri'].'<br><b>Thành phần</b>: '.$save['Lichhop']['thanhphan'].'<br><b>Tiêu đề: </b>: '.$save['Lichhop']['tieude'],
                            'model_connect' => 'Lichhop', // dùng để xóa lời nhắn/trả lời thì xóa luôn giao việc
                            'model_nhanlai' => 'Lichhop',// là model của lời nhắn, dùng để lấy foreign key cho khi save vào modelsave_from_giaoviec
                            'id_nhanlai' => 1, // là number giá trị cho foreign key của model_nhanlai
                            'stick_xong' => 1, // đây là giá trị của người gửi xác nhận là giao việc đó đã xong hay chưa, mặc định set = 1, nếu bên dưới có yêu cầu phản hồi thì sẽ set = 0,
                            'string_receive_id' => '', // chuỗi lưu tất cả id người nhận, đôi lúc cần dùng search theo người nhận search cho lẹ,
                            'count_user_sent' => 0 // đếm xem là gửi cho tất cả bao nhiêu người
                        )
                    );
                    $save_giaoviec['Nguoinhan'] = array();

                    // ======== save giao viec attach them nhung nguoi duoc thong bao
                    foreach( $this->data['Nguoinhanloinhan'] as $value)
                    {
                        array_push( $save_giaoviec['Nguoinhan'], array(
                            'user_id' => $value['user_id'],
                            'yeucau_phanhoi' => $value['yeucau_phanhoi']
                        ) );

                        $save_giaoviec['Giaoviec']['string_receive_id'] .=  $value['user_id'].',';

                        if( $value['yeucau_phanhoi'] == 1 )$save_giaoviec['Giaoviec']['stick_xong'] = 0;
                    }
                    // ======== end ================================================

                    $save_giaoviec['Giaoviec']['count_user_sent'] = count($save_giaoviec['Nguoinhan']);

                    $this->loadModel('Giaoviec');
                    if( $this->Giaoviec->saveAll($save_giaoviec) )
                    {
                        //
                    }
                }

                $this->redirect( '/lichhops/danhsach' );

            }else{

                $this->Session->setFlash('Lưu lịch họp thất bại', 'default', array('class' => 'message_success'));
            }

        }else{

            $tmp['Lichhop']['ngay_tmp'] = date('d/m/Y');
            $tmp['Lichhop']['phongban_id'] = $this->Auth->user('codetbl_id');
            $this->data = $tmp;
        }

        $phongbans = $this->Lichhop->Phongban->find('list',array(
            'conditions' => array(
                'code1' => 'usr',
                'code2' => 'grp',
                'trangthai' => 4
            ),
            'fields' => array('id', 'name'),
            'order' => 'name asc'
        ));

        //unset($phongbans[521], $phongbans[522], $phongbans[315], $phongbans[520], $phongbans[979], $phongbans[626]);
        $this->set('phongbans', $phongbans);

        $this->layout = 'ajax';
    }

    function sua( $id )
    {
        if(!is_numeric($id))exit;

        if(!empty($this->data))
        {
            $save['Lichhop'] = $this->data['Lichhop'];
            $save['Lichhop']['thoigian'] = $this->Common->string2date($save['Lichhop']['ngay_tmp']).' '.$save['Lichhop']['ngay']['hour'].':'.$save['Lichhop']['ngay']['min'].':00';

            $save['File'] = $this->move_file_to_app_upload($this->data);
            if( !isset($save['File'][0]) )
            {
                $tmp = $this->Lichhop->File->find('first', array(
                    'contain' => false,
                    'conditions' => array(
                        'File.item_id' => $id,
                        'File.controller' => 'lichhops',
                        'File.trangthai' => 4
                    )
                ));
                if( isset($tmp['File']) )
                {
                    unset($tmp['File']['id'], $tmp['File']['created'], $tmp['File']['modified'], $tmp['File']['nguoitao']);
                    $save['File'] = $tmp['File'];
                }

            }else{
                $save['File'] = $save['File'][0];
            }

            $this->Lichhop->create();
            if($this->Lichhop->saveAll($save))
            {
                $this->Session->setFlash('Cập nhật lịch họp thành công', 'default', array('class' => 'message_success'));

                $this->redirect( '/lichhops/danhsach' );

            }else{
                $this->Session->setFlash('Lưu thất bại, vui lòng bấm F5 và thao tác lại. Vui lòng nhập lại.', 'default', array('class' => 'message_error'));
            }

        }else{

            $tmp1 = $this->Lichhop->find('first', array(
                'fields' => array('id', 'thoigian', 'tieude', 'noidung', 'vitri', 'thanhphan', 'thanhphan_num', 'phongban_id', 'chutri'),
                'contain' => false,
                'conditions' => array(
                    'Lichhop.id' => $id
                )
            ));

            $tmp['Lichhop'] = $this->Common->html_decode($tmp1['Lichhop']);
            $tmp['Lichhop']['ngay']['hour'] = substr($tmp['Lichhop']['thoigian'], 11,2);
            $tmp['Lichhop']['ngay']['min'] = substr($tmp['Lichhop']['thoigian'], 14,2);
            $tmp['Lichhop']['hiddenthoigian'] = substr($tmp['Lichhop']['thoigian'], 0, 10);
            $tmp['Lichhop']['phongban_id'] = $tmp1['Lichhop']['phongban_id'];
            $tmp['Lichhop']['chutri'] = $tmp1['Lichhop']['chutri'];
            $tmp['Lichhop']['ngay_tmp'] = date('d/m/Y', strtotime( $tmp1['Lichhop']['thoigian'] ));
            $this->data = $tmp;
        }

        $phongbans = $this->Lichhop->Phongban->find('list',array(
            'conditions' => array(
                'code1' => 'usr',
                'code2' => 'grp',
                'trangthai' => 4
            ),
            'fields' => array('id', 'name'),
            'order' => 'name asc'
        ));

        $this->set('phongbans', $phongbans);

        $this->layout = 'ajax';

    }

    function danhsach( $phongban_id = 0 )
    {
        $cond = array(
            'Lichhop.trangthai' => 4,
            'Lichhop.hienthi' => 1
        );

        if( is_numeric($phongban_id) && $phongban_id > 0 )
        {
            $cond['Lichhop.phongban_id'] = $phongban_id;
        }

        $this->set('phongban_id', $phongban_id);

        $this->paginate = array(
            'fields' => array('id', 'tieude', 'noidung', 'nguoitao', 'created'),
            'conditions' => $cond,
            'order' => 'Lichhop.created desc',// id
            'limit' => 8,
            'contain' => array('Nguoitao','File')
        );
        $this->set('datas', $this->paginate());

        $phongbans = $this->Lichhop->Phongban->find('list',array(
            'conditions' => array(
                'code1' => 'usr',
                'code2' => 'grp',
                'trangthai' => 4
            ),
            'fields' => array('id', 'name'),
            'order' => 'name asc',
            'contain' => false
        ));

        $this->set('phongbans', $phongbans);

        $this->layout = 'ajax';
    }

    function xem_chitiet( $id )
    {
        if( !$this->request->is('ajax') || !is_numeric($id))exit;

        $this->set('data', $this->Lichhop->find('first', array(
            'conditions' => array(
                'Lichhop.id' => $id,
                'Lichhop.trangthai' => 4
            ),
            'fields' => array(
                'id', 'thoigian', 'tieude', 'noidung', 'vitri', 'thanhphan', 'thanhphan_num',
                'created', 'chutri', 'phongban_id','nguoitao'
            ),
            'contain' => array(
                'Nguoitao',
                'File'
            )
        )));

        $phongbans = $this->Lichhop->Phongban->find('list',array(
            'conditions' => array(
                'code1' => 'usr',
                'code2' => 'grp',
                'trangthai' => 4
            ),
            'fields' => array('id', 'name'),
            'order' => 'name asc'
        ));

        $this->set('phongbans', $phongbans );
    }

    function _week_range($fromday = null, $today = null)
    {
        $ts = strtotime('now');
        if(isset($fromday))
        {
            return array($fromday.' 00:00:00', $today.' 23:59:59');
        }
        //$date = date('Y-m-d', strtotime('-'.($tuan*7).'  days'));
        //$ts = strtotime($date);
        $ts = strtotime('now');
        //if(date('N', $ts) == 1)$start = $ts;
        $start =(date('w', $ts) == 1) ? $ts : strtotime('last monday', $ts);
        if(date('w', $ts) == 0)$start = strtotime('next monday', $ts);

        return array(date('Y-m-d', $start).' 00:00:00', date('Y-m-d', strtotime('next saturday', $start)).' 23:59:59');
    }

    function get_tuan($ngay_dang_xem = null, $javascript = null)
    {
        if(!isset($ngay_dang_xem))
        {
            $tuan = date('Y-m').'-01';
            $time_tuan = strtotime($tuan);
            if(date('N', $time_tuan) == 7)$tuan = date('Y-m').'-02';
            $tuan1 = date('Y-m-d',strtotime('next saturday', $time_tuan));

            $thang = date('m'); $nam = date('Y');
        }else{
            $time_tuan = strtotime($ngay_dang_xem);
            $tuan = date('Y-m', $time_tuan).'-01';
            if(date('N', strtotime($tuan)) == 7)$tuan = date('Y-m', $time_tuan).'-02';

            $tuan1 = date('Y-m-d',strtotime('next saturday', strtotime($tuan)));

            $thang = date('m', strtotime($ngay_dang_xem)); $nam = date('Y', strtotime($ngay_dang_xem));
        }
        $tuans[] = array($tuan, $tuan1);
        for($i=0;$i<4;$i++)
        {
            $tuan = date('Y-m-d',strtotime('next monday', strtotime($tuan))); $tuan1 = date('Y-m-d',strtotime('next saturday', strtotime($tuan)));
            $tuans[] = array($tuan, $tuan1);
        }
        $this->set('tuans', $tuans);

        $this->set('thang',$thang);
        $this->set('nam',$nam);

        if(!isset($javascript))return $tuans;
    }

    //160617 Doan them
    function congviec_tuan()
    {
        $stt = 0;
        //thu: 0:sun, 1:mom, 6:satur
        $day = 1;
        $Year = date("Y");
        $thu = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        for($i = 1; $i < 7; $i++){
            $stt = $stt + 1;
            $day = $i;
            $Week = date("W");
            $DateByNumberOfWeek  = date('Y-m-d', strtotime($Year . "W". $Week  . $day));
            //$tuan[$thu[$i]] = $DateByNumberOfWeek;
            $datas = $this->Lichhop->find('all', array(
            'fields' => array( 'id','tieude', 'noidung', 'chutri', 'thanhphan', 'vitri', 'thoigian', 'nguoitao', 'modified' ),
                'conditions' => array(
                    'Lichhop.trangthai' => 4,
                    'Lichhop.thoigian' => $DateByNumberOfWeek
                ),
                'contain' => array(
                    'Nguoitao'
                ),
                'order' => 'Lichhop.id asc'
            ));
            $tuans[] = array("stt" => $stt, "thu" => $thu[$i], "ngay" => $DateByNumberOfWeek, 'lich' => $datas);

            $Week = date("W") + 1;
            $DateByNumberOfWeek  = date('Y-m-d', strtotime($Year . "W". $Week  . $day));
            //$tuantiep[$thu[$i]] = $DateByNumberOfWeek;
            $datas = $this->Lichhop->find('all', array(
            'fields' => array( 'id','tieude', 'noidung', 'chutri', 'thanhphan', 'vitri', 'thoigian', 'nguoitao', 'modified' ),
                'conditions' => array(
                    'Lichhop.trangthai' => 4,
                    'Lichhop.thoigian' => $DateByNumberOfWeek
                ),
                'contain' => array(
                    'Nguoitao'
                ),
                'order' => 'Lichhop.id asc'
            ));
            $tuantieps[] = array("stt" => $stt, "thu" => $thu[$i], "ngay" => $DateByNumberOfWeek, 'lich' => $datas);
        }

        $this->set('tuans', $tuans);
        $this->set('tuantieps', $tuantieps);
    }

    public function danhsach_ngay($ngay = null)
    {
        $datas = $this->Lichhop->find('all', array(
            'fields' => array( 'id','tieude', 'noidung', 'chutri', 'thanhphan', 'vitri', 'thoigian', 'nguoitao', 'modified' ),
                'conditions' => array(
                    'Lichhop.trangthai' => 4,
                    'Lichhop.thoigian' => $ngay
                ),
                'contain' => array(
                    'Nguoitao'
                ),
                'order' => 'Lichhop.id asc'
            ));

        $this->set('datas', $datas);
        $this->set('ngay', $ngay);

        $this->layout = 'ajax';
    }

    function them_ngay($ngay)
    {
        if(!empty($this->data))
        {
            $this->Lichhop->create();
            if ($this->Lichhop->save($this->data) ){
                $this->redirect( '/lichhops/danhsach_ngay/'.$ngay );
            }
        }

        $this->set('ngay', $ngay);

        $this->layout = 'ajax';
    }

    function sua_ngay( $id, $ngay ) {
        if(empty($this->data)){
            if(!is_numeric( $id ))exit;
            $cond = array('Lichhop.trangthai' => 4,'Lichhop.id' => $id);
            $tmp = $this->Lichhop->find( 'first',array(
                'contain' => false,
                'conditions' => $cond
            ));

            if(empty($tmp))exit;

            $this->data = $tmp;
        }else{
            $data = $this->data['Lichhop'];
            $data['id'] = $id;
            if ($this->Lichhop->save($data) ){
                $this->redirect( '/lichhops/danhsach_ngay/'.$ngay );
            }
        }
        $this->set('id', $id);
        $this->set('ngay', $ngay);
        $this->layout = 'ajax';
    }
    //160617 Doan them ---
}
