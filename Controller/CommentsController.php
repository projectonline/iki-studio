<?php
class CommentsController extends AppController {
    var $name = 'Comments';

    var $components = array( 'Session', 'Common', 'Auth', 'Search.Prg', 'Cmfile' );

    function beforeRender( )
    {
        // goi den before filter cha
        parent::beforeRender();

//		$this->helpers[] = 'AjaxMultiUpload.Upload';
    }

    public $presetVars = array(
        array('field' => 'tieude', 'type' => 'fulltext')
    );

    function search($model, $model_id )
    {
        // CONDITION
        $this->Prg->commonProcess('Comment');
        $cond = $this->Comment->parseCriteria($this->passedArgs);
        $tmp['Comment'] = $this->request->params['named'];
        $this->request->data = $tmp;

        $cond['Comment.trangthai'] = 4;

        if ( !isset($cond['no_model']) )
        {
            $cond['Comment.model']   = $model;
            $cond['Comment.item_id'] = $model_id;
        }else{
            unset($cond['no_model']);
        }


        $this->paginate = array(
//			'fields' => array('Comment.*'),
            'conditions' => $cond,
            'contain' => array('Nguoitao'),
            'limit' => 5,
            'order' => 'Comment.id desc'
        );
        $this->set('datas', $this->paginate());
    }

    function index($page = 1, $id_last = 0 )
    {
        $cond = array( 'Comment.trangthai' => 4);
        if ($id_last > 0 && is_numeric($id_last))
        {
            $cond['Comment.id <'] = $id_last;
        }

        $datas = $this->Comment->find('all', array(
            'fields'     => array('Comment.*'),
            'conditions' => $cond,
            'contain'    => array('Nguoitao'),
            'limit'      => 50,
            'order'      => 'Comment.id desc'
        ));
        $this->set('datas', $datas);
        $this->set('page', $page);

        if ($this->request->is('ajax') )
        {
            $this->set('ajax', true);
        }
    }

    function sua_comment_ajax($id, $model){
        $this->loadModel('Comment');
        if (!empty($this->data)) {

            $save = $this->data;
            $save['Comment']['id']    = $id;
            if ($this->Comment->save($save, false )){
                die(nl2br($save['Comment']['noidung']));
            }

        }
        $this->data = $this->Comment->find('first', array(
            'fields'     => array('id', 'noidung', 'noidung_attach', 'item_id'),
            'conditions' => array('id' => $id),
            'contain'    => false
        ));

        $noidung_attach = $this->data['Comment']['noidung_attach'];

        if ($model == 'Task') {
            $this->loadModel('Task');
            $tmp = $this->Task->find('first', array(
                'fields'     => array('id', 'congtrinh_id'),
                'conditions' => array('id' => $tmp['Comment']['item_id']),
                'contain'    => false
            ));
            $this->set('congtrinh_id', $tmp['Task']['congtrinh_id']);
        }
        $this->set('id', $id);

        if (strlen($noidung_attach) > 0) {
            $tmp = explode('<br>', str_replace("\n", "<br>", $noidung_attach));
            unset($tmp[0]);
            $files = array();
            foreach ($tmp as $value) {
                $tmp1 = explode('</a>', $value);
                $file_size = trim(str_replace(array('<em>(', ')</em>'), '', $tmp1[1]));
                $file = str_replace(array('<b>', '</b>','<br>'), '', $tmp1[0]);
                $file = explode('>', $file);
                $file_name = $file[1];
                $file_id = explode('title', $file[0]);
                $file_id = explode('/uploaders/download/', $file_id[0]);
                $file_id = str_replace("' ", '', $file_id[1]);

                $files[] = array($file_id, $file_name, $file_size);
            }
            $this->set('show_noidung_attach', $files);
        }
    }

    //diult them sua de chuyen ve tin nhan chinh
    function sua($congtrinh_id, $item_id, $model, $traloi_id){
        $this->loadModel($model);
        $con_nc = array(
            $model.'.trangthai'    => 4,
            $model.'.quantrong'    => 1,
            $model.'.congtrinh_id' => $congtrinh_id,
        );
        $dsctqt = $this->$model->find("list",array(
            'fields'     => array('id','tieude'),
            'contain'    => false,
            'conditions' => $con_nc
        ));
        $this->set('dsctqt', $dsctqt);

        $div_update = 'Comment_'.$model.'_'.$item_id;
        if (!empty($this->data))
        {
            $data = $this->data;
            if (isset($data['Comment']['congtrinh_id']) && $data['Comment']['congtrinh_id'] !=0)
            {
                $con_nc[$model.'.id'] = $data['Comment']['congtrinh_id'];
                $count_comment = $this->$model->find("first",array(
                    'fields'     => array('comment_count',),
                    'contain'    =>false,
                    'conditions' => $con_nc
                ));
                $save['id'] = $traloi_id; $save['item_id'] = $data['Comment']['congtrinh_id'];
                if ($this->Comment->save($save, false)){
                    $save_comment['id'] =  $data['Comment']['congtrinh_id'];
                    $save_comment['comment_count'] = $count_comment[$model]['comment_count'] + 1;
                    $this->Task->save($save_comment, false);
                    $div_update = $model.'_'.$data['Comment']['congtrinh_id'];
                }
            }
        }
        $this->set('update_div', $div_update);// DIV update la ten controller
        $this->set('traloi_id',$traloi_id);
        $this->layout = 'ajax';
    }
    //diult ket thuc them loi nha

    function them($item_id, $model, $div_update_more ='', $div_update_them ='' )
    {
        if (is_numeric($div_update_more) || is_numeric($div_update_them))
        {
            $file_congtrinh = 1;
            $this->set('file_congtrinh', $file_congtrinh);
        }

        $this->set('model', $model);

        // check truong hợp comment ở task, sẽ ghi nhận lại công trình đang click
        if ( is_numeric($div_update_more) )
        {
            $congtrinh_id = $div_update_more;
            $div_update_more = '';
            $this->set('congtrinh_id', $congtrinh_id);
        }

        $controller = Inflector::tableize($model);



        if (!empty($this->data) && $this->request->is('Ajax') && is_numeric($item_id) && is_string($model))
        {
            $save = $this->data;
            $save['Comment']['model'] = $model;
            $save['Comment']['item_id'] = $item_id;
            $save['Comment']['trangthai'] = 4;

            $this->Comment->create();
            if ($this->Comment->save($save) ){

                $this->loadModel($model);
                $count = $this->Comment->find('count', [
                    'conditions' => ['model' => $model, 'item_id' => $item_id, 'trangthai' => 4],
                    'contain' => false
                ]);
                $this->$model->save(['id' => $item_id, 'comment_count' => $count], false);

                $save['Comment']['id'] = $this->Comment->id;
                $this->set('data', $save['Comment']);
                $this->render('save_success');
            }else{
                // get lai cac file da upload ajax
                $this->set('view_file_uploaded', true);
            }
        }

        $this->set('controller', $controller);

        // NHỚ UPDATE COUNTER_CACHE BẰNG TAY

        $this->set('item_id', $item_id);

        $div_update = 'Comment_'.$model.'_'.$item_id;

        /*if ($div_update_more != '' && ($div_update_more !='ketoannoibo' || $div_update_more !='kinhdoanhnoibo'))
        {
            $div_update = $div_update_more.'Comment_'.$model.'_'.$item_id;
        }
        else*/if ($div_update_more != '' && $div_update_more =='ketoannoibo'){
            $div_update = 'Comment_Loinhanketoan_noibo_'.$item_id;
        }
        elseif ($div_update_more != '' && $div_update_more =='kinhdoanhnoibo'){
            $div_update = 'Comment_Loinhanpkinhdoanh_noibo_'.$item_id;
        }elseif ($div_update_more != '' && $div_update_more =='vibimnoibo'){
            $div_update = 'Comment_Loinhanpkinhdoanh_noibo_'.$item_id;
        }else{

            if ($div_update_them != '')
            {
                $div_update_more = $div_update_them.'Comment_'.$model.'_'.$item_id;
                $div_update = $div_update_them.'Comment_'.$model.'_'.$item_id;
            }
            elseif ($div_update_more !='') {
                $div_update =$div_update_more.'Comment_'.$model.'_'.$item_id;
                $div_update_more = $div_update_more.'Comment_'.$model.'_'.$item_id;
            }
        }

        $this->set('div_update_more',$div_update_more);

        $this->set('update_div', $div_update);// DIV update la ten controller

        $this->layout = 'ajax';
    }

//160616 Doan them
    function them_congtrinh($item_id, $model, $div_update_more ='', $div_update_them ='' )
    {
        /*if (is_numeric($div_update_more) || is_numeric($div_update_them))
        {
            $file_congtrinh = 1;
            $this->set('file_congtrinh', $file_congtrinh);
        }*/

        $this->set('model', $model);

        // check truong hợp comment ở task, sẽ ghi nhận lại công trình đang click
        if ( is_numeric($div_update_more) )
        {
            $congtrinh_id = $div_update_more;
            $div_update_more = '';
            $this->set('congtrinh_id', $congtrinh_id);
        }

        $controller = Inflector::tableize($model);

        if (!empty($this->data)/* && $this->request->is('Ajax') && is_numeric($item_id) && is_string($model)*/)
        {
            $save = $this->data;
            $save['Comment']['model'] = $model;
            $save['Comment']['item_id'] = $item_id;
            $save['Comment']['trangthai'] = 4;

            $save['File'] = $this->move_file_to_app_upload($this->data);

            if( isset($save['File'][0]))
            {
                $save['File'] = $save['File'][0];
                $save['File']['item_id'] = $save['Comment']['tab'];
                $save['File']['congtrinh_id'] = $congtrinh_id;
            }else{
                unset($save['File']);
            }
            /*$this->loadModel('File');
            if($this->File->saveAll($save)){
                //$this->redirect( '/congtrinhs/lists_file/'.$tab.'/'.$congtrinh_id );
            }*/

            $this->Comment->create();
            if ($this->Comment->saveAll($save) ){


                $this->loadModel($model);
                $count = $this->Comment->find('count', [
                    'conditions' => ['model' => $model, 'item_id' => $item_id, 'trangthai' => 4],
                    'contain' => false
                ]);
                $this->$model->save(['id' => $item_id, 'comment_count' => $count], false);

                $save['Comment']['id'] = $this->Comment->id;
                $this->set('data', $save['Comment']);
                $this->render('save_success');
                $this->redirect( '/comments/danhsach/'.$congtrinh_id );
            }else{
                // get lai cac file da upload ajax
                $this->set('view_file_uploaded', true);
            }
        }

        $this->set('controller', $controller);

        // NHỚ UPDATE COUNTER_CACHE BẰNG TAY

        $this->set('item_id', $item_id);

        $div_update = 'Comment_'.$model.'_'.$item_id;

        /*if ($div_update_more != '' && ($div_update_more !='ketoannoibo' || $div_update_more !='kinhdoanhnoibo'))
        {
            $div_update = $div_update_more.'Comment_'.$model.'_'.$item_id;
        }
        else*//*if ($div_update_more != '' && $div_update_more =='ketoannoibo'){
            $div_update = 'Comment_Loinhanketoan_noibo_'.$item_id;
        }
        elseif ($div_update_more != '' && $div_update_more =='kinhdoanhnoibo'){
            $div_update = 'Comment_Loinhanpkinhdoanh_noibo_'.$item_id;
        }elseif ($div_update_more != '' && $div_update_more =='vibimnoibo'){
            $div_update = 'Comment_Loinhanpkinhdoanh_noibo_'.$item_id;
        }else{*/

            if ($div_update_them != '')
            {
                $div_update_more = $div_update_them.'Comment_'.$model.'_'.$item_id;
                $div_update = $div_update_them.'Comment_'.$model.'_'.$item_id;
            }
            elseif ($div_update_more !='') {
                $div_update =$div_update_more.'Comment_'.$model.'_'.$item_id;
                $div_update_more = $div_update_more.'Comment_'.$model.'_'.$item_id;
            }
        //}

        $tabs[1] = "Input";
        $tabs[2] = "Output";
        //$tabs[3] = "Original File";
        $tabs[4] = "Process";
        $tabs[5] = "MOM";
        $tabs[6] = "Contract";
        $tabs[7] = "Legal Docs.";
        $tabs[8] = "Reference";
        $tabs[9] = "Site Picture";
        //$tabs[10] = "Previous Backup";
        $this->set('tabs', $tabs);

        $this->set('div_update_more',$div_update_more);

        $this->set('update_div', $div_update);// DIV update la ten controller

        $this->layout = 'ajax';
    }

    public function danhsach($congtrinh_id = null, $tab = 0)
    {
        $cond = array('Comment.trangthai' => 4,
                        'Comment.model' => 'Congtrinh',
                        'Comment.item_id' => $congtrinh_id);
        if($tab != 0){
            $cond['Comment.tab'] = $tab;
        }
        $this->paginate = array(
            'conditions' => $cond,
            'order' => 'Comment.modified desc',
            'limit' => 10,
            'contain' => array(
                'Nguoitao',
                'File'
        ));

        $this->set('data', $this->paginate());
        $this->set('tab', $tab);
        $this->set('congtrinh_id', $congtrinh_id);
        $this->set('div_update', 'comment_congtrinh_'.$congtrinh_id);

        $this->layout = 'ajax';

    }
//160616 Doan them ---

    function them_romchatrieng($item_id)
    {
        if (!empty($this->data)) {
            $save = $this->data['Comment'];
            $save['model']   = 'CongtrinhsCongviec';
            $save['item_id'] = $item_id;

            $this->loadModel('Comment');
            if ($this->Comment->save($save)) {
                $this->set('data', array(
                    'noidung'   => $save['noidung'],
                    'id'        => $this->Comment->id,
                    'item_id'   => $save['item_id'],
                    'model'     => $save['model'],
                    'trangthai' => 4,
                    'quantrong' => 0
                ));

                $s = array();
                $s['id'] = $save['item_id'];
                $s['ngay_thao_luan'] = date('Y-m-d H:i:s');
                $this->loadModel('CongtrinhsCongviec');
                $this->CongtrinhsCongviec->save($s);

                $this->render('save_success');
            }
        }
        $this->set('update_div',  'CMRomchatrieng_CM_'.$item_id);// DIV update la ten controller
        $this->layout = 'ajax';
    }

    function _get_user_giaoviec($item_id, $mymodel )
    {
        $result = array();

        // lay danh sach user da duoc thong bao truoc do
        $this->loadModel('Giaoviec');
        $tmp = $this->Giaoviec->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                'Giaoviec.model_nhanlai' => $mymodel,
                'Giaoviec.id_nhanlai'    => $item_id,
                'Giaoviec.nguoitao'      => $this->Auth->user('id'),
                'Giaoviec.trangthai'     => 4
            ),
            'contain' => false,
            'order' => 'id desc'
        ));

        if ( isset($tmp['Giaoviec']) )
        {
            $this->loadModel('GiaoviecsUser');
            $result = $this->GiaoviecsUser->find('list', array(
                'fields' => array('id', 'user_id'),
                'conditions' => array(
                    'GiaoviecsUser.giaoviec_id' => $tmp['Giaoviec']['id']
                ),
                'contain' => false
            ));
            $result = array_unique($result);
            if ( count($result) > 80 )
            {
                return array( 100 => 1);
            }else{

                // LAY RA NHUNG NGUOI COMMENT SAU NAY VA THEM VAO
                $comment = $this->Comment->find('first', array(
                    'fields' => array('id', 'created'),
                    'conditions' => array(
                        'Comment.trangthai' => 4,
                        'Comment.item_id'   => $item_id,
                        'Comment.model'     => $mymodel,
                        'Comment.nguoitao'  => $this->Auth->user('id')
                    ),
                    'order' => 'id desc'
                ));

                if ( isset($comment['Comment']))
                {
                    $cond = array(
                        'Comment.trangthai'   => 4,
                        'Comment.item_id'     => $item_id,
                        'Comment.model'       => $mymodel,
                        'Comment.created >='  => $comment['Comment']['created'],
                        'Comment.nguoitao <>' => $this->Auth->user('id')
                    );
                }else{
                    $cond = array(
                        'Comment.trangthai'   => 4,
                        'Comment.item_id'     => $item_id,
                        'Comment.model'       => $mymodel,
                        'Comment.nguoitao <>' => $this->Auth->user('id')
                    );
                }

                $comment = $this->Comment->find('all', array(
                    'fields'     => array('id', 'nguoitao'),
                    'conditions' => $cond,
                    'group'      => 'nguoitao',
                    'limit'      => 50
                ));

                if (isset($comment[0]))
                    $result = array_unique(array_merge(Set::extract('/Comment/nguoitao', $comment), $result));
            }

        }else{

            // lay ra danh sach tat ca nhung nguoi da tung tra loi cho loi nhan nay
            $cond = array(
                'Comment.trangthai' => 4,
                'Comment.item_id'   => $item_id,
                'Comment.model'     => $mymodel,
                'Comment.nguoitao <>' => $this->Auth->user('id')
            );

            $comment = $this->Comment->find('all', array(
                'fields' => array('id', 'nguoitao'),
                'conditions' => $cond,
                'group' => 'nguoitao',
                'limit' => 50
            ));

            if ( isset($comment[0]) )
                $result = array_unique(Set::extract('/Comment/nguoitao', $comment));

            $this->loadModel($mymodel);
            $nguoitaoloinhans = $this->{$mymodel}->find('first', array(
                'fields' => array('id', 'nguoitao'),
                'conditions' => array(
                    $mymodel.'.id' => $item_id
                ),
                'contain' => false
            ));
            $result[] = $nguoitaoloinhans[$mymodel]['nguoitao'];
        }

        return $result;
    }

    //diult chinh sua, vaf them ngay 25/11/2013

    //diult them cho cau cao lanh
    function them_caolanh($item_id, $model, $div_update_more ='', $div_update_them ='' )
    {
        if (is_numeric($div_update_more) || is_numeric($div_update_them))
        {
            $file_congtrinh = 1;
            $this->set('file_congtrinh', $file_congtrinh);
        }

        $this->set('model', $model);

        // check truong hợp comment ở task, sẽ ghi nhận lại công trình đang click
        if ( is_numeric($div_update_more) )
        {
            $congtrinh_id = $div_update_more;
            $div_update_more = '';
            $this->set('congtrinh_id', $congtrinh_id);
        }

        $controller = Inflector::tableize($model);

        //DIULT: KIEM TRA VÀ THEM THEO DOI HOP DONG

        if (isset($this->data['CongtrinhsKqtte']) && ($this->data['CongtrinhsKqtte']['checkbox_TDHD']))
        {
            if (strlen($this->data['CongtrinhsKqtte']['ngay']) > 0)
            {
                $this->loadModel('CongtrinhsKqtte');
                $save_tdhd['CongtrinhsKqtte']['ghichu']       = $this->Common->string2date($this->data['CongtrinhsKqtte']['ghichu']);
                $save_tdhd['CongtrinhsKqtte']['noidung']      = $this->Common->html($this->data['Comment']['noidung']);
                $save_tdhd['CongtrinhsKqtte']['congtrinh_id'] = $congtrinh_id;
                $save_tdhd['CongtrinhsKqtte']['ngay']         = $this->Common->string2date($this->data['CongtrinhsKqtte']['ngay']);
                $this->CongtrinhsKqtte->create();
                $this->CongtrinhsKqtte->saveAll($save_tdhd);
            }
            else{
                $this->Session->setFlash('Vui lòng chọn ngày', 'default', array('class' => 'message_error'));
            }
        }//DIULT: KET THUC KIEM TRA VÀ THEM THEO DOI HOP DONG

        // KIEM TRA SMS
        $check_sms = true; $save_SMS = array();
        if ( isset($this->data['Sms'] ) && $this->data['Sms']['checkbox'] )
        {
            if ( strlen($this->data['Sms']['to']) > 0)
            {

                $sms_user_to = explode(',', $this->data['Sms']['to']);
                foreach ($sms_user_to as $key => $value)
                {
                    $tmp = $this->Comment->Nguoitao->find('first', array(
                        'fields' => array('id', 'tel', 'line_dienthoai', 'username'),
                        'conditions' => array(
                            'Nguoitao.username'  => trim($value),
                            'Nguoitao.trangthai' => 4
                        ),
                        'contain' => false
                    ));

                    if ( isset($tmp['Nguoitao']) && strlen($tmp['Nguoitao']['tel']) >= 10)
                    {
                        $save_SMS[] = array(
                            'noidung'  => substr($this->data['Sms']['text'], 0, 160),
                            'sdt'      => str_replace(array(' ', ',', '.'), '', $tmp['Nguoitao']['tel']),
                            'user_id'  => $tmp['Nguoitao']['id'],
                            'username' => $tmp['Nguoitao']['username']
                        );
                    }else{

                        $check_sms = false;
                        $this->Session->setFlash('Thành viên: '.trim($value).' không tồn tại hoặc chưa có số điện thoại.', 'default', array('class' => 'message_error'));
                        break;
                    }
                }

            }else{

                $check_sms = false;
                $this->Session->setFlash('Vui lòng nhập tên người nhận SMS ở mục "đến"', 'default', array('class' => 'message_error'));
            }

            if ( strlen(trim(str_replace('<SMS qlda> '.$this->Auth->user('username').':', '', $this->data['Sms']['text']))) < 3)
            {
                $check_sms = false;
                $this->Session->setFlash('Vui lòng nhập nội dung SMS', 'default', array('class' => 'message_error'));
            }

        }

        if ($check_sms && !empty($this->data) && $this->request->is('Ajax') && is_numeric($item_id) && is_string($model))
        {
            $save = $this->data;
            //z$save);
            $save['Comment']               = $this->Common->html($save['Comment']);
            $save['Comment']['model']      = $model;
            $save['Comment']['item_id']    = $item_id;
            $save['Comment']['public_cdt'] = 1;

            if ($this->Comment->save_giaoviec_attach_file($save, array('model_nhanlai' => $model, 'id_nhanlai' => $item_id, 'congtrinh_id' => (isset($congtrinh_id)?$congtrinh_id:0))) )
            {

                // SAVE SMS MESSAGE
                $this->loadModel('Smstmp');
                if ( !empty($save_SMS) )
                {
                    if ( !$this->Smstmp->saveAll($save_SMS))
                    {
                        $this->Session->setFlash('SMS lưu bị lỗi hệ thống, vui lòng báo gấp cho IT, xin cám ơn.', 'default', array('class' => 'message_error'));
                    }
                }
                // END SMS

                // cap nhat lai du lieu cua $model
                $count = $this->Comment->find('count', array(
                    'conditions' => array(
                        'Comment.model'     => $model,
                        'Comment.trangthai' => 4,
                        'Comment.item_id'   => $item_id
                    ),
                    'contain' => false
                ));

                $this->loadModel($model);

                $save_tmp = array(
                    'id' => $item_id,
                    'comment_count' => $count
                );

                if ( isset($save['Comment']['quantrong']) && $save['Comment']['quantrong'] && $this->{$model}->hasField('comment_count_qt') )
                {
                    $count_qt = $this->Comment->find('count', array(
                        'conditions' => array(
                            'Comment.model'     => $model,
                            'Comment.trangthai' => 4,
                            'Comment.item_id'   => $item_id,
                            'Comment.quantrong' => 1
                        ),
                        'contain' => false
                    ));

                    $save_tmp['comment_count_qt'] = $count_qt;
                }

                $this->loadModel($model);
                if ($model == 'Task' )
                {
                    // == Dem so luong user tham gia vao loi nhan  ====
                    $tmp = $this->Task->find('first', array(
                        'fields' => array('id', 'num_cm_per_department'),
                        'conditions' => array(
                            'Task.id' => $item_id
                        ),
                        'contain' => false
                    ));

                    $tmp = explode('_._', $tmp['Task']['num_cm_per_department']);
                    $tmps = array();
                    $check_new_phongban = true;
                    foreach($tmp as $value )
                    {
                        if ( !isset($value[1]) )continue;

                        $value = explode('_', $value);
                        if ($value[0] == $this->Auth->user('codetbl_id') )
                        {
                            $value[1] += 1;
                            $check_new_phongban = false;
                        }
                        $tmps[] = $value[0].'_'.$value[1];
                    }
                    if ($check_new_phongban )
                    {
                        $tmps[] = $this->Auth->user('codetbl_id').'_1';
                    }

                    $save_tmp['num_cm_per_department'] = ''.implode('_._', $tmps).'';

                }// end == Dem so luong user tham gia vao loi nhan ====


                if ($this->{$model}->save($save_tmp, false));
                // ket thuc cap nhat

                $this->set('data', array(
                    'noidung'   => $this->Comment->noidung_save,
                    'id'        => $this->Comment->id,
                    'item_id'   => $item_id,
                    'model'     => $model,
                    'quantrong' => ((isset($save['Comment']['quantrong']) && $save['Comment']['quantrong'] == 1 )?1:0)
                ));

                $this->render('save_success');
            }else{

                // get lai cac file da upload ajax
                $this->set('view_file_uploaded', true);
            }
        }else{
            $tmp = $this->_get_user_giaoviec($item_id, $model);
            $this->set('user_giaoviecs', $tmp);
        }

        $this->set('controller', $controller);

        // NHỚ UPDATE COUNTER_CACHE BẰNG TAY

        $this->set('item_id', $item_id);

        $div_update = 'Comment_'.$model.'_'.$item_id;

        if ($div_update_more != '' )
        {
            $div_update = $div_update_more.'Comment_'.$model.'_'.$item_id;
        }
        //diult moi vao ngay 25/11/2013
        if ($div_update_them != '')
        {
            $div_update = $div_update_them.'Comment_'.$model.'_'.$item_id;
        }


        $this->set('update_div', $div_update);// DIV update la ten controller
        //thay image

        $this->layout = 'ajax';
    }
    // diult ket thuc them cho cau cao lanh


    function next_comments($id_next, $model, $item_id,$kiemtra , $congtrinh_id)
    {

        if ($this->request->is('Ajax') && is_numeric($id_next) && is_string($model))
        {
            $this->set('datas', $this->Comment->find('all', array(
                'fields'     => array('id', 'noidung','noidung_attach', 'created', 'nguoitao'),
                'order'      => 'Comment.id desc',
                'conditions' => array(
                    'Comment.id <'      => $id_next,
                    'Comment.item_id'   => $item_id,
                    'Comment.model'     => $model,
                    'Comment.trangthai' => 4
                ),
                'contain' => array('Nguoitao'),
                'limit' => 3
            )));
            // lay danh sach loi nhan trong tasks cua cong trinh duoc danh dau quan trong
            if (isset($congtrinh_id) && $congtrinh_id !=0)
            {
                $this->loadModel($model);
                $con_nc = array(
                    $model.'.trangthai'    => 4,
                    $model.'.quantrong'    => 1,
                    $model.'.congtrinh_id' => $congtrinh_id,
                );
                $dsctqt = $this->$model->find("list",array(
                    'fields'     => array('id','tieude',),
                    'contain'    => false,
                    'conditions' => $con_nc
                ));
                $this->set('dsctqt', $dsctqt);
            }

            $this->set('model_loinhan', $model);
            $this->set('kiemtra', $kiemtra);
            $this->set('congtrinh_id', $congtrinh_id);
            $this->set('item_id', $item_id);

        }else{
            exit;
        }
    }
    function next_comments_all($id_next, $model, $item_id,$kiemtra , $congtrinh_id)
    {

        if ($this->request->is('Ajax') && is_numeric($id_next) && is_string($model))
        {
            $this->set('datas', $this->Comment->find('all', array(
                'fields'     => array('id', 'noidung','noidung_attach', 'created', 'nguoitao', 'like', 'like_people', 'quantrong'),
                'order'      => 'Comment.id desc',
                'conditions' => array(
                    'Comment.id <'      => $id_next,
                    'Comment.item_id'   => $item_id,
                    'Comment.model'     => $model,
                    'Comment.trangthai' => 4
                ),
                'contain' => array('Nguoitao'),
            )));
            // lay danh sach loi nhan trong tasks cua cong trinh duoc danh dau quan trong
            if (isset($congtrinh_id) && $congtrinh_id !=0)
            {
                $this->loadModel($model);
                $con_nc = array(
                    $model.'.trangthai'    => 4,
                    $model.'.quantrong'    => 1,
                    $model.'.congtrinh_id' => $congtrinh_id,
                );
                $dsctqt = $this->$model->find("list",array(
                    'fields'     => array('id','tieude',),
                    'contain'    => false,
                    'conditions' => $con_nc
                ));
                $this->set('dsctqt', $dsctqt);
            }

            $this->set('model_loinhan', $model);
            $this->set('kiemtra', $kiemtra);
            $this->set('congtrinh_id', $congtrinh_id);
            $this->set('item_id', $item_id);

        }else{
            exit;
        }
    }
    //diult ket thuc them va chinh sua ngay 25/11/2013

    function all_comment_quantrong($model, $item_id )
    {
        if ($this->request->is('Ajax') && is_numeric($item_id) && is_string($model))
        {
            $this->set('datas', $this->Comment->find('all', array(
                'fields' => array('id', 'noidung', 'created', 'nguoitao', 'like', 'like_people', 'quantrong'),
                'order' => 'Comment.id desc',
                'conditions' => array(
                    'Comment.item_id'   => $item_id,
                    'Comment.model'     => $model,
                    'Comment.trangthai' => 4,
                    'Comment.quantrong' => 1
                ),
                'contain' => array('Nguoitao')
            )));
        }else{
            echo 'Có lỗi trong quá trình xử lý. Thao tác của bạn không hợp lệ.'; exit;
        }
    }

    function click_like($model, $id, $type )
    {
        if ( !is_numeric($id) && !$this->request->is('Ajax') )exit;

        $data = $this->Comment->find('first', array(
            'fields' => array('id', 'like', 'like_people', 'modified', 'model'),
            'conditions' => array('Comment.id' => $id),
            'contain' => false,
        ));

        if ( !isset($data['Comment']['id']) )exit;

        $save['id'] = $id;
        $save['modified'] = $data['Comment']['modified'];

        if ($data['Comment']['like_people'] == '')
        {
            $save['like'] = 1;
            $save['like_people'] = $this->Auth->user('username');
        }else{
            if ($type == 1 )
            {
                $save['like'] = $data['Comment']['like'] + 1;
                $tmp = explode('_._', $data['Comment']['like_people']);
                $tmp[] = $this->Auth->user('username');
                $save['like_people'] = implode('_._', $tmp);
            }else{

                // unlike
                $save['like'] = $data['Comment']['like'] - 1;
                $save['like_people'] = str_replace($this->Auth->user('username'), '', $data['Comment']['like_people']);
                $save['like_people'] = str_replace('_.__._', '_._', $save['like_people']);
            }
        }

        if ($this->Comment->save($save) )
        {
            echo 'ok';
            $this->Comment->updateCounter($data['Comment']['model']);
        }
        exit;
    }

    //diult them cho cau cao lanh
    function next_comments_caolanh($id_next, $model, $item_id,$kiemtra , $congtrinh_id)
    {

        if ($this->request->is('Ajax') && is_numeric($id_next) && is_string($model))
        {
            $this->set('datas', $this->Comment->find('all', array(
                'fields' => array('id', 'noidung', 'created', 'nguoitao', 'like', 'like_people', 'quantrong','public_cdt'),
                'order' => 'Comment.id desc',
                'conditions' => array(
                    'Comment.id <'       => $id_next,
                    'Comment.item_id'    => $item_id,
                    'Comment.model'      => $model,
                    'Comment.trangthai'  => 4,
                    'Comment.public_cdt' => 1,
                ),
                'contain' => array('Nguoitao'),
                'limit' => 5
            )));
            // lay danh sach loi nhan trong tasks cua cong trinh duoc danh dau quan trong
            if (isset($congtrinh_id) && $congtrinh_id !=0)
            {
                $this->loadModel($model);
                $con_nc = array(
                    $model.'.trangthai'    => 4,
                    $model.'.quantrong'    => 1,
                    $model.'.congtrinh_id' => $congtrinh_id,
                );
                $dsctqt = $this->$model->find("list",array(
                    'fields'     => array('id','tieude',),
                    'contain'    => false,
                    'conditions' => $con_nc
                ));
                $this->set('dsctqt', $dsctqt);
            }

            $this->set('model_loinhan', $model);
            $this->set('kiemtra', $kiemtra);
            $this->set('congtrinh_id', $congtrinh_id);
            $this->set('item_id', $item_id);

        }else{
            exit;
        }
    }

    //diult them để cho cau cao lanh
    function chon_comment_view($id,$item_id, $file_view = 1 )
    {
        if ( !$this->request->is('ajax') || !is_numeric($id))exit;
        $this->loadModel('Task');
        $tmp = $this->Task->find('first', array(
            'fields'     => array('id', 'commnet_public'),
            'conditions' => array( 'Task.id' => $item_id ),
            'contain'    => false
        ));
        //pr($tmp);exit();
        if ($file_view ==1)
            $tmp_public['commnet_public'] = $tmp['Task']['commnet_public'] + 1;
        else
            $tmp_public['commnet_public'] = $tmp['Task']['commnet_public'] - 1;
        $tmp_public['id'] = $item_id;
        $save['id'] = $id;
        $save['public_cdt'] = $file_view;
        //$save['congtrinh_id'] = $congtrinh_id;

        if ($this->Comment->save($save, false) )
        {
            $this->Comment->updateCounter('Comment');
            if ($this->Task->save($tmp_public))
            {
                $this->Task->updateCounter('Task');
                echo 'ok';
            }
        }
        exit;
    }
    // diult ket thuc code cau cao lanh
}
