<?php
class CodetblsController extends AppController {

	var $name = 'Codetbls';
	var $components = array( 'Session', 'Common', 'Auth', 'Search.Prg' );

	public $presetVars = array(
	);

	function beforeFilter( )
	{
		// goi den before filter cha
		parent::beforeFilter( );
	}

	function all()
	{
		// CONDITION
		$this->Prg->commonProcess('Codetbl');
		$cond = $this->Codetbl->parseCriteria($this->passedArgs);
		$tmp['Codetbl'] = $this->request->params['named'];
		$this->request->data = $tmp;

		$cond['Codetbl.trangthai'] = 4;

		$this->paginate = array(
			'conditions' => $cond,
			'order' => 'Codetbl.id desc',
			'limit' => 20,
			'contain' => false
		);

		$this->set('datas', $this->paginate());
	}

	function all_them()
	{
		if(!empty($this->data))
		{
			$tmp = $this->data['Codetbl']['date_tmp'];

			$save = $this->Common->html($this->data['Codetbl']);
			$save['date'] = $tmp;

			if($this->Codetbl->save($save))
			{
				$this->Session->setFlash('Lưu thành công', 'default', array('class' => 'message_success'));
			}else{
				$this->Session->setFlash('Lưu thất bại', 'default', array('class' => 'message_error'));
			}
		}

	}

	function all_sua($id)
	{
		if(!is_numeric($id))exit;
		if(!empty($this->data))
		{
			$tmp = $this->data['Codetbl']['date_tmp'];

			$save = $this->Common->html($this->data['Codetbl']);
			$save['id'] = $id;
			$save['date'] = $tmp;

			if($this->Codetbl->save($save))
			{
				$this->Session->setFlash('Lưu thành công', 'default', array('class' => 'message_success'));
			}else{
				$this->Session->setFlash('Lưu thất bại', 'default', array('class' => 'message_error'));
			}
		}

		$tmp = $this->Codetbl->find('first', array(
			'contain' => false,
			'conditions' => array('Codetbl.id' => $id)
		));
		$tmp['Codetbl'] = $this->Common->html_decode($tmp['Codetbl']);
		$tmp['Codetbl']['date_tmp'] = $tmp['Codetbl']['date'];

		$this->request->data = $tmp;

	}

	function _chudautu_get_cond()
	{
		$cond['Codetbl.code1'] = 'chudautu';
		$cond['Codetbl.code2'] = 'ten';
		$cond['Codetbl.trangthai'] = 4;

		if(isset($this->request->query['name']))
		{
			$this->request->query = $this->Common->html($this->request->query);
			if(strlen($this->request->query['name']) > 0)
				$cond[] = $this->Common->fulltext('Codetbl.name', $this->request->query['name']);

			$tmp[$this->modelClass] = $this->request->query;
			$this->data = $tmp;
		}
		return $cond;
	}

	function chudautu()
	{

		$this->paginate = array(
			'conditions' => $this->_chudautu_get_cond(),
			'order' => 'Codetbl.name asc',
			'limit' => 40,
			'contain' => false);
		$this->set('datas',$this->paginate());

	}

	function chudautu_them()
	{
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['code1'] = 'chudautu';$data[$this->modelClass]['code2'] = 'ten';
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu chủ đầu tư thành công.'));
			}else{
				$this->set('message_error', __('Lưu chủ đầu tư thất bại.'));
			}
		}

	}

	function chudautu_sua($id)
	{
		if(!is_numeric($id))exit;
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['id'] = $id;
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu chủ đầu tư thành công.'));
			}else{
				$this->set('message_error', __('Lưu chủ đầu tư thất bại.'));
			}
		}

		$tmp = $this->{$this->modelClass}->find('first', array(
			'contain' => false,
			'conditions' => array('Codetbl.id' => $id)
		));
		$tmp[$this->modelClass] = $this->Common->html_decode($tmp[$this->modelClass]);

		$this->request->data = $tmp;

	}

	function congty()
	{
		$cond['Codetbl.code1'] = 'congtrinhs';$cond['Codetbl.code2'] = 'congty';$cond['Codetbl.trangthai'] = 4;
		$this->paginate = array('conditions' => $cond,'order' => 'Codetbl.name asc','limit' => 40,
			'contain' => false);
		$this->set('datas',$this->paginate());

	}

	function congty_them()
	{
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['code1'] = 'congtrinhs';$data[$this->modelClass]['code2'] = 'congty';
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu công ty thành công.'));
			}else{
				$this->set('message_error', __('Lưu công ty thất bại.'));
			}
		}

	}

	function congty_sua($id)
	{
		if(!is_numeric($id))exit;
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['id'] = $id;
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu công ty thành công.'));
			}else{
				$this->set('message_error', __('Lưu công ty thất bại.'));
			}
		}
		$tmp = $this->{$this->modelClass}->find('first', array(
			'contain' => false,
			'conditions' => array('Codetbl.id' => $id)
		));
		$tmp[$this->modelClass] = $this->Common->html_decode($tmp[$this->modelClass]);

		$this->request->data = $tmp;
	}

	function loai()
	{
		$cond['Codetbl.code1'] = 'ctr';
		$cond['Codetbl.code2'] = 'tpy';
		$cond['Codetbl.trangthai'] = 4;
		$cond['Codetbl.parent'] = 0;

		$this->set('datas',$this->{$this->modelClass}->find('all', array(
			'conditions' => $cond,
			'order' => 'Codetbl.name asc',
			'contain' => array('LoaiCon' => array('id', 'name'))
		)));


		$cond['Codetbl.parent'] = 1;
		$this->set('no_datas',$this->{$this->modelClass}->find('all', array(
			'conditions' => $cond,
			'order' => 'Codetbl.name asc',
			'contain' => false
		)));
	}

	function loai_them()
	{
		if(!empty($this->data))
		{
			$save = $this->Common->html($this->request->data[$this->modelClass]);
			$save['code1'] = 'ctr';
			$save['code2'] = 'tpy';

			if( is_numeric($save['loai_cha_option']) )
			{
				$save['name'] = $save['loai_con'];
				$save['parent'] = $save['loai_cha_option'];
				$this->{$this->modelClass}->create();
				if($this->{$this->modelClass}->save($save))
				{
					$this->request->data = array();
					$this->set('message_success', 'Lưu loại công trình thành công.');
				}else{
					$this->set('message_error', 'Lưu loại công trình thất bại.');
				}
			}elseif( strlen( $save['loai_cha'] ) > 0 ){
				$save['name'] = $save['loai_cha'];

				// SAVE LOAI CHA TRUOC
				$this->{$this->modelClass}->create();

				if( $this->{$this->modelClass}->save($save))
				{
					$this->request->data = array();
					$this->set('message_success', 'Lưu loại cha công trình thành công.');
				}else{
					$this->set('message_error', 'Lưu loại cha công trình thất bại.');
				}

				$save['parent'] = $this->{$this->modelClass}->id;

				if( strlen( $save['loai_con'] ) > 0 )
				{
					$save['name'] = $save['loai_con'];

					$this->{$this->modelClass}->create();
					if($this->{$this->modelClass}->save($save))
					{
						$this->request->data = array();
						$this->set('message_success', 'Lưu loại công trình thành công.');
					}else{
						$this->set('message_error', 'Lưu loại công trình thất bại.');
					}
				}
			}


//            if($this->{$this->modelClass}->save($data))
//            {
//                $this->set('message_success', __('Lưu loại công trình thành công.'));
//            }else{
//                $this->set('message_error', __('Lưu loại công trình thất bại.'));
//            }

//			$this->Session->setFlash('The tintuc has been saved successfully', 'default', array('class' => 'flash_success'));
//          $this->Session->setFlash('The tintuc could not be saved. Please, try again.', 'default', array('class' => 'flash_error'));

		}



//        $tmps = $this->{$this->modelClass}->find('all', array(
//            'conditions' => array(
//                'Codetbl.code1' => 'ctr',
//                'Codetbl.code2' => 'tpy',
//                'Codetbl.trangthai' => 4,
//                'Codetbl.parent' => 0
//            ),
//            'contain' => array('LoaiCon' => array('id', 'name')),
//            'fields' => array('id', 'name')
//        ));
//        $tmp_2 = array();
//        foreach( $tmps as $tmp )
//        {
//            $tmp_2 = array_merge($tmp_2, array(
//                $tmp[$this->modelClass]['name'] => Set::classicExtract($tmp['LoaiCon'], '{\w+}.name')
//            ));
//
//        }
//        $this->set( 'tmp', $tmp_2);

		$this->set('loai_cha', $this->{$this->modelClass}->find('list', array(
			'conditions' => array(
				$this->modelClass.'.code1' => 'ctr',
				$this->modelClass.'.code2' => 'tpy',
				$this->modelClass.'.trangthai' => 4,
				$this->modelClass.'.parent' => 0
			),
			'contain' => false,
			'fields' => array('id', 'name')
		)));
	}

	function loai_sua($id)
	{
		if(!is_numeric($id))exit;
		if(!empty($this->data))
		{
			$save = $this->Common->html($this->data[$this->modelClass]);
			$save['id'] = $id;
			$save['parent'] = $save['loai_cha_option'];

			if($this->{$this->modelClass}->save($save))
			{
				$this->set('message_success', 'Lưu loại công trình thành công.');
			}else{
				$this->set('message_error', 'Lưu loại công trình thất bại.');
			}
		}
		$tmp = $this->{$this->modelClass}->find('first', array(
			'contain' => array('LoaiCha' => array('id', 'name')),
			'conditions' => array('Codetbl.id' => $id)
		));
		$tmp[$this->modelClass] = $this->Common->html_decode($tmp[$this->modelClass]);
		$tmp[$this->modelClass]['loai_cha_option'] = $tmp['LoaiCha']['id'];

		$this->request->data = $tmp;


		$this->set('loai_cha', $this->{$this->modelClass}->find('list', array(
			'conditions' => array(
				'Codetbl.code1' => 'ctr',
				'Codetbl.code2' => 'tpy',
				'Codetbl.trangthai' => 4,
				'Codetbl.parent' => 0
			),
			'contain' => false,
			'fields' => array('id', 'name')
		)));
	}

	function nhom()
	{
		$cond['Codetbl.code1'] = 'congtrinhs';$cond['Codetbl.code2'] = 'nhom';$cond['Codetbl.trangthai'] = 4;
		$this->paginate = array('conditions' => $cond,'order' => 'Codetbl.name asc','limit' => 40,
			'contain' => false);
		$this->set('datas',$this->paginate());

	}

	function nhom_them()
	{
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['code1'] = 'congtrinhs';$data[$this->modelClass]['code2'] = 'nhom';
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu nhóm công trình thành công.'));
			}else{
				$this->set('message_error', __('Lưu nhóm công trình thất bại.'));
			}
		}

	}

	function nhom_sua($id)
	{
		if(!is_numeric($id))exit;
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['id'] = $id;
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu nhóm công trình thành công.'));
			}else{
				$this->set('message_error', __('Lưu nhóm công trình thất bại.'));
			}
		}
		$tmp = $this->{$this->modelClass}->find('first', array(
			'contain' => false,
			'conditions' => array('Codetbl.id' => $id)
		));
		$tmp[$this->modelClass] = $this->Common->html_decode($tmp[$this->modelClass]);
		$this->request->data = $tmp;

	}

	function hangmuc()
	{
		$cond['Codetbl.code1'] = 'congtrinhs';$cond['Codetbl.code2'] = 'hangmuc';$cond['Codetbl.trangthai'] = 4;
		$this->paginate = array('conditions' => $cond,'order' => 'Codetbl.name asc','limit' => 40,
			'contain' => false);
		$this->set('datas',$this->paginate());

	}

	function hangmuc_them()
	{
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['code1'] = 'congtrinhs';$data[$this->modelClass]['code2'] = 'hangmuc';
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu hạng mục công trình thành công.'));
			}else{
				$this->set('message_error', __('Lưu hạng mục công trình thất bại.'));
			}
		}

	}

	function hangmuc_sua($id)
	{
		if(!is_numeric($id))exit;
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['id'] = $id;
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu hạng mục công trình thành công.'));
			}else{
				$this->set('message_error', __('Lưu hạng mục công trình thất bại.'));
			}
		}
		$tmp = $this->{$this->modelClass}->find('first', array(
			'contain' => false,
			'conditions' => array('Codetbl.id' => $id)
		));
		$tmp[$this->modelClass] = $this->Common->html_decode($tmp[$this->modelClass]);
		$this->request->data = $tmp;
	}

	function nhomthanhvien()
	{
		$cond['Codetbl.code1'] = 'usr';$cond['Codetbl.code2'] = 'grp';$cond['Codetbl.trangthai'] = 4;
		$this->paginate = array('conditions' => $cond,'order' => 'Codetbl.sort asc','limit' => 40,
			'contain' => array('Truongbophan'));
		$this->set('datas',$this->paginate());

	}

	function nhomthanhvien_them()
	{
		if(!empty($this->data))
		{
			$data[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$data[$this->modelClass]['code1'] = 'usr';$data[$this->modelClass]['code2'] = 'grp';
			if($this->{$this->modelClass}->save($data))
			{
				$this->set('message_success', __('Lưu nhóm công trình thành công.'));
			}else{
				$this->set('message_error', __('Lưu nhóm công trình thất bại.'));
			}
		}

	}

	function nhomthanhvien_sua($id)
	{
		if(!is_numeric($id))exit;
		if(!empty($this->data))
		{
			$data = $this->Common->html($this->data[$this->modelClass]);
			$data['id'] = $id;
			$data['parent'] = $this->data['User']['id'];
			if(!is_numeric($data['parent'])){
				$this->set('message_error', __('Vui lòng chọn trưởng bộ phận này.'));
			}else{
				if($this->{$this->modelClass}->save($data))
				{
					$this->set('message_success', __('Lưu nhóm công trình thành công.'));
				}else{
					$this->set('message_error', __('Lưu nhóm công trình thất bại.'));
				}
			}

		}else{
			$tmp = $this->{$this->modelClass}->find('first', array(
				'contain' => array('Truongbophan'),
				'conditions' => array('Codetbl.id' => $id)
			));
			$tmp[$this->modelClass] = $this->Common->html_decode($tmp[$this->modelClass]);
			$tmp['User']['id'] =  $tmp[$this->modelClass]['parent'];
			$tmp[$this->modelClass]['manager'] =  $tmp['Truongbophan']['username'];
			$tmp['User']['realname'] =  $tmp['Truongbophan']['realname'];

			$this->request->data = $tmp;
		}

	}

	function ngaynghicongty_them()
	{
		if(!empty($this->data))
		{
			$data = $this->Common->html($this->data[$this->modelClass]);
			$data['code1'] = 'chamcongs';$data['code2'] = 'ngaynghicty';
			$data['date'] = $data['year_tmp']['year'].'-'.$data['month_tmp']['month'].'-'.$data['date_tmp']['day'];
			if(strlen($data['name']) < 1 )
			{
				$this->set('message_error', __('Vui lòng điền đầy đủ ngày nghỉ và tên của ngày nghỉ.'));
			}else{
				if($this->{$this->modelClass}->save($data, false))
				{
					$this->loadModel('Chamcong');
					$field_chamcong = array('Chamcong.chamcongchitiet_modified' => 0,'Chamcong.nguoisua' => $this->Auth->user( 'id'));
					$date_tmp = date('Y').'-'.$data['month_tmp']['month'].'-01';
					$cond_chamcong = array('Chamcong.trangthai' => 4,'Chamcong.thangnam' => array(date('Y-m-d', strtotime('prev month', strtotime($date_tmp))),$date_tmp,date('Y-m-d', strtotime('next month', strtotime($date_tmp)))));
					$this->Chamcong->belongsTo = array();
					if( $this->Chamcong->updateAll( $field_chamcong, $cond_chamcong ) ){
						$this->set('message_success', 'Lưu ngày <font color=\'red\'>'.$data['date_tmp']['day'].
						'/'.$data['month_tmp']['month'].'</font> thành ngày <font color=\'red\'>'.$data['name'].'</font> thành công.');
					}else{
						$this->set('message_error', 'Cập nhật bảng chấm công thất bại, vui lòng xóa ngày vừa nhập và thêm lại. <br>Nếu vẫn báo lỗi này xin liên hệ IT ngay. Cám ơn.');
					}
				}else{
					$this->set('message_error', __('Lưu thất bại. Vui lòng F5 vào thao tác lại. Nếu vẫn không được xin liên hệ IT ngay.'));
				}
			}
		}else{
			$tmp = $this->data;
			$tmp[$this->modelClass]['month_tmp'] = date('m');
			$tmp[$this->modelClass]['date_tmp'] = date('d');
			$tmp[$this->modelClass]['year_tmp'] = date('Y');
			$this->data = $tmp;
		}
		$cond['Codetbl.code1'] = 'chamcongs';$cond['Codetbl.code2'] = 'ngaynghicty';$cond['Codetbl.trangthai'] = 4;
		$this->paginate = array('conditions' => $cond,'order' => 'Codetbl.date desc','limit' => 5,
			'contain' => false);
		$this->set('datas',$this->paginate());
	}

	function auto($type = 'chudautu'){

		if(strlen($_GET['term']) < 2)exit;
		$cond['Codetbl.trangthai'] = 4;
		if($type = 'chudautu')
		{
			$cond['Codetbl.code1'] = 'chudautu';$cond['Codetbl.code2'] = 'ten';
		}
		$cond['Codetbl.name like' ]= '%'.$_GET['term'].'%';

		//$cond = am( $cond, $this->Common->fulltext( 'Codetbl.name', $this->Common->html( $_GET['term'] ) ) );
		//$cond = am( $cond, $this->Common->fulltext( 'Codetbl.name', $_GET['term'] ) );
		$data = $this->{$this->modelClass}->find( 'list', array('fields' => array( 'id', 'name' ),'conditions' => $cond,'limit' => 20,'contain' => false,'order' => 'Codetbl.name asc' ) );

		if( count( $data ) == 0 )
		{
			$data[0] = __( 'Không tìm thấy kết quả', TRUE );
		}
		$data = $this->Common->html_decode($data);
		$this->set( 'results', $data );
		$this->layout = 'ajax';
	}

	function them_hangmuc()
	{
		if( !empty( $_POST ) ){

			$save[$this->modelClass] = $this->Common->html( $_POST );
			$save[$this->modelClass]['name'] =  trim( $save[$this->modelClass]['name'] );
			$save[$this->modelClass]['code1'] = 'congtrinhs';
			$save[$this->modelClass]['code2'] = 'hangmuc';

			if( $this->{$this->modelClass}->save( $save ) ){
				$this->set( 'thanhcong', '1' );
				$this->set( 'id', $this->{$this->modelClass}->id );
				$this->set('hangmuc', $this->{$this->modelClass}->find('list',array('contain' => false, 'conditions' => array(
				'Codetbl.code1' => 'congtrinhs', 'Codetbl.code2' => 'hangmuc','Codetbl.trangthai' => 4),'fields' => array('id', 'name'), 'order' => 'name asc')));
				//echo 'ok';exit;
			}else{
				$this->set('message_error', __('Lưu hạng mục công trình thất bại.'));
			}
		}
	}
}
