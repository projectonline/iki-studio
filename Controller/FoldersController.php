<?php
class FoldersController extends AppController{
	var $name='Folders';
	var $link_ctlr='folders';

	var $dinhdang = array(
		'0'    => '/img/iconfile/folder.png',
		'doc'  => '/img/iconfile/word.png',
		'docx' => '/img/iconfile/word.png',
		'xls'  => '/img/iconfile/excel.png',
		'xlsx' => '/img/iconfile/excel.png',
		'ppt'  => '/img/iconfile/ppt.png',
		'pptx' => '/img/iconfile/ppt.png',
		'pdf'  => '/img/iconfile/pdf.png',
		'rar'  => '/img/iconfile/nen.png',
		'zip'  => '/img/iconfile/nen.png',
		'txt'  => '/img/iconfile/text.png',
		'png'  => '/img/iconfile/anh.png',
		'jpg'  => '/img/iconfile/anh.png',
		'jpeg' => '/img/iconfile/anh.png',
		'gif'  => '/img/iconfile/anh.png',
		'tiff' => '/img/iconfile/anh.png',
		'bmp'  => '/img/iconfile/anh.png',
		''     => '/img/iconfile/khong.png'
	);

	function beforeFilter()
	{
		parent::beforeFilter();
		//$this->Auth->authorize = 'controller';
		//$this->Auth->allow('delete');

		// alow initDB
		//$this->Auth->allow('*');
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   : $loaiparent: nhansu, user, phongban
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 4, 2012 )
	//***************************************************************//
	function danhsachfolder( $loaiparent = 'nhansu', $id_loai = 0, $parent = 0, $div = 'folder', $sort = 'modified' )
	{
		// namnb them check_click
		if( is_numeric( $sort ) )
		{
			$this->set('check_click', $sort);
			$sort = 'modified';
		}// end namnb them check_click
		////////////////////////////////////////////////////////////////////////
		if( !isset( $_POST['diachitim'] ) ){
			$cond['Folder.parent'] = $parent;
			$condfile['Uploader.item_id'] = $parent;
			if( $loaiparent == 'user' ){
				$condfile1['Uploader.item_id'] = $parent;
			}
		}else{
			$diachi = str_replace('~', '&', $_POST['diachitim']);
			if( $diachi != '/' ){
				if( substr($diachi, -1, 1) != '/' ){
					$diachi = $diachi . '/';
				}
				$duoi = strrchr(substr($diachi,0,-1),'/');
				$truoc = substr($diachi,0,-(strlen($duoi)));
				$kiemtra = $this->Folder->find( 'first', array(
					'conditions' => array( 'Folder.trangthai' => 4,
									'Folder.ten' => substr($duoi, 1),
									'Folder.duongdan' => $truoc,
									'Folder.loaiparent' => $loaiparent,
									'Folder.id_loai' => $id_loai ),
					'contain' => false
				));
				if( !empty( $kiemtra ) ){
					$cond['Folder.duongdan'] = $diachi;
					$condfile['Uploader.duongdanfolder'] = $diachi;
					if( $loaiparent == 'user' ){
						$condfile1['Uploader.duongdanfolder'] = $diachi;
					}
					$parent = $kiemtra['Folder']['id'];
					$this->Session->write( 'diachi', $diachi );
				}else{
					echo 'khong';exit;
				}
			}else{
				$cond['Folder.parent'] = 0;
				$condfile['Uploader.item_id'] = 0;
				if( $loaiparent == 'user' ){
					$condfile1['Uploader.item_id'] = 0;
				}
			}
		}
		////////////////////////////////////////////////////////////////////////
		$cond['Folder.trangthai'] = 4;
		$cond['Folder.loaiparent'] = $loaiparent;
		$cond['Folder.id_loai'] = $id_loai;
		$fields = array( 'Folder.*',
			'DATE_FORMAT(Folder.created, \'%d/%m/%Y %H:%i\') as created',
			'DATE_FORMAT(Folder.modified, \'%d/%m/%Y %H:%i\') as modified');
		$contain = array( 'Nguoitao', 'Nguoisua' );

		$data = $this->Folder->find( 'all', array(
			'fields' => $fields,
			'conditions' => $cond,
			'contain' => $contain,
			'order' => 'Folder.'.$sort.' desc'
		));

		if( isset( $_POST['diachitim'] ) && !empty( $data[0] ) ){
			$parent = $data[0]['Folder']['parent'];
		}
		////////////////////////////////////////////////////////////////////////
		$condfile['Uploader.trangthai'] = 4;
		$condfile['Uploader.controller'] = $this->link_ctlr.'_'.$loaiparent.'_'.$id_loai;
		if( $loaiparent == 'user' ){
			$condfile1['Uploader.controller'] = 'users';
			$condfile1['Uploader.nguoitao'] = $id_loai;
			$condfile1['Uploader.trangthai'] = 4;
			/*$condfile1 = array(
				'Uploader.controller' => 'users',
				'Uploader.nguoitao' => $id_loai,
				'Uploader.trangthai' => 4,
				'Uploader.item_id' => $parent
			);*/
			$condfile2 = $condfile;
			$condfile = array();
			$condfile['OR'] = array( $condfile1, $condfile2 );
		}
		$fieldsfile = array( 'Uploader.*',
			'DATE_FORMAT(Uploader.created, \'%d/%m/%Y %H:%i\') as created',
			'DATE_FORMAT(Uploader.modified, \'%d/%m/%Y %H:%i\') as modified');
		$containfile = array( 'Nguoitao' );

		$datafile = $this->Folder->Uploader->find( 'all', array(
			'fields' => $fieldsfile,
			'conditions' => $condfile,
			'contain' => $containfile,
			'order' => 'Uploader.'.$sort.' desc'
		));

		if( isset( $_POST['diachitim'] ) && !empty( $datafile[0] ) ){
			$parent = $datafile[0]['Uploader']['item_id'];
		}
		////////////////////////////////////////////////////////////////////////
		$datatruoc = $this->Folder->find( 'first', array(
			'conditions' => array(	'Folder.trangthai' => 4,
									'Folder.id' => $parent,
								 ),
			'contain' => false
		));

		if( empty( $datatruoc ) ){
			$datatruoc['Folder']['parent'] = 0;
		}
		////////////////////////////////////////////////////////////////////////
		if( !empty( $_POST['data'] ) ){
			if( isset( $_POST['data']['id'] ) ){
				$this->set('folder_file', $_POST['data']);
			}
			if( isset( $_POST['data']['tim'] ) ){
				$this->set('tim', $_POST['data']['tim']);
			}
			if( isset( $_POST['data']['diachi'] ) ){
				if( isset( $_POST['data']['back'] ) ){
					$duoi = strrchr(substr($_POST['data']['diachi'],0,-1),'/');
					if( $parent != 0 ){
						$this->Session->write( 'diachi', substr($_POST['data']['diachi'],0,-(strlen($duoi))) );
						//$this->set('diachi', substr($_POST['data']['diachi'],0,-(strlen($duoi))) );
					}else{
						$this->Session->write( 'diachi', '/' );
						//$this->set('diachi', '/');
					}
				}else{
					$this->Session->write( 'diachi', $_POST['data']['diachi'].$datatruoc['Folder']['ten'].'/' );
					//$this->set('diachi', $_POST['data']['diachi'].$datatruoc['Folder']['ten'].'/');
				}
			}
		}else if( !empty( $_GET['diachi'] ) ){
			$this->Session->write( 'diachi', $_GET['diachi'] );
			//$this->set('diachi', $_GET['diachi']);
		}else if( !$this->Session->check( 'diachi' ) || $parent == 0 ){
			$this->Session->write( 'diachi', '/' );
			//$this->set('diachi', '/');
		}
		////////////////////////////////////////////////////////////////////////
		$data = array_merge( $data, $datafile );
		$this->set('data', $data);
		$this->set('datatruoc', $datatruoc);
		$this->set('loaiparent', $loaiparent);
		$this->set('id_loai', $id_loai);
		$this->set('parent', $parent);
		$this->set('div', $div);
		$this->set('dinhdang', $this->dinhdang);

		if( $this->request->is('ajax') )
		{
			$this->set('click_ajax', true);
		}
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   : $loaiparent: nhansu, user, phongban
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 4, 2012 )
	//***************************************************************//
	function themfolder( $loaiparent = 'nhansu', $id_loai = 0, $parent = 0 )
	{
		if( isset( $this->data['diachi'] ) ){
			$this->Session->write( 'diachi', $this->data['diachi'] );
			//$this->set('diachi', $this->data['diachi']);
		}
		if(!empty($this->data['Folder']))
		{
			if( isset( $this->data['Folder']['duongdan'] ) ){
				$this->Session->write( 'diachi', $this->data['Folder']['duongdan'] );
				//$this->set('diachi', $this->data['Folder']['duongdan']);
			}

			//$data = $this->Common->html($this->data['Folder']);
			$data = $this->data['Folder'];

			$data['loaiparent'] = $loaiparent;
			$data['id_loai'] = $id_loai;
			$data['parent'] = $parent;

			if($this->Folder->save($data))
			{
				$this->redirect('/'.$this->link_ctlr.'/danhsachfolder/'.$loaiparent.'/'.$id_loai.'/'.$parent);
			}
		}
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   : $loaiparent: nhansu, user, phongban
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 4, 2012 )
	//***************************************************************//
	function move_file_to_folder( $data_file, $loaiparent, $id_loai, $parent )
	{
		if(!isset($data_file['File']))return array( array(), 0 );

		APP::import('Component', 'Cmfile');
		$Cmfile = new CmfileComponent(new ComponentCollection());

		$tmp_all = array();
		$tmp = array();
		$tmp['controller'] = 'folders_'.$loaiparent.'_'.$id_loai;
		$tmp['item_id'] = $parent;
		$tmp['duongdanfolder'] = $data_file['duongdanfolder'];

		foreach( $data_file['File'] as $value )
		{
			if( $value['error'] != 0 )continue;
			$tmp['dungluong'] = $value['size'];

			//$ext = split( '\.', basename($value['name']) );
			$ext = strtolower(substr(strrchr($value['name'],'.'),1));
			$file = $this->Auth->user('username').'_'.date('Y_m_d_H_i_s').
						'_rand_'.rand( 1, 9999 ).'.'.$ext;

			$duongdanfolder = APP.'upload/large/folders/';
			if(!is_dir( $duongdanfolder ))
			{
				mkdir( $duongdanfolder ); chmod( $duongdanfolder, 0777 );
			}
			$tmp['file'] = $Cmfile->move_to( $value['tmp_name'], APP.'upload/large/folders/'.$loaiparent.'/', $file );

			if( !$tmp['file'] )continue;
			$tmp['name'] = $value['name'];

			$tmp_all[] = $tmp;
		}
		return $tmp_all;
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   : $loaiparent: nhansu, user, phongban
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 4, 2012 )
	//***************************************************************//
	function themfile( $loaiparent = 'nhansu', $id_loai = 0, $parent = 0, $check_click = 0 )// namnb them check_click
	{
		if( isset( $this->data['diachi'] ) ){
			$this->Session->write( 'diachi', $this->data['diachi'] );
			//$this->set('diachi', $this->data['diachi']);
		}
		if(!empty($this->data['File']))
		{
			if( isset( $this->data['duongdanfolder'] ) ){
				$this->Session->write( 'diachi', $this->data['duongdanfolder'] );
				//$this->set('diachi', $this->data['duongdanfolder']);
			}
			$save = $this->move_file_to_folder( $this->data, $loaiparent, $id_loai, $parent );
			//$this->loadModel('Uploader');

			if( isset($save[0]) ){
				$this->Folder->Uploader->create();
				if(	$this->Folder->Uploader->saveAll($save) ){
					if( $loaiparent == 'user' ){
						$this->redirect('/users/view/'.$id_loai.'/?parent='.$parent.'&check_click='.$check_click);
					}else{
						$this->redirect('/'.$this->link_ctlr.'/danhsachfolder/'.$loaiparent.'/'.$id_loai.'/'.$parent.'/folder/'.$check_click.'/#'.$check_click);// namnb them check_click
					}
				}
			}
		}
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   : $loaiparent: nhansu, user, phongban
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 6, 2012 )
	//***************************************************************//
	function layfoldertheoparent( $parent = 0 )
	{
		$cond['Folder.trangthai'] = 4;
		$cond['Folder.parent'] = $parent;

		$data = $this->Folder->find( 'list', array(
			'fields' => array( 'id' ),
			'conditions' => $cond,
			'contain' => false
		));

		return $data;
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 6, 2012 )
	//***************************************************************//
	function layfiletheodsforder( $dsfolder, $loaiparent, $id_loai )
	{
		$cond['Uploader.trangthai'] = 4;
		$cond['Uploader.controller'] = $this->link_ctlr.'_'.$loaiparent.'_'.$id_loai;;
		$cond['Uploader.item_id'] = $dsfolder;

		$data = $this->Folder->Uploader->find( 'list', array(
			'fields' => array( 'id' ),
			'conditions' => $cond,
			'contain' => false
		));

		return $data;
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 6, 2012 )
	//***************************************************************//
	var $danhsachfolder = array();
	function layfolder( $id )
	{
		$dsfolder = $this->layfoldertheoparent( $id );
		foreach( $dsfolder as $folderi ){
			$this->danhsachfolder[] = $folderi;
			$this->layfolder( $folderi );
		}
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 5, 2012 )
	//***************************************************************//
	function xoafolder( $id = 0, $loaiparent = 'nhansu', $id_loai = 0 )
	{
		if( !is_numeric( $id ) )exit;
		$this->Folder->id = $id;
		if( $this->Folder->exists() )
		{
			$this->danhsachfolder[] = $id;
			$this->layfolder( $id );
			$dsfile = $this->layfiletheodsforder( $this->danhsachfolder, $loaiparent, $id_loai );

			$field['trangthai'] = 9;
			$field['nguoisua'] = $this->Auth->user( 'id' );
			$field['modified'] = '"'.date('Y-m-d H:i:s').'"';

			$cond = array('Folder.id' => $this->danhsachfolder);
			$condfile = array('Uploader.id' => $dsfile);

			if( $this->Folder->updateAll( $field, $cond ) && $this->Folder->Uploader->updateAll( $field, $condfile ) ){
				$this->Folder->updateCounter('Folder');
				$this->Folder->updateCounter('Uploader');
				echo 'ok';
			}
		}
		exit;
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 5, 2012 )
	//***************************************************************//
	function xoafile( $id = 0 )
	{
		if( !is_numeric( $id ) )exit;
		$this->Folder->Uploader->id = $id;
		if( $this->Folder->Uploader->exists() )
		{
			$data['id'] = $id;
			$data['trangthai'] = 9;
			$data['nguoisua'] = $this->Auth->user( 'id' );
			if( $this->Folder->Uploader->save( $data ) ){
				$this->Folder->updateCounter('Folder');
				echo 'ok';
			}
		}
		exit;
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 9, 2012 )
	//***************************************************************//
	function doiten( $id = 0, $loai = 0 )
	{
		if( is_numeric( $id ) )
		{
			$data = array();
			if( $loai == 0 ){
				$cond['Folder.trangthai'] = 4;
				$cond['Folder.id'] = $id;
				$data = $this->Folder->find( 'first', array(
					'conditions' => $cond,
					'contain' => false,
				));
			}else{
				$condfile['Uploader.trangthai'] = 4;
				$condfile['Uploader.id'] = $id;
				$data = $this->Folder->Uploader->find( 'first', array(
					'conditions' => $condfile,
					'contain' => false,
				));
				$file_extension = substr(strrchr($data['Uploader']['name'],'.'),1);
				$data['Folder']['ten'] = str_replace('.'.$file_extension, '', $data['Uploader']['name']);
			}
			$this->data = $data;
		}

		if( !empty( $_POST ) ){
			//$data = $this->Common->html($_POST);
			$data = $_POST;

			$data['id'] = $id;
			$data['nguoisua'] = $this->Auth->user( 'id' );
			$mod = 'Folder';
			if( $loai != 0 ){
				$this->loadModel('Uploader');
				$mod = 'Uploader';
				$data['name'] = $data['ten'].'.'.strtolower(substr(strrchr($this->data['Uploader']['name'],'.'),1));
				$data['ten'] = $data['name'];
			}
			if( $this->$mod->save( $data ) ){
				if( $loai == 0 ){
					$datafolder = $this->Folder->find( 'all', array(
						'conditions' => array( 'Folder.duongdan LIKE' => str_replace('/','_',$_POST['diachi']).$this->data['Folder']['ten'].'_'.'%' ),
						'contain' => false,
					));
					foreach( $datafolder as $fi ){
						$fi['Folder']['duongdan'] = str_replace($_POST['diachi'].$this->data['Folder']['ten'].'/', $_POST['diachi'].$data['ten'].'/', $fi['Folder']['duongdan']);
						$this->Folder->create();
						$this->Folder->save( $fi['Folder'] );
					}
					$datafile = $this->Folder->Uploader->find( 'all', array(
						'conditions' => array( 'Uploader.duongdanfolder LIKE' => str_replace('/','_',$_POST['diachi']).$this->data['Folder']['ten'].'_'.'%' ),
						'contain' => false,
					));
					foreach( $datafile as $fi ){
						$fi['Uploader']['duongdanfolder'] = str_replace($_POST['diachi'].$this->data['Folder']['ten'].'/', $_POST['diachi'].$data['ten'].'/', $fi['Uploader']['duongdanfolder']);
						$this->Folder->Uploader->create();
						$this->Folder->Uploader->save( $fi['Uploader'] );
					}
				}
				////////////////////////////////////////////////////////////////
				$this->Folder->updateCounter('Folder');
				$this->Folder->updateCounter('Uploader');
				$this->set( 'tenluu', $data['ten'] );
				$this->set( 'thanhcong', '1' );
			}
		}

		$this->set('id', $id);
		$this->set('loai', $loai);
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 10, 2012 )
	//***************************************************************//
	function movefolderfile( $loaiparent = 'nhansu', $id_loai = 0, $parent = 0, $loai = 0 )
	{
		if( !empty( $_POST['data'] ) ){
			$field = array(); $cond = array(); $luufolder = 0;
			if( isset( $_POST['data']['id']['Folder'] ) ){
				$diachicu = str_replace('~', '&', $_POST['data']['diachicu']);
				$diachi = str_replace('~', '&', $_POST['data']['diachi']);
				$field['parent'] = $parent;
				$field['nguoisua'] = $this->Auth->user( 'id' );
				$field['duongdan'] = '"'.$diachi.'"';
				$field['modified'] = '"'.date('Y-m-d H:i:s').'"';
				$cond = array('Folder.id' => $_POST['data']['id']['Folder']);
				if( $this->Folder->updateAll( $field, $cond ) ){
					$luufolder = 1;
					$datafolder = $this->Folder->find( 'all', array(
						'conditions' => array( 'Folder.duongdan LIKE' => str_replace('/','_',$diachicu).'%' ),
						'contain' => false,
					));
					foreach( $datafolder as $fi ){
						$fi['Folder']['duongdan'] = str_replace($diachicu, $diachi, $fi['Folder']['duongdan']);
						$this->Folder->create();
						$this->Folder->save( $fi['Folder'] );
					}
					$datafile = $this->Folder->Uploader->find( 'all', array(
						'conditions' => array( 'Uploader.duongdanfolder LIKE' => str_replace('/','_',$diachicu).'%' ),
						'contain' => false,
					));
					foreach( $datafile as $fi ){
						$fi['Uploader']['duongdanfolder'] = str_replace($diachicu, $diachi, $fi['Uploader']['duongdanfolder']);
						$this->Folder->Uploader->create();
						$this->Folder->Uploader->save( $fi['Uploader'] );
					}
				}
			}else{
				$luufolder = 1;
			}

			$fieldfile = array(); $condfile = array(); $luufile = 0;
			if( isset( $_POST['data']['id']['Uploader'] ) ){
				$fieldfile['item_id'] = $parent;
				$fieldfile['nguoisua'] = $this->Auth->user( 'id' );
				if( $loai == 0 ){
					$fieldfile['duongdanfolder'] = '"'.str_replace('~', '&', $_POST['data']['diachi']).'"';
				}else{
					$datatruoc = $this->Folder->find( 'first', array(
								'conditions' => array(	'Folder.trangthai' => 4,
														'Folder.id' => $parent,
													 ),
								'contain' => false
							));
					if( $datatruoc['Folder']['duongdan'] == '' ){
						$datatruoc['Folder']['duongdan'] = '/';
					}
					$fieldfile['duongdanfolder'] = '"'.$datatruoc['Folder']['duongdan'].$datatruoc['Folder']['ten'].'/"';
				}
				$fieldfile['modified'] = '"'.date('Y-m-d H:i:s').'"';
				$condfile = array('Uploader.id' => $_POST['data']['id']['Uploader']);
				$this->Folder->Uploader->create();
				if( $this->Folder->Uploader->updateAll( $fieldfile, $condfile ) ){
					$luufile = 1;
				}
			}else{
				$luufile = 1;
			}

			if( $luufolder == 1 && $luufile == 1 ){
				$this->Folder->updateCounter('Folder');
				$this->Folder->updateCounter('Uploader');
				if( $loai == 0 ){
					$this->redirect('/'.$this->link_ctlr.'/danhsachfolder/'.$loaiparent.'/'.$id_loai.'/'.$parent.'/');
				}else{
					exit;
				}
				//echo 'ok';
			}
		}
		//exit;
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Apr 17, 2012 )
	//***************************************************************//
	function search( $loaiparent = 'nhansu', $id_loai = 0, $div = 'folderfilesearch', $tim )
	{
		$cond['Folder.trangthai'] = 4;
		$cond['Folder.loaiparent'] = $loaiparent;
		$cond['Folder.id_loai'] = $id_loai;
		if( $tim != '' ){
			//$cond = am( $cond, $this->Common->fulltext( 'Folder.ten', $this->Common->html( $tim ) ) );
			$cond['Folder.ten LIKE'] =  '%'.trim( $tim ).'%';
		}
		$fields = array( 'Folder.*',
			'DATE_FORMAT(Folder.created, \'%d/%m/%Y %H:%i\') as created',
			'DATE_FORMAT(Folder.modified, \'%d/%m/%Y %H:%i\') as modified');
		$contain = array( 'Nguoitao', 'Nguoisua' );

		$data = $this->Folder->find( 'all', array(
			'fields' => $fields,
			'conditions' => $cond,
			'contain' => $contain,
			'order' => 'Folder.modified desc'
		));
		////////////////////////////////////////////////////////////////////////
		$condfile['Uploader.trangthai'] = 4;
		$condfile['Uploader.controller'] = $this->link_ctlr.'_'.$loaiparent.'_'.$id_loai;
		if( $tim != '' ){
			//$condfile = am( $condfile, $this->Common->fulltext( 'Uploader.name', $this->Common->html( $tim ) ) );
			$condfile['Uploader.name LIKE'] =  '%'.trim( $tim ).'%';
		}
		if( $loaiparent == 'user' ){
			$condfile1 = array(
				'Uploader.controller' => 'users',
				'Uploader.nguoitao' => $id_loai,
				'Uploader.trangthai' => 4,
			);
			if( $tim != '' ){
				$condfile1['Uploader.name LIKE'] =  '%'.trim( $tim ).'%';
			}
			$condfile2 = $condfile;
			$condfile = array();
			$condfile['OR'] = array( $condfile1, $condfile2 );
		}
		$fieldsfile = array( 'Uploader.*',
			'DATE_FORMAT(Uploader.created, \'%d/%m/%Y %H:%i\') as created',
			'DATE_FORMAT(Uploader.modified, \'%d/%m/%Y %H:%i\') as modified');
		$containfile = array( 'Nguoitao' );

		$datafile = $this->Folder->Uploader->find( 'all', array(
			'fields' => $fieldsfile,
			'conditions' => $condfile,
			'contain' => $containfile,
			'order' => 'Uploader.modified desc'
		));
		////////////////////////////////////////////////////////////////////////
		/*$datatruoc = $this->Folder->find( 'first', array(
			'conditions' => array(	'Folder.trangthai' => 4,
									'Folder.id' => $parent,
								 ),
			'contain' => false
		));

		if( empty( $datatruoc ) ){
			$datatruoc['Folder']['parent'] = 0;
		}
		////////////////////////////////////////////////////////////////////////
		if( !empty( $_POST['data'] ) ){
			$this->set('folder_file', $_POST['data']);
		}*/
		////////////////////////////////////////////////////////////////////////
		$data = array_merge( $data, $datafile );
		$this->set('data', $data);
		//$this->set('datatruoc', $datatruoc);
		$this->set('loaiparent', $loaiparent);
		$this->set('id_loai', $id_loai);
		//$this->set('parent', $parent);
		$this->set('div', $div);
		$this->set('tim', $tim);
		$this->Session->write( 'diachi', '/' );
		//$this->set('diachi', '/');
		$this->set('dinhdang', $this->dinhdang);
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( Jun 4, 2012 )
	//***************************************************************//
	function chitietfile( $id )
	{
		if( !is_numeric($id) ) exit;

		$data = $this->Folder->Uploader->find( 'first', array(
			'conditions' => array( 'Uploader.id' => $id, 'Uploader.trangthai' => 4 ),
			'contain' => false
		));

		$this->set('data', $data);
	}
}
