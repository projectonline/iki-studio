<?php
class ThuviensController extends AppController {
	var $name = 'Thuviens';
	var $components = array( 'Common', 'Cmfile', 'Auth' );

	function beforeFilter( )
	{
		// goi den before filter cha
		parent::beforeFilter();
	}

	function _get_cond()
	{
		$cond = array();
		$cond['Thuvien.trangthai'] = 4;

		if($this->Session->check('THUVIEN_index') && !isset($this->request->params['url']['data']['tieude'])){
			$this->request->params['url']['data'] = $this->Session->read('THUVIEN_index');
		}

		if(isset($this->request->params['url']['data']['tieude']))
		{
			$this->request->params['url']['data'] = $this->Common->html($this->request->params['url']['data']);
			if(strlen($this->request->params['url']['data']['tieude']) > 0)
				$cond[] = $this->Common->fulltext('Thuvien.tieude', $this->request->params['url']['data']['tieude']);
			if(strlen($this->request->params['url']['data']['mota']) > 0)
				$cond[] = $this->Common->fulltext('Thuvien.mota', $this->request->params['url']['data']['mota']);
			if(strlen($this->request->params['url']['data']['nguoitao']) > 0)
				$cond['Nguoitao.username LIKE'] = '%'.$this->request->params['url']['data']['nguoitao'].'%';

			$this->Session->write('THUVIEN_index', $this->request->params['url']['data']);
			$this->data = $this->request->params['url']['data'];
		}
		return $cond;
	}

	function index($typeleft=null,$type=null)
	{
		$cond = $this->_get_cond();
		if(isset($type) && is_numeric($type) && isset($type) && is_numeric($typeleft))
		{
			$cond['Thuvien.type'] = $type; $cond['Thuvien.typeleft'] = $typeleft;
			$this->set('typeleft', $typeleft);$this->set('type', $type);
		}

		$fields = array( 'Thuvien.*','DATE_FORMAT(Thuvien.created, \'%d/%m/%Y %H:%i\') as created');
		$this->paginate = array(
			'fields' => $fields,'conditions' => $cond,'order' => 'Thuvien.modified desc','limit' => 20,
			'contain' => array('Nguoitao','File'));
		$datas = $this->paginate();
		$this->set('datas', $datas);

		$this->_get_leftmenu();
	}

	function checklist($typeleft=null,$type=null)
	{
		$cond = $this->_get_cond();
		if(isset($type) && is_numeric($type) && isset($type) && is_numeric($typeleft))
		{
			$cond['Thuvien.type'] = $type; $cond['Thuvien.typeleft'] = $typeleft;
			$this->set('typeleft', $typeleft);$this->set('type', $type);
			$this->set( 'count_thv', $this->Thuvien->find('count',array(
			'conditions' => array(
				'Thuvien.type' => 173,
				'Thuvien.trangthai' => 4,
				'Thuvien.typeleft' => $typeleft,
			),
			'contain' => array('File'),
			)));
		}
		$fields = array( 'Thuvien.*','DATE_FORMAT(Thuvien.created, \'%d/%m/%Y %H:%i\') as created');
		$this->paginate = array(
			'fields' => $fields,'conditions' => $cond,'order' => 'Thuvien.modified desc','limit' => 20,
			'contain' => array('Nguoitao','File'));
		$datas = $this->paginate();

		$this->set('datas', $datas);
		$this->set('typeleft',$typeleft);

		$this->_get_leftmenu();
	}

	function _get_leftmenu()
	{
		$this->set('lefts', $this->{$this->modelClass}->Type->find('list',array('conditions' => array(
			'code1' => 'thv', 'code2' => 'lef'),'fields' => array('id', 'name'), 'order' => 'sort asc')));
		$this->set('tops', $this->{$this->modelClass}->Type->find('list',array('conditions' => array(
			'code1' => 'thv', 'code2' => 'top'),'fields' => array('id', 'name'), 'order' => 'id asc')));
		$this->set('danhba_type', $this->{$this->modelClass}->Type->find('list',array('conditions' => array(
			'code1' => 'dba', 'code2' => 'chv'),'fields' => array('id', 'name'), 'order' => 'sort asc')));
	}

	function them($typeleft=null,$type=null)
	{
		if(!empty($this->data))
		{
			$save[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);

			$save['File'] = $this->move_file_to_app_upload($this->data, $this->link_ctlr );

			if( isset($save['File'][0]) ){

				$this->{$this->modelClass}->create();
				if($this->{$this->modelClass}->saveAll($save))
				{
					$this->{$this->modelClass}->save_giaoviec_ytuong($this->data) ;
					$this->redirect('/thuviens/index/'.$save[$this->modelClass]['typeleft'].'/'.$save[$this->modelClass]['type']);
				}else{
					$this->set('message_error', 'Lưu mới thất bại');
				}
			}else{
				$this->set( 'message_error', 'Vui lòng chọn file.' );
			}
		}
		$tmp = array();
		if(is_numeric($typeleft)){ $tmp[$this->modelClass]['typeleft'] = $typeleft; $this->set('typeleft', $typeleft);}
		if(is_numeric($type)){ $tmp[$this->modelClass]['type'] = $type; }

		$this->data = $tmp;
		$this->_get_leftmenu();
	}

	function themchecklist($typeleft=null,$type=null)
	{
		if(!empty($this->data))
		{
			$save[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);

			$save['File'] = $this->move_file_to_app_upload($this->data, $this->link_ctlr );

			if( isset($save['File'][0]) ){

				$this->{$this->modelClass}->create();
				if($this->{$this->modelClass}->saveAll($save))
				{
					$this->{$this->modelClass}->save_giaoviec_ytuong($this->data) ;
					$this->redirect('/thuviens/checklist/'.$save[$this->modelClass]['typeleft'].'/'.$save[$this->modelClass]['type']);
				}else{
					$this->set('message_error', 'Lưu mới thất bại');
				}
			}else{
				$this->set( 'message_error', 'Vui lòng chọn file.' );
			}
		}
		$tmp = array();
		if(is_numeric($typeleft)){ $tmp[$this->modelClass]['typeleft'] = $typeleft; $this->set('typeleft', $typeleft);}
		if(is_numeric($type)){ $tmp[$this->modelClass]['type'] = $type; }

		$this->data = $tmp;
		$this->_get_leftmenu();
	}

	function sua($id)
	{
		if(!is_numeric($id))exit;
		if(!empty($this->data))
		{
			$save[$this->modelClass] = $this->Common->html($this->data[$this->modelClass]);
			$save[$this->modelClass]['id'] = $id;

			$save['File'] = $this->move_file_to_app_upload($this->data, $this->link_ctlr );

			if( !isset($save['File'][0]) ){
				unset($save['File']);
			}

			if( $this->{$this->modelClass}->saveAll($save))
			{
				$this->redirect('/thuviens/index/'.$save[$this->modelClass]['typeleft'].'/'.$save[$this->modelClass]['type']);
			}else{
				$this->set( 'message_error', 'Chỉnh sửa thư viện thất bại.' );
			}
		}
		$tmp = $this->{$this->modelClass}->find('first', array('contain' => array('File'),
			'conditions' => array('Thuvien.id' => $id)
		));
		$tmp[$this->modelClass] = $this->Common->html_decode($tmp[$this->modelClass]);
		$this->set('typeleft', $tmp[$this->modelClass]['typeleft']);
		$this->_get_leftmenu();

		$this->data = $tmp;
	}
	function xem_thongtin( $id,  $id_giaoviec = 0 ,$kiemtra = 0)
	{
		if( !$this->request->is('ajax') || !is_numeric($id) )exit;

		$this->set('data', $this->Thuvien->find('first', array(
			'conditions' => array(
				'Thuvien.trangthai' => 4,
				'Thuvien.id' => $id
			),
			'contain' => array('File','Nguoitao', 'Comment' => array('Nguoitao', 'limit' => 10) ),
		)));
		$this->set('id_giaoviec', $id_giaoviec);
		$this->set('kiemtra',$kiemtra);
	}

	function xem_checklist( $id,  $id_giaoviec = 0)
	{
		if( !$this->request->is('ajax') || !is_numeric($id) )exit;

		$this->set('data', $this->Thuvien->find('first', array(
			'conditions' => array(
				'Thuvien.trangthai' => 4,
				'Thuvien.id' => $id
			),
			'contain' => array('File','Nguoitao', 'Comment' => array('Nguoitao', 'limit' => 10) ),
		)));
		$this->set('id_giaoviec', $id_giaoviec);
	}
}

