<?php
class CommonComponent extends Component{

	function create_symlink( $source, $dest, $force = false )
	{
		// namnb: thêm vào điều kiện is_dir trước khi tạo symlink
		// substr để bỏ đi dấu / ở cuối cùng, kiểm tra thư mục là đúng
		if( file_exists( $source ) == true && is_dir( substr( $dest, 0, -1 ) ) )
		{
			$dest .= basename( $source );
			if( file_exists( $dest ) == true )
			{
				if( $force == true )
				{
					unlink( $dest );           // xoa link cu
					symlink( $source, $dest ); // tao symlink
				}
				chmod( $dest, 0777 );
				return true;
			}
			if( @symlink( $source, $dest ) == FALSE )
			{
				@unlink( $dest );
				if(!symlink( $source, $dest ))// tao symlink
				return false;
			}
			chmod( $dest, 0777 );      // cho phep truy cap
			return true;
		}

		return false;
	}

	function html( $data )
	{
		return $data;
	}

	function html_decode( $data = array( ) )
	{
		return $data;
	}


	function fulltext( $field, $text, $conditions = array() )
	{
		$text = $this->html( trim($text) );

//		// $text phải được html trước khi truyền vào
//		$nhieu_fields = array( );
//		if(!is_array($field))
//		{
//			$nhieu_fields[] = $field;
//		}else
//		{
//			return $conditions;
//			$nhieu_fields = $field;
//		}

//		$field_search = implode( ',', $nhieu_fields );

		// bắt đầu tách chuỗi, thay tat ca khoang trang bang 1 khoang trang
		$text = ereg_replace('  +', ' ', trim( str_replace( ' ', ' ', $text )));
		$arr_words = split(' ', $text);
		if(!is_array($arr_words))
		{
			$arr_words[] = $text;
		}

		foreach( $arr_words as $key => $word )
		{
			$arr_words[$key] = $this->_add_sign_d_D($word);
		}

		$conditions['OR'] = array(
			'MATCH ('.$field.') AGAINST (\'+'.implode(' +', $arr_words).'\' IN BOOLEAN MODE )',
			$field.' LIKE' => '%'.$text.'%'
		);

//		$conditions[] = 'MATCH ('.$field.') AGAINST (\'+'.implode(' +', $arr_words).'\' IN BOOLEAN MODE )';
		return $conditions;
	}

	function _add_sign_d_D( $mystring = null)
	{
		if( strpos($mystring, 'd') !== false || strpos($mystring, 'D') !== false)
		{
			$mystring = '('.$mystring.' '.str_replace(array('d', 'D'), array('đ', 'Đ'), $mystring ).')';
		}

		if( strpos($mystring, 'đ') !== false || strpos($mystring, 'Đ') !== false)
		{
			$mystring = '('.$mystring.' '.str_replace(array('đ', 'Đ'), array('d', 'D'), $mystring ).')';
		}
		return $mystring;
	}

	function string2date( $string = null, $ngancach = '-' )
	{
		if(strlen($string) == 10)
		{
			$arr = split('[\-\/\ \:-]', $string);
			$string = $arr[2].$ngancach.$arr[1].$ngancach.$arr[0];
		}
		return $string;
	}

	function string2date_full( $string = null, $ngancach = '-' )
	{
		if(strlen($string) == 10)
		{
			$arr = split('[\-\/\ \:-]', $string);

			if( strlen($arr[0]) == 1 )
			{
				$arr[0] = '0'.$arr[0];
			}

			if( strlen($arr[1]) == 1 )
			{
				$arr[1] = '0'.$arr[1];
			}

			// m/d/yyyy
			$string = $arr[2].$ngancach.$arr[1].$ngancach.$arr[0];
		}
		return $string;
	}

	function is_date( $input_date = null, $mode = 'dmy' )
	{

		//Nam nhap vao phai tu 1000-9999

		//Chuoi nhap vao phai co chieu dai la 10
		if( strlen( $input_date ) != 10 ){ return FALSE; }

		//Loai bo tat ca dau '/-\'
		//Nhung cai nao ki tu dat biet can them \ thi phai dat truoc
		$input_date = mb_ereg_replace( '[-/]','', $input_date );

		//Boi vi dau \ ko kiem tra bang regex dc nen danh xai` str_replace
		$input_date = str_replace( '\\', '', $input_date );

		//Sau khi loai bo thi chieu dai chuoi ddmmyyyy phai bang = 8
		//Neu ko kiem tra thi ngay 2//11/2008 se tra ve TRUE
		if( strlen( $input_date ) != 8 ){ return FALSE; }

		//Kiem tra tinh hop le cua thang/ngay/nam
		if( $mode == 'dmy' )
		{
			//1: dd/mm/yyyy
			$Day   = sprintf( '%d', substr( $input_date, 0,  2 ) );
			 $Month = sprintf( '%d', substr( $input_date, 2,  2 ) );
			 $Year  = sprintf( '%d', substr( $input_date, 4,  4 ) );
		}elseif( $mode == 'mdy' )
		{
			//2: mm/dd/yyyy
			$Month = sprintf( '%d', substr( $input_date, 0,  2 ) );
			 $Day   = sprintf( '%d', substr( $input_date, 2,  2 ) );
			 $Year  = sprintf( '%d', substr( $input_date, 4,  4 ) );
		}elseif( $mode == 'ymd' )
		{
			//3: yyyy/mm/dd
			$Year  = sprintf( '%d', substr( $input_date, 0,  4 ) );
			 $Month = sprintf( '%d', substr( $input_date, 4,  2 ) );
			 $Day   = sprintf( '%d', substr( $input_date, 6,  2 ) );
		}
		//The year is between 1 and 32767 inclusive.
		 return checkdate( $Month, $Day, $Year );
	}

	function date_convert( $input = null, $typein = 'dmy', $typeout = 'ymd', $symbol = '-', $hour = false  )
	{

		$input_root = $input;
		if( $hour == TRUE  )
		{
			$hour = substr( $input, 11, 8 );
		}

		// bo gio
		 $input = substr( $input, 0, 10 );

		 //Kiem tra nguoi nhap co nhap dung hay khong truoc khi convert
		if( $this->is_date( $input, $typein ) == FALSE )
		{
			return FALSE;
		}
		if( strlen( $input ) == 10 )
		{

			if( $typein == 'ymd' )
			{
				   $year = substr( $input, 0, 4 );
				   $month = substr( $input, 5, 2 );
				   $day = substr( $input, 8, 2 );
			}
			elseif( $typein == 'dmy' )
			{
				$day = substr( $input, 0, 2 );
				   $month = substr( $input, 3, 2 );
				   $year = substr( $input, 6, 4 );
			}

			if( $typeout == 'ymd' )
			{
				$output = $year.$symbol.$month.$symbol.$day;
			}elseif( $typeout == 'dmy' )
			{
				$output = $day.$symbol.$month.$symbol.$year;
			}
			//Neu giu lai time
			if( $hour == TRUE )
			{
				return $output.' '.$hour;
			}else
			{
				return $output;
			}
		}
		return $input_root;
	}
}
