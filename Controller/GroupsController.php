<?php
class GroupsController extends AppController {

	var $name = 'Groups';

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('*');
	}

	function index() {

		// phan trang
		$this->paginate = array(
			'conditions' => array('Group.status' => 4)
		);
		$groups = $this->paginate();
		$this->set('groups', $groups);
		$this->layout = 'default';
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid User'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Group->create();

			$this->data['Group']['name'] = $this->Common->html($this->data['Group']['name']);
			$this->data['Group']['status'] = 4;
			if ($this->Group->save($this->data)) {
				$this->Session->setFlash(__('Nhóm đã được lưu'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Nhóm lưu thất bại. Vui lòng thử lại.'));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid User'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The User has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.'));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( doannt ) ( May 28, 2010 )
	//***************************************************************//
	function listgroup(  ) {
		$cond = array( 'Group.status' => 4 );
		// XÓA GROUP SUPERADMIN NẾU NHƯ NGƯỜI NHẬP KHÔNG PHẢI LÀ SUPERADMIN
		if( $this->Auth->user('group_id') != 3 )$cond['Group.name <>'] = 'SuperAdmin';

		$dsgroup = $this->Group->find( 'list', array(
						'fields' => array( 'id', 'name' ),
						'contain' => false, 'conditions' => $cond,
		) );

		$this->set( 'dsgroup', $dsgroup );
		return $dsgroup;
	}
}
