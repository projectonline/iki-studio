<legend>Tin tức - <?php echo $data[$model]["tieude"]; ?></legend>
<div id="tintucs_chitiet" style="width: 650px;" class="item">
    <div class="title" style="margin:3px;padding-bottom:9px;">
        <span style="color:red"><?php echo $data[$model]["tieude"]; ?></span>
    </div>

    <div class="content" style="max-height:300px;overflow:auto">

        <?php if( strpos($data[$model]["noidung"], 'table') ){
                echo str_replace( array('<br>', 'width='), '', $data[$model]["noidung"] );
        }else{
                echo nl2br($data[$model]["noidung"]);
        } ?>

    </div>

    <em>( <?php echo $data["Nguoitao"]["username"].", ".date("d/m/Y H:i",strtotime($data[$model]["created"])); ?>)

    <?php if( isset( $admin ) || in_array( AuthComponent::user('id'), array(356, 357) ) ){
        echo $this->Js->link( __("Edit"), "/$controller/sua/".$data[$model]["id"],
            array( 'before' => '$(\'#loading\').show()',
                   'update' => '#window_content', 'title' => __('Sửa tin tức này'),
                   'success' => '$("#loading").hide();window_show();'
                   ) );
        } ?>
    </em>
</div>

<?php echo $this->Js->writeBuffer(); ?>