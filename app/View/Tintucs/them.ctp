<legend>Thêm mới tin tức</legend>
<?php echo $this->Form->create($model); ?>
	<h3>Tiêu đề (*):</h3>
	<?php echo $this->Form->input($model.'.tieude', array('size' => 60 ));?>

	<br>
	<h3>Nội dung (*):</h3>
	<?php echo $this->Form->input($model.'.noidung', array('id' => $controller.'_textarea_id', 'rows' => 20, 'cols' => 50, 'required' => false));?>

	<center>
		<input title="Lưu" value=" Lưu thông tin " type="submit" class="Button">
		<a onclick="$('#window_content').dialog( 'close' );" href="javascript:void(0)" class="Button" style="float: right;">Đóng lại</a>
	</center>
<?php echo $this->Form->end(); ?>

<?php echo $this->element('active_tinymce'); ?>