<?php if(isset($admin) || in_array( AuthComponent::user('id'), array(356, 357) )){ ?>
    <div style="position:absolute;top:3px; right:5px">
    <?php  echo $this->Js->link( "New", "/tintucs/them",
            array( 'before' => '$(\'#loading\').show()',
                   'update' => '#window_content',
                   'title' => 'Thêm mới tin tức',
                   'success' => '$("#loading").hide();window_show();'
       ) );
    ?>
    </div>
<?php } ?>
<div id="scroll_tintuc" class="item" id="<?php echo $model.'_'.$data[$model]['id']; ?>" style="position:relative;height: 283px;">

    <div style="height: 276px;overflow:hidden">
        <div style="margin:3px;padding-bottom:9px;">
            <span class="title">
                <?php if( isset($random_all) ){ ?>
                    <span style="color:#3b5998;"><?php echo $data[$model]["tieude"]; ?></span>
                <?php }else{ ?>
                    <span style="color:red;"><?php echo $data[$model]["tieude"]; ?></span>
                <?php } ?>
            </span> <!-- Doan dong <em>(tin tức ngẫu nhiên)</em> Doan dong -->
        </div>


        <div id="tintucs_content" class="content" style="overflow:hidden;">

            <?php if( strpos($data[$model]["noidung"], 'table') ){
                    echo str_replace( array('<br>', 'width='), '', $data[$model]["noidung"] );
            }else{
                    echo nl2br($data[$model]["noidung"]);
            } ?>

        </div>
    </div>

    <script type="text/javascript">
        $(function(){

            if( $("#tintucs_content").height() < 230 )$("#tintucs_xemthem_first").hide();

            // $("#tintucs_cover").jScrollPane();
            /*
            var scrollpane = $('#scroll_tintuc');
              scrollpane.jScrollPane({
                verticalDragMinHeight: '37',
                verticalDragMaxHeight: '37'
              });
    */
        });
    </script>

    <em>( <?php echo $all_user_qlda[$data[$model]["nguoitao"]]['username'] .", ".date("d/m/Y H:i",strtotime($data[$model]["created"])); ?>)
        <?php if( isset( $admin ) ){
            echo $this->Js->link( __("Edit"), "/$controller/sua/".$data[$model]["id"],
                array( 'before' => '$(\'#loading\').show()',
                       'update' => '#window_content', 'title' => __('Sửa tin tức này'),
                       'success' => '$("#loading").hide();window_show();'
                       ) );
            ?>| <a href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]['id']; ?>)">Del</a>
    <?php } ?>
    </em>

    <div id="tintucs_xemthem_first" style="position:absolute; bottom:0; right:0;background-color: #FFFFFF;">
        <?php echo $this->Js->link( 'More...', "/tintucs/chitiet/".$data[$model]["id"],
                array( 'before' => '$(\'#loading\').show()',
                       'update' => '#window_content', 'title' => __('Sửa tin tức này'),
                       'success' => '$("#loading").hide();window_show();',
                       'escape' => false,
                       'style' => 'padding:4px 11px'
           ) );
         ?>
    </div>
</div>

<?php $i = 1; $STT = 1;
if( $this->request->params['paging'][$model]['page'] > 1 )
{
    $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
} ?>

<ul id="tintucs_abc">
<?php foreach( $datas as $data ){ ?>
    <li id="<?php echo $model.'_'.$data[$model]['id']; ?>"><?php echo $STT++;

            echo '. '.$this->Js->link( $data["Tintuc"]["tieude"], "/tintucs/chitiet/".$data[$model]["id"],
                array( 'before' => '$(\'#loading\').show()',
                       'update' => '#window_content', 'title' => __('Sửa tin tức này'),
                       'success' => '$("#loading").hide();window_show();',
                       'escape' => false
           ) );
         ?>&nbsp;<em>(<?php echo $all_user_qlda[$data[$model]["nguoitao"]]['username'].", ".date("d/m/Y H:i",strtotime($data[$model]["created"])); ?>)</em>

        <?php if( (strtotime($data[$model]["created"]) + (DAY*3) ) >= strtotime("now") ) {  ?>
            <img src="/img/icon_new.png" />
        <?php } ?>
        <?php if( isset( $admin ) ){
            echo '<em>'.$this->Js->link( __("Edit"), "/$controller/sua/".$data[$model]["id"],
                    array( 'before' => '$(\'#loading\').show()',
                           'update' => '#window_content', 'title' => __('Sửa tin tức này'),
                           'success' => '$("#loading").hide();window_show();'
                           ) );
                ?>| <a href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]['id']; ?>)">Del</a></em>
        <?php } ?>
    </li>
<?php } ?>
</ul>

<style>
    #tintucs_abc a{
        /*text-transform: lowercase;*/
    }
</style>

<?php echo $this->element('paging_search', array('update' => '#tintucs_cover', 'no_scroll' => 1)); ?>
<?php //echo $this->Js->writeBuffer(); ?>

<script type="text/javascript">
    $(function(){
        if( $("#tintucs_content").height() < 151 ){ $("#tintucs_xemthem_first").remove(); }
    });
</script>

<?php echo $this->Js->writeBuffer(); ?>
<style>
#tintucs_cover table{
    max-width: 640px !important;
}
</style>