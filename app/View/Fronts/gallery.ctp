<div id="mainWrapper">
  	<?php echo $this->element('front/header') ?>

	<script type="text/javascript" language="javascript">var projName = '0'</script>		<link href="/css/photoswipe.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="/js/klass.min.js"></script>
	<script type="text/javascript" src="/js/code.photoswipe-3.0.5.min.js"></script>


    <style type="text/css">
		.topMenuHighLighter3{
			background: #BBB;
			padding:3px 10px 5px !important; 
		}
		#sliderBar{display:none}
	</style>

	
	<div id="galleryCover">
		
	</div>
	
	<div id="titleBar">
		<p>&nbsp;</p>
	</div>
	
<p id="filters" style="text-align:center">
	<br />
  <a href="#" data-filter="*">Show All</a> | 
  <a href="#" data-filter=".Corporate">Corporate</a> | <a href="#" data-filter=".Base_Building">Base_Building</a> | <a href="#" data-filter=".Retail">Retail</a> | <a href="#" data-filter=".Educational">Educational</a> | <a href="#" data-filter=".Residential_Housing">Residential_Housing</a></p>
	
	<div id="mainContainer">
		<div id="imageLoadingMessage">Loading images, please wait... </div>
		<div id="galleryContainer">
			<div id="datto_boston" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Datto Boston</p></div><div id="datto" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Datto, Inc</p></div><div id="confidential" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Confidential Financial Client</p></div><div id="newmans" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Newman's Own</p></div><div id="newmans_base" class="images Base_Building"><img class="thumb" width="150px" src="/img/project/thumb/09.jpg" /><p class="cover"></p><p class="text">Newman's Own</p></div><div id="building_land_technology" class="images Retail Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Building & Land Technology</p></div><div id="ge_capital_nyc" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">GE Capital NYC</p></div><div id="ge_capital_norwalk_2" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">GE Capital Norwalk, CT</p></div><div id="ge_capital_norwalk" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">GE Capital Norwalk CT</p></div><div id="ge_capital_aviation_services" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">GE Capital Aviation Services</p></div><div id="greenwich_american_centre" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Greenwich American Centre</p></div><div id="vineyardvines" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Vineyard Vines</p></div><div id="mbia_insurance" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">MBIA Insurance</p></div><div id="quorum" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Quorum Federal Credit Union</p></div><div id="citibank_ny" class="images Retail Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Citibank Manhasset NY</p></div><div id="cannondale_office" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Cannondale Sports Unlimited Office</p></div><div id="cannondale" class="images Retail"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Cannondale Sports Unlimited Retail Lab</p></div><div id="naughty" class="images Retail"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Naughty Water Whiskey Bar and Grille</p></div><div id="citibank_dc" class="images Retail Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Citibank DC</p></div><div id="richard_mitchell" class="images Retail"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Richard's Mitchell's Greenwich</p></div><div id="general_atlantic" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">General Atlantic</p></div><div id="hudsons" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Hudson's Bay Trading Company</p></div><div id="kgibbs" class="images Educational"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Katharine Gibbs School</p></div><div id="ncc" class="images Educational"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Norwalk Community College</p></div><div id="citibank" class="images Retail Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Citibank NY</p></div><div id="cadbury" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Cadbury</p></div><div id="antares" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Antares</p></div><div id="diageo" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Diageo</p></div><div id="ge_money" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">GE Money</p></div><div id="ice" class="images Educational"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">ICE</p></div><div id="ims_health_ct" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">IMS Health CT</p></div><div id="ims_health_nj" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">IMS Health NJ</p></div><div id="lime_rock_partners" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Lime Rock Partners</p></div><div id="moab_oil" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Moab Oil</p></div><div id="nestle_waters" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Nestle Waters</p></div><div id="one_river_road" class="images Base_Building"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">One River Road</p></div><div id="philips" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Philips</p></div><div id="preferred_investments" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Preferred Investments</p></div><div id="rd_scinto" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">RD Scinto</p></div><div id="rd_scinto_renaissance" class="images Residential_Housing"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">RD Scinto Renaissance</p></div><div id="rfr_realty" class="images Base_Building"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">RFR Realty</p></div><div id="sfaft_subway" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">SFAFT (Subway)</p></div><div id="sun_products_hq" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Sun Products HQ</p></div><div id="sun_products_rd" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Sun Products RD</p></div><div id="tudor_investments" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Tudor Investments</p></div><div id="uconn" class="images Educational"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">UCONN</p></div><div id="us_tobacco" class="images Base_Building"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">US Tobacco</p></div><div id="vertrue" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Vertrue</p></div><div id="littlejohn" class="images Corporate"><img class="thumb" width="150px" src="/img/project/thumb/1.jpg" /><p class="cover"></p><p class="text">Littlejohn</p></div>		</div>
		
		<div id="galleryContainerTemp">
			
		</div>
		
		<div id="imageSubGallery" style="display:none">
			<div class="datto_boston"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
</div><div class="datto"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/04.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/05.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/06.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/07.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/08.jpg" /></a>
</div><div class="confidential"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
</div><div class="newmans"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/04.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/06.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/07.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/08.jpg" /></a>
<a href="/img/project/large/09.jpg" title=""><img width="80px" src="/img/project/thumb/09.jpg" /></a>
<a href="/img/project/large/10.jpg" title=""><img width="80px" src="/img/project/thumb/10.jpg" /></a>
</div><div class="newmans_base"><a href="/img/project/large/09.jpg" title=""><img width="80px" src="/img/project/thumb/09.jpg" /></a>
<a href="/img/project/large/10.jpg" title=""><img width="80px" src="/img/project/thumb/10.jpg" /></a>
</div><div class="building_land_technology"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
</div><div class="ge_capital_nyc"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
</div><div class="ge_capital_norwalk_2"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
</div><div class="ge_capital_norwalk"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
</div><div class="ge_capital_aviation_services"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
</div><div class="greenwich_american_centre"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
</div><div class="vineyardvines"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
<a href="/img/project/large/8.jpg" title=""><img width="80px" src="/img/project/thumb/8.jpg" /></a>
</div><div class="mbia_insurance"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
</div><div class="quorum"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
</div><div class="citibank_ny"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
<a href="/img/project/large/8.jpg" title=""><img width="80px" src="/img/project/thumb/8.jpg" /></a>
<a href="/img/project/large/9.jpg" title=""><img width="80px" src="/img/project/thumb/9.jpg" /></a>
</div><div class="cannondale_office"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/10.jpg" title=""><img width="80px" src="/img/project/thumb/10.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
<a href="/img/project/large/8.jpg" title=""><img width="80px" src="/img/project/thumb/8.jpg" /></a>
<a href="/img/project/large/9.jpg" title=""><img width="80px" src="/img/project/thumb/9.jpg" /></a>
</div><div class="cannondale"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
</div><div class="naughty"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
</div><div class="citibank_dc"><a href="/img/project/large/1.jpg"title="Citibank 1000 CT Ave Washington DC"><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
</div><div class="richard_mitchell"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
</div><div class="general_atlantic"><a href="/img/project/large/1.jpg"title="General Atlantic awarded their assignment to relocate from downtown Greenwich to 600 Steamboat Road directly on the Harbor."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="General Atlantic capitalized on this opportunity by creating a more open, team oriented environment that provided an egalitarian sharing of the magnificent waterside views."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="We sought to retain a consistent global experience using a similar palette as other General Atlantic offices with light veneers, wall coverings and flooring thus providing a neutral back drop for art and the floor to ceiling panoramic window walls."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The design was pushed forward to provide a forward looking version of the General Atlantic model with state of the art IT and AV systems allowing global connectivity with other General Atlantic and clients' offices."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
</div><div class="hudsons"><a href="/img/project/large/1.jpg"title="Hudson's Bay Trading Company's executive offices are the New York home for a premier Canadian retailer and owners of Lord and Taylor in the United States."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Located on the top two floors of a 100 year old loft building adjacent to the Lord and Taylor store on Fifth Avenue, this office provides a collaborative environment for both the New York based staff and hotelling space for traveling executives."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The twelfth floor features a dividable, grand meeting space on the sunny south side facing the Empire State Building three blocks away."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="There is a gallery-like feel to the space featuring skylights, concrete floors, steel stairs, white walls and glass. An extensive art collection, provided by the owner, adds the final touch."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg"title="In collaboration with Robert Johnson we introduced very comfortable, fine furnishings and drapery dividers to add a soft fashion-house/studio feeling."><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg"title="There are 20 small glass front private offices and numerous conventional conference rooms."><img width="80px" src="/img/project/thumb/6.jpg" /></a>
</div><div class="kgibbs"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/04.jpg" /></a>
</div><div class="ncc"><a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
</div><div class="citibank"><a href="/img/project/large/1.jpg"title="330 Madison Ave, NYC. CPG Architects has been providing professional design and architectural services to Citibank for over 20 years.  During that time we have focused not only on design but also made it our goal to become experts in all phases of project development for their corporate and retail projects."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Citibank considers us a strong partner in each of these areas because of our ongoing experience working as part of their Global Planning and Design team.  We were tasked to value engineer the flagship we designed at Foggy Bottom in Washington, DC while development on this project was in progress.  This flagship prototype employs the latest Citibank branding, digital media, millwork, furniture and finish concepts which CPG was integral in establishing."><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The 20,000 sq. ft. renovation at Madison Ave. and 42nd street in NYC was actualized while the branch remained operational over a three-year period.  CPG was instrumental in the planning, scheduling and implementation of the rollout which entailed building a temporary branch on location; complete gutting of the new space and relocation back into the finished space as shown."><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg"title="We have designed projects for Citibank in New York, New Jersey, Connecticut, Maryland, Virginia and the District of Columbia, and consulted on projects nationally.  We have performed and are actively working on complete ground up design and construction services for several Citibank branches."><img width="80px" src="/img/project/thumb/05.jpg" /></a>
<a href="/img/project/large/1.jpg"title="This work evolved into creating a comprehensive set of design standards, guidelines and project management tools that architects are using throughout North America.  These experiences have taught us much about the prototype process and where to look for opportunities to save both time and money, valuable lessons that we will apply for your program."><img width="80px" src="/img/project/thumb/06.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/07.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/08.jpg" /></a>
<a href="/img/project/large/09.jpg" title=""><img width="80px" src="/img/project/thumb/09.jpg" /></a>
</div><div class="cadbury"><a href="/img/project/large/1.jpg"title="CPG was retained to design a new headquarters for Motts formerly in Stamford and Snapple in White Plains."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="We designed the new headquarters and combined the best of both cultures and incorporated new standards and planning concepts to work with very large 80,000 SF floors totaling 135,000 SF"><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The project was extremely successful and exciting for both CPG and the Motts/Snapple staffs."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
</div><div class="antares"><a href="/img/project/large/1.jpg"title="As part of their office space on the top floor of Stamford Harbor Park, Fairfield County Real Estate Developer, Antares, created a Public Relations/Marketing Space surrounding their reception area."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
</div><div class="diageo"><a href="/img/project/large/1.jpg"title="Through a competitive selection process, CPG was awarded the Diageo Headquarters Relocation project in Norwalk, CT."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/10.jpg"title="As the largest adult beverage manufacturer the project was designed around the branding areas with the primary focus being flexibility of space and employee efficiency considerations."><img width="80px" src="/img/project/thumb/10.jpg" /></a>
<a href="/img/project/large/1.jpg"title="It is certainly one of the most exciting and challenging projects we have worked on."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
<a href="/img/project/large/8.jpg" title=""><img width="80px" src="/img/project/thumb/8.jpg" /></a>
<a href="/img/project/large/9.jpg" title=""><img width="80px" src="/img/project/thumb/9.jpg" /></a>
</div><div class="ge_money"><a href="/img/project/large/1.jpg"title="GE Money now occupies a new 250,000 square foot headquarters in three individual buildings, in a campus setting in Stamford."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
</div><div class="ice"><a href="/img/project/large/1.jpg"title="The Institute for Culinary Education was a recent CPG project that was a mix of cooking school, administrative work areas and offices, and public use areas."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The main challenge was to enlarge and update an existing school, while creating spaces that would help draw in the public and potential clients."><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The project is on 3 floors in an older, mixed use warehouse building in Manhattan."><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The client really wanted to stay in their existing location, but expand. They were able to do this by carefully planning and staging their construction around operational kitchens and classes."><img width="80px" src="/img/project/thumb/04.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The end result is a light and airy industrial-inspired space that doubled their original capacity."><img width="80px" src="/img/project/thumb/05.jpg" /></a>
</div><div class="ims_health_ct"><a href="/img/project/large/1.jpg"title="IMS is a 4,000 person international management consulting firm servicing the healthcare and pharmaceutical industries."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="IMS retained CPG to craft an expertly detailed headquarters space in a contemporary international style."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Inspired by the IMS CEO's vision, CPG created a clean, modern, yet warm and comfortable architectural envelope enhanced with fine furnishings and art."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The high performance infrastructure invisibly and quietly provides support to this highly finished executive environment."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
</div><div class="ims_health_nj"><a href="/img/project/large/1.jpg"title="IMS Health, an international research and marketing firm working with pharmaceutical clients, retained CPG to reshape an entire 3-story building as a regional headquarters."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The entire project consisted of 120,000 SF and was completed on a fast-track basis in multiple stages to accommodate the client's schedule."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="CPG also assisted IMS in the building selection. The centerpiece of the design was a central atrium featuring a new open stair and large graphic murals based on IMS' collection of images."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Branding was a key element throughout the facility, which included on the ground floor a client-facing conference center and training rooms, cafeteria and a fitness center."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
</div><div class="lime_rock_partners"><a href="/img/project/large/1.jpg"title="Lime Rock Partners is an investment management firm specializing in energy companies worldwide with offices in Houston, Aberdeen and Dubai. Lime Rock retained CPG to design its headquarters facility in Connecticut."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The design challenge was to create an open, expansive feeling in a plan where perimeter private offices prevailed."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The special patterned glass front balanced the admission of light and view to the interior while screening the occupants from full exposure."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The space features a river view entry foyer, a large open community dining space, boardroom and fitness center."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The color palette is neutral green-gray and white creating a backdrop for an extensive art collection, a Ferrari-red building core and the continuous perimeter water and woodland views."><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The thirty person staff is offered a family-like atmosphere with a variety of spaces offering the opportunity to learn, collaborate and socialize."><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
</div><div class="moab_oil"><a href="/img/project/large/1.jpg"title="Moab Oil, Norwalk, CT, 15,000 SF"><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="MOAB Oil retained CPG to design their new office in a renovated industrial building in South Norwalk."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Their extremely informal frat house culture inspired us to incorporate a list of features that would be a young man's dream come true."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The 30 trading position team work space is surrounded by video monitors and supported by a sports bar, video gaming center, kitchen full of beer and frozen pizza."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg"title="While the office has a cutting-edge technological infrastructure the architectural style is compatible with the turn of the 20th century industrial, marine building and neighborhood."><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Pool and dart room"><img width="80px" src="/img/project/thumb/6.jpg" /></a>
</div><div class="nestle_waters"><a href="/img/project/large/1.jpg"title="Last year we were proudly retained by Nestle for the relocation of their North American headquarters from Greenwich, CT to a new as yet determined home.  CPG at first assisted with site selection, reviewing several properties and then providing due diligence analysis and test fit-outs of the short-listed buildings and sites."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The relocation of their HQ involved the preparation of long term personnel and space programs, departmental adjacency studies, workflow options, new workplace standards, studies for providing collaborative spaces and budget and schedule guidelines."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Nestle Waters is viewing this move to new quarters as a rare opportunity to reinvent their work style and culture.  They regard their present primarily closed environment as not providing a setting to maximize the collective energy and creativity of their 500 person headquarters staff."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="In September of 2010, Nestle Waters excitedly moved into their new 175,000 square foot headquarters at 900 Long Ridge Rd., Stamford, CT.  The four story building was stacked with community facilities on the first floor including Reception, Cafeteria, Servery and Kitchen, a large open collaborative area known as the Town Center, 125 seat Auditorium, 2 Training rooms and numerous Conferencing Rooms."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The three upper floors provide open plan design consisting of just three workspace standards for the 500 plus employees.  The balance of each floor is devoted to conferencing, war rooms, 2-3 person conversation rooms and open collaborative areas."><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Each floor is organized around an open Bistro and galley Pantry that shares a new four story skylit open staircase, helping to foster interaction and community."><img width="80px" src="/img/project/thumb/6.jpg" /></a>
<a href="/img/project/large/7.jpg" title=""><img width="80px" src="/img/project/thumb/7.jpg" /></a>
<a href="/img/project/large/8.jpg" title=""><img width="80px" src="/img/project/thumb/8.jpg" /></a>
</div><div class="one_river_road"><a href="/img/project/large/1.jpg"title="One River Road was a tired nondescript building in a prime Greenwich location."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The Indian Harbor Investment Group had the vision to under-stand the potential and retained CPG to design a new headquarters for their firm."><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg"title="One goal was to replan to create a new efficient envelope by filling in the existing fire stair and building a new entrance/stair/elevator rotunda.  Working closely with zoning officials we were able to add over 10% gross area."><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Another goal was to strip the building to the raw structure and rebuild in a contemporary, efficient manner using natural sustainable materials."><img width="80px" src="/img/project/thumb/04.jpg" /></a>
<a href="/img/project/large/1.jpg"title="A third goal was to design a flexible interior layout using demountable glass partitions."><img width="80px" src="/img/project/thumb/05.jpg" /></a>
<a href="/img/project/large/1.jpg"title="A lower level, formerly storage, was transformed into prime office space by incorporating a glass enclosed garden terrace."><img width="80px" src="/img/project/thumb/06.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/07.jpg" /></a>
</div><div class="philips"><a href="/img/project/large/1.jpg"title="This North American Headquarters facility introduces the Dutch Global Standards to the U.S. staff."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The planning incorporates a much more open environment using one size workstation and one size office in an 80/20 ratio."><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The European imagery standards were also integrated featuring a very clean, simple, white back-drop for Philips' product displays and branding."><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/04.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/05.jpg" /></a>
</div><div class="preferred_investments"><a href="/img/project/large/1.jpg"title="Preferred Investment Solutions, formerly known as Kenmar is an international investment firm that CPG designed in Rye Brook, NY."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/02_stitch.jpg"title="The client wanted a space that was very open and professional while drawing its inspiration from lofts in New York City. The over-all aesthetic of the environment is urban, with nods to an industrial space yet comfortable."><img width="80px" src="/img/project/thumb/02_stitch.jpg" /></a>
<a href="/img/project/large/03_stitch.jpg"title="Clean materials such as concrete, cork, and exposed metals help to evoke the playfulness the client was looking for, but maintaining a definite business atmosphere."><img width="80px" src="/img/project/thumb/03_stitch.jpg" /></a>
</div><div class="rd_scinto"><a href="/img/project/large/1.jpg"title="The Scinto office space was a complete gut and redesign of an existing office space for a CT developer."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The client was looking to update their image and also re-think how they worked within their space."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Additionally, there was a desire to showcase their art collection in a much more formal and gallery atmosphere. The office furniture was a combination of Geiger Settings system, in natural cherry with stainless steel accents, and contemporary classics. The CEO suite was appointed with custom imported executive furniture from France and Switzerland as well as refurbished existing pieces."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The overarching theme for the design of the space was to incorporate their expanded needs, while making the space more comfortable."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg"title="This was done while creating a space that appeared more formal to their clients. The use of limestone juxtaposed against other warm, natural fibers and fabrics combined to form an office environment that was both functional and elegant."><img width="80px" src="/img/project/thumb/5.jpg" /></a>
</div><div class="rd_scinto_renaissance"><a href="/img/project/large/1.jpg"title="Developer, Bob Scinto, retained CPG to design a stand alone 6,000 s.f. building to house a sales center and model residence in order to market his new rental and condominium tower in Shelton CT."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The building was be completed early in 2008."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The design of the model served as a prototype for the specifications for all interior layout, millwork, trim, finishes,hardware, and kitchen and bathroom design."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="CPG also consulted on the final layouts of all the units as well as the design of the building lobby and common spaces."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
</div><div class="rfr_realty"><a href="/img/project/large/1.jpg"title="RFR Realty of NYC purchased 7 buildings in Stamford, CT.  (Formerly owned by Equity Office Properties)  RFR hired CPG to renovate/update the public areas of the buildings."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Areas of work included: Main entrance lobbies, site signage, exterior plaza, multi-tenant corridor, and public restrooms"><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/5.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/6.jpg" /></a>
</div><div class="sfaft_subway"><a href="/img/project/large/1.jpg"title="CPG recently designed the corporate headquarters for the Subway SFAFT offices in Milford, CT."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The client turned to CPG to help them relocate their employees from an existing corporate facility to a new, energetic space that represents their vitality."><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The space consists of a mostly open plan work area balanced with a small group of private offices. The largest portion of the space was designed as shared "community" space for elements such as conference, dining, entertaining, presentation and branding."><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The wall graphics, lighting, and materials selected for the project were meant to reflect and build on Subway's highly visible marketing campaigns."><img width="80px" src="/img/project/thumb/04.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The energy of the employees and culture of the Subway brand are reflected directly as well as subtly throughout the architectural design."><img width="80px" src="/img/project/thumb/05.jpg" /></a>
</div><div class="sun_products_hq"><a href="/img/project/large/1.jpg"title="Sun Products Corporation was created in late 2008 through the purchase of the laundry products division from Unilever.  CPG was retained to provide designs simultaneously for a new headquarters facility in Wilton CT (60,000 SF)."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="While the company consisted of only the core executive team (CEO, CFO, CIO and HR) CPG applied its extensive experience in corporate design and architecture and imagined solutions that could be built with the flexibility to adapt to the ultimate goal of accommodating a combined staff of almost 300."><img width="80px" src="/img/project/thumb/02.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Key goals in the creation were to create an architecture that symbolized Sun's financial strength to business partners and to provide a very high quality of life to their potential future senior level staff."><img width="80px" src="/img/project/thumb/03.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Sun Products has been a model client who believes that architecture and design will help their efforts to attract and retain the best and the brightest."><img width="80px" src="/img/project/thumb/04.jpg" /></a>
<a href="/img/project/large/1.jpg"title="They have worked hard with the CPG team to ensure that the architecture enables them in their efforts to create a culture that encourages collaboration, imagination and teamwork."><img width="80px" src="/img/project/thumb/05.jpg" /></a>
</div><div class="sun_products_rd"><a href="/img/project/large/1.jpg"title="Sun Products Corporation was created in late 2008 through the purchase of the laundry products division from Unilever.  CPG was retained to provide designs simultaneously for a Research and Development and consumer research building in Trumbull CT (45,000 SF) and an executive office in Toronto (20,000 SF) to serve as a base for Canadian operations."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="While the company consisted of only the core executive team (CEO, CFO, CIO and HR) CPG applied its extensive experience in corporate design and architecture and imagined solutions that could be built with the flexibility to adapt to the ultimate goal of accommodating a combined staff of almost 300."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Key goals in the creation were to create an architecture that symbolized Sun's financial strength to business partners and to provide a very high quality of life to their potential future senior level staff."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The mandate for the Research and Development facility was to create a design that offered cutting edge labs and an image that symbolized Sun's commitment to research."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Sun Products has been a model client who believes that architecture and design will help their efforts to attract and retain the best and the brightest.  They have worked hard with the CPG team to ensure that the architecture enables them in their efforts to create a culture that encourages collaboration, imagination and teamwork."><img width="80px" src="/img/project/thumb/5.jpg" /></a>
</div><div class="tudor_investments"><a href="/img/project/large/1.jpg"title="Tudor Investments, Greenwich American Center, Greenwich, CT. Approximately 100,000 SF"><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="CPG has been working with Tudor since 1995 when they moved their headquarters from NYC to Greenwich.  The facility accommodated 40 traders at 600 Steamboat Road.  Since then they have grown into two facilities."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The past few years we have completed a multi-phased project at the Greenwich American Center.  The first phase was 21,000 SF on the 2nd floor, followed by a 25,000 SF expansion on the 1st floor."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
</div><div class="uconn"><a href="/img/project/large/1.jpg"title="GE Capital and University of Connecticut formed a partnership, called Edgelab, in the fall of 2000. University faculty and students work jointly with GE Capital executives to research emerging technologies such as biometrics, digital signature and voice recognition to develop real e-business solutions."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="CPG designed Edgelab, an 11,000 sprawling facility to stimulate creative sharing of ideas. The training classroom was a 50' curved movable wall easily handles groups of various sizes in a number of furniture configurations. The center is designed with the latest in video technology connecting to GE Capital offices and other UCONN facilities."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The center is so successful the CPG has already started planning Phase II expanding open area workstations, labs, and additional training facilities."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
</div><div class="us_tobacco"><a href="/img/project/large/1.jpg"title="As part of the Project Momentum initiative to increase efficiency and reduce cost, UST sold its decades old Greenwich Headquarters building and moved into a group of three interconnected buildings in High Ridge Park in Stamford, CT."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The center building had is courtyard enclosed under skylights to create a 3-story town center atrium. A new monumental stair presides over a large gathering space with coffee/catering bar and several semi-private enclosed booths."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The atrium is ringed with various sized conference rooms, each customized and branded to take on the look and feel of USTs leading smokeless tobacco and wine labels."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The cores of the typical office floors are set apart with wood slat walls and a rustic slate pathway reminiscent of the company's rural roots."><img width="80px" src="/img/project/thumb/4.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Corners of the cores are wrapped in full height photographic murals that carry on the ambience and tradition of the various brands."><img width="80px" src="/img/project/thumb/5.jpg" /></a>
</div><div class="vertrue"><a href="/img/project/large/1.jpg"title="Formerly Memberworks, in Norwalk, CT Vertrue's CEO wanted to break out of the private offices and tall panel workstations in their move to Norwalk, CT."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The desire to promote interaction, collaboration and flexibility became the motive force behind the design. Everyone got the same work area, a completely mobile free floating ensemble of table, files and storage, all on wheels and with no barriers."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
<a href="/img/project/large/1.jpg"title="Coiled power and data drops termed pythons or I.V.s (information viaducts) from the ceiling to each work area allow everyone to set up as they wish. Vertrue and CPG took care to bring the employees along during the designprocess with a series of meetings, contests and a project website. The result at move-in was a cultural sea-change enthusiastically embraced and praised by everyone."><img width="80px" src="/img/project/thumb/3.jpg" /></a>
<a href="/img/project/large/1.jpg" title=""><img width="80px" src="/img/project/thumb/4.jpg" /></a>
</div><div class="littlejohn"><a href="/img/project/large/1.jpg"title="Littlejohn and Co (Greenwich, CT, 15,191 SF) relocated to a larger office and hired CPG to provide a complete turnkey service of design through construction administration on their behalf. The design challenge was to re-use existing workstations and create a brand new look and feel that reflects a more current design with a traditional feel."><img width="80px" src="/img/project/thumb/1.jpg" /></a>
<a href="/img/project/large/1.jpg"title="The new look and feel focused on the accessibility of the new board room and conference rooms off the reception and helped group teams together for a more cohesive working environment."><img width="80px" src="/img/project/thumb/2.jpg" /></a>
</div>		</div>
		
					
	</div>
	
</div>


<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/2183733.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->
</body>
</html>

<script language="javascript1.2">
		var sorting = false;
		
		var ct; //timer variable
		
		$(window).ready(function(){
			if(projName != 0) {	//if the project name is available then open the lightbox
				ct = setInterval("triggerClickToOpenLightBox(projName)",500);
			}
		});
		
		$(document).ready(function() {
		
			//$('#galleryContainer').hide();
			$('.loading').hide();
		
			var newImageDetailPosition = ($(window).width()/2) - ($('#imageDetail').width()/2);
			$('#imageDetail').css('left', newImageDetailPosition);
						
			
			$('.images').click(function(event) {			
				triggerClickToOpenLightBox($(this).attr('id'));			
			});
			
			$('.images').mouseover(
					function(event) {
						
						$(this).find('p.cover').show();	
						$(this).find('p.text').show();	
					});
					
			$('.images').mouseleave(function(event) {
				$(this).find('p.cover').hide('scale');
				$(this).find('p.text').hide();
			});
			
			
			
			$('img.loading').hide();
			// initialize image gallery
			
			var totalImages = $('.images img.thumb').length;
			//alert(totalImages);
			
			$('.images img.thumb').each(function() {
			
				if($(this)[0].complete) {
					$('#galleryContainer').show();
					totalImages--;
					$('#galleryContainer').fadeIn(1, function() {
								
					});
					//if(sorting) return;
					if(totalImages == 0) {
						//alert('sdsd');
						$('#imageLoadingMessage').hide();
						$('#galleryContainer').isotope({
						  // options
						  itemSelector : '.images',
						  layoutMode : 'masonry',
						  filter: $('#filters a:eq(1)').attr('data-filter')
						}).isotope( 'reLayout', function() {
							
							//$('#galleryContainer').isotope( {filter:'*'} );
							var t = setTimeout(callOnTimer, 1);
							
						} );
					}
					
				} 
			
				$(this).load(function() {
					
					$('#galleryContainer').show();
					totalImages--;
					$('#galleryContainer').fadeIn(1, function() {
								
					});
					//if(sorting) return;
					if(totalImages == 0) {
						//alert('sdsd');
						$('#imageLoadingMessage').hide();
						$('#galleryContainer').isotope({
						  // options
						  itemSelector : '.images',
						  layoutMode : 'masonry',
						  filter: $('#filters a:eq(1)').attr('data-filter')
						}).isotope( 'reLayout', function() {
							
							//$('#galleryContainer').isotope( {filter:'*'} );
							var t = setTimeout(callOnTimer, 1);
							
						} );
					}
				
				});
		});
		
		
		$('#filters a').click(function(){
		  var selector = $(this).attr('data-filter');
		  $('#galleryContainer').isotope({ filter: selector });
		  return false;
		});
		
		
		$('.datto_boston a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.datto a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.confidential a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.newmans a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.newmans_base a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.building_land_technology a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ge_capital_nyc a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ge_capital_norwalk_2 a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ge_capital_norwalk a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ge_capital_aviation_services a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.greenwich_american_centre a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.vineyardvines a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.mbia_insurance a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.quorum a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.citibank_ny a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.cannondale_office a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.cannondale a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.naughty a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.citibank_dc a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.richard_mitchell a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.general_atlantic a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.hudsons a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.kgibbs a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ncc a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.citibank a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.cadbury a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.antares a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.diageo a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ge_money a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ice a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ims_health_ct a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.ims_health_nj a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.lime_rock_partners a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.moab_oil a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.nestle_waters a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.one_river_road a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.philips a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.preferred_investments a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.rd_scinto a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.rd_scinto_renaissance a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.rfr_realty a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.sfaft_subway a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.sun_products_hq a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.sun_products_rd a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.tudor_investments a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.uconn a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.us_tobacco a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.vertrue a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
$('.littlejohn a').lightBox({overlayBgColor: '#FFF', overlayOpacity: 0.9});
		
	});
	
		var toggle = 'fitRows';
		function sortIso() {
			$('#galleryContainer').isotope({
						  // options
						  itemSelector : '.images',
						  layoutMode : toggle,
						});
						if(toggle == 'masonry') {
							toggle = 'fitRows';
						} else {
							toggle = 'masonry';
						}
		}
		
		function callOnTimer() {
			$('#galleryContainer').isotope( {filter:'*'} );
		}
		
		function timerIsotope() {
			var t = setTimeout('callOnTimer', 10000);
		}
		
		function triggerClickToOpenLightBox(element) {
			//if the image container is loaded then clear the interval - dinesh
			if($('#'+element).html() && ct){
				clearInterval(ct);
			}
			
			
			
			if($(window).width() > 1025)  {
				$('.' + element + ' a:first').trigger('click');
			} else {
				eval('ps_'+element+'.show(0);');
			}
		}
		
	</script>
    
    
    <script type="text/javascript">
		
		var gallery1Instance='', gallery2Instance='', options = {};
		var ps_datto_boston = '';var ps_datto = '';var ps_confidential = '';var ps_newmans = '';var ps_newmans_base = '';var ps_building_land_technology = '';var ps_ge_capital_nyc = '';var ps_ge_capital_norwalk_2 = '';var ps_ge_capital_norwalk = '';var ps_ge_capital_aviation_services = '';var ps_greenwich_american_centre = '';var ps_vineyardvines = '';var ps_mbia_insurance = '';var ps_quorum = '';var ps_citibank_ny = '';var ps_cannondale_office = '';var ps_cannondale = '';var ps_naughty = '';var ps_citibank_dc = '';var ps_richard_mitchell = '';var ps_general_atlantic = '';var ps_hudsons = '';var ps_kgibbs = '';var ps_ncc = '';var ps_citibank = '';var ps_cadbury = '';var ps_antares = '';var ps_diageo = '';var ps_ge_money = '';var ps_ice = '';var ps_ims_health_ct = '';var ps_ims_health_nj = '';var ps_lime_rock_partners = '';var ps_moab_oil = '';var ps_nestle_waters = '';var ps_one_river_road = '';var ps_philips = '';var ps_preferred_investments = '';var ps_rd_scinto = '';var ps_rd_scinto_renaissance = '';var ps_rfr_realty = '';var ps_sfaft_subway = '';var ps_sun_products_hq = '';var ps_sun_products_rd = '';var ps_tudor_investments = '';var ps_uconn = '';var ps_us_tobacco = '';var ps_vertrue = '';var ps_littlejohn = '';		
		(function(window, PhotoSwipe){
		
			document.addEventListener('DOMContentLoaded', function(){
					
					ps_datto_boston = PhotoSwipe.attach( window.document.querySelectorAll('.datto_boston a'), options ),ps_datto = PhotoSwipe.attach( window.document.querySelectorAll('.datto a'), options ),ps_confidential = PhotoSwipe.attach( window.document.querySelectorAll('.confidential a'), options ),ps_newmans = PhotoSwipe.attach( window.document.querySelectorAll('.newmans a'), options ),ps_newmans_base = PhotoSwipe.attach( window.document.querySelectorAll('.newmans_base a'), options ),ps_building_land_technology = PhotoSwipe.attach( window.document.querySelectorAll('.building_land_technology a'), options ),ps_ge_capital_nyc = PhotoSwipe.attach( window.document.querySelectorAll('.ge_capital_nyc a'), options ),ps_ge_capital_norwalk_2 = PhotoSwipe.attach( window.document.querySelectorAll('.ge_capital_norwalk_2 a'), options ),ps_ge_capital_norwalk = PhotoSwipe.attach( window.document.querySelectorAll('.ge_capital_norwalk a'), options ),ps_ge_capital_aviation_services = PhotoSwipe.attach( window.document.querySelectorAll('.ge_capital_aviation_services a'), options ),ps_greenwich_american_centre = PhotoSwipe.attach( window.document.querySelectorAll('.greenwich_american_centre a'), options ),ps_vineyardvines = PhotoSwipe.attach( window.document.querySelectorAll('.vineyardvines a'), options ),ps_mbia_insurance = PhotoSwipe.attach( window.document.querySelectorAll('.mbia_insurance a'), options ),ps_quorum = PhotoSwipe.attach( window.document.querySelectorAll('.quorum a'), options ),ps_citibank_ny = PhotoSwipe.attach( window.document.querySelectorAll('.citibank_ny a'), options ),ps_cannondale_office = PhotoSwipe.attach( window.document.querySelectorAll('.cannondale_office a'), options ),ps_cannondale = PhotoSwipe.attach( window.document.querySelectorAll('.cannondale a'), options ),ps_naughty = PhotoSwipe.attach( window.document.querySelectorAll('.naughty a'), options ),ps_citibank_dc = PhotoSwipe.attach( window.document.querySelectorAll('.citibank_dc a'), options ),ps_richard_mitchell = PhotoSwipe.attach( window.document.querySelectorAll('.richard_mitchell a'), options ),ps_general_atlantic = PhotoSwipe.attach( window.document.querySelectorAll('.general_atlantic a'), options ),ps_hudsons = PhotoSwipe.attach( window.document.querySelectorAll('.hudsons a'), options ),ps_kgibbs = PhotoSwipe.attach( window.document.querySelectorAll('.kgibbs a'), options ),ps_ncc = PhotoSwipe.attach( window.document.querySelectorAll('.ncc a'), options ),ps_citibank = PhotoSwipe.attach( window.document.querySelectorAll('.citibank a'), options ),ps_cadbury = PhotoSwipe.attach( window.document.querySelectorAll('.cadbury a'), options ),ps_antares = PhotoSwipe.attach( window.document.querySelectorAll('.antares a'), options ),ps_diageo = PhotoSwipe.attach( window.document.querySelectorAll('.diageo a'), options ),ps_ge_money = PhotoSwipe.attach( window.document.querySelectorAll('.ge_money a'), options ),ps_ice = PhotoSwipe.attach( window.document.querySelectorAll('.ice a'), options ),ps_ims_health_ct = PhotoSwipe.attach( window.document.querySelectorAll('.ims_health_ct a'), options ),ps_ims_health_nj = PhotoSwipe.attach( window.document.querySelectorAll('.ims_health_nj a'), options ),ps_lime_rock_partners = PhotoSwipe.attach( window.document.querySelectorAll('.lime_rock_partners a'), options ),ps_moab_oil = PhotoSwipe.attach( window.document.querySelectorAll('.moab_oil a'), options ),ps_nestle_waters = PhotoSwipe.attach( window.document.querySelectorAll('.nestle_waters a'), options ),ps_one_river_road = PhotoSwipe.attach( window.document.querySelectorAll('.one_river_road a'), options ),ps_philips = PhotoSwipe.attach( window.document.querySelectorAll('.philips a'), options ),ps_preferred_investments = PhotoSwipe.attach( window.document.querySelectorAll('.preferred_investments a'), options ),ps_rd_scinto = PhotoSwipe.attach( window.document.querySelectorAll('.rd_scinto a'), options ),ps_rd_scinto_renaissance = PhotoSwipe.attach( window.document.querySelectorAll('.rd_scinto_renaissance a'), options ),ps_rfr_realty = PhotoSwipe.attach( window.document.querySelectorAll('.rfr_realty a'), options ),ps_sfaft_subway = PhotoSwipe.attach( window.document.querySelectorAll('.sfaft_subway a'), options ),ps_sun_products_hq = PhotoSwipe.attach( window.document.querySelectorAll('.sun_products_hq a'), options ),ps_sun_products_rd = PhotoSwipe.attach( window.document.querySelectorAll('.sun_products_rd a'), options ),ps_tudor_investments = PhotoSwipe.attach( window.document.querySelectorAll('.tudor_investments a'), options ),ps_uconn = PhotoSwipe.attach( window.document.querySelectorAll('.uconn a'), options ),ps_us_tobacco = PhotoSwipe.attach( window.document.querySelectorAll('.us_tobacco a'), options ),ps_vertrue = PhotoSwipe.attach( window.document.querySelectorAll('.vertrue a'), options ),ps_littlejohn = PhotoSwipe.attach( window.document.querySelectorAll('.littlejohn a'), options );			
			}, false);
			
			
		}(window, window.Code.PhotoSwipe));
		
		
		
		
		
</script>