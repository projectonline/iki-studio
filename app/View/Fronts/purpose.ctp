<div id="mainWrapper">
  	<?php echo $this->element('front/header') ?>
  	<div id="titleBar">
	  <p>&nbsp;</p>
	</div>

	<div id="mainContainer">
		<div id="contentLeftPanel">
			<ul class="contentNavigation">
				
              
                <li class="currentMenuHighLighter"><a href="#">Sustainability</a></li>
                <li><a href="#">Giving Back</a></li>
                   <li><a href="#">Circling Back &#8211; Revisiting Past Planning Concepts</a></li>
              
			</ul>
  </div>
  
  
		
		<div id="contentRightPanel" style="position:relative">
        	<p id="goBack">
                    <a style="background:#DD092F; text-decoration:none; font-weight:bold; color:#FFFFFF; padding:15px" href="javascript:" onclick="goBack()">Menu</a>
             </p>
        
        
          
          <!-- tab2 starts here -->
          	<div class="contentText">
		    <p><strong>CPG's Approach to Sustainability</strong></p>
<p>With almost every new project comes the discussion of how best to incorporate green building practices.  When we talk to our clients about the process of LEED and other programs we don't initially talk about the points needed to get the level of certification you want to hit &#8211; rather we ask about the points you want to make to your employees to provide the healthiest environment possible.  It really comes down to &quot;what shade of green&quot; you want to be and the type of environment you want to provide.  So whether our client is the landlord or the tenant our method and message are the same.<br />
  CPG does, as part of the basic services, assist in the analysis and evaluation process of determining the cost and intrinsic values associated with implementing a LEED-certified project. The first step is to always meet with our client to understand guiding factors and goals: </p>
<ul>
  <li>  If our client is the landlord we need to understand the improvements they desire to make to the base building infrastructure.  Improvements may be market-driven, or for environmental or cost-saving purposes.  It has been shown that green buildings command greater rents and sale prices.</li>
  <li>If our client is the tenant we want to support their enthusiasm for enhancing their workplace in the direction of sustainability and healthy environments.  Improvements may be implemented to improve air quality, lighting, thermal comfort, and water conservation.  As a result, such business environments improve productivity and reduce employee sickness and absenteeism.<br />
</li>
</ul>
  After analyzing the project conditions, CPG develops a &quot;scorecard&quot; reflecting all the credits sought to attain the optimum level using the appropriate LEED ratings system. In addition, the related costs associated with these credits will be examined to identify which credits can be achieved at reasonable expense.  CPG will coordinate its efforts with the project team and the landlord. <br />
    <br />
    It should be noted that CPG includes many LEED-required elements in our projects as a standard office policy.  These include low-VOC paints and adhesives, Greenguard-certified furniture systems, using recycled content in building materials, water-saving fixtures, energy efficient lighting and switching, recycling and construction-waste management to name a few.<!-- IMAGE PER PAGE -->
<p><img src="/img/9.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/></p>

		  </div>          
          
          <!-- tab3 starts here -->
                    <div class="contentText">
  <p><strong>Giving Back</strong></p>
  <p>We wish we could do what Gates, Buffet and others are doing, giving away billions.  That is just not in the cards for us, but what we can do is the following:</p>
  <ul>
    <li>    We will continue to donate our planning and design services to organizations that only count on their fundraising to support their beneficiaries.  We are so proud of the groups, listed below, that we have helped over the years.</li>
    <li> We will continue to help those in our Architectural and Interior Design profession that are looking for work.  Our program is designed to help individuals, to help the profession and finally to help the community. </li>
  </ul>
  <p class="givingBack"><img style="display:block; width:100%" src="images/giving_logos.jpg" alt="all logos" /></p>
  <p>&nbsp;</p>
<p><img src="/img/4.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/></p>
                
		  </div>          
          
           <!-- tab3 starts here -->
                    <div class="contentText">
  <p><strong>Circling Back - Revisiting Past Planning Concepts</strong><br />
    <br />
    The planning of office space continues to change to align with advances in technology and work patters desired by the newest generation of knowledge workers.  Companies call their &quot;new planning initiatives&quot;  by many names such as &quot;Workplace Strategies, new Ways of Working (WOW), Agile Working, Smart Working and Work Differently.  The common denominators are similar with the primary focus in most cases being saving space and second, creating flexible work areas to support MANY ways of working.<br />
    <br />
    Planning has almost &quot;circled back&quot; to the concepts used by Frank Lloyd Wright in designing the Johnson Wax Building and other office towers.  Those open desking solutions were possible primarily because the only technology that needed to be supplied to the desk was the telephone and an electric cord.  Calculators were manual &#8211;operated with a crank handle.  With the improvements in our latest &quot;smart devices&quot; the only thing a knowledge worker needs available to be affective all day is a power station. <br />
    <br />
  The growth of mobility enables companies to revisit their real estate strategies and realign the amount of space they occupy with the use patterns of the staff assigned to the facility.  We have implemented AGILE WORKING principles in many of our projects over the past five years.  Some of our clients focus is on space saving while others take advantage of new work patterns by:  </p>
  <ul>
    <li> Changing the ratio of offices to workstations</li>
    <li>    	Reducing the size of workstations while adding special use rooms for hands-down focus work</li>
    <li>    	Introducing video conference rooms and video booths to reduce travel &#8211; while maintaining collaborative opportunities</li>
  </ul>
  <p>    While our designs for Diageo, GE, Nestle, Gartner, IMS and many others have approached the subject of &quot;smart working&quot; in different ways the underlining goals are finding ways to provide flexibility for the mobile worker, finding ways for them to stay connected with others and finding ways to improve their products and services.<br />
    <br />
  Our goal in designing these spaces is to plan the appropriate amount of flexibility to support many ways of working.</p>
  <p>&nbsp;</p>
<p>&nbsp;</p>
<p><img src="/img/6.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/></p>
                
		  </div>          
          
          
		</div>
        
	</div>

</div>


<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/2183733.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->
</body>
</html>



<script language="javascript1.2">

	$(document).ready(function(e) {
		var c = 0;
		$('.contentNavigation a').click(function() {
			//alert($(this).index('.contentNavigation a'));
			var index = $(this).index('.contentNavigation a')
			openLink(index);
			
			//if About CPG clicked
			if(index == 1) {
				c++;
				//show About CPG's sub-menu
				//$('.about_submenu').css('display','block');
				if(c == 1) {	//for first time animate and show the sub menu
					$('.about_submenu').animate({
						height: 'toggle'
					}, 500);
				}
			} else {
				if($(this).parent().parent().hasClass('about_submenu')){
					//if sub menu items is clicked then show About CPG's sub-menu
					$('.about_submenu').css('display','block');
				} else {
					//if other than About CPG and its sub menu items is clicked then hide About CPG's sub-menu
					//$('.about_submenu').css('display','none');
					if($('.about_submenu').is(':visible')) {
						$('.about_submenu').animate({
							height: 'toggle'
						}, 500);
					}
					c = 0;	//reset and hide the sub menu
				}
			}
			
		});
		$('.contentNavigation a').attr('href', 'javascript:');
		openLink(0);
		goBack();
	});
	
	function openLink(index) {
		
		$('#contentRightPanel .contentText').hide();
		
		$('.contentNavigation a').css('background-color', '#FFF');
		$('.contentNavigation a').css('color', '#777');
		$('.contentNavigation a:eq(' + index + ')').css('background-color', '#DD092F');
		$('.contentNavigation a:eq(' + index + ')').css('color', '#FFF');
		$('#contentRightPanel .contentText:eq(' + index + ')').fadeIn(1500, function() {
				$(this).siblings('a').hide();
		});

		$('#contentRightPanel').css('z-index', 502);
		if(index == $('.contentNavigation a').length-1) {
			 /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
            var disqus_shortname = 'cpgarchitects'; // required: replace example with your forum shortname

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();	
		}
	}

	function goBack() {
		$('#contentRightPanel').css('z-index', 500);
	}


</script>

<style type="text/css">
		.topMenuHighLighter4{
			background: #BBB;
			padding:3px 10px 5px !important; 
		}
		#sliderBar{display:none}
		.about_submenu{display:none}
		.about_submenu a{font-size:12px !important}
		
</style>
