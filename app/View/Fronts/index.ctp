<div id="mainWrapper">
	<?php echo $this->element('front/header') ?>
	<script src="/js/jquery.easing.1.3.js" type="text/javascript"></script>
	<script src="/js/scripts_3.js" type="text/javascript"></script>
	<script src="/js/jquery.zaccordion.js" type="text/javascript"></script>
	
	<div id="example3"><ul></ul></div>
	<style type="text/css">
	#homeIntro{position:relative; width:100%; border:0px dotted #0000FF; overflow:hidden; height:500px; left:0}
	#belt{position:absolute; left:0; top:0; width:}
	#homeIntro .images{position:absolute; left:0; top:0; overflow:hidden; width:500px; height:500px}
	#homeIntro .images .captions{position:absolute; width:100%; background:#FFFFFF; color:#333333; height:100px; top:10px; left:0; z-index:30; opacity:0.4; filter:alpha(opacity=40);}
	#homeIntro .images .captions .ctext{padding:10px 5px; font-size:24px; font-weight:bold}
	#homeIntro .images .captions .clinks{padding:5px; font-size:16px}
	#homeIntro .images img{position:absolute; top:0; left:0; z-index:29}
	
	#goNext a{background:#FFFFFF; color:#333333; position:fixed; top:275px; right:10px; z-index:35; padding:10px; font-size:24px; opacity:0.7; filter:alpha(opacity=70); text-decoration:none}
	
	#goPrev a{background:#FFFFFF; color:#333333; position:fixed; top:275px; left:10px; z-index:35; padding:10px; font-size:24px; opacity:0.7; filter:alpha(opacity=70); text-decoration:none}
	</style>
    
    <div id="goNext">
    	<a href="javascript:" id="clickNext">Next</a>
    </div>
    
    <div id="goPrev">
    	<a href="javascript:" id="clickPrev">Prev</a>
    </div>
    
    <div id="homeIntro">
    	<div id="belt">
        	
            <div class="images">
                <div class="captions">
                    <p class="ctext">Hudson Bay Trading Company</p>
                    <p class="clinks"><a href="gallery.php?p=hudsons">Click to view</a></p>
                </div>
                
                <img src="/img/3.jpg" />
            </div>
            
            <div class="images">
                <div class="captions">
                    <p class="ctext">Nestle Waters, NA</p>
                    <p class="clinks"><a href="gallery.php?p=nestle_waters">Click to view</a></p>
                </div>
                
                <img src="/img/4.jpg" />
            </div>
        
            <div class="images">
                <div class="captions">
                    <p class="ctext">Lime Rock Partners!</p>
                    <p class="clinks"><a href="gallery.php?p=lime_rock_partners">Click to view</a></p>
                </div>
                
                <img src="/img/1.jpg" />
            </div>
            
            <div class="images">
                <div class="captions">
                    <p class="ctext">General Atlantic</p>
                    <p class="clinks"><a href="gallery.php?p=general_atlantic">Click to view</a></p>
                </div>
                
                <img src="/img/5.jpg" />
            </div>
            
            <div class="images">
                <div class="captions">
                    <p class="ctext">Moab Oil</p>
                    <p class="clinks"><a href="gallery.php?p=moab_oil">Click to view</a></p>
                </div>
                
                <img src="/img/2.jpg" />
            </div>
            
            <div class="images">
                <div class="captions">
                    <p class="ctext">Diageo</p>
                    <p class="clinks"><a href="gallery.php?p=diageo">Click to view</a></p>
                </div>
                
                <img src="/img/3.jpg" />
            </div>
            
        </div>
    </div>
    
	<div id="mainContainer">
		<div id="leftPanel"></div>
	</div><br>
	<div align="left">
	<h1 style="font-size:24px; line-height:24px; color:#aaaaab">Architectural & Interior Design Services</h1>
	<p style="font-size:14px; line-height:28px; color:#aaaaab"  >CPG Architects specializes in chic, contemporary and comfortable designs that truly represent you and your business.  Corporate, retail and residential clients trust our expert designers and technical teams to transform living and working environments into pleasing, elegant, modern displays.<br><br>
    Our corporate architecture design firm in Stamford, Connecticut, combines the latest styles, top-quality materials and innovative structures to give you truly unique and impressive spaces. Our team is comprised of licensed architects, design professionals, and project managers, all who bring years of experience and individualized knowledge to clients and projects. Combining all of our familiarity and capabilities creates a goal-oriented synergy between our staff and you, the client.  We work with your space and turn the needs, image and desires of your company into a new, vibrant environment that clients, employees and business leaders all enjoy working in.  Our design teams optimize the flow of the room and create clean, cohesive decor that welcomes clients and creates lasting first impressions.  Each space is designed to maximize efficiency as well as comfort and class.<br><br> 
    We work with you from design to implementation to assure every detail fulfills your vision and specifications.  Read on or see our gallery of work to find out more about our designs, process and services.  Contact CPG Architects to begin transforming your office. </i></font><br><br><br><br><br><br><br><br><br><br><br></div>
</div>

<script language="javascript1.2">
	var url = '/home.xml';
	var position = 0;
	var increment = 500;
	$(document).ready(function() {
		initHomeCarouselHover();
		
		$('.fadein img:gt(0)').hide();
		setInterval(function(){
		  $('.fadein :first-child').fadeOut(5000)
			 .next('img').fadeIn(5000)
			 .end().appendTo('.fadein');}, 
		  5000);
		  
		ajaxCallXml(url);
		
		
		$('#belt .images').each(function() {
				$(this).css('left', position+'px');
				position += increment;
		});
		
		
		
	});
	
	function initHomeCarouselHover() {
		$('.slides li').mouseover(function() {
			
			$(this).find('p.text').show();
			$(this).find('p.cover').show();
			$(this).find('p.cover').stop().animate({top:'177px'});
		});
		$('.slides li').mouseout(function() {
			$(this).find('p.text').hide();
			$(this).find('p.cover').stop().animate({top:'288px'});
		});
	}
	
	$('#clickNext').click(function()  {
		console.log(parseInt($('#belt').css('left')) + ' - ' + (increment * ($('#belt .images').length - 2)));
		if(parseInt($('#belt').css('left')) < (increment * ($('#belt .images').length - 2))) {
			var leftPos = (parseInt($('#belt').css('left')) - increment) + 'px';
			$('#belt').animate({'left':leftPos});
		}
	});
	
	$('#clickPrev').click(function()  {
		if(parseInt($('#belt').css('left')) < 0) {
			var leftPos = (parseInt($('#belt').css('left')) + increment) + 'px';
			$('#belt').animate({'left':leftPos});
		}
	});
	
</script>

<style type="text/css">
.topMenuHighLighter1{
		background: #BBB;
		padding:3px 10px 5px !important; 
	}
	#sliderBar{display:none}

</style>