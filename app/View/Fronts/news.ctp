<div id="mainWrapper">
  <?php echo $this->element('front/header') ?>
  <div id="titleBar">
	  <p>&nbsp;</p>
	</div>

	<div id="mainContainer">
		<div id="contentLeftPanel">
			<ul class="contentNavigation">
				

<li class="currentMenuHighLighter"><a href="#">At the Connecticut Food Bank </a></li>
                <li><a href="#">Gary Unger Passes the Baton</a></li>
              
                <li ><a href="#">AIA Honor Award, 2015</a></li>
                
                   <li><a href="#">Spotlight: Vineyard Vines</a></li>
              
			</ul>
  </div>
  
  
		
		<div id="contentRightPanel" style="position:relative">
        	<p id="goBack">
                    <a style="background:#DD092F; text-decoration:none; font-weight:bold; color:#FFFFFF; padding:15px" href="javascript:" onclick="goBack()">Menu</a>
             </p>
        
        
            <!-- tab2 starts here -->
            
             <div class="contentText">

<p class="givingBack"><img class="showinmobile" src="/img/canstruction.jpg" width="639" height="350" /></p>
		    <p ><strong>Giving back and having a BLAST doing it!</strong><br>

<br>

A few CPG’ers spent the day today at the Connecticut Food Bank with Academy of Information Technology & Engineering students, building a giant structure out of cans for the AIA Connecticut Canstruction Event! <br>

<br>

This has been two months in the making, with CPG acting as mentors to help the students along the way. <br>
<br>

Our team, comprised of 7 students,held a food drive at their school to stock pile all the cans needed to build their structure, which ended up using over 2,000 cans!
They came up with the idea, which was a Food Bank built out of cans, and we provided a sketch up to help them visualize how it would all go together and how many cans they would actually need.<br>
 
<br>

Out of the 5 teams that were entered, ours walked away with “Best Meal” for the variety of cans that were used AND all of the cans used in the event are going to be donated to the Connecticut Food Bank! Way to go team!<br>

<br>

Thanks to Lorenzo Leone and Chelsea Dimpflmaier for representing CPG Architects! For more pictures see our <a href="https://www.facebook.com/CPGArchitects/">FaceBook page.</a><br>

		    </p>
		    <p>&nbsp;</p>
</div>        
        <div class="contentText">
		    <p ><strong>Gary Unger On Passing the Baton at CPG Architects</strong><br>
		      <br>
		      &quot;This week I finalized a plan to pass the baton to three 20+ year employees to run CPG Architects.<br>
              <br>
              If you have ever run a relay you know that passing a baton to another person(s) is not easy. It's all about timing and positioning. Done perfectly passing the baton can actually allow the team to leap forward. That's what we plan to do with our new management team - make a big leap forward.<br>
              <br>
              After 35 years of running CPG, I decided the timing was right to make a change in day-to-day management. For me, the right time to make the change was less when I wanted to transition, and more when others were ready to take over. It is extremely important for all staff, clients and consultants to understand that the new team has all the knowledge and resources necessary to maintain and grow the firm's positive creative momentum.<br>
              <br>
              Determining the best way to transition was much more difficult than I thought. Over the years I looked at buying other firms, selling the company and even merging with others. In many cases the options were ok for me, but terrible for others in the firm. I have always said the only reason we did so well was because of the efforts of others and so the decision to hand off the firm, lock stock and barrel, was an easy one.<br>
              <br>
              Karen Lika, Sue Wall and Jenny Paik have each been successful in marketing, project management and financial control for over 20 years with the company and are the logical ones to take CPG to a new level. Most important, this change creates opportunities for existing staff to move up. It is very tough to find quality people and CPG has been fortunate. Of our 42 staff, 4 have been with the firm for over 30 years, 9 over 20 years and 8 over 10 years. <br>
              <br>
              I wish Karen, Sue, Jenny and the entire CPG team my best to continue delivering the most creative professional services.&quot;<br>
              <br>
		    </p>
		    <p>&nbsp;</p>
</div>        
        <!-- tab3 starts here -->
        
        <div class="contentText">
		    <p class="givingBack"><img src="/img/aia.jpg" width="650" height="350" /></p>
		    <p>CPG Wins Honor Award, From AIA CT Business Architecture 2015</p>
		    <p> The Business Architecture Awards statewide award honors architects for solving business problems for Connecticut clients, thereby demonstrating the power of architecture to shape business performance, to improve peoples' lives and provide a value added service to clients in a business setting that far exceeds the costs of that service. Projects may include non-profit businesses. The award acknowledges architects and their clients whose projects enhance the built environment and achieve business goals.</p>
		    <p>Honor Award, Over 50 Employees Dorel Sports, Wilton, CT</p>
		    <p>Jury comments: <em>The project  looks like its story; it merges both the staff and visitor experience and meets  all the corporate strategic goals. The design supports the firm’s branding and  gets out the message of what it does. Interesting and well executed, it shows  that you can have fun doing a project.</em></p>
<p><a href="http://aiact.org/awards-program/aia-connecticut-business-architecture-awards/business-architecture-2015-awards/">Click here to visit the AIA Awards website.</a> | <a href="http://cpgarch.com/gallery.php?p=cannondale_office">Click here to view photos in our gallery.</a></p>
		    <p>&nbsp;</p>
</div>        
        <!-- tab34starts here -->
        
        <div class="contentText">
		    <p class="givingBack"><a href="http://www.bloomberg.com/news/videos/2015-05-06/inside-the-preppiest-office-in-america"><img src="/img/vines.jpg" width="650" height="350" border="0" /></a></p>
		    <p>Vineyard Vines has made its name as an alternative to those aging labels. The growing company, known for its nautical and casual style, has just moved into a new 90,000-square-foot headquarters in Stamford, Connecticut, designed by CPG Architects.</p>
            <p><a href="http://www.bloomberg.com/news/videos/2015-05-06/inside-the-preppiest-office-in-america">Click here to visit Bloomberg.com news feature</a>. | <a href="http://cpgarch.com/gallery.php?p=vineyardvines">Click here to view photos in our gallery.</a></p>
</div>        
        
 </div>
        
	</div>

</div>


<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/2183733.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->
</body>
</html>



<script language="javascript1.2">

	$(document).ready(function(e) {
		var c = 0;
		$('.contentNavigation a').click(function() {
			//alert($(this).index('.contentNavigation a'));
			var index = $(this).index('.contentNavigation a')
			openLink(index);
			
			//if About CPG clicked
			if(index == 1) {
				c++;
				//show About CPG's sub-menu
				//$('.about_submenu').css('display','block');
				if(c == 1) {	//for first time animate and show the sub menu
					$('.about_submenu').animate({
						height: 'toggle'
					}, 500);
				}
			} else {
				if($(this).parent().parent().hasClass('about_submenu')){
					//if sub menu items is clicked then show About CPG's sub-menu
					$('.about_submenu').css('display','block');
				} else {
					//if other than About CPG and its sub menu items is clicked then hide About CPG's sub-menu
					//$('.about_submenu').css('display','none');
					if($('.about_submenu').is(':visible')) {
						$('.about_submenu').animate({
							height: 'toggle'
						}, 500);
					}
					c = 0;	//reset and hide the sub menu
				}
			}
			
		});
		$('.contentNavigation a').attr('href', 'javascript:');
		openLink(0);
		goBack();
	});
	
	function openLink(index) {
		
		$('#contentRightPanel .contentText').hide();
		
		$('.contentNavigation a').css('background-color', '#FFF');
		$('.contentNavigation a').css('color', '#777');
		$('.contentNavigation a:eq(' + index + ')').css('background-color', '#DD092F');
		$('.contentNavigation a:eq(' + index + ')').css('color', '#FFF');
		$('#contentRightPanel .contentText:eq(' + index + ')').fadeIn(1500, function() {
				$(this).siblings('a').hide();
		});

		$('#contentRightPanel').css('z-index', 502);
		if(index == $('.contentNavigation a').length-1) {
			 /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
            var disqus_shortname = 'cpgarchitects'; // required: replace example with your forum shortname

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();	
		}
	}

	function goBack() {
		$('#contentRightPanel').css('z-index', 500);
	}


</script>

<style type="text/css">
		.topMenuHighLighter8{
			background: #BBB;
			padding:3px 10px 5px !important; 
		}
		#sliderBar{display:none}
		.about_submenu{display:none}
		.about_submenu a{font-size:12px !important}
		
</style>
