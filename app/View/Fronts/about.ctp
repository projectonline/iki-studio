<div id="mainWrapper">
  <?php echo $this->element('front/header') ?>

  <div id="titleBar">
	  <p>&nbsp;</p>
	</div>

	<div id="mainContainer">
    
		<div id="contentLeftPanel">
			<ul class="contentNavigation">
				<li class="currentMenuHighLighter"><a href="#">It's All About Possibilities</a></li>
                <li><a href="#">About CPG</a></li>
                <li><a href="#">Leadership</a></li>
                                <li><a href="#">Services</a></li>
                                 <li><a href="#">Experience</a></li>
                <li><a href="#">Process</a></li>
                <li><a href="#">Representative Client List</a></li>
                <li><a href="#">Awards</a></li>
               	<li><a href="#">Careers</a></li>
			</ul> 
            
           
  		</div>
        
		 
         
		<div id="contentRightPanel" style="">
            <p id="goBack">
                    <a style="background:#DD092F; text-decoration:none; font-weight:bold; color:#FFFFFF; padding:15px" href="javascript:" onclick="goBack()">Menu</a>
             </p>
		<!-- tab1 starts her
        e -->
					<div class="contentText">
			  <p><strong>Possibilities</strong></p>
			  <p class="quote"><em>&#8220;All great architecture is the design of space that contains, cuddles, exalts, or stimulates the persons in that space.&#8221;&ndash; Philip Johnson</em></p>
			  <hr />
			  <p>For more than 30 years, CPG Architects has been helping organizations focus on that 'exalting' element of the work environment.</p>
<p>Through our unique, future-focused process and our comprehensive array of services, we create work environments that minimize distraction while maximizing comfort and creativity. Our greatest enjoyment is to design environments that people love to work in.</p>
			  <p>By taking the time to listen and fully understand our clients' needs, our team is able to integrate business, real estate, and facility management strategies while adding value at every step of the process.</p>
			  <p>So come on in. Get comfortable. And learn how CPG can help your firm exalt.</p>
			  	
                <!-- IMAGE PER PAGE -->
              <p><img src="/img/5.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/></p>
                
		  </div>          
		<!-- tab2 starts here -->
          	<div class="contentText">
			  <p><strong>About CPG</strong></p>
			  <p class="quote"><em>&#8220;The brain is an organ that starts working the moment you get up in the morning and does not stop until you get to the office.&#8221;&ndash; Robert Frost</em></p>
        <hr />
			  <p>In a way, we're in the business of keeping brains working&hellip;while working. At CPG, we do that by creating environments that foster comfort, productivity, and innovation. Every project reflects our commitment to:</p>
	        <ul>
        <li> Identifying and delivering the most value to our clients</li>
		        <li>Providing leading-edge resources our employees need to deliver		          that value</li>
		        <li>Creating the right design environment to achieve dynamic results</li>
	        </ul>
<p>It's not always easy to live up to that commitment. But we've been doing it for our clients <strong>since 1979</strong> &mdash;thanks to the dedication, hard work, and some of the best and creative, well, brains in architecture and design.              
			  	
              <!-- IMAGE PER PAGE -->
              <p><img src="/img/8.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/></p>
                
		  </div>          
		<!-- tab3 starts here -->
          	<div class="contentText">
			   <p><strong>Leadership</strong></p>
               <p class="quote"><em>&#8220;I don't want yes-men, I want people to always tell me the truth&mdash;even if it costs them their jobs.&#8221;&ndash; Samuel Goldwyn</em></p>
               <hr />
               <p>CPG's strength lies in its diverse team of highly qualified architects and interior designers who&mdash;unlike Mr. Goldwyn's ideal employees&mdash;are eager to &quot;push the envelope&quot; if a new idea or a different approach is in the client's best interests. </p>
               <p>CPG is a dynamic and growing 45+ person regional architecture, interiors, and planning firm with offices in Stamford, CT and Scottsdale AZ.</p>
               <p>Although our clients are mostly corporate, we also work on a number of retail, educational and heathcare assignments.</p>
               <p>Our success has so much to do with the fact that 17% of our staff has been with the company over 20 years, 7% over 15 years and 15% over 10 years and 39% over 5 years.  Of our current staff, 12% returned to CPG after a hiatus.</p>


<!-- staffs block starts here -->
             
<div id="staffs">
                
                	<div class="staff">
                    	<img src="/img/gary.jpg"  />
                        <div class="shortDesc">
           	     		  <h3><strong>Gary Allen Unger</strong><br />
            	     		    <em>Founder</em></h3>
                            <a href="javascript:" class="readMore">Bio</a> >
                      </div>
                        <div class="longDesc">	<img style="float:left; width:10%" src="/img/gary.jpg"  alt="Gary"   >
                        	<p><strong>Gary Allen Unger</strong></p>
                            
                        	<p>Gary founded CPG Architects in November of 1979.</p>
                       	  <p>Gary's focus is on the organization and procedures that position CPG to provide the most sophisticated, most cost-effective architectural/ interior design services.  He continuously challenges and enhances the company's approach to design and development in order to address this issue.  CPG's life cycle tracking and on-line project management services are but two examples of Gary's commitment to superior client service.</p>
                        	<p>Gary began his career at two architectural firms in New York City.  He then spent seven years at American Express, where he managed the design of the headquarters building in New York and, for five additional years, managed the design of all American Express facilities worldwide &#8211;3500 offices in 75 countries.</p>
                        	<p>In 1979, Gary established CPG Architects with the commitment to deliver the best of design, service and quality to his clients.  Since its inception, CPG has kept that commitment on over 6000 projects and a steadily growing list of clientele. </p>
                            
                            <!-- IMAGE PER PAGE -->
            
                        </div>
                    </div>
                    
                    
                    <!-- staff 2 -->
                    
                    <div class="staff">
                    	<img src="/img/karen.jpg"  />
                        <div class="shortDesc">
               	     		<h3><strong>Karen M. Lika</strong><br />
       	     		        <em>Principal</em></h3>
                            <a href="javascript:" class="readMore">Bio</a> >
                      </div>
                        <div class="longDesc">	
                        	<img style="float:left; width:10%" src="/img/karen.jpg"  alt="Karen"   ><p><strong>Karen M. Lika</strong></p>
                        	<p>Principal</p>
                            <p>Karen discovered early of her passion for design.  Developing as a designer, she realized how her innate interest in people from all walks of life affords her a wide and vital perspective on the design process.</p>
                            <p>Over 25 years, Karen's skill as a designer has been augmented by her ability in developing and nurturing relationships.  Striving for a rich level of communication and understanding of a client's real needs and concerns always guides her approach to the entire assignment.  Often she takes into account pertinent issues beyond the project at hand.</p>
                            <p>Now as a principal of a well regarded, dynamic and experienced firm, Karen seamlessly melds her approach to the client world with the inner realm of her dedicated and creative staff.  Karen's keen insight keeps the thousands of details and dozens of project team members aligned with the final destination.  Karen relishes a challenge and usually succeeds, garnering many kudos and sincere appreciation.</p>
                            <p>Karen is a wellness and fitness devotee and volunteers her time to mentor adolescent girls in crisis through Project Return in Westport, CT. </p>
                        </div>
                    </div>
                    
                    
                    
                     <!-- staff 3 -->
                    
	  <div class="staff">
                    	<img src="/img/jenny.jpg"  />
                        <div class="shortDesc">
               	     		<h3><strong>Jenny Paik, LEED AP</strong><br />
       	     		        <em>Principal</em></h3>
                            <a href="javascript:" class="readMore">Bio</a> >
        				</div>
                        <div class="longDesc">	
                        	<img style="float:left; width:10%" src="/img/jenny.jpg"  alt="Jenny"   ><p><strong>Jenny Paik, LEED AP</strong></p>
                        	<p>Principal</p>
                            <p>Jenny fine-tuned her natural savoir-faire in interior design during thirty years of experience in the field.  Prior to joining CPG she was with Skidmore Owings &amp; Merrill, their NYC office.  Today in her role as a Principal, Jenny continues to focus on all aspects of a project, from preliminary planning through the execution of fine detail.  Jenny is in daily contact with clients throughout each phase of the project.  Her hands-on approach makes certain the project goals become reality and the desired outcome is achieved within budget and schedule.</p>
                            <p>Her passion in design and extensive knowledge in managing have resulted in many happy clients. Jenny values and takes great pride in her many long term and repeat clients, and the rewards from her dedication and involved approach result in win-win outcomes.</p>
        </div>
                    </div>
                    
                    
                     <!-- staff 4 -->
                    
                     <div class="staff">
                    	<img src="/img/carl.jpg"  />
                        <div class="shortDesc">
                       	  <h3><strong>Carl Mirbach</strong><br />
                   	      <span><em>Creative Director</em></span></h3>
                            <a href="javascript:" class="readMore">Bio</a> >
                       </div>
                        <div class="longDesc">	
                        <img style="float:left; width:10%" src="/img/carl.jpg"  alt="Carl"   >
                        	<p><strong>Carl Mirbach</strong></p>
                        	<p>Creative Director</p>
                            <p>Carl has spent over thirty five years directing the design of corporate interiors and providing leadership within the New York Metropolitan Interior Design community.  He is an open-minded communicator who seeks to find innovative design solutions inspired by each client's unique criteria, goals and culture.  Carl is driven by his belief that design affects both the bottom line and quality of life.  He works to uncover and express the &quot;Executive Mandate&quot;, to create and present the project vision, and to keep the team focused on its realization during the project's development and implementation. </p>
                        </div>
           	      </div>
                    
                    
                    
                     <!-- staff 5 -->
                    
<div class="staff">
                    	<img src="/img/mikey.jpg"  />
                        <div class="shortDesc">
                        	<h3><strong>Michael Mallardi, AIA</strong><br />
                   	        <em>VP of Architecture</em></h3>
                            <a href="javascript:" class="readMore">Bio</a> >
    </div>
                        <div class="longDesc">	
                        <img style="float:left; width:10%" src="/img/mikey.jpg"  alt="Mickey"   >
                        	<p><strong>Michael Mallardi, AIA</strong></p>
                        	<p>VP of Architecture</p>
                          <p>Mickey favors the clearest, least complicated forms that express the spirit and character and achieves the practical needs of the client.  He vigorously advocates learning about the client's culture and vision and establishing specific goals and objectives about people, money, time and quality before undertaking design.  He enjoys generating and testing concepts, by interacting with the client and the whole project team, then developing the chosen concepts into carefully crafted and coordinated design.  Three decades at CPG, Mickey is equally at home with very small and very large projects and especially savors fashioning the details that make a project unique.  He is grateful for the opportunity to help translate dreams and imagination into reality.  And he is still learning about what makes a good architect.</p>
                            <p>Mickey also devotes time to performing as a guitarist, singer-songwriter and actor and volunteering time to help preschool kids learn about music.</p>
    </div>
                    	
                  </div>
                    
                        <!-- staff 6 -->
                    
		  <div class="staff">
                    	<img src="/img/jim.jpg"  />
                        <div class="shortDesc">
                        	<h3><strong>James Sackett, AIA</strong><br />
                   	        <em>VP, Director of  Design</em></h3>
                            <a href="javascript:" class="readMore">Bio</a> >
            </div>
                        <div class="longDesc">	
                        <img style="float:left; width:10%" src="/img/jim.jpg"  alt="James"   >
                        	<p><strong>James R. Sackett, AIA</strong></p>
                        	<p>VP, Director of Design</p>
                          <p>Jim's professional talent is in design and management of complex commercial projects.  During his tenure at CPG, he has been key in helping large companies redesign existing space.</p>
                            <p>Jim cut his teeth in NYC with over 8 years of experience in corporate interior work primarily in the banking and advertising industries.  For 10 years prior to joining CPG, Jim was principal of his own firm and developed a broad range of experience in commercial building renovations, high end residential, industrial, and retail.  At CPG, Jim's charge is the coordination of CPG's design effort.  His challenge is to raise the design bar while respecting the client's program, image and budget objectives – to interface design objectives with construction detailing, finishes and specifications.  Jim is one of our in-house gurus regarding building codes and accessibility as well as preparation and coordination of specification manuals, outlining specifics, leaving little or no room for chance. </p>
                        </div>
                  </div>
                <!-- staffs block ends here -->
                
                
		  </div>
          
 </div>          
          <!-- tab4 starts here -->
          	<div class="contentText">
			   <p><strong>Services</strong></p>
  <table border="0">
                 <tr>
                   <td width="33%" align="left" valign="top"><p><em>Strategic</em></p>
                     <ul>
                       <li> Due Diligence</li>
                       <li>Feasibility Studies</li>
                       <li>Tenant Development</li>
                       <li> Master Planning </li>
                       <li>Facilities Management </li>
                       <li>Outsourcing</li>
                   </ul></td>
                   <td width="33%" align="left" valign="top"><p><em>Design</em></p>
                     <ul>
                       <li> Interiors</li>
                       <li>Core &amp; Shell</li>
                       <li>Graphics &amp; Identity</li>
                       <li>Specialty Consulting</li>
                       <li>Branding</li>
                       <li>Sustainability/LEED</li>
                   </ul></td>
                   <td width="33%" align="left" valign="top"><p><em>Implementation</em></p>
                     <ul>
                       <li> Project Management</li>
                       <li>Turnkey</li>
                       <li>Procurement</li>
                       <li> Construction Management</li>
                       <li>Furniture Management</li>
                   </ul></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
<p>&nbsp;</p>
<p><br />
    <br />
    <br />
  </p>
  <!-- IMAGE PER PAGE -->
              <img src="/img/10.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/>
                
		  </div>      	<!-- tab5 starts here -->
          	<div class="contentText">
			   <p><strong>Experience</strong></p>
			   <p class="quote"><em>&#8220;Look at me: I worked my way up from nothing to a state of extreme poverty.&#8221;<br />
		       &ndash; Groucho Marx</em>			   </p>
			   <p>While Groucho may have understated his success for comedic effect, there's nothing exaggerated about his rise from poverty to the heights of success&mdash;and nothing funny about the hard work it took to get there.</p>
<p>Since 1979, CPG has become a highly respected industry leader, working with organizations of every size in virtually every industry, year after year.</p>
		    <p>In a field where &quot;one-project wonders&quot; dominate the landscape, CPG has leveraged its commitment to client value and constant innovation to build a solid track record of success&mdash;for both our clients' organizations and ours.</p>
			   <p>In fact, more than 70% of our business in any given year is repeat business&mdash;a track record even Groucho might admire.		  </p>
          <!-- IMAGE PER PAGE -->
              <img src="/img/3.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/>
                
		  </div>
		<!-- tab6 starts here -->
          	<div class="contentText">
			   <p><strong>Process</strong></p>
			   <p class="quote"><em>&#8220;I think the next wave of office design will focus on eliminating the only remaining obstacle to office productivity: your happiness.&#8221;&ndash; Scott Adams</em></p>
      <hr />
			   <p>At CPG, we also believe that each new wave of office design ideas can have a dramatic effect on productivity.  Unlike Dilbert and his colleagues, however, the results for our clients will be greater satisfaction. </p>
<p>At the heart of our approach is an innovative process for gathering information about the needs of our clients.  We call this process <em>Envisioning</em> and it serves as a solid foundation for the development of design concepts.   <em>Envisioning</em> is programming on steroids.  We believe each client's solutions are unique, and while planning concepts may look similar, implementation is often very different. </p>
			   <p>Many firms focus and push the latest planning trends. At CPG we would rather find out what's important to each company and then present appropriate planning alternatives.</p>
  <br />
  <!-- IMAGE PER PAGE -->
<p><img src="/img/7.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/></p>
                
		  </div>
		<!-- tab7 starts here -->
          	<style type="text/css">
	.clientLink a{padding:5px;}
	.clientLink a:hover{background:#FFFFFF}
	.clientWrap div{float:left; width:200px; margin-right:10px}
</style>
<div class="contentText" style="width: 835px;">
		    <p><strong>Representative Client List</strong></p>

                <table class="clientLink" border="0" cellspacing="0" cellpadding="10">
                  <tr>
                    <td  colspan="3" align="center" valign="top">
                    <a href="javascript:" onclick="showClient(this);">A</a>
                    <a href="javascript:" onclick="showClient(this);">B</a>
                    <a href="javascript:" onclick="showClient(this);">C</a>
                    <a href="javascript:" onclick="showClient(this);">D</a>
                    <a href="javascript:" onclick="showClient(this);">E</a>
                    <a href="javascript:" onclick="showClient(this);">F</a>
                    <a href="javascript:" onclick="showClient(this);">G</a>
                    <a href="javascript:" onclick="showClient(this);">H</a>
                    <a href="javascript:" onclick="showClient(this);">I</a>
                    <a href="javascript:" onclick="showClient(this);">J</a>
                    <a href="javascript:" onclick="showClient(this);">K</a>
                    <a href="javascript:" onclick="showClient(this);">L</a>
                    <a href="javascript:" onclick="showClient(this);">M</a>
                    <a href="javascript:" onclick="showClient(this);">N</a>
                    <a href="javascript:" onclick="showClient(this);">O</a>
                    <a href="javascript:" onclick="showClient(this);">P</a>
                    <a href="javascript:" onclick="showClient(this);">Q</a>
                    <a href="javascript:" onclick="showClient(this);">R</a>
                    <a href="javascript:" onclick="showClient(this);">S</a>
                    <a href="javascript:" onclick="showClient(this);">T</a>
                    <a href="javascript:" onclick="showClient(this);">U</a>
                    <a href="javascript:" onclick="showClient(this);">V</a>
                    <a href="javascript:" onclick="showClient(this);">W</a>
                    <a href="javascript:" onclick="showClient(this);">X</a>
                    <a href="javascript:" onclick="showClient(this);">Y</a>
                    <a href="javascript:" onclick="showClient(this);">Z</a>
                    </td>
                  </tr>
                </table>





                <!-- Clients of letter A -->
                <div class="clientWrap" id="clientA">

                <div>
						A.T. Clayton<br />
                      ABB<br />
                      ACE USA<br />
                      ACNielsen<br />
                      ADP Service Corporation<br />
                      ADP Services Inc.<br />
                      ADT Security<br />
                      Aegis Mortgage Corp<br />
                      AFRT Dianon<br />
                      AIG Corporation<br />
                      AIG Marketing<br />
                      AIG Trading Corp.<br />
                      Aimco<br />
                      AIMCOR<br />
                    Akzo Nobel<br />
                 </div>
                    <div>
                    	Albert D. Phelps<br />
                        Albourne America LLC<br />
                      Alea North America Company<br />
                      Alliance Data Systems<br />
                      Allstate/First CT Lending<br />
                      Alpha Chartering<br />
                      Amdocs/9 West Broad<br />
                      Amdocs/Atlanta<br />
                      Amdocs/Boston<br />
                      Amdocs/NJ Amdocs Inc.<br />
                      American Bailey Fueltech<br />
                      American Apparel<br />
                      American Bailey<br />
                      American Bible Society<br />
                      American Express Service Ctr</br>
                      American Family Mutual <br />
                      Amscan Inc.<br />
                    </div>
                    <div> Ann Taylor<br />
                      Anne &amp; Jan, Inc.<br />
    Antares<br />
                      Antares Hat Factory<br />
                      Antares Stamford Harbor Park<br />
                      Antares Yale &amp; Towne <br />
                      Aperture Technologies<br />
                      Apollo Real Estate Advisors<br />
                      Apollo: 1290 6th Avenue<br />
                      Arrow Electronics<br />
                      Atlantic Info at Lee Farm<br />
                      Atlantic-Pacific Capital, Inc.<br />
                      Auspex Group<br />
                      Autocue Holdings, Inc.<br />
                      Axel Johnson
                     </div>


            </div>


            <!-- Clients of letter B -->

            <div class="clientWrap" id="clientB">
            <div>
            Baltimore Gas &amp;                     Electric <br />
Banco Popular<br />
Bank of New York<br />
Barnum Financial Group<br />
Barry Blau Partners<br />
Bay Management<br />
Beacon Mill Village<br />
Beaumont Hospital<br />
Benckiser Sonsumer Products<br />
Benenson Capital<br />
Benenson Cap. 1600 Summer<br />
Benenson Cap. 210 Livingston<br />
<br /><br />

</div><div>

Biltmore Allstate<br />
Biltmore Apt Tower<br />
Biltmore Geo Bagley<br />
BKM Total Office<br />
Blair Asset Management, LP<br />
Bloomberg Financial                     Markets<br />
BMW Oyster Bay<br />
Boardroom                     Reports<br />
Bowtie Partners<br />
Breitling Security<br />
Breitling USA, Inc.<br />
Bresnan Broadband Holdings, LLC<br /><br />

</div><div>

Bresnan Communications, Inc.<br />
                    Briarcliffe College<br />
Brink&#39s International<br />
Bristol-Myers Squibb<br />
Brocade Comm. Systems<br />
Brynwood Partners<br />
BSMG <br />
Building &amp; Land Technology<br />
Bulk Ocean Chartering Inc.<br />
Bunge Limited<br />
Business Dev. Bank of Canada<br />
Butler America<br />
BGT Limited<br />

</div>
</div>

            <!-- Clients of letter C -->
            <div class="clientWrap" id="clientC">

                <div>
                	C. Dean Metropoulos &amp; Co.<br />
                    Cadbury Schweppes<br />
                    Cadillac of Greenwich<br />
                    California Business Interiors<br />
                    Camelot Capital<br />
                    Canadian Red Cross<br />
                    Career Blazers<br />
                    Career Education Corporation<br />
                    Casey, Quirk &amp; Assoc.<br />
                    Castlenet, LLC<br />
                    CB Richard Ellis<br />
                    CBG Investment Advisors<br />
                    CBRE PITT PL MEDRAD<br />
                    CDW Computer Centers, Inc.<br />
                    CEC AIU Atlanta<br />
                    CEC Briarcliffe Bethpage<br />
                    CEC Gibbs Farmington<br />
                    CEC Gibbs NYC<br />
                    CEC Patchogue<br />
                    CEC SBI<br />
                    CEC UDS (NYC)<br />
                    Cedars-Sinai<br />
                    Cendant<br />
                    Centennial Communications<br />
                    Centerplate<br />
                    CGI Group Inc.<br />
                    Champion International<br />
                    Chevy Chase Bank<br />
Child Guidance Center<br />
Chrysler Building<br />
Chrysler East<br />
Cirrus Tenant Lease Services<br />
Citibank<br />
Citibank 1391 Madison Ave.<br />
Citibank 145 E. 42nd<br />
Citibank 1633 Broadway<br />
Citibank 267 5th Ave.<br />
Citibank 501 2nd Ave.<br />
Citibank 5254 Copper Sq.<br />
Citibank 750 Washington<br />
Citibank Bee Cave Austin<br />
Citibank Black Rock Fairfield<br />
Citibank Boston<br />
<br /><br />

</div><div>
 Citibank Cedar Springs Dallas<br />
Citibank Chelsea<br />
Citibank Chinatown<br />
Citibank CRS<br />
Citibank Danbury<br />
Citibank East Haven<br />
Citibank Fort Lee <br />
Citibank Glastonbury<br />
Citibank Greenwich<br />
Citibank Hartford Civic Center<br />
Citibank Meriden<br />
Citibank Middletown<br />
Citibank N. Plainfield<br />
Citibank New Canaan<br />
Citibank New City<br />
Citibank New Haven<br />
Citibank Newington<br />
Citibank Newtown<br />
Citibank North Haven<br />
Citibank Norwalk<br />
Citibank NYC<br />
Citibank Oceanside<br />
Citibank Orange<br />
Citibank Paramus<br />
Citibank Prototype<br />
Citibank Purchase<br />
Citibank Ramsey<br />
Citibank Ridgefield<br />
Citibank Shelton<br />
Citibank Smith Barney<br />
Citibank South Norwalk<br />
Citibank Southington<br />
Citibank Stamford<br />
Citibank Suffern<br />
Citibank Thornwood<br />
Citibank Waterbury<br />
Citibank Westfield<br />
Citibank West Hartford<br />
Citibank West Orange<br />
Citibank Westport<br />
Citibank Wethersfield<br />
Citibank White Plains<br />
<br /><br />

</div><div>
Citibank Yonkers<br />
Citibank Yorkville<br />
Citicorp<br />
Citicorp Investments<br />
Citigroup<br />
Citigroup Mortgage<br />
Citibank Bridgeport<br />
Citibank Woodbridge<br />
Citizens Communications<br />
Citizens Utilities<br />
City Harvest<br />
Clancy Moving<br />
Claremont Technology Group<br />
Clearview Investments<br />
Clocktower Close<br />
Colangelo Group<br />
Colangelo Synergy Marketing<br />
Columbia University<br />
Commonfund<br />
Commonfund Group<br />
Compass Group<br />
Computer Sciences<br />
Constellation Power Source<br />
Continental Airlines<br />
Corporate Branding<br />
Corporate Executive Board/London<br />
Corporate Executive Board/D.C.<br />
Countrywide Home Loans Inc.<br />
CPC Resources<br />
CPG One Dock Street<br />
CRA Realty Advisors<br />
Crane Company<br />
Crescent Jewelers<br />
Crestar Bank<br />
Cromwell Leather<br />
Crozer Keystone Real Estate<br />
Crozer-Keystone Health<br />
CT Conference of Municipalities<br />
Cushman &amp; Wakefield<br />
Cushman &amp; Wakefield 222 E 41<br />
Cycling Sports Group<br />


</div>




</div>

            <!-- Clients of letter D -->
            <div class="clientWrap" id="clientD">

            	<div>
            		Dannon HQ<br>
            		Datto <br>
           		  Davis Companies<br />
                    Deloitte<br />
                    Deloitte &amp; Touche LLP<br />
                    Deloitte Canada<br />
                    Deloitte Stamford<br />
                    Deloitte Wilton<br />
                    Department of Ecology<br />
                    Desco Group Inc.<br />
                    Diageo 530 5th Ave. NYC<br />
                    Diageo Chicago<br />
                    Diageo Dallas<br />
                    Diageo Distillery TN<br />
                  <br />
                  	</div><div>
                   Diageo Headquarters<br />
Diageo Innovations<br />
Diageo Miami<br />
Diageo Napa Valley<br />
Diageo Norwalk<br />
Diageo NY Branding <br />
Diageo NYC<br />
Diageo NYC Wines<br />
Diageo Reckson<br />
Diageo San Francisco<br />
Diageo Toronto<br />
Diageo WOW<br />
Diageo Global Standards <br />

</div><div>

Dillon Residence<br />
DMG Advisors<br />
Dobson Cellular<br />
Dock Street Holdings LLC<br>
Dorel Sports <br />
Dr. Greenwald Dentistry<br />
Dr. Lord<br />
Dr. Sadeghi<br />
Dr. Waller<br />
DRA Advisors<br />
Dresdner/Kleinwort/Benson<br />
Duggan Realty Advisors<br />
Dworkin Residence

</div>

</div>

            <!-- Clients of letter E -->
            <div class="clientWrap" id="clientE">

            	<div>
            		8 Sound Shore Drive<br />
                    80 Field Point Road Greenwich<br />
                    850 3rd Avenue<br />
                    885 3rd Avenue<br />
                    E Commerce Corp.<br />
                    E Media, Inc.<br />
                    Edison Schools<br />
                    Edison Schools ASU<br />
                    Edison Schools St. Louis<br />
                    Edwards &amp; Zuck<br />
                    Einstein Bagels<br />
                    Ellington Management Group<br />
EMCI<br />
                  <br />

                  </div><div>
                   eMedia, Inc.<br />
Empire State Building<br />
Equity 1301 6th Avenue<br />
Equity 527 Madison<br />
Equity 717 5th Avenue<br />
Equity 850 3rd Avenue 	  	<br />
Equity 1301 6th Avenue<br />
Equity 177 Broad<br />
Equity 4 Stamford Plaza<br />
Equity 527 Madison Avenue<br />
Equity 850 Third Ave<br />
Equity Office Properties<br />

</div><div>

 Equity Offices Park Avenue<br />
Equity One<br />
Equity Park Avenue Tower<br />
Equity Stamford Plaza Cafe<br />
Equity Worldwide Plaza<br />
Ericsson NYC<br />
Ernst &amp; Young<br />
Esl Federal Credit Union<br />
Essention Inc.<br />
Enterprise Technology Corp.<br />
Excel Partners<br />
Experian

</div>

</div>

            <!-- Clients of letter F -->
            <div class="clientWrap" id="clientF">

            	<div>

            		4 Stamford Plaza<br />
                    40 Broad Street<br />
                    40 Broad Street<br />
                    425 Park Avenue<br />
                    470 West Avenue<br />
                    48 Wall Street<br />
                    502 Canal Street, Stamford<br />
555/575 Market Street<br />
55A East 8th Avenue<br />
                  <br />

                  </div>
                  <div>
                   Factory Connection <br />
                    Factset<br />
Fairfield Metro Center<br />
Favour Royal<br />
First Comp<br />
First Federal Bank of CA<br />
First Security Services Co.<br />
Fisk Building<br />
Flag Venture Management<br />
Fletcher Knight<br />
Frontier Insurance <br /><br />

</div><div>
 Flinn Ferguson<br />
                    Flohr Residence<br />
Focus First America<br />
Focus Vision Worldwide<br />
Food Management Partners<br />
Frame King<br />
Fred F French<br />
Freeman, Loftus &amp; Manley<br />

</div>
</div>

            <!-- Clients of letter G -->
            <div class="clientWrap" id="clientG">
            		<div>
                    GAB Robins<br />
                    GAB Robins North America Inc.<br />
                    Gac Marketing<br />
                    Gac Prefered Investment Solutions<br />
                    Gartner Group<br />
                    Garvin Nathaniel<br />
                    GE Lee Farm<br />
                    GE 10 Riverview Dr<br />
                    GE 101 East Ridge<br />
                    GE 120 Long Ridge Road<br />
                    GE 1600 Summer Street<br />
                    GE 201 High Ridge Road<br />
                    GE 260 High Ridge Road<br />
                    GE 260 Long Ridge Road<br />
                    GE 291 Long Ridge Road<br />
                    GE 777 Long Ridge Road<br />
                    GE Capital Cef<br />
                    GE Capital Cloe<br />
GE Capital Corporation<br />
GE Capital Inventory Finance<br />
GE Capital Rfs<br />
GE Capital, Capital Markets<br />
GE Capital, Corp Services<br />
GE Capital, Crefs<br />
GE Capital, E-commerce<br />
GE Capital, Equity<br />
GE Capital, Eric<br />
GE Capital, Sfg<br />
GE Capital, Sourcing<br />
GE Card Services<br />
GE Cef<br />
GE Cef Vfs 10 Riverview<br />
                  <br />

                  </div><div>
                   GE Company<br />
                    GE Company / 120 Long Ridge<br />
GE Company/1600 Summer<br />
GE Company/201 High Ridge<br />
GE Company/260 Long Ridge<br />
GE Company/301 Lee Farm<br />
GE Company/777 Lrr<br />
GE Company/Treasury Group<br />
GE Credit Corporation<br />
GE Investment<br />
GE Lee Farms<br />
GE Money<br />
GE Security<br />
GE Sfg <br />
GE Software Group<br />
GECC/777 Long Ridge<br />
GECC/UCONN<br />
GECC 120 Long Ridge<br />
GECC 260 Long Ridge<br />
GECC 291 Long Ridge<br />
GECC 335 Madison Ave.<br />
GECC 101 Merritt 7<br />
GECC 401 Merritt 7<br />
GECC 501 Merritt 7<br />
GECC 40 Old Ridgebury<br />
GECC 42 Old Ridgebury<br />
GECC 44 Old Ridgebury<br />
GECC 777 Lr Bldg A<br />
GECC Cef Lee Farms<br />
GECC Crefs 291 Long Ridge<br />
GECC/UCONN<br />
GE Cef Vfs Lee Farm<br />
GE Cfs <br /><br />

</div><div>

GECIS 40 Old Ridgebury<br />
                    GECIS NYC<br />
Geller &amp; Company<br />
General Atlantic<br />
General Electric Capital Corporation<br />
General Electric Credit Corp<br />
General Electric VFS<br />

General Mills Operations, Inc.<br />
General Mills, Inc.<br />
General Re<br />
General Signal<br />
Geneve Corporation<br />
Gesualdi Residences<br />
Gibbs College<br />
Gibbs College Norwalk<br />
Gibbs Farmington<br />
Global Security ABC/Disney<br />
GMAC<br />
GMAC Mortgage<br />
Goodfellow Ashmore<br />
Gordian Group<br />
Greenwald Dentistry<br />
Greenwich American Cener<br />
Greenwich Capital Markets<br />
Greenwich Hospital<br />
Greenwich Tech<br />
Greenwich Technology Partners<br />
Gridiron Capital<br />
Group 601<br />
Group 601 / UPS Stores<br />
Grubb &amp; Ellis Company<br />
Gtech Corporation<br />
GVA Williams

</div>
</div>

            <!-- Clients of letter H -->
            <div class="clientWrap" id="clientH">
            	<div>
                H. Muehlstein & Co.<br />
            		Haestad Methods<br />
                    Harbor Plaza<br />
                    Harrison Executive Park<br />
                    Hasbro Inc.<br />
                    Healthcare Realty Services<br />
                    Healthnet<br />
                    Healthnet 2005 NYC<br />
                    Healthnet 2005 Shelton<br />
                    Healthnet Kcb<br />
                    Healthnet NYC<br />
                    Healthnet Parking Garage<br />
                    Healthnet Shelton<br />
                  <br />
                  </div><div>
                  Houlihan &amp; Lawrence<br>
                    Helmsley 112 W 34th St<br />
                    Helmsley 1333 Broadway<br />
Helmsley 1350 Broadway<br />
Helmsley 1359 Broadway<br />
Helmsley 1400 Broadway<br />
Helmsley 501 7th Avenue 	  	<br />
Helmsley-Spear Inc.<br />
Henry Schein<br />
Hewitt Associates<br />
Hines/Chrysler Capital<br />
Hines/US Trust<br />
Hines Countrywide<br /><br />

</div><div>

Hines Interests Limited<br />
Hoffman Brothers<br />
Hoffman Investment Partners<br />
Holly Duran Partners<br />
Holly Pond<br />
Honeywell International Inc.<br />
Honeywell Support Services<br />
Houghton Mifflin<br />
Howard Systems<br />
Howard Systems International<br />
HSBC Bank<br />
HSBC Bank USA<br />
Hudson's Bay Trading Co.

</div>
</div>

            <!-- Clients of letter I -->
            <div class="clientWrap" id="clientI">

            	<div>
                    i.park Norwalk<br />
                    IBM Corporation<br />
                    IBM Retirement Funds<br />
                    Idelle At Lee Farm<br />
                    Idelle Labs<br />
                    Imagineering<br />
                    Imperial Investment<br />
                  IMS Health New Jersey<br />                    <br />

                  </div><div>
                  Indeed<br />
                  Indian Harbor Group<br />
Infor Prolease<br />
Informix<br />
Inrange Technologies<br />
Insightexpress<br />
Institute of Culinary Education<br />
Intellirisk<br />
Intermountain Health Care<br />
IMS Health Norwalk<br />

</div><div>

Internal Revenue Service<br />
International Paper<br />
International Toy Center<br />
Interoffice Management<br />
Ipsos-Asi, Inc.<br />
IRMC<br />
ISS<br />
ITDS

</div>

</div>

            <!-- Clients of letter J -->
            <div class="clientWrap" id="clientJ">

            	<div>
                	J. Walter Thompson<br />
                    JAT Capital<br />
                    Jeffries &amp; Company Inc.<br />                    <br />
                    </div>

                    <div>John Frieda <br />
                  John W. Henry &amp; Co., Inc.<br />
                  </div><div>
                  	JP Morgan<br />
JP Morgan Chase
</div>
</div>

            <!-- Clients of letter K -->
            <div class="clientWrap" id="clientK">

            	<div>
            		Karp Associates<br />
                    Kay's Construction<br />
                    Kazi Foods<br />
                  <br />

                  </div><div>
                  Kew Management<br />
Kidd &amp; Company<br />
Kids In Crisis<br />
</div><div>
	King Casey<br />
Knights of Columbus

</div></div>

            <!-- Clients of letter L -->
            <div class="clientWrap" id="clientL">

            	<div>
            		L&amp;L Acquisitions, Llc<br />
                    L&amp;L Holding Co., Inc.<br />
                    Laerdal Medical Corp.<br />
                    Laerdal Medical Group<br />
                    Lasalle Partners<br />
                    Learning Care Group<br />
                    Leclair Ryan<br />
                    Lee Farm Corporate Park<br />
                    Lee Farms<br />
                    Legg Mason<br />
                  <br />

                  </div><div>

                   Lehman Brothers<br />
                    Leo Mckeague<br />
Levy Organization<br />
LG Securities America, Inc. <br />
Lgs Pogo<br />
Life Care<br />
Life Reasuurance Corp.<br />
Lifecare, Inc.<br />
Lime Rock Partners<br />
Lincoln At Merritt 8<br />
Lincoln Bldg / Helmsley<br /><br />

</div><div>
Lincoln Bldg / Newmark<br />
                    Lincoln Building<br />
Litchfield Hills Ortho. Canton<br />
Litchfield Hills Orthopedics<br />
Littlejohn &amp; Co., LLC<br />
Long Term Capital Management<br />
Louis Vuitton<br />
Louis Vuitton N.A. Inc.<br />
LVMH Fashion Americas<br />
Leggat Mccall Properties</div>
</div>

            <!-- Clients of letter M -->
            <div class="clientWrap" id="clientM">
            	<div>
            		Mack-Cali Realty<br />
                    Madge Network<br />
                    MAI Systems Inc.<br />
                    Maplewood Senior Living<br />
                    Marakon<br />
                    Marcus &amp; Millichap<br />
                    Marketing 1 To 1<br />
                    Marlboro Associates, Llc<br />
                    Marshall Wace<br />
                    Mass Mutal<br>
                  Match MG<br />
                    MBIA<br />
                    MBIA Insurance Corporation<br />
                    McCoy, Inc.<br />
                    McDonald's<br />
                    Mcgraw Hill / 1221 6th Ave<br />
                    Mcgraw Hill / 2 Penn<br />
                    Mcgraw Hill / Atlanta<br />
                    Mcgraw Hill / Lexington Ma<br />
                    Mcgraw Hill Companies, Inc.<br />
                    MCI Telecommunications<br />
                    Mcnamara &amp; Kenny<br />
                    <br />

                    </div><div>
                    Media General<br />
                    Memberworks Incorporated<br />
                    Meritage Properties LLC<br />
Merritt 7 101<br />
Merritt 7 201<br />
Merritt 7 301<br />
Merritt 7 401<br />
Merritt 7 501<br />
Merritt 7 Corporate Park<br />
Merritt 8 	  	Merritt 8 2nd Fl<br />
Merritt 8 Acquisitions, Llc<br />
Merritt 8 Corporate Park<br />
Merritt Corp Woods<br />
Merritt On The River<br />
Merritt Woods<br />
Met Center<br />
Metlife<br />
Metro Center<br />
Metropolitan Life Insurance Co.<br />
Metropoulos Group Inc.<br />
MGR<br /><br />

</div><div>
Midway Investors<br />
Milestone Security<br />
                    Mill Management<br />
Millennium Partners<br />
Mintz Levin<br />
Mistubishi Imaging (USA), Inc.<br />
Mitler &amp; Mercaldo<br />
Mitsui &amp; Co. (USA), Inc.<br>
Mitchells <br />
Mks Instruments<br />
Moab Oil<br />
Modem Media<br />
Modem Media. Poppe Tyson<br />
Morgan Guaranty Trust<br />
Morgan Stanley Dean Witter<br />
Mormac Marine<br />
Mott's Snapple<br />
Mott's Snapple Headquarters<br />
Mountain Development<br />
Murtha Cullina<br />
Myllykoski At Merritt 7<br />
Meded Healthcare Group

</div>
</div>

            <!-- Clients of letter N -->
            <div class="clientWrap" id="clientN">

            	<div>
                    901 Kuehner<br />
                    969 High Ridge Road<br />
                    99 Hawley Lane<br />
                    Natwest Markets<br />
                    Navisite<br />
                    NBC Olympics<br />
                    NBC Olympics Athens<br />
                    NBC Olympics Torino<br />
                    NBC Olympics Athens<br />
                    NBC Olympics Torino<br />
NCC Center For Info <br />
Neopost<br />
Nestle Waters<br />
                    <br />
                  </p>

                  </div><div>
                   Nestle Waters Globe<br />
Nestle Waters, NA, Inc.<br>
Newman&rsquo;s Own<br />
Newmark &amp; Co. RE, Inc.<br />
Next Wave<br />
Nextel Communication 	  	<br />
Nickerson
8 Sound Shore Dr.<br>
Nichols MD <br />
Nine West<br />
Norelco<br />
North Castle Partners<br />
North Sound Capital<br />
Northern Trust Company<br /><br />

</div><div>
 Norwalk Community College<br />
Norwalk Medical Group<br />
Novastar Financial<br />
Nsight Telservices<br />
NVR Inc.<br />
NY Medical Imaging<br />
NY Times Building<br />
Nyala Farms<br />
Nyala Farms 100 &amp; 400<br />
NYC Landmarks Commission<br />
Northwest Medical Center

</div>
</div>

            <!-- Clients of letter O -->
            <div class="clientWrap" id="clientO">

            	<div>
            		100 Wall Street, NYC<br />
                    1010 3rd Avenue<br />
                    1010 Washington Blvd.<br />
                    11 Lake Avenue<br />
                    11 West 42nd<br />
                    1111 Summer Street<br />
                    1177 Summer Street<br />
                    1600 Summer Street<br />
                    OCI<br />
                  <br />

                  </div><div>
                   Office Depot<br />
                    Ohio Savings Bank<br />
Old Post Office Restaurant<br />
Oldendorff<br />
Olnick Headquarters<br />
Olstein &amp; Associates, L.p.<br />
Olympic Construction<br />
One Dock Street<br />
One River Road<br />

</div><div>
 Ons &amp; Richter<br />
Opticare Eye Care<br />
Oriel Instruments<br />
Orscheln Management<br />
OTA Limited Partnership<br />
OTR Global Trading LLC<br />
Outlooksoft Corporation<br />
One Stamford Landing

</div>
</div>

            <!-- Clients of letter P -->
            <div class="clientWrap" id="clientP">

            	<div>
            		Palladio Capital<br />
                    Palmero Partners<br />
                    Paloma / JP Morgan<br />
                    Paloma Partners Management Co.<br />
                    Park 80 West<br />
                    Parsons Brinckerhoff<br />
                    Parsons Transportation Group Inc.<br />
                    PAS Associates<br />
                    Paul Hastings Janofsky &amp; Walker<br />
                    Pavarini Construction<br />
                    Paw Partners<br />
                    Pearson Vue<br />
                    Pet Food Express<br />
Pharmacia &amp; Upjohn Company<br />
                  <br />
                  </div><div>
                   Philips<br />
PHS, Neptune<br />
PHS, Paramus<br />
PHS, Shelton<br />
PHS, Stratford<br />
Physicians Health Services<br />
Pitney Bowes Credit Corp<br />
Pitney Bowes Inc.<br />
Pitney Bowes Shelton<br />
Pitt Pl Invesmart<br />
Plantinum Technologies<br />
Playtex Products, Inc.<br />
PNC Bank<br />
PNC Bank (Riggs) Oakton<br />


</div><div>
 PNC Bank Dulles<br />
Preferred Investment Solutions<br />
Preotle, Lane &amp; Associates, Ltd.<br />
Pricewaterhousecoopers<br />
Primedia 1600 Summer Street<br />
Primedia 969 Hrr<br />
Progressive Insurance<br />
Proliance<br />
Provident Bank<br />
Provident Bank / Harriman<br />
Provident Bank / Montebello<br />
Prudential<br />
Prudential Savings<br />
Ptarmigan Capital

</div>
</div>

            <!-- Clients of letter Q -->
            <div class="clientWrap" id="clientQ">

            	<div>
            		QFS/GAC<br />
                  Quantitative Fin. Strategies<br />
              
                  Quebecor World Inc.<br />
                      Quorum Federal Credit Union<br />
                 </div>
</div>

            <!-- Clients of letter R -->
            <div class="clientWrap" id="clientR">

            	<div>
            		Reader's Digest<br />
                    Reckson 1 Landmark<br />
                    Reckson 1055 Washington Blvd<br />
                    Reckson 2 Landmark<br />
                    Reckson 225 High Ridge East<br />
                    Reckson 4 Landmark<br />
                    Reckson Realty<br />
                    Reflections Salon<br />
                    REI - 73 Sand Pit<br />
                    REI / Aegis Mortgage Corp<br />
                    REI Lee Farms GE Software Group<br />
                    REI Property &amp; Asset Man.<br />
                    Related Properties<br />
                    Republic Bank Of New York<br />
                    Reynolds Metals<br />
                    RIM - Research in Motion<br />
                    RFR Realty<br />
                    Rfs 200 Pondview<br />
                    RFS Cablewave Systems<br />
                    Rhone Group<br />
                  <br />

                  </div><div>

                  Regeneron<br>
                  Renaissance Re<br>
                  Richards Barry Joyce<br />
Riggs Bank - 1919 Penn.<br />
Riggs Bank - 20th &amp; L<br />
Riggs Bank - 7th &amp; I Street<br />
Riggs Bank - Ballston<br />
Riggs Bank - Capitol Hill<br />
Riggs Bank - Cityplace<br />
Riggs Bank - Congressional<br />
Riggs Bank - Dulles Airport 	  	<br />
Riggs Bank - Dulles Town Center<br />
Riggs Bank - Fairwood<br />
Riggs Bank - Fallsgrove<br />
Riggs Bank - Friendship<br />
Riggs Bank - Gainesville<br />
Riggs Bank - Intelsat<br />
Riggs Bank - Middleburg<br />
Riggs Bank - National Airport<br />
Riggs Bank - Oakton<br />
Riggs Bank - Rockville<br>
Rubicon Technology Partners <br />

</div><div>

Riggs Bank - Rosslyn<br />
Riggs Bank - S. Washington<br />
Riggs Bank - Sterling<br />
Riggs Bank - Universal<br />
Riggs Bank - Wildwood<br />
Riggs Bank - Wisconsin<br />
Riggs Bank, N.A.<br />
Robert D. Scinto Inc.<br />
Robins Afb<br />
Robinson &amp; Cole<br />
Rockefeller Center<br />
Rockland Trust<br />
Romeo Residence<br />
Roy F. Weston<br />
Royce &amp; Associates<br />
Rvi Services Co., Inc.<br />
Ryan Beck

</div>
</div>

            <!-- Clients of letter S -->
            <div class="clientWrap" id="clientS">
            	<div>
            		65 E. 8th Avenue<br />
                    666 5th Avenue<br />
                    717 Fifth Avenue<br />
                    73 Sand Pit<br />
                    733 Summer Street<br />
                    Saab of Westport<br />
                    Sands 40 / 42 Old Ridgebury<br />
                    Savin Corp.<br />
                    Schieffelin &amp; Somerset Co.<br />
                    Scinto (Multiple Projects)<br />
                    Scinto Fairfield<br />
                    Scinto Il Palio<br />
                    Scinto International Plaza<br />
                    Scinto Jewish Home<br />
<br />

	</div><div>
Scinto Renaissance<br />
Scinto Tower 1<br />
SDS Capital<br />
Seaboard 600 Summer<br />
Seaboard / 1 Dock Street<br />
Seaboard / 470 West Avenue<br />
Seaboard / 5 Stamford Landing<br />
Seaboard Argus<br />
Seaboard Beacon Mill Village<br />
Seaboard Clocktower Close<br />
Seaboard Development<br />
Seaboard New Canaan<br />
Seaboard Offices<br />
Seaboard Property Man.<br /><br />

</div><div>
SiriusDecisions<br />
Stamford Town Center<br />
State Of Connecticut<br />
Statoil<br />
Statoil Marketing &amp; Trading<br />
Statoil North America Inc.<br />
Staubach Lease Management Inc.<br />
Still River<br />
Stolt Nielsen<br>
Summit Development <br />
Sun Products HQ<br />
Sun Products R&amp;D<br />
Suntrust Real Estate<br />
Swearingen Realty Group, Llc<br />
Swiss RE American Holding Corp.

</div>
</div>

            <!-- Clients of letter T -->
            <div class="clientWrap" id="clientT">
            	<div>
            		225 High Ridge Road<br />
                    230 Park Avenue<br />
                    300 Park Tower<br />
                    316 Courtland Ave<br />
                    345 Main Street, Stamford<br />
                    T. Rowe Price<br />
                    Tauck Tours<br />
                    Team Health, Inc.<br />
                    Telcordia Technologies<br />
                    Texaco<br />
                    The Atrium <br />
                    <br />

            	</div><div>

            The Olnick Organization<br />
The Taubman Company<br />
The Thomson Corporation<br />
The Winter Organization<br />
Three Stamford Landing<br />
Time Warner Cable<br />
Tishman Speyer<br />
Tishman Speyer City Spire<br />
Tishman Speyer GAC<br />
Tishman Speyer NYT Building<br />
<br />

</div><div>

Tishman Speyer Properties<br />
Tishman Speyer Rockefeller Cen.<br />
Tockstein School<br />
Tom Fowler Residence<br />
Tommy Hilfiger<br />
Tower / GAC<br />
Tower Realty Company<br />
Tower Realty Management<br />
Tower Records<br />
Trammell Crow Company<br />
Tudor Investment Corporation<br />

</div>
</div>

            <!-- Clients of letter U -->
            <div class="clientWrap" id="clientU">
            	<div>
                    UCONN<br />
                    Unger Residence<br />
                    United Distillers<br />
                    United Rentals<br />
                    UPS Stores 	  	Ury &amp; Moskow<br />
U.S. Patent &amp; Trademark Office <br />
USG Insurance<br />
USI<br />
US Tobacco
</div>
</div>

            <!-- Clients of letter V -->
            <div class="clientWrap" id="clientV">

            	<div>
            		VA Medical Center<br />
                    Van Dyk Baler Corp.<br />
                    Vertrue Incorporated<br />
                  <br />
                  </div><div>
                  Vertrue Omaha<br>
                  Vineyard Vines
                  <br />
Vivendi Universal 	  	VNU-USA<br />
Voc Bulk Shipping USA, Inc.<br />

</div><div>
 Voc Shipholdings BV<br />
VSI Communications
</div>
</div>

            <!-- Clients of letter W -->
            <div class="clientWrap" id="clientW">

            	<div>
            		W&amp;M Properties<br />
                    Wakefern Food Corporation<br />
                    Walker Digital<br />
                    Warrantech, Inc.<br />
                    Watkins Motor Lines<br />
                    Watson Wyatt<br />
                    Webster Bank<br />
Webster Bank Building <br />
                  <br />

                  </div><div>
                   Werner &amp; Kennedy<br />
West Lyon Farm<br />
West Lyon Farm Condo Assoc. 	  	<br />
Wien &amp; Malkin<br />
Wilmington Finance<br />
Witco Corporation <br /><br />

</div><div>

 Workplace Solutions<br />
World Courier<br />
Worldly Gifts<br />
Wralc/lneaw<br />
W.R. Grace<br />
Wunderman Cato Johnson

</div>
</div>

            <!-- Clients of letter X -->
            <div class="clientWrap" id="clientX">

            	<div>
            	<p>Xerox Corporation<br /> 	  	
                XPO Logistics, Inc.<br />
                    Xtex<br />
                  </p>
                </div>
</div>

            <!-- Clients of letter Y -->
            <div class="clientWrap" id="clientY">

            	<div>
            		Yale New Haven Hospital<br />
                  Yale Hospital Playground <br />
                  Yellow Book<br />
Young &amp; Rubicam

</div>
</div>

            <!-- Clients of letter Z -->
            <div class="clientWrap" id="clientZ">
            	<div>
            		Zuniversity.com 	  	<br />
                  Zurich Re<br />
                  </div>
            </div>







<!-- IMAGE PER PAGE -->


		  </div>


<script language="javascript1.2">
	function showClient(obj) {
		var letter = $(obj).text();
		$('.clientWrap').hide();
		$('#client' + letter).show();
		$(obj).siblings('a').css('background-color', '');
		$(obj).css('background-color', '#FFF');
	}

	$(document).ready(function(e) {
		$('.clientWrap').hide();
		$('#clientA').show();
		$('.clientLink a:first').css('background-color', '#FFF');
    });

</script>            
            <!-- tab8 starts here -->
          	<div class="contentText">
		    <p><strong>Awards</strong></p>
            
            <p align="left" class="quote">2015, AIA Connecticut Business Award for Dorel Sports | <a href="http://cpgarch.com/news.php">Read More</a></p>
  <hr />
<p align="left" class="quote">2012, Commercial Honor Award, U.S. Green Building Award Council, Project: Nestle Headquarters</p>
  <hr />
		    <p align="left" class="quote">2012, Award of Merit for the Illuminating Engineering Society of North America, Project: Nestle Headquarters</p>
		     <hr />
		     <p align="left" class="quote">2010, Gary Unger, The March of  Dimes &quot;Real Estate Person of the Year&quot; </p>
		     <hr />
		     <p align="left" class="quote">2007, AIA 2007  Connecticut Business Architecture Award, Project: Vertue </p>
<hr />
		     <p align="left" class="quote">2007, Building of the Year Award, Southern Connecticut Building Owners and Managers Association (BOMA), Project: 8 Sound Shore Drive</p>
		     <hr />
		     <p align="left" class="quote">2005, New York  Construction Interior Fit-Out Award, Project: Diageo</p>
<p class="quote">&nbsp;</p>
<!-- IMAGE PER PAGE -->
<p><img src="/img/2.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/></p> </div>            
		<!-- tab9 starts here -->
          	<div class="contentText">
		    <p><strong>Careers</strong></p>
		    <p>We have organized the office into studios and find the team approach works best for our projects. We are proud of our success over the years and extremely proud of our staff. Many of our employees have been with the firm for more than 10 years, with several also reaching their 20-year anniversaries. We believe this longevity speaks for our commitment to providing a professional work environment, allowing growth opportunities, for making available flexible work schedules and offering a competitive compensation program.</p>
<p>A compensation program is measured not only by base salary but also the many other benefits provided through employment with the firm. Our package includes salaries that are competitive in the marketplace, and eligible employees may also receive performance bonuses. In addition, our top-notch benefits program provides a generous policy for paid time off including holiday and vacation time, and also provides time during business hours for professional growth.</p>
		    <p>Our corporate culture is unparalleled and we enjoy the most professional reputation in the industry.</p>
<p>Candidates please contact us and send resume <a href="mailto: info@cpgarch.com">here</a>. All resumes treated with the utmost confidentiality. Or if you have a special need, please call us at 203 967 3456.          </p>
		    <p>&nbsp;</p>
 <!-- IMAGE PER PAGE -->
            <p><img src="/img/1.jpg"  alt="quote" width="200" height="396" style="position:absolute; right:0; top:0"/></p>
                
		  </div>
      </div>
        
	</div>

</div>


<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/2183733.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->
</body>
</html>



<script language="javascript1.2">

	$(document).ready(function(e) {
		var c = 0;
		$('.contentNavigation a').click(function() {
			//alert($(this).index('.contentNavigation a'));
			var index = $(this).index('.contentNavigation a')
			openLink(index);
			
			//if About CPG clicked
			if(index == 1) {
				c++;
				//show About CPG's sub-menu
				//$('.about_submenu').css('display','block');
				if(c == 1) {	//for first time animate and show the sub menu
					$('.about_submenu').animate({
						height: 'toggle'
					}, 500);
				}
			} else {
				if($(this).parent().parent().hasClass('about_submenu')){
					//if sub menu items is clicked then show About CPG's sub-menu
					$('.about_submenu').css('display','block');
				} else {
					//if other than About CPG and its sub menu items is clicked then hide About CPG's sub-menu
					//$('.about_submenu').css('display','none');
					if($('.about_submenu').is(':visible')) {
						$('.about_submenu').animate({
							height: 'toggle'
						}, 500);
					}
					c = 0;	//reset and hide the sub menu
				}
			}
			
		});
		$('.contentNavigation a').attr('href', 'javascript:');
		
		
		openLink(0);
		goBack();
	});
	
	function openLink(index) {
		$('#contentRightPanel .contentText').hide();
		
		$('.contentNavigation a').css('background-color', '#FFF');
		$('.contentNavigation a').css('color', '#777');
		$('.contentNavigation a:eq(' + index + ')').css('background-color', '#DD092F');
		$('.contentNavigation a:eq(' + index + ')').css('color', '#FFF');
		$('#contentRightPanel .contentText:eq(' + index + ')').fadeIn(1500, function() {
				$(this).siblings('a').hide();
		});
		//$(window).scrollTop(550);
		$('#contentRightPanel').css('z-index', 502);
		
	}

	function goBack() {
		$('#contentRightPanel').css('z-index', 500);
	}
	
	
	
	// this is for the staffs block in the leadership.php
	if($('#staffs')) {
		$('.shortDesc .readMore').click(function() {
			if($(this).parent('.shortDesc').siblings('.longDesc').find('.close').length == 0)  {
				$(this).parent('.shortDesc').siblings('.longDesc').prepend('<a class="close" href="javascript:" onclick="$(this).parent().fadeOut();">CLOSE X</a>');
			}
			$(this).parent('.shortDesc').siblings('.longDesc').fadeIn();	
		});	
	}
	


</script>

<style type="text/css">
		.topMenuHighLighter2{
			background: #AAA;
			padding:3px 10px 5px !important; 
		}
		#sliderBar{display:none}
		.about_submenu{display:none}
		.about_submenu a{font-size:12px !important}
		
</style>
