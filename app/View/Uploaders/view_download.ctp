<?php if($type == 'new'){ ?>
    <center style="font-size: 13px; margin: 20px;">Hệ thống đang cập nhật file mới, sau khoảng <span id="time" style="color:red">15</span>s bạn vào lại công trình để download file mới, xin cám ơn.</center>

<?php }else{ ?>
    <center id="center_1" style="font-size: 17px; margin: 20px; color: green;">Hệ thống đang cập nhật file với dữ liệu mới, sau khoảng <span id="time" style="color:red">15</span>s dữ liệu sẽ cập nhật thành công, xin cám ơn.</center>
    <center id="center_2" style="font-size: 13px; margin: 20px;">Bạn có thể click <a href="javascript:void(0)">Download</a> để tải file về máy tính sau khi 15s chạy hết.</center>
    <center id="center_3" style="display:none; font-size: 17px; margin: 20px;">Bạn có thể click <a href="/uploaders/download/233880">Download</a> để tải file về máy tính.</center>
    <script type="text/javascript">
        var cloud_setInterval = 15;
        $(function(){
            cloud_setInterval = setInterval("cloud_download_run_time()", 1000);
        });

        var cloud_time = 15;
        function cloud_download_run_time(){
            $("#time").text(cloud_time);
            if (cloud_time == 0){
                clearInterval(cloud_setInterval);
                $("#center_1").remove();
                $("#center_2").remove();
                $("#center_3").show();
            }
            cloud_time = cloud_time - 1;
        }
    </script>
<?php } ?>