<?php echo $this->Form->create($model, array('onsubmit' => 'return false')); ?>
<div style="margin-left: 60px;width: 250px;">
<table>
	<tr>
		<td colspan="2"><b>Tên thành viên</b> <em>(ví dụ: namn, namnb, ...)</em>: </td>
	</tr>
	<tr>
		<td><?php echo $this->Form->input("username", array( "size" => 20)); ?></td>
		<td><input type="submit" value=" Tìm " class="Button" onclick="search_uploaders_history();"></td>
	</tr>
</table>
</div>
<?php echo $this->Form->end(); ?>

<?php if(empty($datas)){ ?>
	<em><?php echo "(Hiện tại chưa có thông tin nào)"; ?> </em>
<?php }else{ ?>

	<?php $STT = 1;
			if( $this->request->params['paging'][$model]['page'] > 1 )
			{
				$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
			} ?>
	<table>
	<?php foreach( $datas as $key => $data ){ ?>
		<tr>
			<td>
				<b><?php echo $STT++; ?>.</b> [<img src="/img/icon/historydownload_danhsachloinhan.png" />]
				&nbsp;<b>[<?php if(isset($option_loai[$data[$model]["controller"]])){ echo $option_loai[$data[$model]["controller"]]; }else{ echo "<span title='".$data[$model]["controller"]."'>Khác</span>"; }?>]</b>
				&nbsp;[<?php if($data[$model]["nguoitao"] != 0){
							echo $this->element("avatar_view",array(
								"dataUser" => $data["Nguoitao"]
							));
						}else{
							echo $data[$model]["controller"];
						} ?>]
				&nbsp;[<?php echo $this->element("link_download_file", array(
								"id_file" => $data[$model]["id"],
								"name_file" => $data[$model]["name"],
								"dungluong_file" => $data[$model]["dungluong"] ));
						?>]
				<?php $time_created = strtotime($data[$model]["created"]);
					echo "<em>(".date("d/m/Y H:i", $time_created).")</em>";
				if( ($time_created) > strtotime(date("Y-m-d"))){ ?><img src="/img/icon_new.png" /><?php } ?><!-- KB-->
			</td>
		</tr>
	<?php } ?>
	</table>

	<?php echo $this->element( "paging", array( "update" => "historyuploadfiles" ) ); ?>

<?php } ?>