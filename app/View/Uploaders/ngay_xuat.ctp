<legend>Ngày xuất</legend>

<?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create($model, array( 'type' => 'file' )); ?>

<table class="CssTable">

<tr>
	<td>Ngày xuất (*):</td>
	<td style="width: 200px;">
		<?php echo $this->Form->input($model.'.ngay_xuat_tmp', array('style' => 'float: left; width: 100px;', 'readonly' => true));?>
		<?php echo $this->element('img_pop_calendar'); ?>
	</td>
</tr>

<tr>
	<td colspan="2" class="Center">
		<?php
			echo $this->Js->submit( 'Lưu ngày xuất', array(
				'before' => '$("#loading").show()',
				'class' => 'Button',
				'success' => 'handle_response(data, "ngay_xuat_'.$id.'");'
			));
		?>
	</td>
</tr>
</table>

<?php echo $this->Form->end(); ?>