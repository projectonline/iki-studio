<legend>Chọn file bên dưới đề đính kèm</legend>

<?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create($model, array( "type" => "file", "target" => "iframe-post-form", "onsubmit" => "if(kiem_tra_rong())beginUpload(); else return false;" )); ?>

<div id="update_submit_form_iframe" style="width:600px">


	<table class="CssTable">
		<tr>
			<td><b>STT</b></td>
			<td style="max-width:297px"><b>Tên File</b></td>
			<td><b>Attach</b></td>
			<td><b>Loại</b></td>
			<td><b>Download</b></td>
			<td></td>
		</tr>
		<?php

			App::uses('NumberHelper', 'View/Helper');
			$toReadableSize = new NumberHelper($this);

		   $STT = 1;
				if( $this->request->params['paging'][$model]['page'] > 1 )
				{
					$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
				}

			foreach( $datas as $data ){

				$dungluong = $toReadableSize->toReadableSize($data[$model]["dungluong"]);

		?>
		<tr id="<?php echo $model.'_'.$data[$model]['id']; ?>">
			<td><?php echo $STT++; ?></td>
			<td class="NgatDong">
				<?php echo $data[$model]["name"]; ?></a>
				<em>(<?php echo $dungluong; ?>)</em>
			</td>
			<td>
				<center>
					<?php echo $this->Form->input("Attach.stick", array(
							"type" => "checkbox",
							"label" => "",
							"onclick" => 'chon_attach_file_nay('.$data[$model]['id'].', this);'
					));?>
				</center>
			</td>
			<td>
				<span title="<?php echo $data[$model]['controller']; ?>">
					<?php if(isset($option_loais[$data[$model]["controller"]])){ echo $option_loais[$data[$model]["controller"]]; }else{ echo '(khác)'; } ?>
				</span>
			</td>
			<td>
				<div id="chon_attach_file_nay_div_<?php echo $data[$model]['id']; ?>" style="display:none">
					<span id="chon_attach_file_nay_<?php echo $data[$model]['id']; ?>">
						<br>
						<?php echo $this->element("link_download_file", array(
								"id_file" => $data[$model]["id"],
								"name_file" => wordwrap($data[$model]["name"], 10, "\n", true),
								"dungluong_file" => $dungluong
							));

							echo $this->Form->hidden("Attach.".$STT, array("value" => $data[$model]["id"]."_._".$data[$model]["name"]."_._".$dungluong ));
						?>
						<img style="cursor: pointer; width: 12px; height: 12px;" src="/img/icon/toggle_delete_phanhoi.png"
							onclick="xoa_chon_attach_file(<?php echo $data[$model]['id']; ?>)" title="Bỏ file này" />,&nbsp;
					</span>
				</div>
				<span><?php echo $this->element("link_download_file", array(
						"id_file" => $data[$model]["id"],
						"name_file" => 'download'
					)); ?>
				</span>
			</td>
			<td>
				<?php if( ($data[$model]["controller"] == 'users') || ( (strtotime($data[$model]['created']) + TGIAN_DUOC_PHEP_XOA_FILE) > strtotime('now') ) ){ ?>
				<a title="Xóa file upload này" href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]['id']; ?>)">Xóa</a>
				<?php } ?>
			</td>
		</tr>

		<?php } ?>
	</table>



	<?php echo $this->element( 'paging', array('update' => '#window_content_2') ); ?>

	<table>
		<tr>
			<td style="vertical-align:top"><b>Upload file mới</b>: </td>
			<td><?php echo $this->element( 'upload_progress' ); ?>
				<br>
				<div id="save_attach_file_ifame" style="display:none">

					<?php
						$tmp_arr = array(
							'phaply' => 'Pháp lý',
							'bienban' => 'Biên bản',
							'banve' => 'Bản vẽ',
							'khaosat' => 'Khảo sát',
							'bantinh' => 'Bản tính',
							'giaodich' => 'Thuyết minh - Dự toán',
							'congtrinhsbaocaotuan' => 'Báo cáo tuần',
							'congtrinhsbct' => 'Báo cáo tháng'
						);

						echo $this->Form->hidden($model.'.congtrinh_id', array('value' => $congtrinh_id));
					?>
						<span class="NotUser" <?php if( isset($this->data['Uploader']) && $this->data['Uploader']['model_loai'] == "User" ){ ?> style="display:none" <?php } ?>>

					<?php
						echo '<b>Công trình</b>: <span style="color:#3B5998">'.$congtrinh['Congtrinh']['tieude'].'</span>';

						if( $congtrinh_id == 246 )
						{
							echo ' <i style="color:#91797D">(lưu mặc định)</i>';
							$tmp_arr['User'] = 'Trang cá nhân';
						}

					?>
						<center><hr style="width:200px; margin:4px 0"></center>

						</span><!-- END class="NotUser"-->

					<?php

						echo $this->Form->input($model.'.model_loai', array(
							'empty' => '---',
							'div' => false,
							'options' => $tmp_arr,
							'onchange' => 'run_check_upload_vao_trangcanhan(this)',
							'label' => '<b>Trong mục</b> <span style="color:red">(*)</span>: ' , 'style' => 'width: 427px;height: 23px;'
						));
					?>
					<span class="NotUser" <?php if( isset($this->data['Uploader']) && $this->data['Uploader']['model_loai'] == "User" ){ ?> style="display:none" <?php } ?> >
					<?php
						echo '<br>Tiêu đề <span style="color:red">(*)</span>: ';
						echo $this->Form->input($model.'.tieude', array('style' => 'width:420px', 'onblur' => 'if($("#UploaderNoidung").val() == "")$("#UploaderNoidung").val($("#UploaderTieude").val())'));
						echo 'Nội dung <span style="color:red">(*)</span>: ';
						echo $this->Form->input($model.'.noidung', array('rows' => 2, 'style' => 'width:420px'));
					?>

					</span><!-- END class="NotUser"-->

					<div id="messageerror_iframe_upload" class="message_error" style="display:none">Vui lòng nhập đầy đủ các mục (*)</div>

					<div style="clear:both"></div>
					<button type="submit" class="Button" style="margin-top:10px">Lưu</button>
				</div>
			</td>
		</tr>
	</table>

	<div style="text-align:right;margin-right:30px;">Click <a href="javascript:void(0)" onclick="parent.$('#window_content_2').dialog('close');">đóng attach file</a> nếu chọn xong.</div>


</div>

<style>
	.NgatDong{
		word-break: break-all;
		width: 251px;
	}
</style>

<?php $this->Form->end(); ?>



<?php if( !isset($check_post) ){

	echo $this->element( "iframe_post_form", array("id_update_after_submit" => "update_submit_form_iframe", "contain_form" => "window_content_2") ); ?>



<script type="text/javascript">

	function run_check_upload_vao_trangcanhan(object)
	{
		if( $(object).val() == "User" )
		{
			$("#UploaderTieude").val(" ");
			$("#UploaderNoidung").val(" ");
			$(".NotUser").hide();
		}else{
			$(".NotUser").show();
		}
	}

	function kiem_tra_rong()
	{
		$("#messageerror_iframe_upload").hide();
		if($("#UploaderTieude").val() == "" || $("#UploaderNoidung").val() == "" || $("#UploaderModelLoai").val() == "" )
		{

			$("#messageerror_iframe_upload").fadeIn();

			tmp = parent.$("#contain_iframe_uploader");
			if( tmp.attr("id") == "contain_iframe_uploader" )
			{
				tmp.height($(document).height());
			}

			return false;
		}

		$("form#submit_uploader_iframe").submit();

		return true;
	}

</script>

<?php } ?>

