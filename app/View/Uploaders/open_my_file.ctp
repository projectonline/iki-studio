<?php echo $this->element( 'head' ); ?>
<?php echo $this->element( 'css' ); ?>
<?php echo $this->element( 'js' ); ?>
<?php echo $this->element( "window" ); ?>
<?php echo $this->element( 'main_loading' ); ?>
<div class="NoiDung" style="background-color:#FFFFFF">
    <table class="CssTable">
    <tr>
        <td>STT</td>
        <td>Tên File</td>
        <td>Click Chọn</td>
        <td>STT</td>
        <td>Tên File</td>
        <td>Click Chọn</td>
    </tr>
    <?php

if( count($datas) > 0){
       $STT = 1;
            if( $this->request->params['paging'][$model]['page'] > 1 )
            {
                $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
            }

        for( $k=0; $k < 10; $k++ ){

        if( !isset($datas[$k]) )break;
        $data = $datas[$k];
    ?>
    <tr>
        <td><b><?php echo $STT; ?></b></td>
        <td>
            <?php if( $data[$model]["dungluong"] > 1000 )
                {
                    $data[$model]["dungluong"] = floor($data[$model]["dungluong"]/1000)." KB";
                }else{ $data[$model]["dungluong"] .= " B"; }

                echo $this->element("link_download_file", array(
                    "id_file" => $data[$model]["id"],
                    "name_file" => $data[$model]["name"],
                    "dungluong_file" => $data[$model]["dungluong"] ));
            ?>
        </td>
        <td id="chon_attach_file_nay_td_click_<?php echo $data[$model]['id']; ?>">
            <center><a style="color:red" href="javascript:void(0)" title="Chọn file này" onclick="chon_attach_file_nay(<?php echo $data[$model]['id']; ?>)">chọn</a></center>
        </td>
        <td id="chon_attach_file_nay_td_<?php echo $data[$model]['id']; ?>" style="display:none">
            <span id="chon_attach_file_nay_<?php echo $data[$model]['id']; ?>">
                <br><?php echo $this->element("link_download_file", array(
                        "id_file" => $data[$model]["id"],
                        "name_file" => $data[$model]["name"],
                        "dungluong_file" => $data[$model]["dungluong"] ));

                    echo $this->Form->hidden("Attach.".$STT, array("value" => $data[$model]["id"]."_._".$data[$model]["name"]."_._".$data[$model]["dungluong"] ));
                ?>
                <img style="cursor: pointer; width: 12px; height: 12px;" src="/img/icon/toggle_delete_phanhoi.png"
                    onclick="xoa_chon_attach_file(<?php echo $data[$model]['id']; ?>)" title="Bỏ file này" />,&nbsp;
            </span>
        </td>

        <!--COT 2-->

        <?php $k += 1;

            if( !isset($datas[$k]) )
            {
                echo '<td></td><td></td><td></td>';
                break;
            }

            $data = $datas[$k];

        ?>
        <td><b><?php echo $STT+5; ?></b></td>
        <td>
            <?php if( $data[$model]["dungluong"] > 1000 )
                {
                    $data[$model]["dungluong"] = floor($data[$model]["dungluong"]/1000)." KB";
                }else{ $data[$model]["dungluong"] .= " B"; }

                echo $this->element("link_download_file", array(
                    "id_file" => $data[$model]["id"],
                    "name_file" => $data[$model]["name"],
                    "dungluong_file" => $data[$model]["dungluong"] ));
            ?>
        </td>
        <td id="chon_attach_file_nay_td_click_<?php echo $data[$model]['id']; ?>">
            <center><a style="color:red" href="javascript:void(0)" title="Chọn file này" onclick="chon_attach_file_nay(<?php echo $data[$model]['id']; ?>)">chọn</a></center>
        </td>
        <td id="chon_attach_file_nay_td_<?php echo $data[$model]['id']; ?>" style="display:none">
            <span id="chon_attach_file_nay_<?php echo $data[$model]['id']; ?>">
                <br><?php echo $this->element("link_download_file", array(
                        "id_file" => $data[$model]["id"],
                        "name_file" => $data[$model]["name"],
                        "dungluong_file" => $data[$model]["dungluong"] ));

                    echo $this->Form->hidden("Attach.".$data[$model]["id"], array("value" => $data[$model]["id"]."_._".$data[$model]["name"]."_._".$data[$model]["dungluong"] ));
                ?>
                <img style="cursor: pointer; width: 12px; height: 12px;" src="/img/icon/toggle_delete_phanhoi.png"
                    onclick="xoa_chon_attach_file(<?php echo $data[$model]['id']; ?>)" title="Bỏ file này" />,&nbsp;
            </span>
        </td>
        <!--END COT 2-->

    </tr>
    <?php $STT++;
        } ?>
    </table>
    <?php echo $this->element( 'paging' ); ?>
    <div style="text-align:right;margin:right:10px;">Click <a href="javascript:void(0)" onclick="javascript:self.close();">vào đây</a> nếu chọn xong</div>

<?php } ?>
    <!--END IF COUNT-->

    <!--UPLOAD FILE-->
    <?php echo $this->Form->create($model, array( 'onsubmit' => 'beginUpload()', 'type' => 'file' )); ?>
    <table>
        <tr>
            <td style="width: 436px;">
                <!--THONG TIN-->
                <?php echo $this->element("message"); ?>
                <table class="CssTable">
                    <tr>
                        <td style="width: 147px;"><b>Chọn công trình (*):</b></td>
                        <td><?php echo $this->element("congtrinhs_search", array("field_congtrinhsearch" => $model.".congtrinh",
                            "size" => 18, "thutu" => "upload_file_congtrinh" )); ?></td>
                    </tr>
                    <tr>
                        <td><b>Chọn loại (*):</b></td>
                        <td><?php echo $this->Form->input($model.".loai", array("options" => $loais, "label" => false) ); ?></td>
                    </tr>
                    <tr>
                        <td><b>Nội dung tóm tắt về file được upload (*):</b></td>
                        <td><?php echo $this->Form->input($model.".noidung", array("rows" => 3, "cols" => 20, "label" => false)); ?></td>
                    </tr>
                </table>
                <!--END THONG TIN-->
            </td>
            <td>
                <?php echo $this->element( "upload_progress" ); ?>
                <br><br>
                <center><button class="Button" type="submit">Lưu</button></center>
            </td>
        </tr>
    </table>
    <!--END UPLOAD FILE-->
    <?php echo $this->Form->end(); ?>
</div>
<script type="text/javascript">
    $(function(){
        $("#loading", window.opener.document).hide();
    });
</script>
<style>
    th, .TrHead{
        padding: 2px;
    }
    *{
        font-weight:none;
        font-size:12px;
    }
</style>