<fieldset>
    <div class="NoiDung">
        <?php echo $this->Form->create($model); ?>
            <?php echo $this->element("message"); ?>
            <table class="CssTable">
                <tr>
                    <td style="width: 126px;"><b>Chọn công trình (*):</b></td>
                    <td><?php echo $this->element("congtrinhs_search", array("field_congtrinhsearch" => $model.".congtrinh",
                        "size" => 18, "thutu" => "upload_file_congtrinh" )); ?></td>
                </tr>
                <tr>
                    <td><b>Chọn loại (*):</b></td>
                    <td><?php echo $this->Form->input($model.".loai", array("options" => $loais, "label" => false) ); ?></td>
                </tr>
                <tr>
                    <td><b>Nội dung tóm tắt về file được upload (*):</b></td>
                    <td><?php echo $this->Form->input($model.".noidung", array("rows" => 3, "cols" => 20, "label" => false)); ?></td>
                </tr>
                <tr>
                    <td>Upload File (*):</td>
                    <td><?php echo $this->element( "upload_progress" ); ?></td>
                </tr>
            </table><br>
            <center style="padding-left: 140px; padding-right: 83px;">
                <?php echo $this->Js->submit( "Lưu", array("class" => "Button",
                    "url" => "/uploaders/upload_file_congtrinh",
                    'success' => "handle_response_upload_file_congtrinh(data);", "style" =>"float:left" ));?>
                <button class="Button" onclick="$('#upload_file_congtrinh','#historyuploadfiles').slideUp();return false;">Đóng Lại</button>
            </center>
        <?php echo $this->Form->end(); ?>
    </div>
</fieldset>