<legend>Danh sách các lời nhắn quan trọng</legend>
<div style="max-width:550px">
	<?php foreach( $datas as $tmp ){
			$traloi = $tmp[$model];
			$traloi["Nguoitao"] = $tmp["Nguoitao"];
	?>
	<div rel_id="<?php echo $traloi['id']; ?>" class="div1NoiDungTraLoi" id="Comment_<?php echo $traloi['id']; ?>" onmouseover="$('span#xoa_traloi_ajax',this).show();" onmouseout="$('span#xoa_traloi_ajax',this).hide();">
		<div class="content_traloi <?php if( isset($traloi["quantrong"]) && $traloi["quantrong"] ){ ?>BackgroundImportant<?php } ?>" style="position:relative;">
			<?php echo $this->element("avatar_view",array(
					"created" => date('d/m/Y H:i', strtotime($traloi["created"])),
					"dataUser" => $traloi["Nguoitao"]
			)); ?>:
			<?php if( (strtotime($traloi["created"]) + 129600) > strtotime("now") ){ ?>
				<img src="/img/icon_new.png" />
			<?php }
			echo $this->Thumbnail->make_links_blank($traloi["noidung"]);
			if (strlen(trim($traloi["noidung_attach"])) > 0)
				echo '<br><b>Attached files:</b>'.$traloi["noidung_attach"];

			if(isset($admin) || AuthComponent::user('id') == $traloi["nguoitao"]){ ?>
				<span id="xoa_traloi_ajax" style="display:none">
					<em><a href="javascript:void(0)" onclick="xoa_ajax('Comment',<?php echo $traloi['id']; ?>,'<?php echo $model;?>')">Xóa</a></em>
				</span>
			<?php } ?>

			<?php if( isset($traloi["quantrong"]) && $traloi["quantrong"] ){ ?>
				<img class="StarImportant" src="/img/admin_star.png">
			<?php } ?>
		</div>
	</div>
	<?php } ?>
</div>
