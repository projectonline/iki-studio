<div style="margin:5px">
	<?php echo $this->Form->input('Sms.checkbox', array(
			'type' => 'checkbox',
			'label' => 'Gửi SMS',
			'onclick' => 'send_text_as_sms(this.checked)'
	));?>

</div>
<div style="<?php if( !isset($this->data['Sms']) || $this->data['Sms']['checkbox'] == 0 ){ ?>display: none<?php } ?>" id="Sms_text_div">
<?php echo $this->Form->input('Sms.to', array(
		'style' => 'width:200px;margin-bottom: 4px;',
		'label' => 'đến (*): ',
		'placeholder' => 'thotv, tongna, a8',
		'div' => false
));?><em>vd: thotv, tongna, a8</em>
<?php echo $this->Form->input('Sms.text', array(
			'rows' => 2,
			'style' => 'width:543px;',
			'maxlength' => 160,
			'onchange' => 'count_send_text_length()'
));?>
<em>(giới hạn <span id="sms_show_charater_type" style="color:red">160</span>/160 kí tự 1 tin nhắn qua điện thoại)</em>
</div>




<script type="text/javascript">

	function locdau(str)
	{
		//str= str.toLowerCase();

		// Chữ Thường
		str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
		str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
		str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
		str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
		str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
		str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
		str= str.replace(/đ/g,"d");

		// Chữ Hoa
		str= str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g,"a");
		str= str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g,"e");
		str= str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g,"i");
		str= str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g,"o");
		str= str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g,"u");
		str= str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g,"y");
		str= str.replace(/Đ/g,"d");

		//str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");
		/* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
		str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
		str= str.replace(/^\-+|\-+$/g,"");

		//cắt bỏ ký tự - ở đầu và cuối chuỗi
		return str;
	}

	<?php if( empty( $this->data ) ){ ?>
	var SMS_da_thaydoi_smstext = true;
	<?php }else{ ?>
	var SMS_da_thaydoi_smstext = false;
	<?php } ?>

	$(function(){

		$('#SmsText').on('keyup propertychange paste', function(){

			$("#sms_show_charater_type").html($("#SmsText").val().length);

			SMS_da_thaydoi_smstext = false;
		});

		$('#CommentNoidung').on('keyup propertychange paste', function(){

			if( SMS_da_thaydoi_smstext && $("#SmsCheckbox").is(":checked") )
			{
				send_text_as_sms_auto_type();
			}
		});

		$("#sms_show_charater_type").html($("#SmsText").val().length);

	});

	function send_text_as_sms_auto_type()
	{
		var text = $("#SmsText");

		var prefix = "<SMS qlda> <?php echo AuthComponent::user('username'); ?>: ";

		// Trim nội dung -> lọc dấu -> chỉ lấy 160 kí tự đầu tiên SMS
		var string = locdau($.trim($("#CommentNoidung").val()));

		string = prefix + string;
		text.val( string.substr( 0, 160) );

		$("#sms_show_charater_type").html($("#SmsText").val().length);
	}

	function send_text_as_sms(checked)
	{
		$("#Sms_text_div").slideToggle();

		if( checked )
		{
			var text = $("#SmsText");

			var prefix = "<SMS qlda> <?php echo AuthComponent::user('username'); ?>: ";

			if( $.trim(text.val()) == "" || text.val() == prefix )
			{
				// Trim nội dung -> lọc dấu -> chỉ lấy 160 kí tự đầu tiên SMS
				var string = locdau($.trim($("#CommentNoidung").val()));

				string = prefix + string;
				text.val( string.substr( 0, 160) );
			}
		}

		$("#sms_show_charater_type").html($("#SmsText").val().length);
	}

	// function count(){
	//   var txtVal = $('textarea').val();
	//   var words = txtVal.trim().replace(/\s+/gi, ' ').split(' ').length;
	//   var chars = txtVal.length;
	//   if(chars===0){words=0;}
	//   $('#counter').html('<br>'+words+' words and '+ chars +' characters');
	// }
	// count();

	// $('textarea').on('keyup propertychange paste', function(){
	//     count();
	// });
</script>