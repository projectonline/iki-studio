<legend><?php if( isset($get_thongtin_title[$model]) )echo $get_thongtin_title[$model];?></legend>
<div class="item" id="<?php echo $model."_".$data[$model]['id']; ?>" style="width:600px;">
	<div class="from">
		<?php $link_img = '/img/icon/'.$controller.'_danhsachloinhan.png';
				if( !file_exists(APP.'webroot'.$link_img) )
				{
					$link_img = '/img/icon/tasks_danhsachloinhan.png';
				}
		?>
		<img src="<?php echo $link_img; ?>" />
		<?php echo $this->element("avatar_view",array(
				"created" => date("d/m/Y H:i", strtotime($data[$model]["created"])),
				"dataUser" => $data["Nguoitao"]
		));?>
		<?php if( (strtotime($data[$model]["created"]) + 129600) > strtotime("now")){ ?>
			<img src="/img/icon_new.png" />
		<?php } ?>
	</div>
	<div class="content">
		<?php
		echo $this->Thumbnail->make_links_blank($data[$model]["noidung"]);
		if (strlen(trim($data[$model]["noidung_attach"])) > 0)
			echo '<br><b>Attached files:</b>'.$data[$model]["noidung_attach"];
		 ?>
	</div>

	<?php if( isset($data['Congtrinh']) ){ ?>
	<br><b><span style="color:blue">Công trình:</span> <a href="/congtrinhs/view/<?php echo $data['Congtrinh']['id']; ?>"><?php echo $data['Congtrinh']['tieude']; ?></a></b>
	<?php } ?>

	<div id="morong" style="text-align: center;">
		<a id="xemthem" href="javascript:void(0);" title="Để gọn, thông báo chỉ hiện <b>một phần</b> nhỏ, <br>click để xem toàn bộ nội dung">... Mở rộng ...</a>
	</div>

	<div class="ThemMoiLichCongViec">
		<?php echo $this->Js->link( "Trả lời", "/comments/them/".$data[$model]["id"].'/'.$model.'/xemthem_thongtin_',
				array( "update" => "#window_content_3",
					   'before' => '$(\'#loading\').show()',
					   'success' => "$(\"#loading\").hide();window_show_3();",
					   "title" => "Trả lời lời nhắn" ) ); ?>
	</div>

	<!--COMMENT-->
	<?php echo $this->element('../Comments/show_comment', array('data' => $data, 'div_update_more' => 'xemthem_thongtin_')); ?>
	<!--END COMMENT-->

</div>

<script type="text/javascript">
	$(function(){ $("#window_content").tooltip();});
</script>
