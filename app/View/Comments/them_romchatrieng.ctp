<legend>Thêm thảo luận</legend>
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create() ?>
<h3>Thêm thảo luận (*):</h3>
<div style="height:5px;">&nbsp;</div>
<?php echo $this->Form->input("Comment.noidung", array(
      "rows" => 10,
      "style" => 'width:543px;'
));?>
<div style="text-align: center; margin-top: 4px">
  <?php
    echo $this->Js->submit( "Lưu", array(
      'before' => '$("#loading").show()',
      "class" => "Button",
      'success' => "handle_response_comment(data, '".$update_div."');"
    ));
  ?>
</div>
<?php echo $this->Form->end(); ?>
