<legend>Sửa trả lời</legend>
<?php
  echo $this->Session->flash();
  echo $this->Form->create('Comment');
?>
  <h3><img src="/img/icon/tasks_danhsachloinhan.png" />&nbsp;Nội dung trả lời (*):</h3>
  <div style="height:5px">&nbsp;</div>
  <?php echo $this->Form->input("noidung", array(
        "rows"  => 10,
        "style" => 'width:543px;'
      )) ?>
  <?php echo $this->Form->hidden('congtrinh_id', array("value" => (isset($congtrinh_id)?$congtrinh_id:0))) ?>
  <?php echo $this->Form->hidden('item_id') ?>
  <?php if($model == "Task"){ ?>
  <div style="margin:5px">
    <?php echo $this->Form->input('quantrong', array("type" => "checkbox", "label" => "Thông tin quan trọng"));?>
  </div>
  <?php } ?>

  <div style="text-align: center; margin-top: 4px">
  <?php
    echo $this->Js->submit('Lưu', array(
      'before'  => '$("#loading").show()',
      'class'   => 'Button',
      'success' => "$('#Comment_noidung_".$id."').html(data);$('#loading').hide();$('#window_content').dialog('close');"
    ));
  ?>
  </div>
  <?php
    if(isset($congtrinh_id) && $congtrinh_id > 0)
      echo $this->element("upload_attach_file_congtrinh").'<br>';
    echo $this->element("upload_attach_file");

  echo $this->Form->end();
  if($model == 'Task'){ ?>
    <span id="hide_send_all">&nbsp;</span>
<?php } ?>
