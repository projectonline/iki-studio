<div id="headerArea"><h2>Danh sách trả lời:</h2></div>

<?php echo $this->Form->create($model); ?>
<?php echo $this->Session->flash(); ?>

<div style="clear:both; height: 26px;">

	<div style="float:left">
	<?php echo $this->Form->input('q_searchall', array('label' => '<span style="color:green">Tìm ở tất cả các mục</span>', 'type' => 'checkbox' ));?>
	</div>

	<div style="float:right">
	<?php echo $this->Js->submit( "Tìm", array(
			'before' => '$(\'#loading\').show()',
			"class" => "Button",
			'update' => "#window_content",
			"div" => false
	));?>
	</div>

</div>

<table class="Data" style="width:540px">
	<tr>
		<td>STT</td>
		<td style="width:77px">Người tạo</td>
		<td>Nội dung</td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo $this->Form->input('q_nguoitao', array('label' => false, 'style' => 'width:90%', 'placeholder' => 'vd:namnb' ));?></td>
		<td><?php echo $this->Form->input('q_noidung', array('label' => false, 'style' => 'width:98%' ));?></td>
	</tr>

	<?php $STT = 1;
	if( $this->request->params['paging'][$model]['page'] > 1 )
	{
		$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
	} ?>

	<?php foreach($datas as $data){ ?>
	<tr>
		<td>
			<?php echo $STT++;
				if( (strtotime($data[$model]["modified"]) + 259200) > strtotime("now") ){ ?>
				<img src="/img/icon_new.png" />
			<?php } ?>
		</td>
		<td>
			<?php echo $this->element("avatar_view",array(
					"dataUser" => $data["Nguoitao"]
			)); ?>
		</td>
		<td style="background-color: #EDEFF4;">
			<?php echo $this->element("avatar_view",array(
					"created" => date('d/m/Y H:i', strtotime($data[$model]["created"])),
					"dataUser" => $data["Nguoitao"]
			)); ?>:
			<?php
				echo $this->Thumbnail->make_links_blank($data[$model]["noidung"]);
				if (strlen(trim($data[$model]["noidung_attach"])) > 0)
					echo '<br><b>Attached files:</b>'.$data[$model]["noidung_attach"];
			 ?>
		</td>
	</tr>
	<?php } ?>

</table>

<?php echo $this->element('paging_search', array('show_info' => 'trả lời', 'update' => '#window_content')); ?>
<?php echo $this->Form->end(); ?>

<script type="text/javascript">
	$(function(){
		$('#loading').hide();
	});
	function xem_them_tin()
	{
		var model_id = $("#model_id").val();
		var page = $("#page").val();

		$.ajax({
			beforeSend:function (XMLHttpRequest) {
				$('#loading').show();
				$("#model_id", "#comments_index").remove();
				$("#page", "#comments_index").remove();
			},
			success:function (data, textStatus) {
				$("#loading").hide();
				$("#comments_index tbody").append(data);
				$(this).remove();
			},
			url:"/comments/index/" + page + "/" + model_id
		});
	}
</script>
