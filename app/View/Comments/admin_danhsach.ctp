<div class="traloi">
<?php $STT = 1;
    if( $this->request->params['paging']['Comment']['page'] > 1 )
    {
         $STT = ($this->request->params['paging']['Comment']['page']-1)*$this->request->params['paging']['Comment']['limit'] + 1;
    }

    foreach( $data as $traloi ){
        $style = 'border:1px dashed #7916E7;background-color:#DBE4FB;';

    ?>
    <?php if($traloi['Comment']['tab'] != 6 || ($traloi['Comment']['tab'] == 6 && isset($admin))){ ?>
    <div rel_id="<?php echo $traloi['Comment']['id'] ?>" class="div1NoiDungTraLoi" id="Comment_<?php echo $traloi['Comment']['id'] ?>">
        <div class="ContainLinkDeleteComment ContainLinkHover content_traloi <?php if( isset($traloi['Comment']["quantrong"]) && $traloi['Comment']["quantrong"] ){ ?>BackgroundImportant<?php } ?>">
            <?php
            if(isset($traloi["Nguoitao"])){
                echo $this->element("avatar_view",array(
                        "created" => date('ymd H:i', strtotime($traloi['Comment']["created"])),
                        "dataUser" => $all_user_qlda[$traloi["Nguoitao"]["id"]]
                ));
            }else{
                echo $this->element("avatar_view",array(
                        "created" => date('ymd H:i', strtotime($traloi['Comment']["created"])),
                        "dataUser" => $all_user_qlda[$traloi["Nguoitao"]["id"]]
                ));
            }
            ?>:
            <?php if( (strtotime($traloi['Comment']["created"]) + 129600) > strtotime("now") ){ ?>
                <img src="/img/icon_new.png" />
            <?php } ?>

            <span id="Comment_noidung_<?php echo $traloi['Comment']["id"] ?>">
        <?php
            echo $this->Thumbnail->make_links_blank($traloi['Comment']["noidung"]);
            if (isset($traloi['Comment']["noidung_attach"]) && strlen(trim($traloi['Comment']["noidung_attach"])) > 0)
                echo '<br><b>Attached files:</b>'.$traloi['Comment']["noidung_attach"]; ?>
            </span>

            <span id="Comment_file_<?php echo $traloi['Comment']["id"] ?>">
            <?php if( isset($traloi['File'][0]) ){
                    foreach ($traloi['File'] as $key => $value) {
                        echo '<br><img src="/img/icon/file_upload.gif" />';
                        echo $this->element("link_download_file", array(
                           "id_file" => $value["id"],
                           "name_file" => $value["name"],
                           "dungluong_file" => $value["dungluong"] ));
                    }

                } ?>
            </span>
            <?php if(isset($admin) || (AuthComponent::user('id') == $traloi["Nguoitao"]["id"] && (strtotime($traloi['Comment']["created"]) + 3600) > strtotime('now') )){ ?>
            <em class="LinkDeleteComment">
            <a href="javascript:void(0)" onclick="xoa_ajax('Comment',<?php echo $traloi['Comment']['id'] ?>,'<?php echo $model ?>')">Del</a>
            </em>
            <?php } ?>
        </div>

    </div>
    <?php } ?>
    <?php } ?>
</div>

<?php echo $this->element( "paging", array( "update" => $div_update ) ) ?>