<div class="traloi"> <!--  id="<?php //if(isset($div_update_more))echo $div_update_more ?>Comment_<?php //echo $model.'_'.$data[$model]['id'] ?>">  -->
<?php $STT = 1;
    if( $this->request->params['paging'][$model]['page'] > 1 )
    {
         $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
    }
?>
    <?php

    $check_border_before = false; // check cho truong hop 2 border lien tuc
    $cho_xoa_comment_user_dau_tien = 0;
    foreach( $data as $traloi ){
        $style = 'border:1px dashed #7916E7;background-color:#DBE4FB;';
        $check_border = false;
        if(isset($div_update_more) && isset($tatca_giaoviec) && in_array($traloi['Comment']['id'], $tatca_giaoviec)){
            $check_border = true;

            if( $check_border_before )
            {
                $style .= 'border-top:none';
            }
            $check_border_before = true;
        }else{
            $check_border_before = false;
        }
    ?>
    <?php if($traloi['Comment']['tab'] != 6 || ($traloi['Comment']['tab'] == 6 && isset($admin))){ ?>
    <div rel_id="<?php echo $traloi['Comment']['id'] ?>" <?php if($check_border){ echo 'style="'.$style.'"'; } ?> class="div1NoiDungTraLoi" id="Comment_<?php echo $traloi['Comment']['id'] ?>">
        <div class="ContainLinkDeleteComment ContainLinkHover content_traloi <?php if( isset($traloi['Comment']["quantrong"]) && $traloi['Comment']["quantrong"] ){ ?>BackgroundImportant<?php } ?>">
            <?php
            if(isset($traloi["Nguoitao"])){
                echo $this->element("avatar_view",array(
                        "created" => date('d/m/Y H:i', strtotime($traloi['Comment']["created"])),
                        "dataUser" => $all_user_qlda[$traloi["Nguoitao"]["id"]]
                ));
            }else{
                echo $this->element("avatar_view",array(
                        "created" => date('d/m/Y H:i', strtotime($traloi['Comment']["created"])),
                        "dataUser" => $all_user_qlda[$traloi["Nguoitao"]["id"]]
                ));
            }
            ?>:
            <?php if( (strtotime($traloi['Comment']["created"]) + 129600) > strtotime("now") ){ ?>
                <img src="/img/icon_new.png" />
            <?php } ?>

            <span id="Comment_noidung_<?php echo $traloi['Comment']["id"] ?>">
        <?php
            echo $this->Thumbnail->make_links_blank($traloi['Comment']["noidung"]);
            if (isset($traloi['Comment']["noidung_attach"]) && strlen(trim($traloi['Comment']["noidung_attach"])) > 0)
                echo '<br><b>Attached files:</b>'.$traloi['Comment']["noidung_attach"]; ?>
            </span>

            <span id="Comment_file_<?php echo $traloi['Comment']["id"] ?>">
            <?php if( isset($traloi['File']['id']) ){
                    echo '<br><img src="/img/icon/file_upload.gif" />';
                    echo $this->element("link_download_file", array(
                        "id_file" => $traloi['File']["id"],
                        "name_file" => $traloi['File']["name"],
                        "dungluong_file" => $traloi['File']["dungluong"] ));
                } ?>
            </span>
        <?php
            //diult them ngay 25/11/2013
            if(isset($kiemtra) && $kiemtra ==1)
            {
                if(in_array(AuthComponent::user('id'), array(4, 138,351)) && $data[$model]['quantrong'] == 0)
                {
                    if($count_quantrong > 0)
                    {?>
                        <?php

                        echo $this->Js->link( "Chuyển về tin nhắn chính", "/comments/sua/".$congtrinh_id.'/'.$data[$model]["id"].'/'.$model.'/'.$traloi['Comment']['id'].'/'.$data[$model]['comment_count'],
                            array( 'before' => '$(\'#loading\').show()',
                                "update" => "#window_content",
                                    "title" => "Chuyển về tin nhắn chính",
                                   'success' => "$(\"#loading\").hide();window_show();"
                            ) );
                    }
                }

                if((isset($admin) || AuthComponent::user('id') == $traloi["Nguoitao"]["id"]) && ($congtrinh_id == 1857)){ ?>
                    <?php if($traloi['Comment']["public_cdt"] == 1 ){ ?>
                    <img title="Không public ra ngoài" id="comment_img_<?php echo $traloi['Comment']["id"] ?>" src="/img/congviec_moingay.png" style=" height: 15px;width: 15px;opacity:0.3;" onclick="chon_comment_view(<?php echo $traloi['Comment']["id"] ?>,<?php echo $data[$model]['id'] ?>, 0)" />
                    <?php }else{ ?>
                    <img title="Public ra ngoài" id="comment_img_<?php echo $traloi['Comment']["id"] ?>" src="/img/congviec_moingay.png" style=" height: 15px;width: 15px;" onclick="chon_comment_view(<?php echo $traloi['Comment']["id"] ?>,<?php echo $data[$model]['id'] ?>, 1)" />
                <?php }
                }
            }// diult ket thuc thenm code ngay 25/11/2013
        ?>

        <?php

            // namnb: theo Kaize id=784, kiểm tra không cho xóa comment nếu đã có người nhắn tiếp rồi
            if(isset($admin) || !$cho_xoa_comment_user_dau_tien || $cho_xoa_comment_user_dau_tien == $traloi["Nguoitao"]["id"]){
                if(isset($admin) || AuthComponent::user('id') == $traloi["Nguoitao"]["id"]){
                    if(($model == 'Loinhanketoan' && $data['Loinhanketoan']['trangthai'] == 5) || ($model == 'Loinhanpkinhdoanh' && $data['Loinhanpkinhdoanh']['trangthai'] == 5) || ($model == 'Loinhanvibim' && $data['Loinhanvibim']['trangthai'] == 5)){
                    $bien_tmp = $model;
                    $model = $model."_noibo" ?>
                    <em class="LinkDeleteComment"><a href="javascript:void(0)" onclick="xoa_ajax('Comment',<?php echo $traloi['Comment']['id'] ?>,'<?php echo $model ?>')">Del</a></em>
                    <?php
                        $model = $bien_tmp;
                    }elseif( (strtotime($traloi['Comment']["created"]) + 3600) > strtotime('now')  || in_array(AuthComponent::user('id'), array(1,2,3))){ // Kaizen: khaitt: 1h thì tin của người đó đăng lên sẽ được khóa lại  ?>
                    <em class="LinkDeleteComment">
                        <?php /*if(AuthComponent::user('id') == $traloi["Nguoitao"]["id"]){ ?>
                        <!-- <a href="javascript:void(0)" onclick="sua_comment_ajax(<?php echo $traloi['Comment']['id'] ?>, '<?php echo $model ?>')">Edit</a>&nbsp;|&nbsp; -->
                        <?php }*/ ?>
                        <a href="javascript:void(0)" onclick="xoa_ajax('Comment',<?php echo $traloi['Comment']['id'] ?>,'<?php echo $model ?>')">Del</a>
                    </em>
                    <?php }
                }
            }
            $cho_xoa_comment_user_dau_tien = $traloi["Nguoitao"]["id"]; ?>

            <!--LIKE-->
            <?php if( !isset($div_update_more) && $model == 'Loinhanngoaikhoavt' ){ ?>

            <?php echo "<div id=\"Comment_liked_".$traloi['Comment']["id"]."\" class=\"ClickLikeDiv\" style=\"font-size: 11px;margin-top: 3px;\">";

                $string_like = "";
                if( !($traloi['Comment']["like"] > 0) )
                    $traloi['Comment']["like"] = 0;

                $string_like_title = "";
                if( AuthComponent::user('id') == 4 )
                {
                    $string_like_title = 'title="'.str_replace('_._', ' ', $traloi['Comment']["like_people"]).'"';
                }
                $string_like = '<span '.$string_like_title.' class="LikeCount">Có <span class="NumCount">'.$traloi['Comment']["like"].'</span> người thích</span> ';

                if ( !in_array( AuthComponent::user('username'), explode("_._", $traloi['Comment']["like_people"]) ) ) {
                    echo $string_like."<a title=\"Thích nội dung này\" class=\"ClickLike\" href=\"javascript:void(0)\" onclick=\"click_like('Comment', ".$traloi['Comment']["id"].", ".$traloi['Comment']["like"].", 1, 'comments')\">Thích</a>";
                }else{
                    echo $string_like."<a title=\"Bỏ thích nội dung này\" class=\"ClickLike\" href=\"javascript:void(0)\" onclick=\"click_like('Comment', ".$traloi['Comment']["id"].", ".$traloi['Comment']["like"].", 0, 'comments')\">Bỏ thích</a>";
                }
                echo "</div>";
            ?>

            <?php } ?>
            <!--END LIKE-->

            <?php if( isset($traloi['Comment']["quantrong"]) && $traloi['Comment']["quantrong"] ){ ?>
            <img class="StarImportant" src="/img/admin_star.png">
            <?php } ?>
        </div>
        <?php if($check_border && isset($tatca_giaoviec) && isset($ktr)){ ?>
            <div id="Giaoviec_Quantrong_<?php echo $ktr[$traloi['Comment']['id']];?>" style="margin-top: -15px;">
                 <br/> <a href="javascript:void(0)" onclick="confirm_comment_quantrong('<?php echo $ktr[$traloi['Comment']['id']];?>');">Quan trọng</a>
            </div>
        <?php }?>
    </div>
    <?php } ?>
    <?php } ?>
</div>

<?php if(isset($data[$model]['comment_count']) && isset($data['Comment']) && $data[$model]['comment_count'] > count($data['Comment']) && isset($traloi['Comment']['id']) ){ ?>
<div class="TRALOILOINHAN">

    <?php if( isset($data[$model]['comment_count_qt']) && $data[$model]['comment_count_qt'] > 0 )
        {

            echo $this->Js->link( $data[$model]['comment_count_qt'].'<img style="width:10px;" src="/img/admin_star.png">',
                "/comments/all_comment_quantrong/" . $model ."/". $data[$model]['id'],
                array(
                    'before' => '$(\'#loading\').show()',
                    "update" => "#window_content_4",
                    "title" => "Xem tất cả trả lời quan trọng",
                    'success' => "$(\"#loading\").hide();window_show_4();$('#window_content_4').parent('div').css('left', 450 + Math.floor(($(window).width() - 1060)/2));",
                    "escape" => false
           ) )."&nbsp;/&nbsp;";

        } ?>

    <?php
        $string_title_count_cm = '';
        if(isset($data[$model]['num_cm_per_department']) && $this->Session->check("phongban_ALL") )
        {
            $phongban_ALL = $this->Session->read("phongban_ALL");
            $tmps = explode('_._', $data[$model]['num_cm_per_department']);

            $string_title_count_cm = '<b>Thống kê số lượng comment:</b><br>';

            $k = 1;
            foreach( $tmps as $tmp )
            {
                $tmp = explode('_', $tmp);

                if( isset($phongban_ALL[$tmp[0]]) && strlen($tmp[0]) > 1 )
                {
                    $string_title_count_cm .= $k.'. '.$phongban_ALL[$tmp[0]].': '.$tmp[1].'<br>';
                }
                $k += 1;
            }
        }


    ?>
    <?php if(isset($kiemtra) && ($kiemtra == 1) && ($count_quantrong > 0) && ($data[$model]['quantrong'] == 0)) {?>


    <?php // diult chinh sua ngay 25/11/2013 ?>

    <a rel2="<?php echo ($data[$model]['comment_count'] - count($data['Comment'])) ?>"
        id="<?php if(isset($div_update_more))echo $div_update_more ?>Comment_<?php echo $model.'_'.$data[$model]['id'] ?>_link_view_more"
        href="javascript:void(0)" onclick="xemthem_comments(<?php echo $traloi['Comment']['id'] ?>, '<?php echo $model ?>', <?php echo $data[$model]['id'] ?>,'<?php echo $kiemtra ?>','<?php echo $congtrinh_id ?>'<?php if(isset($div_update_more))echo ', \''.$div_update_more.'\'' ?>)"
        title="<?php echo $string_title_count_cm ?>">More... (<?php echo ($data[$model]['comment_count'] - count($data['Comment'])) ?>)
    </a>
    <?php }elseif(isset($kiemtra) && ($kiemtra == 1)) { ?>

    <a rel2="<?php echo ($data[$model]['comment_count'] - count($data['Comment'])) ?>"
        id="<?php if(isset($div_update_more))echo $div_update_more ?>Comment_<?php echo $model.'_'.$data[$model]['id'] ?>_link_view_more"
        href="javascript:void(0)" onclick="xemthem_comments(<?php echo $traloi['Comment']['id'] ?>, '<?php echo $model ?>', <?php echo $data[$model]['id'] ?>,'<?php echo $kiemtra ?>','<?php echo $congtrinh_id ?>'<?php if(isset($div_update_more))echo ', \''.$div_update_more.'\'' ?>)"
        title="<?php echo $string_title_count_cm ?>">More... (<?php echo ($data[$model]['comment_count'] - count($data['Comment'])) ?>)
    </a> /
    <a id="<?php if(isset($div_update_more))echo $div_update_more ?>Comment_<?php echo $model.'_'.$data[$model]['id'] ?>_link_view_more"
        href="javascript:void(0)" onclick="xemthem_comments_hienhet(<?php echo $traloi['Comment']['id'] ?>, '<?php echo $model ?>', <?php echo $data[$model]['id'] ?>,'<?php echo $kiemtra ?>','<?php echo $congtrinh_id ?>'<?php if(isset($div_update_more))echo ', \''.$div_update_more.'\'' ?>)"
        title="<?php echo $string_title_count_cm ?>">Hiện hết
    </a>
    <?php }else{ ?>
    <a rel2="<?php echo ($data[$model]['comment_count'] - count($data['Comment'])) ?>"
        id="<?php if(isset($div_update_more))echo $div_update_more ?>Comment_<?php echo $model.'_'.$data[$model]['id'] ?>_link_view_more"
        href="javascript:void(0)" onclick="xemthem_comments(<?php echo $traloi['Comment']['id'] ?>, '<?php echo $model ?>', <?php echo $data[$model]['id'] ?>,'0','0'<?php if(isset($div_update_more))echo ', \''.$div_update_more.'\'' ?>)"
        title="<?php echo $string_title_count_cm ?>">More... (<?php echo ($data[$model]['comment_count'] - count($data['Comment'])) ?>)
    </a>
    <?php }?>

    <?php // diult ket thuc chnh sua , them code ngay 25/11/2013?>
    <span id="<?php if(isset($div_update_more))echo $div_update_more ?>Comment_<?php echo $model.'_'.$data[$model]['id'] ?>_link_view_more_search_comment" style="display:none">
        &nbsp;/&nbsp;<?php echo $this->Js->link( "Tìm kiếm",
                    "/comments/search/" . $model ."/". $data[$model]['id'],
                    array(
                        'before' => '$(\'#loading\').show()',
                        "update" => "#window_content",
                        "title" => "Tìm kiếm trả lời",
                        'success' => "$(\"#loading\").hide();window_show();"
               ) );
        ?>
    </span>
</div>
<?php } ?>
<?php echo $this->element( "paging", array( "update" => $div_update ) ) ?>