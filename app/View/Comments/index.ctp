<?php if( !isset($ajax) ){ ?>
<div id="headerArea"><h2>Danh sách tin mới:</h2></div>

<center>
	<table id="comments_index" class="Data" style="width:720px">
		<tr>
			<td>STT</td>
			<td>Nội dung</td>
			<td style="width: 72px;">Ngày</td>
			<td style="width: 72px;">&nbsp;</td>
		</tr>
<?php } ?>


		<?php $STT = ($page - 1)*50 + 1; ?>
		<?php foreach($datas as $data){ ?>
		<tr>
			<td>
				<?php echo $STT++;
					if( (strtotime($data[$model]["modified"]) + 259200) > strtotime("now") ){ ?>
					<img src="/img/icon_new.png" />
				<?php } ?>
			</td>
			<td>
				<?php echo $this->element("avatar_view",array(
						"created" => date('d/m/Y H:i', strtotime($data[$model]["created"])),
						"dataUser" => $data["Nguoitao"]
				)); ?>:
				<?php
					echo $this->Thumbnail->make_links_blank($data[$model]["noidung"]);
					if (strlen(trim($data[$model]["noidung_attach"])) > 0)
						echo '<br><b>Attached files:</b>'.$data[$model]["noidung_attach"];
				?>
			</td>
			<td><?php echo date('d/m/Y H:i', strtotime($data[$model]["created"])); ?></td>
			<td>
				<?php echo $this->Js->link( "Xem chi tiết", "/comments/index_full_thongtin/".$data[$model]["item_id"].'/'.$data[$model]["model"].'/xemthem_thongtin_',
					array( "update" => "#window_content",
					   'before' => '$(\'#loading\').show()',
					   'success' => "$(\"#loading\").hide();window_show();check_morong();",
					   "title" => "Trả lời lời nhắn" ) ); ?>

				<?php if( $STT == ($page*50 + 1) ){ ?>
				<input id="model_id" type="hidden" value="<?php echo $data[$model]["id"]; ?>" />
				<input id="page" type="hidden" value="<?php echo $page + 1; ?>" />
				<?php } ?>
			</td>
		</tr>
		<?php } ?>


	<?php if( !isset($ajax) ){ ?>
	</table>
	<?php } ?>

	<a href="javascript:void(0)" onclick="xem_them_tin()" title="Xem thêm các tin cũ hơn">Xem thêm các tin cũ hơn</a>

<?php if( !isset($ajax) ){ ?>
</center>

<script type="text/javascript">
	function xem_them_tin()
	{
		var model_id = $("#model_id").val();
		var page = $("#page").val();

		$.ajax({
			beforeSend:function (XMLHttpRequest) {
				$('#loading').show();
				$("#model_id", "#comments_index").remove();
				$("#page", "#comments_index").remove();
			},
			success:function (data, textStatus) {
				$("#loading").hide();
				$("#comments_index tbody").append(data);
				$(this).remove();
			},
			url:"/comments/index/" + page + "/" + model_id
		});
	}
</script>

<?php } ?>
