<legend>Reply</legend>

<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create($model, array('type' => 'file', 'id' => 'comment_attach_file')); ?>
<?php
    $link_img = '/img/icon/tasks_danhsachloinhan.png';
?>
<h3><img src="<?php echo $link_img;?>" />&nbsp;Content (*):</h3>
<div style="height:5px;">&nbsp;</div>
<?php echo $this->Form->input("Comment.noidung", array(
            "rows" => 10,
            "style" => 'width:543px;'
)); ?>

<table class="CssTable" border="0" cellpadding="2" cellspacing="2">
    <tr>
        <td>Tab (*):</td>
        <td><?php echo $this->Form->input('Comment.tab', array('style' => 'width:156px', 'options' => $tabs ));?></td>
    </tr>
    <tr>
        <td>File :</td>
        <td><input type="file" name="data[File][]" multiple="multiple" />
          <div id="progress" style="display:none">
            <div id="bar"></div >
            <div id="percent">0%</div >
        </td>
    </tr>
</table>
<div style="text-align: center; margin-top: 4px">
    <input id="commentForm" title="Lưu" value=" Lưu " class="Button" type="submit">
</div>
<?php echo $this->Form->end(); ?>

<script type="text/javascript">
  $(function() {
    var progress = $("#progress");
    var bar      = $("#bar");
    var percent  = $("#percent");
    // var status = $('#status');
    ///////////////////////////////////////
    $("#comment_attach_file").ajaxForm({
      beforeSubmit: function() {
        // 
      },
      beforeSend: function() {
        // reset progress
        progress.show();
        var percentVal = '0%';
        bar.width(percentVal)
        percent.html(percentVal);
        return false;
      },
      uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        if (percentComplete == 100) {percentComplete = 99;};
        bar.width(percentVal)
        percent.html(percentVal);
      },
      success: function() {
        var percentVal = '100%';
        bar.width(percentVal)
        percent.html(percentVal);
      },
      complete: function(xhr) {
        $("#comment_<?php echo $model ?>_<?php echo $item_id ?>").html(xhr.responseText);
        $("#window_content").dialog('close');
        $("#loading").hide();
        // if (xhr.responseText == 'ok')
        //   location.reload(false);
        // else
        //   window.location = "http://" + xhr.responseText;
        // location.reload(false);
        // progress.hide();
        // status.html(xhr.responseText);
      }
    });
    ///////////////////////////////////////
  });
</script>