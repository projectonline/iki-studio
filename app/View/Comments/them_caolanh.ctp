<legend>Thêm trả lời</legend>

<?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create();

	$link_img = '/img/icon/'.strtolower($model).'s_danhsachloinhan.png';
	if( !file_exists(APP.'webroot'.$link_img) )
	{
		$link_img = '/img/icon/tasks_danhsachloinhan.png';
	}
?>
	<h3><img src="<?php echo $link_img;?>" />&nbsp;Nội dung trả lời (*):</h3>
	<div style="height:5px;">&nbsp;</div>
	<?php echo $this->Form->input("Comment.noidung", array(
				"rows" => 10,
				"style" => 'width:543px;'
	));?>

	<?php if( $model == "Task" ){ ?>
	<div style="margin:5px">
		<?php echo $this->Form->input("Comment.quantrong", array( "type" => "checkbox", "label" => "Thông tin quan trọng"));?>
	</div>	
		<?php //echo $this->element('../Comments/theo_hd'); 	
	} ?>

	<?php echo $this->element('../Comments/sms'); ?>

	<div style="text-align: center;">
		<?php
			echo $this->Js->submit( "Lưu", array(
				'before' => '$("#loading").show()',
				"class" => "Button",
				'success' => "handle_response_comment(data, '".$update_div."');" // HB_update_tinnhan()"
			));
		?>
	</div>

	<?php // diult them ngay 29/11/2013 de hien file cua cong trinh ?>
	<?php if(isset($file_congtrinh) && $file_congtrinh == 1)
		{
			echo $this->element("upload_attach_file_congtrinh");
			echo '<br>';
		}
	?>


    <?php echo $this->element("upload_attach_file"); ?>

    <?php if( !in_array($model, array("Loinhansinhnhat", "Loinhanngoaikhoavt", "Chamcong")) ){ ?>

		<div style="width:543px;">
			Thông báo cho: <?php echo $this->element("giaoviec"); ?>
		</div>

	<?php }?>

<?php echo $this->Form->end(); ?>

<?php if( $model == 'Task' )
	{ ?>
		<span id="hide_send_all">&nbsp;</span>
<?php } ?>

