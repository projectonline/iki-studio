<?php echo $this->element( 'leftmenu_thv' ); ?>
<?php echo $this->Form->create($model, array('type' => 'GET')); ?>
<div id="divChucNang">
	<a href="/<?php echo str_replace('index','them',$this->request->url);?>/them" class="Button">Thêm</a>
	<input value=" <?php echo __('Tìm'); ?> " type="submit" class="Button" id="btnsearch">
</div>
<div id="divThongTin" style="min-height:500px">
		<table class="Data">
		<tr>
			<td><?php echo __("STT");?></td>
			<td><?php echo __("Tiêu đề");?></td>
			<td><?php echo __("Chú thích");?></td>
			<td><?php echo __("File");?></td>
			<td style="width: 72px;"><?php echo __("Người tạo");?></td>
			<td><?php echo __("Xóa");?></td>
		</tr>
		<tr>
			<td></td>
			<td><?php echo $this->Form->input( "tieude", array("label" => false ) ); ?></td>
			<td><?php echo $this->Form->input( "mota", array("label" => false ) ); ?></td>
			<td></td><td><?php echo $this->Form->input( "nguoitao", array("label" => false ) ); ?></td><td></td>
		</tr>
		<?php $i = 1; $STT = 1;
			if( $this->request->params['paging'][$model]['page'] > 1 )
			{
				$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
			} ?>
		<?php foreach($datas as $data){ ?>
		<tr id="<?php echo $model; ?>_<?php echo $data[$model]['id'];?>" class="row<?php echo $i; $i = 3 - $i;?>">
			<td><?php echo $STT++; ?>
				<?php if( (strtotime($data[$model]["modified"]) + 329600) > strtotime("now") ){ ?>
					<img src="/img/icon_new.png" /><?php } ?>					
			</td>
			<td>
				<?php echo $this->Js->link( $data[$model]["tieude"], "/thuviens/xem_thongtin/".$data[$model]["id"],
						array(
							"update" => "#window_content",
							"title" => "xem chi tiết",
							'success' => "$(\"#loading\").hide();window_show();",
							'class' => 'title_Caitien',
							'escape' => false
				) ); ?>
			</td>
			<!--<td><?php// echo $data[$model]['tieude']; ?></td>-->
			<td><?php echo $data[$model]['mota']; ?></td>
			<td><?php foreach( $data["File"] as $file ){
						echo $this->element("link_download_file", array(
							"id_file" => $file["id"],
							"name_file" => $file["name"],
							"dungluong_file" => $file["dungluong"] ));
					}
					if(isset($data["File"][1])){
						echo "<br>".$this->element("link_download_file_all", array(
							"id_controller" => $data[$model]["id"],
							"count_num_file" => count($data["File"])
						))."<br>";
					} ?>
			</td>
			<td><?php echo $this->element("avatar_view",array(
						"created" => $data[0]["created"],
						"dataUser" => $data["Nguoitao"]
				)); ?>
			</td>
			<td><?php if(isset($admin) || (AuthComponent::user('id') == $data["Nguoitao"]["id"])){ ?>
				<a href="/<?php echo $controller;?>/sua/<?php echo $data[$model]['id'];?>" title="<?php echo __('Sửa pháp lý này');?>"><?php echo __("Sửa"); ?></a>
				<a onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]['id']; ?>)" href="javascript:void(0)" title="<?php echo __('Xóa pháp lý này');?>"><?php echo __("Xóa"); ?></a>
				<?php } ?>
			</td>
		</tr>
		<?php } ?>
		</table>
		<?php echo $this->element('paging', array('show_info' => 'thư viện')); ?>
</div>
<?php echo $this->Form->end(); ?>
