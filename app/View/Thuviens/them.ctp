<?php echo $this->element( 'leftmenu_thv' ); ?>
<?php echo $this->Form->create($model, array( 'onsubmit' => 'beginUpload()', 'type' => 'file' )); ?>
<div id="divThongTin">
		<?php echo $this->element( 'message' );?>
		<table class="CssTable" cellpadding="0" cellspacing="1" width="99%" border="0">
		<tr>
			<td><?php echo __("Tên thư viện"); ?> (*):</td>
			<td><?php echo $this->Form->input($model.'.tieude', array('size' => '80' ));?></td>
		</tr>
		<tr>
			<td><?php echo __("Mô tả chi tiết"); ?> (*):</td>
			<td><?php echo $this->Form->input($model.'.mota', array('rows' => '12', 'cols' => '60' ));?></td>
		</tr>
		<tr>
			<td><?php echo __("Loại thư viện"); ?> (*):</td>
			<td><?php unset($lefts[182]);echo $this->Form->input($model.'.typeleft', array('options' => $lefts ));?> -
				<?php echo $this->Form->input($model.'.type', array('options' => $tops ));?></td>
		</tr>
		<tr>
			<td><?php echo __("Upload File"); ?> (*):</td>
			<td><?php echo $this->element( "upload_progress" ); ?></td>
		</tr>
		<tr>
			<td colspan="2" class="Center">
				<a href="/thuviens" class="Button">Trở về trang thư viện</a>
				<input title="Lưu" value=" Lưu thông tin " type="submit" class="Button">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<?php
					echo "Thông báo cho: ".$this->element("giaoviec");		
				?>	
				<span id="users_search_giaoviec">
				 	<span id="span_id_user_giaoviec_all"><b style="color:red">Gửi thông báo đến tất cả mọi người !</b>
					<input type="hidden" value="1" name="data[Nguoinhanloinhan][check_thongbao_gv_all]">&nbsp;&nbsp;<img style="cursor:pointer" src="/img/icon/toggle_delete.png" onclick="xoa_chon_user_giaoviec('+"'all'"+')" title="Bỏ" />
					</span>
				</span>		
			</td>
		</tr>
		</table>
</div>
<?php echo $this->Form->end(); ?>