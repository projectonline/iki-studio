<legend>Thư Viện Thí Nghiệm</legend>
<div class="item" id="<?php echo $model."_".$data[$model]['id']; ?>" style="width:800px;">
	<div class="from">
		<?php $link_img = '/img/icon/'.$controller.'_danhsachloinhan.png'; ?>
		<img src="<?php echo $link_img; ?>" />
		<?php echo $this->element("avatar_view",array(
				"created" => date("d/m/Y H:i", strtotime($data[$model]["created"])),
				"dataUser" => $data["Nguoitao"]
		));?>
		<?php if( (strtotime($data[$model]["created"]) + 129600) > strtotime("now")){ ?>
			<img src="/img/icon_new.png" />
		<?php } ?>
	</div>

	<center><h3 style="margin: 10px 0;font-weight:bold">Tiêu Đề: <span style="color:#145190;"><?php echo $data[$model]["tieude"]; ?></span></h3></center>

	<div class="content">
		<table id="table_Kaizen_xem_thongtin">
			<tr>
				<td style="width:260px"><center><b>Mô tả / Giải thích</b></center><div style="height:5px">&nbsp</div></td>
				<td style="width:260px"><center><b>File đính kèm</b></center></td>				
			</tr>
			<tr>
				<td style="border-right:1px solid #000000;"><?php echo $data[$model]["mota"]; ?></td>
				<td><?php foreach( $data["File"] as $file ){
						echo $this->element("link_download_file", array(
							"id_file" => $file["id"],
							"name_file" => $file["name"],
							"dungluong_file" => $file["dungluong"] ));
					}
					if(isset($data["File"][1])){
						echo "<br>".$this->element("link_download_file_all", array(
							"id_controller" => $data[$model]["id"],
							"count_num_file" => count($data["File"])
						))."<br>";
					} ?>
			</td>						
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td colspan="2">
					<?php if($kiemtra == 1385){?>
						<a href="/thuviens/checklist/1385/1386">Link vào trong thư viện</a>
					<?php }else{?>
						<a href="/thuviens/">Link vào trong thư viện</a>
					<?php }?>
				</td>
			</tr>
		</table>
	</div>	
	<?php if( $id_giaoviec > 0 ){ ?>
	<div style="text-align: right;margin-right:20px">
		<a href="javascript:void(0)" onclick="confirm_giaoviec_send_all(<?php echo $id_giaoviec; ?>);$('#window_content').dialog('close');" title="Click xác nhận bạn <b>đã đọc xong</b>">Đã đọc xong</a>
	</div>
	<?php } ?>
	<div class="ThemMoiLichCongViec">
		<?php echo $this->Js->link( "Trả lời", "/comments/them/".$data[$model]["id"].'/'.$model.'/xemthem_thongtin_',
				array( "update" => "#window_content_3",
					   'before' => '$(\'#loading\').show()',
					   'success' => "$(\"#loading\").hide();window_show_3();",
					   "title" => "Trả lời lời nhắn" ) ); ?>
	</div>

	<!--COMMENT-->
	<?php echo $this->element('../Comments/show_comment', array('data' => $data, 'div_update_more' => 'xemthem_thongtin_')); ?>
	<!--END COMMENT-->

</div>

<style>
	#table_Kaizen_xem_thongtin td{
		 vertical-align:top;
		 padding: 0 10px;
	}
	.ui-dialog-title{
		float:none !important;
	}
</style>