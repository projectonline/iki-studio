<?php ?>
<div class="xemnhanhtask" onmouseout="hide_xemnhanh_task()" onmouseover="show_xemnhanh_task()">
  <div class="xemnhanh"><a href="javascript:void(0)">>> Xem nhanh lời nhắn >></a></div>
  <div class="xemnhanh_detail_task" style="display:none">
    <?php
    $mau_sac = array( '', ' style="background-color:#D5DCE4;"' ); $i = 1;
    foreach( $xemnhanh_data_task as $key => $data ){
	    $i = 1 - $i;
	    echo '<div ' . $mau_sac[$i] . '><a href="'.$key.'"><span style="width: 21px;display: inline-block;">' . substr($key,15) . '.</span> ' . $data . '</a></div>';
    }
    ?>
    <?php echo $this->element( "paging", array( "update" => $div_update ) ); ?>
  </div>
</div>
<script type="text/javascript">
  $(function(){
    $("#bt_search_xemnhanh").submit(function(){
      var div_contain = $(this).parent("div");
      $(".message_error", div_contain).html("Vui lòng nhập MS là 1 số để tìm").hide();
      var search_value = $.trim($("#id_search_ct", div_contain).val());
      if (search_value == "") {
        $(".message_error", div_contain).show();
        return false;
      }
      if ($(".TaskCT" + search_value).attr("id") == undefined){
        $("#loading").show();
        $.ajax({
          url: "/tasks/xem_chi_tiet_task/" + search_value,
          timeout: 15000,
          error: error_handler,
          success: function(html){
            $("#loading").hide();
            if (html == "khongtontai") {
              $(".message_error", div_contain).html("Không tìm thấy mã số " + search_value + ", vui lòng nhập mã số khác").show();
              return false;
            }
            $("#window_content").html(html);
            window_show();
          }
        });
      }else{
        $.scrollTo(".TaskCT" + search_value, 300, {offset:-30} );
        $(".xemnhanh_detail_task").hide();
      }

      return false;
    });

  });
</script>