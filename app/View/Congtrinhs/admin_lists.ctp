<?php ?>
<div id="headerArea" style="position:relative;">
    <?php if(isset($admin)){ ?>
        <span id="ThemLoiNhan" style="position:absolute;right:0;bottom:-6px">
            <span id="id_themmoi"><a href="javascript:void(0)" onclick="show_hide_themmoi_loinhan_main_index('show'); $('#CongtrinhId').val(''); return false;"><?php echo $link_them_loinhan; ?></a></span>
        </span>
        <span id="id_form_them" style="display:none">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->Form->create($model); ?>
            <?php echo $this->Form->hidden($model.".id");?>
            <input id="CongtrinhLoai" type="hidden" name="data[Congtrinh][loai]" value="<?php echo $loai; ?>">
            <table class="CssTable" cellpadding="0" cellspacing="1" border="0" style="text-align:left">
              <tr>
                <td><b>Mã công trình (*):</b></td>
                <td><?php echo $this->Form->input($model.".macongtrinh", array( "style" => 'width:350px', 'id' => 'macongtrinh' ));?></td>
              </tr>
              <tr>
                <td><b>Tiêu đề (*):</b></td>
                <td><?php echo $this->Form->input($model.".tieude", array( "style" => 'width:350px', 'id' => 'tieudeloinhan' ));?></td>
              </tr>
              <tr>
                <td><b>Mô tả (*):</b></td>
                <td><?php echo $this->Form->input($model.".mota",  array("rows" => 10, "style" => 'width:350px',));?></td>
              </tr>

                <tr>
                  <td colspan="2" align="center">
                    <button href="/admin/congtrinhs/them" title="Lưu" class="Button" type="submit" onclick="them_loinhan(this);return false;;">Lưu</button>
                    <button title="Đóng Lại" class="Button" onclick="show_hide_themmoi_loinhan_main_index(); return false;">Đóng Lại</button>
                  </td>
                </tr>
            </table>
            <?php echo $this->Form->end(); ?>

        </span>
    <?php } ?>
</div>
<?php echo $this->element('../Congtrinhs/lists_small') ?>