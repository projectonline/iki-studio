<?php echo $this->Form->create($model); ?>

<div id="divChucNang">
    <input value=" Tìm " type="submit" class="Button" id="btnsearch">
</div>

<div id="headerArea"><h2>Danh sách công trình</h2></div>

<table class="Data">
    <tr>
        <td class="TdFirst" width="4%">STT</td>
        <td>MSCT</td>
        <td>Tên Công Trình</td>
        <td>Mô Tả</td>
        <td>Loại</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo $this->Form->input('macongtrinh', array('type' => 'text', 'style' => 'width:88%' ));?></td>
        <td><?php echo $this->Form->input('tieude', array('label' => false, 'style' => 'width:96%' ));?></td>
        <td></td>
        <td></td>
        <td></td>
     </tr>
    <?php $STT = 1;
        if( $this->request->params['paging'][$model]['page'] > 1 )
        {
            $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
        } ?>
    <?php foreach($datas as $data){ if( $data[$model]["loai"] != 2 || ($data[$model]["loai"] == 2 && isset($admin)) ) {?>
    <tr id="<?php echo $model; ?>_<?php echo $data[$model]['macongtrinh'];?>">

        <td align="center"><?php echo $STT; ?><?php if( (strtotime($data[$model]["modified"]) + 640800) > strtotime("now") ){ ?>
            <img src="/img/icon_new.png" /><?php } ?>
        </td>

        <td>
            <a href="/admin/<?php echo $controller;?>/view/<?php echo $data[$model]['id'];?>">
                <?php echo $data["Congtrinh"]["macongtrinh"]; ?>
            </a>
        </td>

        <td>
            <a href="/admin/<?php echo $controller;?>/view/<?php echo $data[$model]['id'];?>">
                <?php echo $data[$model]["tieude"]; ?>
            </a>
        </td>
        <td>
            <?php echo nl2br($data[$model]["mota"]); ?>
        </td>
        <td>
            <?php if($data[$model]["loai"] == 2) echo 'Pursued Project'; ?>
        </td>
        <td>
            <?php if(isset($admin)){  } ?>
        </td>
    </tr>
    <?php $STT++; } } ?>
</table>
<?php echo $this->element('paging_search', array('show_info' => 'công trình', 'update' => '#mainContent')); ?>
<?php echo $this->Form->end(); ?>
