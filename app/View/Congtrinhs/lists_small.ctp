<?php
    $xemnhanh_data = array();
    $STT = 1;
    if( $this->request->params['paging'][$model]['page'] > 1 )
        $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
    $STT_xem_nhanh = $STT;
    foreach( $datas as $data ){
?>
    <div id="<?php echo $model."_".$data[$model]["id"] ?>" class="item div1LoiNhan TaskCT<?php echo $data["Congtrinh"]["id"] ?>" onmouseover="$('span#xoa_loinhan_ajax',this).show();" onmouseout="$('span#xoa_loinhan_ajax',this).hide();">
    <div class="title ContainLinkHoverXoa">
        <div id="xemnhanh_task_<?php echo $STT ?>"></div>
        <img style="width: 21px; margin-right: 2px;" src="/iconiki.ico">
        <?php
            $xemnhanh_data["#xemnhanh_task_".$STT] = $data[$model]["tieude"];
             ?>

             <a href="/admin/congtrinhs/view/<?php echo $data["Congtrinh"]['id'];?>">
                <?php echo $STT++ . '. <span id="macongtrinh_'.$data["Congtrinh"]["id"].'">' . $data[$model]["macongtrinh"] . '</span> - <span id="tieude_'.$data["Congtrinh"]["id"].'">'.$data[$model]["tieude"] ?>
            </a>
        <em style="text-transform: none; font-size: 20px; margin: 0px 0px 0px 10px;">
        <a id="tab_xemthem_<?php echo $model."_".$data[$model]['id']; ?>" href="javascript:void(0)" onclick="xemtab('<?php echo $model."_".$data[$model]['id']; ?>',1)" title="Open">+</a>
        <a id="tab_thugon_<?php echo $model."_".$data[$model]['id']; ?>" hidden="hidden" href="javascript:void(0)" onclick="xemtab('<?php echo $model."_".$data[$model]['id']; ?>',2)" title="Close">-</a>
        </em>
        <?php if(isset($admin)){
            echo '<em class="linkHoverXoa" style="text-transform: none; font-weight: normal; font-size: 12px; margin-left : 44px;">
            <a href="javascript:void(0)" onclick="sua_congtrinh('.$data[$model]["id"].')">Edit</a> | '; ?>
            <a href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model ?>',<?php echo $data[$model]["id"] ?>)">Del</a></em>
        <?php } ?>
    </div>

    <div id="tabs_<?php echo $model."_".$data[$model]['id']; ?>" style="display:none">
    <div class="content"><?php echo nl2br($data[$model]["mota"]) ?></div>
    <div id="mota_<?php echo $data[$model]["id"] ?>" style="display:none"><?php echo $data[$model]["mota"] ?></div>
    <div class="date"></div>

    <!--Tab 160615 Doan them-->
    <div id="tabs">
        <ul id="navlist_<?php echo $data["Congtrinh"]["id"]; ?>">
            <li>
                <?php echo $this->Js->link( "Process", "/admin/comments/danhsach/".$data["Congtrinh"]["id"],
                    array( 	'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <li>
                <?php echo $this->Js->link( "Input", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/1",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <li>
                <?php echo $this->Js->link( "Output", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/2",
                    array( 	'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <!-- <li> bỏ
                <?php /*echo $this->Js->link( "Original File", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/3",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );*/
                ?>
            </li> -->
            <li>
                <?php echo $this->Js->link( "MOM", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/5",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <?php if(isset($admin)){ ?>
            <li>
                <?php echo $this->Js->link( "Contract", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/6",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <?php } ?>
            <li>
                <?php echo $this->Js->link( "Legal Docs.", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/7",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <li>
                <?php echo $this->Js->link( "Reference", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/8",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <li>
                <?php echo $this->Js->link( "Site Picture", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/9",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <!-- <li class="list-group-item"> bỏ
                <?php /*echo $this->Js->link( "Previous Backup", "/admin/comments/danhsach/".$data["Congtrinh"]["id"]."/10",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$data["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );*/
                ?>
            </li> -->
        </ul>
        <!-- <div id="dsfile">
        <div id="congtrinhs_tab_<?php echo $data["Congtrinh"]["id"] ?>">
            <!-- data -->
        <!-- </div>
        </div> -->
    </div>
    </div>

    <script type="text/javascript">
    $('#navlist_<?php echo $data["Congtrinh"]["id"]; ?> a').click(function(e) {
        e.preventDefault(); //prevent the link from being followed
        $('#navlist_<?php echo $data["Congtrinh"]["id"]; ?> a').removeClass('selected');
        $('#navlist_<?php echo $data["Congtrinh"]["id"]; ?> li').removeClass('nav');
        $(this).addClass('selected');
        $(this).parent().addClass('nav');
    });
    </script>
    <!--END Tab-->

    <!--COMMENT-->
    <div style="position:relative">
        <div class="ThemTraLoi">
            <?php echo $this->Js->link( "Reply", "/admin/comments/them_congtrinh/".$data[$model]["id"].'/'.$model.'/'.$data["Congtrinh"]["id"],
                array( 'before' => '$(\'#loading\').show()',
                    "update" => "#window_content",
                       'success' => "$(\"#loading\").hide();window_show();"
                ) );
            ?>
        </div>
        <!-- 160616 Doan them -->
        <div id="comment_Congtrinh_<?php echo $data["Congtrinh"]["id"]; ?>">
            <script type="text/javascript">
                var url = "/admin/comments/danhsach/<?php echo $data["Congtrinh"]["id"]; ?>";
                $.ajax({
                    url: url,
                    timeout: 15000,
                    error: error_handler,
                    success: function(html) {
                        $("#comment_Congtrinh_<?php echo $data["Congtrinh"]["id"]; ?>").html(html);
                        //$("#loading").hide();
                    }
                });
            </script>
        </div>
        <!-- 160616 Doan them -->
    </div>
</div>
<?php } ?>

<?php echo $this->element( "paging", array( "update" => $div_update ) ) ?>
<?php echo $this->element( "../Congtrinhs/xemnhanh", array("xemnhanh_data_task" => $xemnhanh_data) ) ?>
<?php echo $this->element("xemnhanh"); ?>

<script type="text/javascript">
    function sua_congtrinh(id){
        $("a", "#id_themmoi").click();

        $("#CongtrinhId").val(id);
        $("#macongtrinh").val($("#macongtrinh_" + id).text());
        $("#tieudeloinhan").val($("#tieude_" + id).text());
        $("#CongtrinhMota").val($("#mota_" + id).text());
    }

    //160618 Doan them
    function xemtab(div_update, xem) {
        if(xem == 1){
            $("#tabs_" + div_update).removeAttr("style");
            $("#tab_xemthem_" + div_update).attr('hidden', "hidden");
            $("#tab_thugon_" + div_update).removeAttr("hidden");
        }else{
            $("#tabs_" + div_update).attr('style', "display:none");
            $("#tab_thugon_" + div_update).attr('hidden', "hidden");
            $("#tab_xemthem_" + div_update).removeAttr("hidden");
        }
        return false;
    }
    //160618 Doan them ---
</script>

<style type="text/css">
    #tabs {
        margin-top: 10px;
    }
    #tabs li {
        display: inline-block;
        border: 1px solid #CACACA;
        padding: 5px 10px;
        border-radius: 4px;
    }

    #dsfile li {
        border-bottom: 1px solid #d0d7e5;
        padding: 5px 2px;
        display: list-item;
        border-radius: 0px;
    }

    .nav { background-color: #d2d9e7; }
    .selected { color: red;  }
</style>