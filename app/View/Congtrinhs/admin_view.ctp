<div class="item div1LoiNhan" style="padding:0 100px">
    <div class="title">
        <img style="width: 21px; margin-right: 2px;" src="/iconiki.ico">
        <?php echo '<span id="macongtrinh_'.$congtrinh["Congtrinh"]["id"].'">' . $congtrinh["Congtrinh"]["macongtrinh"] . '</span> - <span id="tieude_'.$congtrinh["Congtrinh"]["id"].'">'.$congtrinh[$model]["tieude"]; ?>
    </div>
    <div class="content"><?php echo nl2br($congtrinh[$model]["mota"]) ?></div>
    <div id="mota_<?php echo $congtrinh[$model]["id"] ?>" style="display:none"><?php echo $congtrinh[$model]["mota"] ?></div>
    <div class="date"></div>

    <!--Tab 160615 Doan them-->
    <div id="tabs">
        <ul id="navlist_<?php echo $congtrinh["Congtrinh"]["id"]; ?>">
            <li>
                <?php echo $this->Js->link( "Process", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"],
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <li>
                <?php echo $this->Js->link( "Input", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"]."/1",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <li>
                <?php echo $this->Js->link( "Output", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"]."/2",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <!-- <li> bỏ
                <?php /*echo $this->Js->link( "Original File", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"]."/3",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );*/
                ?>
            </li> -->
            <li>
                <?php echo $this->Js->link( "MOM", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"]."/5",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <?php if(isset($admin)){ ?>
            <li>
                <?php echo $this->Js->link( "Contract", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"]."/6",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <?php } ?>
            <li>
                <?php echo $this->Js->link( "Legal Docs.", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"]."/7",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <li>
                <?php echo $this->Js->link( "Reference", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"]."/8",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
            <li>
                <?php echo $this->Js->link( "Site Picture", "/admin/comments/danhsach/".$congtrinh["Congtrinh"]["id"]."/9",
                    array( 'before' => '$(\'#loading\').show()',
                            "update" => "#comment_Congtrinh_".$congtrinh["Congtrinh"]["id"],
                           'success' => "$(\"#loading\").hide();"
                    ) );
                ?>
            </li>
        </ul>
    </div>

    <script type="text/javascript">
    $('#navlist_<?php echo $congtrinh["Congtrinh"]["id"]; ?> a').click(function(e) {
        e.preventDefault(); //prevent the link from being followed
        $('#navlist_<?php echo $congtrinh["Congtrinh"]["id"]; ?> a').removeClass('selected');
        $('#navlist_<?php echo $congtrinh["Congtrinh"]["id"]; ?> li').removeClass('nav');
        $(this).addClass('selected');
        $(this).parent().addClass('nav');
    });
    </script>
    <!--END Tab-->

    <!--COMMENT-->
    <div style="position:relative">
        <div class="ThemTraLoi">
            <?php echo $this->Js->link( "Reply", "/admin/comments/them_congtrinh/".$congtrinh[$model]["id"].'/'.$model,
                array( 'before' => '$(\'#loading\').show()',
                    "update" => "#window_content",
                        "title" => "Trả lời lời nhắn",
                       'success' => "$(\"#loading\").hide();window_show();upload_iframe_window( 'comment_Congtrinh_".$congtrinh["Congtrinh"]["id"]."', 'tinymce' );"
                ) );
            ?>
        </div>
        <!-- 160616 Doan them -->
        <div id="comment_Congtrinh_<?php echo $congtrinh["Congtrinh"]["id"]; ?>">
        <?php //echo $this->element('../Comments/show_comment', array('data' => $data)); ?>
        <?php //echo $this->element('../Comments/danhsach', array('data' => $data)); ?>
        <script>
            var url = "/admin/comments/danhsach/<?php echo $congtrinh["Congtrinh"]["id"]; ?>";
            //$("#loading").show();
            $.ajax({
                url: url,
                timeout: 15000,
                error: error_handler,
                success: function(html) {
                    $("#comment_Congtrinh_<?php echo $congtrinh["Congtrinh"]["id"]; ?>").html(html);
                    //$("#loading").hide();
                }
            });
        </script>
        </div>
        <!-- 160616 Doan them -->
    </div>
    <!--END COMMENT-->

</div>

<style type="text/css">
    #tabs {
        margin-top: 10px;
    }
    #tabs li {
        display: inline-block;
        border: 1px solid #CACACA;
        padding: 5px 10px;
        border-radius: 4px;
    }

    #dsfile li {
        border-bottom: 1px solid #d0d7e5;
        padding: 5px 2px;
        display: list-item;
        border-radius: 0px;
    }

    .nav { background-color: #d2d9e7; }
    .selected { color: red;  }
</style>