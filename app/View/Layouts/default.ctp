<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <?php echo $this->element( 'head' ); ?>
    <?php echo $this->element( 'css' ); ?>
    <?php echo $this->element( 'js' ); ?>
</head>

<body>

    <?php echo $this->element( "window" ); ?>
    <?php echo $this->element( 'main_loading' ); ?>

    <div id="main_top" style="height: 86px;">
        <h2 style="padding: 10px 0">
            <a href="/" title="Space Group"><img style="width: 113px;margin-top: 13px;" src="/img/logo.png"> </a>
        </h2>

        <?php echo $this->element('email_links'); ?>
    </div>

    <div id="main">
        <?php if(AuthComponent::user('trangthai') != 1){?>
            <?php echo $this->element( 'navigation' ); ?>
        <?php }?>
        <div id="mainContent">
            <?php echo $content_for_layout; ?>
        </div>
    </div>


    <?php echo $this->element( 'bottom_menu' ); ?>
    
    <?php //echo $this->element('gopy'); ?>
    <?php echo $this->element('sql_dump'); ?>
    <?php echo $this->Js->writeBuffer(); ?>
    </body>
</html>