<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1">
<title>Architecture Firm and Interior Design Services Stamford CT | CPG Architects</title>
<meta name="description" content="CPG Architects in Stamford CT offers an array of architectural services & interior design that truly adheres to their clients� needs. Click here to see how.">
<meta name="keywords" content="Professional Architect, Commercial Architect, Architect in Stamford CT, Retail Architect, Large Commercial Architects, architecture firm, interior architects, architecture services, interior design, interior designers, corporate architects, commercial architecture firm, Stamford, Connecticut">
<meta name="robots" content="index,follow" />
<meta name="googlebot" content="index,follow" /> 

<link rel="stylesheet" type="text/css" href="/css/styles.css">
<link rel="icon" type="image/png" href="/img/logo.png">
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.7.3.custom.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="/js/jquery.lightbox-0.5.js"></script>
<script type="text/javascript" src="/js/scripts.js"></script>

<script src="/js/modernizr-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/general.js"></script>
<link rel="stylesheet" type="text/css" href="/css/style2.css" media="all"> 
<script language="javascript1.2">
    if(!$.browser.msie || $.browser.version != 8)  {
        document.write('<link rel="stylesheet" type="text/css" href="/css/mobile.css" media="all">'); 
    }
</script>

<link rel="stylesheet" type="text/css" href="/css/isotope.css" media="all"> 
<link rel="stylesheet" type="text/css" href="/css/jquery.lightbox-0.5.css" media="all"> 
<script language="javascript1.2">
    $(document).ready(function() {
        var offset = $('#navigation ul li:first').offset();
        if($(window).width() > 1025)  {
            $('#navigation ul li').mouseover(function() {
                var offset = $(this).offset();
                $('#sliderBar').show();
                $('#sliderBar').stop().animate({left:(offset.left - $('#navigation').offset().left), width:$(this).width()});
            }).mouseout(function(){
                $('#sliderBar').hide();
            });
            $('#sliderBar').animate({left:(offset.left - $('#navigation').offset().left), width:$('#navigation ul li:first').width()});
        }
    });
    
</script>
<!--[if IE 8]>
<style type="text/css">
    #goBack{display:none !important}
    #contentRightPanel{position:relative}
    #homeIntro{display:none}
    #goNext{display:none}
    #goPrev{display:none}
</style>  
<![endif]-->

</head>
<body>
    <?php echo $content_for_layout; ?>
</body>  
</html>