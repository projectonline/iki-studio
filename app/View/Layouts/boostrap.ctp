<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<!-- Bootstrap -->
		<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="/bootstrap/js/html5shiv.min.js"></script>
			<script src="/bootstrap/js/respond.min.js"></script>
		<![endif]-->

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="/bootstrap/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/main.css?v=1.8" />
		<link rel="stylesheet" type="text/css" href="/css/calendar_popcalendar.css" />
		<link rel="stylesheet" type="text/css" href="/js/fancyapps/source/jquery.fancybox.css" />
		<link rel="stylesheet" type="text/css" href="/js/fancyapps/source/helpers/jquery.fancybox-buttons.css" />
		<link rel="stylesheet" type="text/css" href="/js/fancyapps/source/helpers/jquery.fancybox-thumbs.css" />
		<link href="/bootstrap/main.css" rel="stylesheet">

		<!-- JS -->
		<script src="/bootstrap/jquery.min.js"></script>
		<script src="/bootstrap/jquery-ui-1.11.4.custom/jquery-ui.js"></script>

	</head>
	<body>
		<?php echo $this->element('thongtincanhan'); ?>
		<?php echo $this->element('window'); ?>
		<?php echo $this->element('main_loading'); ?>
		<div id="main_top">
			<h2 style="padding: 10px 0">
				<a href="/" title="Space Group"><img width="190" src="/img/Space_LotLy_in.png"></a>
			</h2>
			<?php echo $this->element('email_links'); ?>
		</div>
		<div id="main">
			<?php echo $this->element('navigation'); ?>
			<div id="mainContent">
				<?php echo $content_for_layout; ?>
			</div>
		</div>
		<?php echo $this->element('bottom_menu'); ?>
		<?php echo $this->element("xemnhanh"); ?>
		<div id="saved-success"></div>
		<div id="saved-failed"></div>

		<?php echo $this->element('sql_dump'); ?>

		<?php echo $this->Js->writeBuffer(); ?>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

		<!-- Include all compiled plugins (below), or include individual files as needed -->

		<script type="text/javascript" src="/js/jquery/jquery.scrollTo-min.js"></script>

		<!--<script  type="text/javascript" src="/js/congtrinh/jquery.dropcaptions.js"></script>
		<script  type="text/javascript" src="/js/congtrinh/jquery-1.7.js"></script>
		<script  type="text/javascript" src="/js/congtrinh/Tooltip.js"></script>-->

		<!-- <script type="text/javascript" src="/js/jquery/jquery.mousewheel.pack.js"></script> -->
		<script type="text/javascript" src="/js/fancyapps/lib/jquery.mousewheel-3.0.6.pack.js"></script>

		<script type="text/javascript" src="/js/jquery/jquery.jscrollpane.min.js"></script>
		<!-- <script type="text/javascript" src="/js/fancybox/source/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="/js/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
		<script type="text/javascript" src="/js/fancybox/source/helpers/jquery.fancybox-media.js"></script>
		<script type="text/javascript" src="/js/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>-->
		<script type="text/javascript" src="/js/fancyapps/source/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="/js/fancyapps/source/helpers/jquery.fancybox-buttons.js"></script>
		<script type="text/javascript" src="/js/fancyapps/source/helpers/jquery.fancybox-media.js"></script>
		<script type="text/javascript" src="/js/fancyapps/source/helpers/jquery.fancybox-thumbs.js"></script>

		<script type="text/javascript" src="/js/jquery/jquery.iframe-post-form.js"></script>
		<!-- <script type="text/javascript" src="/js/HB/cufon-yui.js"></script> -->
		<!-- <script type="text/javascript" src="/js/HB/HB.js"></script> -->

		<link rel="stylesheet" href="/tinymce/jscripts/tiny_mce/themes/advanced/skins/o2k7/ui.css">
		<script src="/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

		<script type="text/javascript" src="/js/tamsteps/tamstep.js"></script>
		<script type="text/javascript" src="/js/popcalendar/popcalendar.js"></script>

		<script type="text/javascript" src="/js/jquery.cycle2.min.js"></script>
		<script type="text/javascript" src="/js/jquery_form.js"></script><!-- upload ajax -->

		<script type="text/javascript" src="/js/tooltip/easyTooltip.js"></script>
		<!-- <script src="/bootstrap/js/bootstrap.min.js"></script> -->

		<script type="text/javascript" src="/js/main.js"></script>
		<script type="text/javascript" src="/bootstrap/main.js"></script>
	</body>
</html>