<?php echo $this->Form->create($model, array('type' => 'file')); ?>

<div id="headerArea"><h2>Cập nhật</h2></div>

<?php echo $this->element( 'message' );?>
<table class="Data">
    <tr>
        <td colspan="2">Thông tin </td>
    </tr>
    <tr>
        <td><?php echo __("Mật khẩu cũ"); ?> (*)</td>
        <td colspan="3"><?php echo $this->Form->input($model.'.password_old', array('required' => false, 'type' => 'password' )); ?></td>
    </tr>
    <tr>
        <td><?php echo __("Mật khẩu mới"); ?> (*)</td>
        <td colspan="3"><?php echo $this->Form->input($model.'.password_new', array('required' => false, 'type' => 'password' )); ?></td>
    </tr>
    <tr>
        <td><?php echo __("Mật khẩu mới xác nhận"); ?> (*)</td>
        <td colspan="3"><?php echo $this->Form->input($model.'.password_new_confirm', array('required' => false, 'type' => 'password' )); ?></td>
    </tr>
    <!--
    <tr>
        <td>Hình đại diện (*)</td>
        <td colspan="3">
        <?php if(isset($avatar_new)){ echo $this->Thumbnail->show( array( 'src' => W_UPLOAD.$avatar_new,"w" => 300, "h" => 360 )); }
        echo $this->Form->input( $model.".hinh" , array( 'size' => '60','type' => 'file' )); ?></td>
    </tr>
    -->
    <tr>
        <td colspan="2">
        <center><input id="btnDoiMatKhau" title="Lưu" value=" Lưu " class="Button" type="submit"></center>
        </td>
    </tr>
</table>
<?php echo $this->Form->end(); ?>