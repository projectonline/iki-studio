<div style="padding:20px">
    <?php echo $this->Form->create($model); ?>

    <div id="divChucNang">
    <?php if(isset($admin)){ ?>
        <a href="/admin/users/them" class="Button"> New </a>
    <?php } ?>
    </div>

    <div style="max-width:1000px">
        <table class="Data">
            <tr>
                <td style="width: 50px;">STT</td>
                <td style="width: 120px;">ID</td>
                <td style="width: 148px;">Username</td>
                <td style="width: 81px;">Tên thật</td>
                <td>Ngày sinh</td>
                <td>Email</td>
                <td>Số điện thoại</td>
                <td>Skype</td>
                <td></td>
            </tr>
            <?php $i = 1; $STT = 1;
            if( $this->request->params['paging'][$model]['page'] > 1 )
            {
                $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
            } ?>
            <?php foreach($datas as $data){?>
            <tr>
                <td><?php echo $STT; ?></td>
                <td><?php echo $data[$model]["id"]; ?></td>
                <td><?php echo $data[$model]["username"]; ?></td>
                <td><?php echo $data[$model]["realname"]; ?></td>
                <td><?php echo date('d/m/Y', strtotime($data[$model]["birthday"])); ?></td>
                <td><?php echo $data[$model]["email"]; ?></td>
                <td><?php echo $data[$model]["tel"]; ?></td>
                <td><?php echo $data[$model]["skype"]; ?></td>
                <td><?php if(isset($admin)){ ?>
                <a href="/admin/users/sua/<?php echo $data[$model]["id"]; ?>">Edit</a>
                <?php } ?>
                </td>
            </tr>
            <?php $STT++; } ?>
        </table>

    </div>
    <?php echo $this->element( 'paging', array("show_info" => "thành viên", 'update' => '#mainContent')); ?>

</div>
<?php echo $this->Form->end(); ?>