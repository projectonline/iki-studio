<?php echo $this->Form->create($model, array('type' => 'file')); ?>
<div id="divChucNang">
	<input type="Button" title="<?php echo __('Trở về danh sách tất cả thành viên'); ?>" value=" <?php echo __('Trở về danh sách tất cả thành viên'); ?> " class="Button" id="btnadd" onclick="window.location=&quot;/admin/<?php echo $controller;?>&quot;">
	<input title="<?php echo __('Lưu thành viên'); ?>" value=" <?php echo __('Lưu thành viên'); ?> " type="submit" class="Button" id="btnsearch">
</div>
<div id="headerArea"><h2>Thêm mới</h2></div>

<?php echo $this->element( 'message' );?>
<table class="Data">
	<tr>
		<td colspan="2">Thông tin </td>
	</tr>
	<tr>
		<td><?php echo __("Account"); ?> (Tên đăng nhập) (*):</td>
		<td colspan="3"><?php echo $this->Form->input($model.'.username');?></td>
	</tr>
	<tr>
		<td><?php echo __("Mật khẩu"); ?> (*)</td>
		<td colspan="3"><?php echo $this->Form->input($model.'.passwrd', array('type' => 'password' )); ?></td>
	</tr>
	<tr>
		<td><?php echo __("Mật khẩu xác nhận"); ?> (*)</td>
		<td colspan="3"><?php echo $this->Form->input($model.'.password_xacnhan', array('type' => 'password' )); ?></td>
	</tr>
	<tr>
		<td><?php echo __("Tên đầy đủ"); ?> (Tên thật) (*):</td>
		<td colspan="3"><?php echo $this->Form->input($model.'.realname', array('size' => '30' ));?></td>
	</tr>
	<tr>
		<td><?php echo __("Ngày sinh"); ?> (*):</td></td>
		<td colspan="3"><?php echo $this->Form->input($model.'.birthday_tmp', array('size' => '10',  'style' => 'float:left', 'readonly' => true ));?>
			 <?php echo $this->element("img_pop_calendar"); ?>
			 <div style="clear:both"><em>(click vào hình bên cạnh để chọn ngày)</em></div>
		 </td>
	</tr>
	
	<tr>
		<td><?php echo __("Email"); ?></td>
		<td colspan="3"><?php echo $this->Form->input($model.'.email', array('label' => false )); ?></td>
	</tr>
	<tr>
		<td><?php echo __("Số điện thoại"); ?></td>
		<td colspan="3"><?php echo $this->Form->input($model.'.tel', array('label' => false )); ?></td>
	</tr>
	<tr>
		<td><?php echo __("Skype"); ?></td>
		<td colspan="3"><?php echo $this->Form->input($model.'.skype', array('label' => false )); ?></td>
	</tr>
	<!--
	<tr>
		<td>Hình đại diện (*)</td>
		<td colspan="3">
		<?php if(isset($avatar_new)){ echo $this->Thumbnail->show( array( 'src' => W_UPLOAD.$avatar_new,"w" => 300, "h" => 360 )); }
		echo $this->Form->input( $model.".hinh" , array( 'size' => '60','type' => 'file' )); ?></td>
	</tr>
	-->
</table>
<?php echo $this->Form->end(); ?>