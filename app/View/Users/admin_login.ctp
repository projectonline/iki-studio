<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php echo $this->element( 'head' ); ?>
</head>

<body>

<div id="login" >
		<?php echo $this->Form->create($model, ['style' => 'margin: 10px 0 0 90px;']); ?>
		<table>
			<tr><td colspan="3" align="center"><img src="/img/logo.png" style="width:258px" /><br><br> </td></tr>
			<tr><td style="text-align: right; width: 104px;">Username: </td>
				<td style="padding-left: 14px;">
					<?php echo $this->Form->input("User.username", array( 'class' => 'TEXT')); ?>
				</td>
			</tr>
			<tr><td style="text-align: right; width: 104px;">Password: </td>
			<td style="padding-left: 14px;"><?php echo $this->Form->input("User.password", array( 'class' => 'TEXT')); ?></td></tr>
			<tr>
				<td colspan="2" align="center">
					<center><input type="submit" name="btnG" value="Login" class="lsb"></center>
				</td>
			</tr>

			<tr>
				<td colspan="3">
					<?php echo $this->Session->flash(); ?>
					<?php echo $this->Session->flash('auth'); ?>
				</td>
			</tr>
		</table>
		<?php echo $this->Form->end(); ?>
  </div>

<?php echo $this->element('sql_dump'); ?>
<style>

body{
  /* background: url(/img/noel/hinh-nen-giang-sinh.jpeg);*/
  /*background: url(/img/gtcc_sn.jpg);*/
}

#UserLoginForm{
  width: 328px;
margin: 35px auto 0;
}
#login {

height: 308px;
margin: 150px auto 0;
opacity: 1;
padding-top: 1px;
width: 525px;
background-color: rgba(98, 86, 86, 0.12);
border-radius: 6%;

/*width: 538px;
margin: 76px auto 0;
border-radius: 30%;
background-color: #fff;
height: 337px;
padding-top: 30px;*/
}

input[type="submit"] {
	-webkit-border-radius: 2px;
	-webkit-user-select: none;
	background-color: #f5f5f5;
	background-image: -webkit-linear-gradient(top,#f5f5f5,#f1f1f1);
	border: 1px solid rgba(0,0,0,0.1);
	border-radius: 2px;
	font-size: 13px;
	font-weight: bold;
	line-height: 27px;
	margin: 11px 6px;
	min-width: 54px;
	padding: 3px 26px;
	text-align: center;
	cursor: pointer;
}
input[type="submit"]:hover {
	-webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.1);
	background-image: -webkit-gradient(linear,left top,left bottom,from(#f8f8f8),to(#f1f1f1));
	background-color: #f8f8f8;
	background-image: linear-gradient(top,#f8f8f8,#f1f1f1);
	background-image: -webkit-linear-gradient(top,#f8f8f8,#f1f1f1);
	border: 1px solid #c6c6c6;
	box-shadow: 0 1px 1px rgba(0,0,0,0.1);
	color: #333;
}
#authMessage{
	background-color:#FBE3E4;color:#D12F19;font-weight:700
}
input.TEXT{
	border-color: #CCCCCC #CCCCCC #999999 #999999;
	border-style: solid;
	border-width: 1px;
	height:21px;
	font-size:18px;
	width: 140px;
	padding-left: 4px;
	padding-right: 4px;
}
</style>
<script type="text/javascript">
	$(function() {
		$("#UserUsername").focus();
	});
</script>
</body>
</html>