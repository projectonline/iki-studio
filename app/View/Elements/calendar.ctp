<!-- Jeff Linse's Calendar Element for CakePHP -->

<?php

	$first_of_month = mktime(0, 0, 0, $month, 1, $year);
	$num_days = cal_days_in_month(0, $month, $year);
	$offset = date('w', $first_of_month);
	$rows = 1;
?>
<div class="calendar item">
<h1 style="color:red;">
<img src='/img/icon/loinhandkxevts_danhsachloinhan.png' /><?php echo $loaixe_ten; ?>
</h1><br>
<h3>
<?php //echo $this->Form->create( 'Loinhandkxevt', array( 'id' => 'Loinhandkxevt', 'url' => '/#loinhandkxevts' )); ?>
Tháng <?php echo $this->Form->month('Loinhandkxevt.ngay', array('monthNames' => false, 'empty' => 'Tháng', "onchange" => "tim_dangkyxe( ".$loaixe_id." );")); ?>
Năm <?php echo $this->Form->year('Loinhandkxevt.ngay',date('Y')-1,date('Y'), array('empty' => 'Năm', "onchange" => "tim_dangkyxe( ".$loaixe_id." );")); ?>
<?php //echo $this->Form->end(  ); ?>
</h3>
<table>
	<tr>
		<td style="width:50px">CN</td>
		<td style="width:50px">Hai</td>
		<td style="width:50px">Ba</td>
		<td style="width:50px">Tư</td>
		<td style="width:50px">Năm</td>
		<td style="width:50px">Sáu</td>
		<td style="width:50px">Bảy</td>
	</tr>
	<tr>

	<?php for( $i = 1; $i < $offset + 1; ++$i ): ?>
		<td></td>
	<?php endfor; ?>

	<?php for( $day = 1; $day <= $num_days; ++$day ): ?>
		<?php if( ($day + $offset - 1) % 7 == 0 && $day != 1 ): ?>
			</tr><tr>
			<?php ++$rows; ?>
		<?php endif; ?>
		<td align="center" style="font-size:13px;">
			<?php if( isset($title[$day]) ){ ?>
			<a style="<?php if( isset($mau[$day]) ){ echo $mau[$day]; } ?>" target="_blank" href="<?php echo $day_link.'/'.$loaixe_id.'/'.$year.'-'.$month.'-'.$day.'/'; ?>" title="<?php if( isset($title[$day]) ){ echo $title[$day]; } ?>">
			<b><?php echo $day; ?></b></a>
			<?php }else{ echo $day; } ?>
		</td>
	<?php endfor; ?>

	<?php for( $day; ($day + $offset) <= $rows * 7; ++$day ): ?>
		<td></td>
	<?php endfor; ?>

	</tr>
</table>
</div>
<script type="text/javascript">
$(function() {
	$(".calendar").tooltip();
});
</script>