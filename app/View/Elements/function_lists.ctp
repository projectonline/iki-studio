<span><strong>■ <?php echo $title; ?>
	<?php echo $this->Js->link( "Thêm mới", "/mains/them_sub_congtrinh/".$object_id.'/'.$model,
					array( "update" => "#window_content",
						   'before' => '$(\'#loading\').show()',
						   'success' => "$(\"#loading\").hide();window_show();",
						   "title" => "Thêm mới" ) ); ?>
</strong></span>
<?php
   $STT = 1;
		if( $this->request->params['paging'][$model]['page'] > 1 )
		{
			$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
		}
	foreach( $this->request->data as $data){ ?>
	<div class="item" id="<?php echo $model;?>_<?php echo $data[$model]['id']; ?>">
		<div class="TitleListCongTrinhView">
			<?php echo $STT.". ";
				if(isset($data[$model]['tieude'])){
					echo $data[$model]['tieude'];
				}else{
					echo $STT.". (Mã:".$data[$model]['socongvan'].") ".$data[$model]['to'];
				}?>
		</div>
		<div class="content"><?php echo $data[$model]['mota']; ?></div>
		<div class="file">
			<?php foreach( $data["File"] as $file ){
						echo $this->element("link_download_file", array(
							"id_file" => $file["id"],
							"name_file" => $file["name"],
							"dungluong_file" => $file["dungluong"] ))."<br>";
					}
					if(isset($data["File"][1])){
						echo $this->element("link_download_file_all", array(
							"id_controller" => $data[$model]["id"],
							"count_num_file" => count($data["File"])
						))."<br>";
					} ?>
		</div>
		<em>( <?php echo $data['Nguoitao']['username']; ?>,<?php echo date('d/m/Y H:i', strtotime($data[$model]['created'])); ?> )</em>
		<?php if(isset($admin) || (AuthComponent::user('id') == $data['Nguoitao']['id'])){ ?>
			<?php echo $this->Js->link( "Sửa", "/mains/sua_sub_congtrinh/".$object_id.'/'.$model.'/'.$data[$model]['id'],
					array( "update" => "#window_content",
						   'before' => '$(\'#loading\').show()',
						   'success' => "$(\"#loading\").hide();window_show();",
						   "title" => "Sửa mục này" ) ); ?>&nbsp;|&nbsp;
			<a href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]['id']; ?>, 'Congtrinh')">Xóa</a>
		<?php } ?>
	</div>
<?php $STT ++;}?>
<?php echo $this->element( 'paging', array( 'update' => "subcontent" ) ); ?>