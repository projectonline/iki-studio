<div class = "OD_ci_content">
	<ul id = "OD_left_maintabs ">
		<li id = "OD_thongtinproject" class = "OD_litabs <?php if($trangthai == 1) echo 'OD_tabhover'; ?>" style = "float: left; width: 93px;top:1px;position:relative">
			<?php
				echo $this->Js->link( 'Thông tin dự án', '/tamsteps/view_thongtinduan/'.$congtrinh_id,
					array(
						'update' => '#OD_contain',
					   	'before' => '$(\'#loading\').show()',
					   	'success' => '$(\'#loading\').hide();',
					  	'title' => 'Hãy nhấp vào thông tin dự án'
					 ));
			?>
		</li>
		<li id = "OD_thongtinchudautu" class="OD_litabs  <?php if($trangthai == 2) echo 'OD_tabhover'; ?>" style="float: left; margin-left: 3px; width: 247px; top:1px;position:relative">
			<?php
				if($this->data['Congtrinh']['complete_step2'] >= 1)
				{
					//echo "aaaaaaa";
					echo $this->Js->link( 'Thông tin chủ đầu tư và các đơn vị liên quan', '/tamsteps/view_thongtinchudautu/'.$congtrinh_id,
						array(
							'class' => 'enable',
							'update' => '#OD_contain',
						   	'before' => '$(\'#loading\').show()',
						   	'success' => '$(\'#loading\').hide();',
						   	'title' => 'Thông tin chủ đầu tư và các đơn vị liên quan'
						));
				}
				else
					echo '<a href=\'javascript:void(0)\'>Thông tin chủ đầu tư và các đơn vị liên quan</a>';
			?>
		</li>
	</ul>
</div>
<script>
	$("#OD_thongtinchudautu a").click(function(){
		if(!$(this).hasClass("enable"))
			window_show_OD("Bạn vui lòng nhập đầy đủ thông tinh dự án trước khi qua tab thông tin chủ đầu tư.");
	});
</script>