<div id="containInput">
	<span id="many_input_file">
		<input id="files_multi_1" type="file" name="data[File][]" multiple="multiple" <?php if (!isset( $this->data[$model]['id'] )){ echo 'class="checkempty"'; } ?> />
	</span>
  <a id="click_to_add_many_input_file" onclick="click_to_add_many_input_file()" rel="2" href="javascript:void(0)" style="background:url(/img/upload_progress_plus.png) no-repeat scroll 1px -2px transparent" title="Thêm một upload nữa để chọn file upload từ các thư mục khác nhau">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
	<div id="progress" style="display:none">
      <div id="bar"></div >
      <div id="percent">0%</div >
  </div>
  <div><img src="/img/icon_new.png"><i style="color:#3AAB5B">bạn có thể giữ phím Ctrl chọn nhiều file cùng lúc</i></div>
</div>
<?php
	$multi_file = true;
	if(isset($one_file) && $one_file == 1)
		$multi_file = false;
?>
<?php if( $multi_file ){ ?><center style="font-size:9px">(Chú ý: dung lượng 1 file không quá <?php echo MAX_SIZE_UPLOAD; ?> MB và tổng tất cả file 1 lần upload không quá <?php echo POST_MAX_SIZE; ?> MB)</center><?php } ?>
<script type="text/javascript">
  $(function() {
    var progress = $("#progress");
    var bar      = $("#bar");
    var percent  = $("#percent");
    // var status = $('#status');
    ///////////////////////////////////////
    $("#FormSubmitFile").ajaxForm({
      beforeSubmit: function() {
        $("input[type=submit]", "#FormSubmitFile").each(function(){
          $(this).attr("disabled", "disabled");
        });
        var error_input = false;
        $(".checkempty", "#FormSubmitFile").each(function(){
          if ($.trim($(this).val()) == "")
            error_input = true;
        });
        if (error_input) {
          $("#message_error_ajaxupload").show();
          setTimeout('$("#loading").hide();', 800);
          $("input[type=submit]", "#FormSubmitFile").each(function(){
            $(this).removeAttr("disabled");
          });
          return false;
        }
      },
      beforeSend: function() {
        // reset progress
        progress.show();
        var percentVal = '0%';
        bar.width(percentVal)
        percent.html(percentVal);
        return false;
      },
      uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        if (percentComplete == 100) {percentComplete = 99;};
        bar.width(percentVal)
        percent.html(percentVal);
      },
      success: function() {
        var percentVal = '100%';
        bar.width(percentVal)
        percent.html(percentVal);
      },
      complete: function(xhr) {
        console.log(xhr);
        if (xhr.responseText == 'ok')
          location.reload(false);
        else
          window.location = "http://" + xhr.responseText;
        // location.reload(false);
        // progress.hide();
        // status.html(xhr.responseText);
      }
    });
    ///////////////////////////////////////
  });
  function click_to_add_many_input_file()
  {
    var tmp = $("#click_to_add_many_input_file");
    var number = tmp.attr("rel");

    $("#many_input_file").append('<br><input id="files_multi_' + number + '" type="file" name="data[File][]" multiple="multiple" />');

    addListener_to_input_file("files_multi_" + number);

    tmp.attr("rel", (number + 1));
    return false;
  }
</script>
