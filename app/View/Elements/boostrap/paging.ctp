<?php $this->Paginator->options['url'] = array_merge($this->passedArgs,$search_URL);
  if(!isset($add_js_more))$add_js_more = "";
  if(isset($update)){
    $this->Paginator->options(array(
      'update' => ((substr($update,0,1) == '#')?'':'#').$update,
      'evalScripts' => true,
      'before' => '$(\'#loading\').show()',
      'success' => '$("#loading").hide();$.scrollTo( "'.((substr($update,0,1) == '#')?'':'#').$update.'", 300, {offset:-90} );'.$add_js_more . 'console.log(123);'
    ));
  } ?>
<?php if(isset($show_info) && is_string($show_info)){
    echo "<p><b>Số ".$show_info." tìm thấy là <font color='red'>".$this->Paginator->counter(array('format' => __('%count%')))."</font> ".$show_info.".</b></p>";
  }
?>
<?php if( $this->Paginator->hasPage( null, 2 ) ){ ?>
<nav>
  <ul class="pagination">
    <?php echo $this->Paginator->first('<< Đầu', array('tag' => 'li')) ?>
    <?php echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a')) ?>
    <?php echo $this->Paginator->last('Cuối >>', array('tag' => 'li')) ?>
  </ul>
</nav>
<?php } ?>
