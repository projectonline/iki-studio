<div id="header">
	<a class="facebook_square32 tsc_social_square32" title="Facebook" href="https://www.instagram.com/cpgarchitects/"><img src="/img/in-36.png" width="36" height="36" border="0" style=" position: absolute; right: 0; " /></a>
	<a class="linkedin_square32 tsc_social_square32" title="LinkedIn" href="http://www.linkedin.com/company/cpg-architects"><img src="/img/link-36.png" width="36" height="36" border="0" style=" position: absolute; right: 50px; " /></a>
	<a class="linkedin_square32 tsc_social_square32" title="Twitter" href="https://twitter.com/cpgarchitects"><img src="/img/tw-36.png" width="36" height="36" border="0" style=" position: absolute; right: 100px; " /></a>
	<a class="linkedin_square32 tsc_social_square32" title="Pinterest" href="https://www.facebook.com/pages/CPG-Architects/434487720005339"><img
	src="/img/fb-36.png" width="36" height="36" border="0" style=" position: absolute; right: 150px; " /></a>
	<div id="logo">
		<a style="border:none" href="index.php"><img style="border:none;max-height: 72px;" src="/img/logo.png" /></a>
	</div>
	<a href="javascript:" onclick="$('#navigation').fadeIn();" id="menu-icon-mobile" style="display:none">&nbsp;</a>
	<div id="navigation" style="">
		<div id="sliderBar">
			
		</div>
		<ul>
			<li><a class="topMenuHighLighter1" href="/">Home</a></li>
			<li><a class="topMenuHighLighter2" href="/about">About Us</a></li>
			<li><a class="topMenuHighLighter8" href="/news">News</a></li>
			<li><a class="topMenuHighLighter3" href="/gallery">Gallery of Work</a></li>
			<li><a class="topMenuHighLighter4" href="/purpose">Purpose</a></li>
			<li><a class="topMenuHighLighter7" href="/contact">Contact Us</a></li>
			<li><a class="topMenuHighLighter8" href="/admin">Login</a></li>
		</ul>
	</div>
	
</div>