<ul class="UlDecimal">
	<?php

	$tmp = array();
	if( isset($data["File"]) )
	{
		$tmp = $data["File"];

	}elseif( isset($this->request->data["File"]) ){
		$tmp = $this->request->data["File"];
	}

	foreach($tmp as $file){ ?>
		<li id="Uploader_<?php echo $file['id']; ?>">

			<?php echo $this->element("link_download_file", array(
					"id_file" => $file["id"],
					"name_file" => $file["name"],
					"dungluong_file" => $file["dungluong"]
				));
			?>

			<?php if(isset($admin) || (is_numeric($file['nguoitao']) && ($file['nguoitao'] == AuthComponent::user('id')))){ ?>
				<a onclick="xoa_ajax('Uploader',<?php echo $file['id']; ?>)" href="javascript:void(0)">Xóa</a>
			<?php } ?>
		</li>
	<?php } ?>
</ul>