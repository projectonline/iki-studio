<?php if( !isset( $id_textarea ) )$id_textarea = $controller.'_textarea_id'; ?>

<script type="text/javascript">
	$(function (){

		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "<?php echo $id_textarea; ?>",
			theme : "advanced",
			skin : "o2k7",
			plugins : "lists,advhr,advimage,advlink,emotions,iespell,paste,directionality,visualchars,inlinepopups",
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,fontsizeselect,forecolor,backcolor,sub,sup,charmap,bullist,numlist",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			// Example content CSS (should be your site CSS)
			//content_css : "css/content.css",
			// Drop lists for link/image/media/template dialogs
			//template_external_list_url : "lists/template_list.js",
			//external_link_list_url : "lists/link_list.js",
			//external_image_list_url : "lists/image_list.js",
			//media_external_list_url : "lists/media_list.js",
			// Replace values for the template plugin
			// template_replace_values : {
			// 	username : "Some User",
			// 	staffid : "991234"
			// }
		});
	});
</script>