<?php echo $this->Form->create($model, array('type' => 'GET')); ?>
<div id="divChucNang">
	<input value=" Tìm " type="submit" class="Button">
</div>
<div id="headerArea"><h2>Danh sách</h2></div>

<?php if( isset( $message ) ) echo  $message ; ?>
<table class="Data">
			<tr>
				<th width="4%">STT</td>
				<td>Tên <?php echo $name; ?></td>
				<th width="25%">Công Trình</td>
				<th width="10%">File</td>
				<th width="10%">Người Nhập</td>
				<th width="4%">Xóa</td>
			</tr>
			<tr>
				<td></td>
				<td><?php echo $this->Form->input("tieude", array("label" => false ));?></td>
				<td><?php echo $this->Form->input("congtrinh", array("label" => false ));?></td>
				<td></td>
				<td><?php echo $this->Form->input("username", array("label" => false ));?></td>
				<td></td>
			 </tr>
			<?php $i = 1; $STT = 1;
		if( $this->request->params['paging'][$model]['page'] > 1 )
		{
			$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
		} ?>
			<?php foreach($datas as $data){ ?>
			<tr id="<?php echo $model;?>_<?php echo $data[$model]["id"];?>" class="row<?php echo $i; $i = 3 - $i;?>">
				<td><?php echo $STT; ?><?php if( (strtotime($data[$model]["modified"]) + 640800) > strtotime("now") ){ ?>
					<img src="/img/icon_new.png" /><?php } ?></td>
				<td><?php echo $data[$model]["tieude"]; ?></td>
				<td><a href="/congtrinhs/view/<?php echo $data["Congtrinh"]["id"]."/".$controller;?>#scrollsubcontent">
					<?php echo $data["Congtrinh"]["tieude"]; ?></a>
				</td>
				<td><?php foreach( $data["File"] as $file ){
							echo $this->element("link_download_file", array(
								"id_file" => $file["id"],
								"name_file" => $file["name"],
								"dungluong_file" => $file["dungluong"] ));
						}
						if(isset($data["File"][1])){
							echo "<br>".$this->element("link_download_file_all", array(
								"id_controller" => $data[$model]["id"],
								"count_num_file" => count($data["File"]),
								"controller" => strtolower($model)."s"
							))."<br>";
						} ?>
				</td>
				<td><?php echo $this->element("avatar_view",array(
						"created" => $data[0]["created"],
						"dataUser" => $data["Nguoitao"]
					)); ?>
				</td>
				<td><?php if(isset($admin) || (AuthComponent::user('id') == $data["Nguoitao"]["id"])){ ?>
						<a href="/<?php echo $controller."/sua/".$data["Congtrinh"]["id"]."/".$data[$model]["id"]; ?>" title="Sửa bản vẽ này">Sửa</a>
						<a onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]["id"]; ?>)" href="javascript:void(0)" title="Xóa bản vẽ này">Xóa</a>
					<?php } ?>
				</td>
			</tr>
			<?php $STT++; } ?>
		</table>
		<?php echo $this->element( "paging" ); ?>
<?php echo $this->Form->end(); ?>