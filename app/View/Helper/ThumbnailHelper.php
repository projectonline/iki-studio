<?php
//vendor('phpthumb'.DS.'phpthumb.class');
App::import('Vendor', 'phpthumb', array('file' => 'phpthumb'.DS.'phpthumb.class.php'));

class ThumbnailHelper extends AppHelper {

	function make_links_blank($text)
	{
		$text = str_replace("\n", "<br>", $text);
		return  preg_replace(
		 array(
		   '/(?(?=<a[^>]*>.+<\/a>)
				 (?:<a[^>]*>.+<\/a>)
				 |
				 ([^="\']?)((?:https?|ftp|bf2|):\/\/[^<> \n\r]+)
			 )/iex',
		   '/<a([^>]*)target="?[^"\']+"?/i',
		   '/<a([^>]+)>/i',
		   '/(^|\s)(www.[^<> \n\r]+)/iex',
		   '/(([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-]+)
		   (\\.[A-Za-z0-9-]+)*)/iex'
		   ),
		 array(
		   "stripslashes((strlen('\\2')>0?'\\1<a href=\"\\2\">'.substr('\\2', 0, 60).'</a>\\3':'\\0'))",
		   '<a\\1',
		   '<a\\1 target="_blank">',
		   "stripslashes((strlen('\\2')>0?'\\1<a href=\"http://\\2\">'.substr('\\2', 0, 60).'</a>\\3':'\\0'))",
		   "stripslashes((strlen('\\2')>0?'<a href=\"mailto:\\0\">\\0</a>':'\\0'))"
		   ),
		   $text
	   );
	}

	private $php_thumb;
	private $options;
	private $tag_options;
	private $file_extension;
	private $cache_filename;
	private $error;

	private function init($options = array(), $tag_options = array())    {
		$this->options = $options;
		$this->tag_options = $tag_options;
		$this->set_file_extension();
		$this->set_cache_filename();
		$this->error = '';
	}

	private function set_file_extension()    {
		$this->file_extension = substr($this->options['src'], strrpos($this->options['src'], '.'), strlen($this->options['src']));
	}

	private function set_cache_filename()    {
		ksort($this->options);
		$filename_parts = array();
		$cacheable_properties = array('src', 'new', 'w', 'h', 'wp', 'hp', 'wl', 'hl', 'ws', 'hs', 'f', 'q', 'sx', 'sy', 'sw', 'sh', 'zc', 'bc', 'bg', 'fltr');
		foreach($this->options as $key => $value)    {
			if(in_array($key, $cacheable_properties))    {
				$filename_parts[$key] = $value;
			}
		}

		$this->cache_filename = '';
		foreach($filename_parts as $key => $value)    {
			$this->cache_filename .= $key . $value;
		}
		$this->cache_filename = $this->options['save_path'] . DS . md5($this->cache_filename) . $this->file_extension;
	}

	private function image_is_cached()    {
		if(is_file($this->cache_filename))    {
			return true;
		} else    {
			return false;
		}
	}

	private function create_thumb()    {
		$this->php_thumb = new phpThumb();

		$this->php_thumb->config_allow_src_above_docroot = true;
		$this->php_thumb->config_document_root = APP; //namnb; 20/0710 WWW_ROOT;//APP;

		foreach($this->php_thumb as $var => $value) {
			if(isset($this->options[$var]))    {
				$this->php_thumb->setParameter($var, $this->options[$var]);
			}
		}
		if($this->php_thumb->GenerateThumbnail()) {
			$this->php_thumb->RenderToFile($this->cache_filename);
		} else {
			$this->error = ereg_replace("[^A-Za-z0-9\/: .]", "", $this->php_thumb->fatalerror);
			$this->error = str_replace('phpThumb v1.7.8200709161750', '', $this->error);
		}
	}

	private function show_image_tag($url = false)    {
		if($this->error != '')    {
			$src = $this->options['error_image_path'];
			//$this->tag_options['alt'] = $this->error; namnb 10/11/2010 10:32
		}else {
			$src = $this->options['display_path'] . '/' . substr($this->cache_filename, strrpos($this->cache_filename, DS) + 1, strlen($this->cache_filename));
		}

		// 09/02/2011
		if($url)return $src;
		$img_tag = '<img src="' . $src . '"';

		// --- namnb: change ---
		if(isset($this->tag_options['style']) == false) // neu ko co style
		{
			$my_tag = '';
			if(isset($this->options['w']))    {
				$my_tag = 'width:'.$this->options['w'].'px;';
				//$img_tag .= ' width="' . $this->options['w'] . '"';
			}
			if(isset($this->options['h']))    {
				$my_tag .= 'height:'.$this->options['h'].'px';
				//$img_tag .= ' height="' .  $this->options['h'] . '"';
			}
			if( strlen( $my_tag ) > 0 )
			{
				$img_tag .= ' style="' . $my_tag . '"';
			}// --- end ------------
		}

		foreach($this->tag_options as $key => $value)    {
			$img_tag .= ' ' . $key . '="' . $value . '"';
		}
		$img_tag .=  ' />';

		return $img_tag;
	}

	public function show($options = array(), $tag_options = array())    {
		if(count($tag_options) > 0){
			$options['id'] = $tag_options['id'];
		}
		$options['save_path'] = IMAGE_THUMBNAIL;
		//pr($options['src']);
		$options['display_path'] = '/upload/thumbnails';//URL_STATIC . '/upload/thumbnails';
		$options['error_image_path'] = '/img/default.jpg';//URL_STATIC . '/img/noimage.jpg';

		if( !file_exists($options['src']) ){
			return '<img src="/upload/avatar_all/default.jpg" />';
		}

		if( !isset( $options['w'] ) )
		{
			$options['w'] = IMAGE_THUMBNAIL_WIDTH;
			$options['wp'] = IMAGE_THUMBNAIL_WIDTH;
			$options['wl'] = IMAGE_THUMBNAIL_WIDTH;
		}
		else
		{
			$options['wp'] = $options['w'];
			$options['wl'] = $options['w'];
		}

		if( !isset( $options['h'] ) )
		{
			$options['h'] = IMAGE_THUMBNAIL_HEIGHT;
			$options['hp'] = IMAGE_THUMBNAIL_HEIGHT;
			$options['hl'] = IMAGE_THUMBNAIL_HEIGHT;
		}
		else
		{
			$options['hp'] = $options['h'];
			$options['hl'] = $options['h'];
		}
		$options['q'] = IMAGE_THUMBNAIL_Q;
		$options['zc'] = IMAGE_THUMBNAIL_ZC;
		$options['f'] = IMAGE_THUMBNAIL_FORMAT;

//		if( isset( $tag_options["class"] ) == false )
//		{
//			$tag_options['class'] = 'image_thumnail';
//		}

		//namnb: them vao alt de valid html
//		if( isset( $tag_options["alt"] ) == false )
//		{
//			$tag_options["alt"] = "Anh";
//		}
		// end

		$this->init($options, $tag_options);
		if(!$this->image_is_cached())    {
			$this->create_thumb();
		}

		if(isset($options["url_return"]))return $this->show_image_tag(true);
		return $this->show_image_tag();
	}
	public function show_caolanh($options = array(), $tag_options = array())    {
		$options['save_path'] = IMAGE_THUMBNAIL;
		$options['display_path'] = '/upload/thumbnails';//URL_STATIC . '/upload/thumbnails';
		$options['error_image_path'] = '/img/default.jpg';//URL_STATIC . '/img/noimage.jpg';

		if( !file_exists($options['src']) ){
			return '<img src="/upload/avatar_all/default.jpg" style = "max-height: 25px; width: 15px;" />';
		}

		if( !isset( $options['w'] ) )
		{
			$options['w'] = IMAGE_THUMBNAIL_WIDTH;
			$options['wp'] = IMAGE_THUMBNAIL_WIDTH;
			$options['wl'] = IMAGE_THUMBNAIL_WIDTH;
		}
		else
		{
			$options['wp'] = $options['w'];
			$options['wl'] = $options['w'];
		}

		if( !isset( $options['h'] ) )
		{
			$options['h'] = IMAGE_THUMBNAIL_HEIGHT;
			$options['hp'] = IMAGE_THUMBNAIL_HEIGHT;
			$options['hl'] = IMAGE_THUMBNAIL_HEIGHT;
		}
		else
		{
			$options['hp'] = $options['h'];
			$options['hl'] = $options['h'];
		}
		$options['q'] = IMAGE_THUMBNAIL_Q;
		$options['zc'] = IMAGE_THUMBNAIL_ZC;
		$options['f'] = IMAGE_THUMBNAIL_FORMAT;

//		if( isset( $tag_options["class"] ) == false )
//		{
//			$tag_options['class'] = 'image_thumnail';
//		}

		//namnb: them vao alt de valid html
//		if( isset( $tag_options["alt"] ) == false )
//		{
//			$tag_options["alt"] = "Anh";
//		}
		// end

		$this->init($options, $tag_options);
		if(!$this->image_is_cached())    {
			$this->create_thumb();
		}

		if(isset($options["url_return"]))return $this->show_image_tag(true);
		return $this->show_image_tag();
	}

}
?>
