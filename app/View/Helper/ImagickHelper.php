<?php
class ImagickHelper extends AppHelper {

	var $helpers = array('Html');
	/**
	* path to cache folder
	*
	* @var string
	* @access public
	*/
	// var $cacheDirPath = APP.'upload/imagick';
	/**
	* path to ImageMagick convert tool
	*
	* @var string
	* @access public
	*/
	var $convertPath = '/usr/bin/convert';

	/**
	 * Automatically resizes an image and returns formatted img tag or only url (optional)
	*
	* @param string $filePath Path to the image file, relative to the webroot/ directory.
	* 				// namnb: là đường dẫn trực tiếp trên thư mục linux, ko phải gián tiếp
	* @param integer $width Width of returned image
	* @param integer $height Height of returned image
	* @param boolean $tag Return html tag (default: true)
	* @param boolean $cacheDirPath Path to cache folder (default: this->cachePath)
	* @param boolean $quality JPEG Quality (default: 100)
	* @param boolean $aspect Maintain aspect ratio (default: true, 1 = maintain ratio, 2 = cut image to fit)
	* @param boolean $fill Maintain aspect ratio & fill (default: false)
	* @param array    $options Array of HTML attributes.
	* @access public
	*/
	function resize($filePath, $width, $height, $return_image=true, $quality=100, $aspect=true, $options = array())
	{
		$cacheDirPath = SMALL.'imagick'.DS; // nhớ tạo thư mục này trước
		if( file_exists($filePath) )
		{
			// namnb 29/07/2010
			//$fullpath = ROOT.DS.APP_DIR.DS.WEBROOT_DIR.DS.$this->themeWeb;

			//if(!$cacheDirPath) $cacheDirPath = $this->cachePath;
			//$url = $fullpath.$filePath;

			$cachefilePath = $cacheDirPath.$width.'x'.$height.'_'.basename($filePath);
			// end

			// Verify image exist
			if (!($size = getimagesize($filePath))) {
				return;
			}

			// namnb 29/07/2010
			// Relative file (for return use)
			// $relfile = $this->request->webroot.$this->themeWeb.$cacheDirPath.'/'.$width.'x'.$height.'_'.basename($filePath);

			// Location on server (for resize use)
			//$cachefile = $fullpath.$cacheDirPath.DS.$width.'x'.$height.'_'.basename($filePath);

			// Verify if file is cached
			$cached = false;
			if (file_exists($cachefilePath)) {
				if (@filemtime($cachefilePath) > @filemtime($filePath)) {//@filemtime($url)) {
					$cached = true;
				}
			}

			// Verify resize neccessity
			$resize = false;
			if (!$cached)
			{
				// namnb: kiểm tra kích thước file và resize tương ứng

				// nếu chiều rộng lớn hơn chiều cao thì sẽ lấy tương ứng theo chiều rộng
				if( $size[0] > $size[1] )
				{
					// nếu chiều rộng của hình lớn hơn quy định
					if( $size[0] > $width )
					{
						$height = floor( ($size[1]*$width)/$size[0] ); // chiều cao mới
						$resize = true;
					}
				}else{
					// nếu chiều cao của hình lớn hơn quy định
					if( $size[1] > $height )
					{
						$width = floor( ($size[0]*$height)/$size[1] ); // chiều rộng mới
						$resize = true;
					}
				}
				// $resize = ($size[0] > $width || $size[1] > $height) || ($size[0] < $width || $size[1] < $height);
			}

			$jpgOptions = '';
			if ($resize)
			{
				if($aspect)
				{
					// Use Image Magick build-in keep ratio option
					if($aspect==1)
					{
						$resizeOption = '\'>\'';
					}
					elseif($aspect==2)
					{
						$resizeOption = '^ -gravity center -extent '.$width.'x'.$height;
					}
				} else {
					$resizeOption = '';
				}
				//exec($this->convertPath.' -resize '.$width.'x'.$height.$resizeOption.' -quality '.$quality.$jpgOptions.' '.escapeshellarg($url).' '.escapeshellarg($cachefile).'');
				exec($this->convertPath.' -resize '.$width.'x'.$height.$resizeOption.' -quality '.$quality.$jpgOptions.' '.escapeshellarg($filePath).' '.escapeshellarg($cachefilePath).'');
			}else
			{
				// No resize and no cache, copy image to destination
				if(!$cached)
				{
					//copy($url, $cachefile);
					copy($filePath, $cachefilePath);
				}
			}
		}else{
			$cachefilePath = IMAGES."noimage.jpg";
		}

		if($return_image){
			return $this->Html->image( "/upload/".str_replace( SMALL, "", $cachefilePath ), $options);
		} else {
			return "/upload/".str_replace( SMALL, "", $cachefilePath );
		}
	}
}
?>
