<?php
// Original idea from Matt Curry, rewritten by Eugenio Fage
App::uses('AppHelper', 'View/Helper');

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class CacheHtmlHelper extends AppHelper {
	private $CacheHtmlStrategy=null;

	public function __construct(View $view, $settings = array()) {
		parent::__construct($view, $settings);
		$defaults=array('CacheHtmlStrategy'=>'CacheHtmlStrategy');
		$settings=array_merge($settings,$defaults);

		if(!class_exists($settings['CacheHtmlStrategy'])) $settings['CacheHtmlStrategy']='CacheHtmlStrategy';
		$this->CacheStrategy=new $settings['CacheHtmlStrategy']();
	}

	public function afterLayout($viewFile) {
		$this->CacheStrategy->save($this->request,$this->_View->output);
	}
}


class CacheHtmlStrategy{
	public function save($request,$content){

//		if( !empty($request->data) )
//		{
//			$model = substr(Inflector::camelize($request->params['controller']), 0, -1);
//		}

		$File = new File($this->url2file($request->url), true);
		$File->write($content);
	}

	public function url2file($url){
		$path=WWW_ROOT.'cache'.DS;
		$path.=implode(DS, array_filter(explode('/', $url))).DS;
		$path=str_replace('//','/',$path);

		//namnb
		$path=str_replace(':','_',$path);

		return $path.'index.html';
	}

	public function rendercache($url=null){
		if(!$url){
			if(isset($_SERVER['PATH_INFO']) && $_SERVER['PATH_INFO']){
				$url=$_SERVER['PATH_INFO'];
			}else{
				return;
			}
		}
		$file=cacheUrl2File($this->url2file($url));

		if(file_exists($file)){
			$fp = fopen($file, 'r');
			fpassthru($fp);
			exit;
		}
	}
}