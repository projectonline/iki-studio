<legend>Upload File Vào Công Trình</legend>

<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create($model, array( 'type' => 'file' )); ?>

<table class="CssTable">

<?php if( $loai == 'congvan' || $loai == 'congvandi' || $loai == 'congvanden'  ){ ?>
<tr>
	<td class="title">Số Công Văn (*):</td>
	<td><?php echo $this->Form->input($model.".maso", array("size" => 60));?></td>
</tr>
<?php }if($loai == 'congvanden') {?>
<tr>
	<td class="title">Nơi gửi (*):</td>
	<td><?php echo $this->Form->input($model.".noinhan", array("size" => 60));?></td>
</tr>
<?php } else if($loai == 'congvan' || $loai =='congvandi'){ ?>
	<tr>
		<td class="title">Nơi nhận (*):</td>
		<td><?php echo $this->Form->input($model.".noinhan", array("size" => 60));?></td>
	</tr>
<?php }?>
<tr>
	<td class="title">Tiêu đề (*):</td>
	<td><?php echo $this->Form->input($model.".tieude", array("size" => 60));?></td>
</tr>

<tr>
	<td  class="title">Mô tả về file được upload (*):</td>
	<td><?php echo $this->Form->input($model.".mota", array("rows" => 5, "cols" => 50));?></td>
</tr>

<tr>
	<td  class="title">Upload File (*):</td>
	<td>
		<?php  if( isset( $this->data[$model]['id'] ) )
				echo $this->element( "list_files" ); ?>
		<!--<input type="file" name="data[File][]" multiple="multiple" />-->
		<?php echo $this->element( "upload_progress" ); ?>
	</td>
</tr>
</table>
<div style="background-color: rgb(242, 242, 242);  margin-left: -13px;margin-right: -12px;">
	<table table style="margin-left: -10px; width: 100%; height: 50px;">
	<tr>
		<td  class="Center" colspan="2" style="padding-left: 0px; padding-right: 0px; margin-bottom: 9px; border-bottom-width: 0px;">		
			<input style="font-weight: bold; opacity: 0.75; padding-bottom: 5px; padding-left: 5px;padding-right: 6px;padding-top: 2px;text-align: center;" title="Lưu" value=" Lưu thông tin " type="submit" class="Button">
			<!--<a onclick="$('#window_content').dialog( 'close' );" href="javascript:void(0)" class="Button" style="float: right;">Đóng lại</a>-->
			<a onclick="$('#window_content').dialog( 'close' );" href="javascript:void(0)" class="mrs mlm _42ft _4jy0 _4jy3 _517h" rel="dialog" role="button" style="float: right; margin-right: 13px;">	
				Đóng lại
			</a>
		</td>
	</tr>
	</table>
</div>

<?php echo $this->Form->end(); ?>
<!--DIULT : THEM JAVA THEO DOI HOP DONG-->
<script type="text/javascript">

	function them_theo_doi_hd(checked)
	{
		$("#TDHD_text_div").slideToggle();		
	}
	function them_loi_nhan_trang_chu(checked)
	{
		$("#QuanTrong_text_div").slideToggle();		
	}
</script>
<style type="text/css">
	.ui-widget-content {    
    background-attachment: scroll;    
    background-color: rgb(252, 253, 253);
    background-image: url("images/ui-bg_inset-hard_100_fcfdfd_1x100.png");
   
    background-position: 50% bottom;
    background-repeat: repeat-x;
    background-size: auto auto;
    border-bottom-color: rgb(99, 100, 101);
    border-bottom-style: solid;
    border-bottom-width: 2px;      
   
    border-right-color: rgb(99, 100, 101);
    border-left-color: rgb(99, 100, 101);
    border-right-width: 2px;
    border-left-width: 2px;
    border-top-color: rgb(99, 100, 101);
    border-top-style: solid;
    border-top-width: 2px;
    color: rgb(34, 34, 34);    
    height: auto;
    left: -1px;
    margin-right: -4px;
    max-height: none;
    min-height: 95px;
    width: auto;

}
.ui-dialog-content {
	 bottom: -9px;
}
.ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
    border-bottom-right-radius: 0px;
}
.ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
    border-bottom-left-radius: 0px;
}
.ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
    border-top-right-radius: 0px;
}
.ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
    border-top-left-radius: 0px;
}
.ui-widget-header {
	background-color:rgb(59, 89, 152);	
    background-attachment: scroll;   
    background-image: none;   
    background-position: 50% 50%;
    background-repeat: repeat-x;
    background-size: auto auto;
    border-bottom-color: rgb(66, 151, 215);
    border-bottom-style: solid;
    border-bottom-width: 1px;       
    border-top-color: rgb(66, 151, 215);
    border-top-style: solid;
    border-top-width: 1px;
    color: rgb(255, 255, 255);
    font-weight: bold;
}
.title{
	color: rgb(98, 97, 97);
    font-size: 12px;
    font-weight: bold;
}
</style>