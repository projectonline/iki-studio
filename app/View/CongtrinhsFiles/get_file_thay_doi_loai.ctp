<?php

if( isset( $datas[0] ) ){

	$option_loais = array(
		'bienban' => 'Biên Bản', 'bantinh' => 'Bản Tính',
		'giaodich' => 'TM - DToán','congvan' => 'Công Văn',		
		'khaosat' => 'KS Địa Hình','khaosatdc' => 'KS Địa Chất', 'phaply' => 'Pháp Lý',
		'thuvien' => 'Thư Viện', 'banve' => 'Bản Vẽ',
		'congvanden' => 'Công văn đến', 'congvandi' => 'Công văn đi',
		'nghiemthu' => 'Nghiệm Thu', 'kiemdinhthinghiem' => 'KĐ-TNghiệm',
		'ddahinhcongtrinh' => 'Hình CT'
	);
?>

<?php echo $this->Session->flash(); ?>

<table class="Data" style="margin-top: 4px;margin-bottom: 6px;">
	<tr>
		<td style="width: 18px;"><b>STT</b></td>
		<td><b>Tên File</b></td>
		<td><b>Attach</b></td>
		<td><b>Loại</b></td>
	</tr>
	<?php

		App::uses('NumberHelper', 'View/Helper');
		$toReadableSize = new NumberHelper($this);

	    $STT = 1;
		if( $this->request->params['paging']['Uploader']['page'] > 1 )
		{
			$STT = ($this->request->params['paging']['Uploader']['page']-1)*$this->request->params['paging']['Uploader']['limit'] + 1;
		}

		$dem_dong = 0;

		foreach( $datas as $data ){

			$dungluong = $toReadableSize->toReadableSize($data['Uploader']["dungluong"]);

			$dem_dong += 1;

	?>
	<tr id="<?php echo 'Uploader_'.$data['Uploader']['id']; ?>">
		<td><center><?php echo $STT++; ?></center></td>
		<td class="NgatDong">
			<?php echo $data['Uploader']["name"]; ?></a>
			<em>(<?php echo $dungluong; ?>)</em>

			<?php if( (strtotime($data["Uploader"]["created"]) + 129600) > strtotime("now")){ ?>
				<img src="/img/icon_new.png" style="width: 21px;" />
			<?php } ?>
		</td>
		<td>
			<center>
				<?php echo $this->Form->input("Attach.stick", array(
						"type" => "checkbox",
						"label" => "",
						"onclick" => 'chon_attach_file_nay('.$data['Uploader']['id'].', this);'
				));?>
			</center>

			<div id="chon_attach_file_nay_div_<?php echo $data['Uploader']['id']; ?>" style="display:none">
				<li id="chon_attach_file_nay_<?php echo $data['Uploader']['id']; ?>">
					<?php echo $this->element("link_download_file", array(
							"id_file" => $data['Uploader']["id"],
							"name_file" => wordwrap($data['Uploader']["name"], 10, "\n", true),
							"dungluong_file" => $dungluong
						));

						echo $this->Form->hidden("Attach.".$STT, array("value" => $data['Uploader']["id"]."_._".$data['Uploader']["name"]."_._".$dungluong ));
					?>
					<img style="cursor: pointer; width: 12px; height: 12px;" src="/img/icon/toggle_delete_phanhoi.png"
						onclick="xoa_chon_attach_file(<?php echo $data['Uploader']['id']; ?>)" title="Bỏ file này" />,&nbsp;
				</li>
			</div>

		</td>
		<td>
			<center>
				<span title="<?php echo $data['Uploader']['controller']; ?>">
					<?php

					if( isset($data['CongtrinhsFile']) && isset($data['CongtrinhsFile']["loai"]) )
					{
						if( isset($option_loais[$data['CongtrinhsFile']["loai"]]) )
						{
							echo $option_loais[$data['CongtrinhsFile']["loai"]];

						}else{
							echo $data['CongtrinhsFile']["loai"];
						}

					}else{
						echo 'T Cá Nhân';
					}

					//if(isset($option_loais[$data['Uploader']["controller"]])){ echo $option_loais[$data['Uploader']["controller"]]; }else{ echo '(khác)'; }

					 ?>
				</span>
			</center>
		</td>
	</tr>

	<?php } ?>

	<?php if( $dem_dong < 8 ){

			$tmp = 8- $dem_dong;
			for ($i=0; $i < $tmp; $i++) {

				echo '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
			}
		}
	?>
</table>

<?php echo $this->element( 'paging_search', array('update' => '#danh_sach_file', 'add_js_more' => 'AttachFile_load_khoi_tao();$( "#UploaderModelLoai", "#window_content_2" ).val( "' .$loai. '" );AttachFile_check_upload_vao_trangcanhan();') ); ?>

<?php }else{

	echo '<div style="margin: 60px 0"><center><em>(Không tìm thấy file nào)</em></center></div>';

 } ?>


<!-- Khung upload thêm file -->
<?php
	$option_loais = array(
		'banve' => 'Bản Vẽ',
		'phaply' => 'Pháp Lý',
		'bienban' => 'Biên Bản',
		'bantinh' => 'Bản Tính',
		'giaodich' => 'Thuyết minh - Dự toán',
		'congvan' => 'Công Văn',
		'khaosat' => 'KS Địa Hình',
		'khaosatdc' => 'KS Địa Chất',
		'congvanden' => 'Công văn đến',
		'congvandi' => 'Công văn đi',
		'nghiemthu' => 'Nghiệm Thu',
		'kiemdinhthinghiem' => 'Kiểm định - Thí Nghiệm',
		'congtrinhsbaocaotuan' => 'Báo cáo tuần',
		'congtrinhsbct' => 'Báo cáo tháng',
		//'trangcanhan' => 'TRANG CÁ NHÂN'
	);
?>

<div style="width: 400px; margin: 0 auto;">

	<?php echo $this->Form->create('Uploader', array(
			'type' => 'file',
			'id' => 'upload'
	)); ?>

		<div id="drop">

			<center><a id="clickUploadFileMoi" href="javascript:void(0)" onclick="$('#table_upload_file').show(); $(this).hide();">Upload file mới</a></center>

	        <div id="message_error_upload" class="message_error" style="display:none">Vui lòng nhập đầy đủ các mục (*)</div>

	        <table id="table_upload_file" class="CssTable" style="display: none">
	        	<tr>
	        		<td>Trong mục (*): </td>
	        		<td>
	        			<?php
	        				echo $this->Form->input('Uploader.model_loai', array(
								'empty' => '---',
								'options' => $option_loais,
								'onchange' => 'AttachFile_check_upload_vao_trangcanhan(this)',
								'label' => false,
								'style' => 'width: 278px'
							));
	        			?>
	        		</td>
	        	</tr>

	        	<tr class="Hide_upload_trangcanhan">
	        		<td>Công trình:</td>
	        		<td>
	        			<?php echo $this->Form->input('Uploader.congtrinh', array(
	        					'label' => false,
	        					'style' => 'width: 270px',
	        					'value' => $congtrinh['tieude'],
	        					'disabled' => true
	        			 ) ); ?>

	        			<?php echo $this->Form->hidden('Uploader.congtrinh_id', array(
	        					'value' => $congtrinh['id'],
	        			 ) ); ?>
	        		</td>
	        	</tr>

	        	<tr class="Hide_upload_trangcanhan">
	        		<td>Tiêu đề (*):</td>
	        		<td>
	        			<?php echo $this->Form->input('Uploader.tieude', array(
	        					'style' => 'width: 270px',
	        					'label' => false,
	        					'onblur' => 'AttachFile_kiem_tra_validate_khi_upload_moi()'
	        				));
	        			?>
	        		</td>
	        	</tr>

	        	<tr class="Hide_upload_trangcanhan">
	        		<td>Nội dung (*):</td>
	        		<td>
	        			<?php echo $this->Form->input('Uploader.noidung', array(
	        					'style' => 'width: 270px',
	        					'rows' => 3,
	        					'label' => false,
	        					'onblur' => 'AttachFile_kiem_tra_validate_khi_upload_moi()'
	        				));
	        			?>
	        		</td>
	        	</tr>

	        	<tr>
	        		<td colspan="2">
	        			<div>
		        			<a id="chon_file" href="javascript:void(0)" style="margin-top: 0; background-color:#BDC7D8">Browse files</a>
		        			<input type="file" name="file" multiple />
	        			</div>
	        		</td>
				</tr>

	        </table>
	    </div>

	    <ul>
	        <!-- The file uploads will be shown here -->
	    </ul>

	<?php echo $this->Form->end(); ?>
</div>