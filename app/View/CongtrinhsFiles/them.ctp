<legend>Upload File Vào Công Trình</legend>
<?php echo $this->Session->flash() ?>
<?php echo $this->Form->create($model, array( 'type' => 'file', 'id' => 'FormSubmitFile' )) ?>
<div id="message_error_ajaxupload" class="message_error" style="display:none">Vui lòng nhập đầy đủ các mục * và chọn file upload</div>
<table class="CssTable">
  <tr>
    <td class="title">Tiêu đề (*):</td>
    <td><?php echo $this->Form->input($model.".tieude", array("style" => 'width:386px', 'class' => 'checkempty')) ?></td>
  </tr>
  <tr>
    <td  class="title">Mô tả (*):</td>
    <td><?php echo $this->Form->input($model.".mota", array("rows" => 5, "style" => "width:386px", 'class' => 'checkempty')) ?></td>
  </tr>
  <tr>
    <td  class="title">Upload File <?php if (!isset( $this->data[$model]['id'] )){ echo '(*)'; } ?>:</td>
    <td>
      <?php
      if( isset( $this->data[$model]['id'] ) )
        echo $this->element( "list_files" );
      echo $this->element( "upload_progress" ) ?>
    </td>
  </tr>
  <!--DIULT: THEM THEO DOI HOP DONG-->
  <tr>
    <td colspan="2">
      <div class="title" style="margin-left: 5px;font-weight: 600;">
        <?php echo $this->Form->input('CongtrinhsKqtte.checkbox_TDHD', array(
          'type'    => 'checkbox',
          'class'   => 'title',
          'label'   => 'Thêm vào theo dõi hợp đồng',
          'onclick' => 'them_theo_doi_hd(this.checked)'
        )) ?>
      </div>
      <div style="<?php if( !isset($this->data['CongtrinhsKqtte']) || $this->data['CongtrinhsKqtte']['checkbox_TDHD'] == 0 ){ ?>display: none<?php } ?>" id="TDHD_text_div">
        <div style="border-bottom: 1px solid #D0D7E5;    padding-bottom: 8px;   padding-top: 8px;"> </div>
        <div  style="margin-top: 3px;">
          <div style="float:left;margin-left: 127px;">
            Ngày(*):
          </div>
          <?php echo $this->Form->input('CongtrinhsKqtte.ngay', array("style" => "width:140px;float:left;margin-left: 8px;", "readonly" => true )) ?>
          <?php echo $this->element("img_pop_calendar") ?>
        </div>
        <div style="margin-top: 15px;">
          <div style="float:left;margin-left: 127px;">
            Ghi Chú :
          </div>
          <?php echo $this->Form->input('CongtrinhsKqtte.ghichu', array(
                'rows' => 3,
                'style' => 'width: 350px;float: right;margin-right: 37px;',
                'maxlength' => 160,
                //'onchange' => 'count_send_text_length()'
            ));
          ?>
        </div>
      </div>
    </td>
  </tr>
  <!--DIULT: KET THUC THEM THEO DOI HOP DONG-->
  <?php if (is_numeric($id)){ ?>
  <tr>
    <td>Chuyển công trình khác:</td>
    <td><?php echo $this->Form->input($model.'.tencongtrinh', array(
        'style'    => 'width: 330px;',
        'div'      => false,
        'id'       => 'CongtrinhsCongviecTencongtrinh',
        'readonly' => true
    )) ?>
    <?php echo $this->Form->hidden($model.'.tencongtrinhid', array(
        'id' => 'CongtrinhsCongviecTencongtrinhid'
    )) ?>
    <?php echo $this->Form->hidden($model.'.modified') ?>
    <br>
    <em style="clear:both">(Click <?php echo $this->Js->link( '<img width="19px" src="/img/search.png">',
      '/congtrinhs/search_congtrinh_congviecs_them_cv',
      array(
        'update' => '#window_content_2',
        'before' => '$(\'#loading\').show()',
        'success' => "$(\"#loading\").hide();window_show_2();",
        "title" => "Mở khung tìm kiếm công trình",
        'escape' => false
    ) ) ?> để chọn công trình khác)</em></td>
  </tr>
  <tr>
    <td>Chuyển qua tab khác:</td>
    <td><?php echo $this->Form->input($model.".loai", array(
          "options" => array(
          	'baogia'               => 'Báo Giá',
            'hopdong'               => 'Hợp Đồng',            
            'danhbavts'            => 'Danh bạ CĐT',
            'phaply'               => 'Pháp lý',
            'congvanden'           => 'Công văn đến',
            'congvandi'            => 'Công văn đi',
            'congtrinhsbaocaotuan' => 'Báo cáo tuần',
            'congtrinhsbct'        => 'Báo cáo tháng',
            'nhatkycongtruongs'    => 'Nhật ký CTrường',
            'ddahinhcongtrinh'     => 'Hình công trình',
            'kiemdinhthinghiem'    => 'KĐịnh TNghiệm',
            'decuong'              => 'Đề cương',
            'dongiathinghiem'      => 'Đơn giá thí nghiệm',
            'bienban'              => 'Biên bản',
            'khaosat'              => 'Khảo sát ĐH',
            'khaosatdc'            => 'Khảo sát ĐC',
            'giaodich'             => 'Thuyết minh',
            'banve'                => 'Bản vẽ',
            'bantinh'              => 'Bản tính',
            'kcs'                  => 'File KCS',
            'buttichcdt'           => 'Bút tích CĐT',
            'knxulyhientruong'     => 'Kinh nghiệm - xử lý hiện trường',
            'congvan'              => 'Bảo Lãnh - Bảo Hiểm - Công Văn - Kế Toán',
          ),
          'empty' => '---'
        )  );
        ?></td>
  </tr>
  <?php } ?>
</table>
<div style="background-color: rgb(242, 242, 242);  margin-left: -13px;margin-right: -12px;">
  <table table style="margin-left: -10px; width: 100%; height: 50px;">
    <tr>
      <td  class="Center" colspan="2" style="padding-left: 0px; padding-right: 0px; margin-bottom: 9px; border-bottom-width: 0px;">
        <input style="font-weight: bold; opacity: 0.75; padding-bottom: 5px; padding-left: 5px;padding-right: 6px;padding-top: 2px;text-align: center;" title="Lưu" value=" Lưu thông tin " type="submit" class="Button">
        <!--<a onclick="$('#window_content').dialog( 'close' );" href="javascript:void(0)" class="Button" style="float: right;">Đóng lại</a>-->
        <a onclick="$('#window_content').dialog( 'close' );" href="javascript:void(0)" class="mrs mlm _42ft _4jy0 _4jy3 _517h" rel="dialog" role="button" style="float: right; margin-right: 13px;">
        Đóng lại
        </a>
      </td>
    </tr>
  </table>
</div>
<?php echo $this->Form->end() ?>
<!--DIULT : THEM JAVA THEO DOI HOP DONG-->
<script type="text/javascript">
function them_theo_doi_hd(checked)
{
  $("#TDHD_text_div").slideToggle();
}
function them_loi_nhan_trang_chu(checked)
{
  $("#QuanTrong_text_div").slideToggle();
}
</script>
<style type="text/css">
.ui-widget-content {
    background-attachment: scroll;
    background-color: rgb(252, 253, 253);
    background-image: url("images/ui-bg_inset-hard_100_fcfdfd_1x100.png");
    background-position: 50% bottom;
    background-repeat: repeat-x;
    background-size: auto auto;
    border-bottom-color: rgb(99, 100, 101);
    border-bottom-style: solid;
    border-bottom-width: 2px;
    border-right-color: rgb(99, 100, 101);
    border-left-color: rgb(99, 100, 101);
    border-right-width: 2px;
    border-left-width: 2px;
    border-top-color: rgb(99, 100, 101);
    border-top-style: solid;
    border-top-width: 2px;
    color: rgb(34, 34, 34);
    height: auto;
    left: -1px;
    margin-right: -4px;
    max-height: none;
    min-height: 95px;
    width: auto;
}
.ui-dialog-content {
bottom: -9px;
}
.ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
    border-bottom-right-radius: 0px;
}
.ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
    border-bottom-left-radius: 0px;
}
.ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
    border-top-right-radius: 0px;
}
.ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
    border-top-left-radius: 0px;
}
.ui-widget-header {
background-color:rgb(59, 89, 152);
    background-attachment: scroll;
    background-image: none;
    background-position: 50% 50%;
    background-repeat: repeat-x;
    background-size: auto auto;
    border-bottom-color: rgb(66, 151, 215);
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-top-color: rgb(66, 151, 215);
    border-top-style: solid;
    border-top-width: 1px;
    color: rgb(255, 255, 255);
    font-weight: bold;
}
.title{
color: rgb(98, 97, 97);
    font-size: 12px;
    font-weight: bold;
}
</style>
