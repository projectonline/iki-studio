<legend>Danh sách file bạn đã upload</legend>

<style type="text/css">
#AttachFile_danh_sach_lien_ket a{
	background-color: #3B5998;
	padding: 6px 8px;
	color: #FFF;
	border-radius: 2px;
	display: inline-block;
	margin-top: 3px;
	line-height: 1;
	text-decoration: none;
}
#AttachFile_danh_sach_lien_ket a:hover, #AttachFile_danh_sach_lien_ket a.active {
	background-color: #0986A3;
}

#danh_sach_file table.Data tr:first-child td {
	background-color: #EEE;
}
</style>

<div id="AttachFile_danh_sach_lien_ket" style="margin-bottom: 9px;">
	<a id="AttachFile_link_all" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('all')" class="active">Tất cả</a>	
	<a id="AttachFile_link_phaply" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('phaply')">Pháp lý</a>
	<a id="AttachFile_link_banve" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('banve')">Bản vẽ</a>
	<a id="AttachFile_link_bienban" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('bienban')">Biên bản</a>
	<a id="AttachFile_link_khaosat" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('khaosat')">KS Địa Hình</a>
	<a id="AttachFile_link_khaosat" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('khaosatdc')">KS Địa Chất</a>
	<a id="AttachFile_link_bantinh" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('bantinh')">Bản tính</a>

	<a id="AttachFile_show_them_lien_ket" href="javascript:void(0)" onclick="AttachFile_show_them_lien_ket(this, 1)"> >> </a>
	<span id="AttachFile_danh_sach_lien_ket_them" style="display: none">
		<a id="AttachFile_link_giaodich" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('giaodich')">TM - DToán</a>
		<br>
		<a id="AttachFile_link_baogia" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('baogia')">BG - HĐồng</a>
		<a id="AttachFile_link_congvan" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('congvan')">Công văn</a>
		<a id="AttachFile_link_kiemdinhthinghiem" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('kiemdinhthinghiem')">KĐ - TNghiệm</a>
		<a id="AttachFile_link_nghiemthu" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('nghiemthu')">Nghiệm thu</a>
		<a id="AttachFile_link_congvanden" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('congvanden')">CVăn đến</a>
		<a id="AttachFile_link_congvandi" href="javascript:void(0)" onclick="AttachFile_thay_doi_loai('congvandi')">CVăn đi</a>
		<a href="javascript:void(0)" onclick="AttachFile_show_them_lien_ket(this, 0)"> << </a>
	</span>
</div>

<?php echo $this->Session->flash(); ?>

<div style="width:610px">

	<div id="danh_sach_file">
		<?php echo $danh_sach_file; ?>
		<em style="clear:both">(Click <?php echo $this->Js->link( '<img width="14px" src="/img/search.png">',
						'/congtrinhs/search_congtrinh_attachfile',
						array( "update" => "#window_content_2",
							   'before' => '$(\'#loading\').show()',
							   'success' => "$(\"#loading\").hide();window_show_2();",
							   "title" => "Mở khung tìm kiếm công trình",
							   'escape' => false
			) ); ?> để chọn công trình)</em>
	</div>

	<div style="text-align:right;">Click đóng <a href="javascript:void(0)" onclick="parent.$('#window_content_2').dialog('close');" style="color: red; font-size: 19px">X</a> attach file nếu chọn xong.</div>

</div>

<style>
	.NgatDong{
		word-break: break-all;
		width: 401px;
	}

</style>

<script type="text/javascript">

function AttachFile_load_khoi_tao(){

    var ul = $('#upload ul');

    $('#chon_file').click(function(){

        // Simulate a click on the file input button
        // to show the file browser dialog
        if( AttachFile_kiem_tra_validate_khi_upload_moi() )
        {
        	$(this).parent().find('input').click();

        }else{

        	$("#message_error_upload", "#window_content_2").show();

        	setTimeout( '$("#message_error_upload", "#window_content_2").fadeOut();', 3000 );

        }

    });

    var number_of_files = 0;
    var number_of_file_process = 0;

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({

    	limitConcurrentUploads: 1,

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {

        	for (var x in data.files){

				number_of_files = number_of_files + 1;
			}

            var tpl = $('<li class="working"><input type="text" value="0" data-width="28" data-height="28"'+
                ' data-fgColor="chartreuse" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo(ul);

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function(){

                if(tpl.hasClass('working')){
                    jqXHR.abort();
                }

                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit();
        },

        progress: function(e, data){

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            progress = progress - 1;

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
            }
        },

        done: function(e, data){

        	number_of_file_process = number_of_file_process + 1;

        	// lúc done thì process mới chính xác
            data.context.removeClass('working');
            data.context.find('input').val(100).change();

        	if( number_of_file_process == number_of_files )
        	{
        		console.log($("#UploaderModelLoai").val());
        		$("#AttachFile_link_" + $("#UploaderModelLoai").val() ).click();

        	}

        },

        fail:function(e, data){
            // Something has gone wrong!
            console.log( 'fail' );
            console.log( e );
            console.log( data );
        }

    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

}
</script>


<script type="text/javascript">

	function AttachFile_thay_doi_loai( loai )
	{
		$("#loading").show();

		$.ajax({
			url: "/congtrinhs_files/get_file_thay_doi_loai/" + loai + "/<?php echo $congtrinh_id; ?>" ,
			timeout: 15000,
			error: error_handler,
			success: function(html){

				$("#danh_sach_file").html(html);

				$("#loading").hide();

				$("#AttachFile_danh_sach_lien_ket a").removeClass("active");

				$("#AttachFile_link_" +  loai).addClass("active");

				$( "#UploaderModelLoai", "#window_content_2" ).val( loai );

				AttachFile_check_upload_vao_trangcanhan();

				AttachFile_load_khoi_tao();

				return false;
			}
		});

		return false;
	}

	function AttachFile_show_them_lien_ket( object, loai )
	{
		if( loai )
		{
			$( object ).hide();
			$( "#AttachFile_danh_sach_lien_ket_them" ).show();
		}else{

			$( "#AttachFile_show_them_lien_ket" ).show();
			$( "#AttachFile_danh_sach_lien_ket_them" ).hide();
		}

	}

	function AttachFile_check_upload_vao_trangcanhan()
	{
		var contain = $("#window_content_2");

		if( $("#UploaderModelLoai", contain).val() == "trangcanhan" )
		{
			$("tr.Hide_upload_trangcanhan", contain).hide();

		}else{

			$("tr.Hide_upload_trangcanhan", contain).show();

			// $("#chon_file").css( "background-color", "#BDC7D8" );
		}

		AttachFile_kiem_tra_validate_khi_upload_moi();
	}

	function AttachFile_kiem_tra_validate_khi_upload_moi()
	{

		if( $("#UploaderModelLoai").val() == "trangcanhan" )
		{
			$("#chon_file").css( "background-color", "#3B5998" );

		}else{

			var check = true;
			if( $.trim( $("#UploaderTieude").val() ).length < 1 )
			{
				$("#UploaderTieude").css("border", "1px solid red");

				check = false;

			}else{
				$("#UploaderTieude").css("border", "1px solid #BDC7D8");
			}

			if( $.trim( $("#UploaderNoidung").val() ).length < 1 )
			{
				$("#UploaderNoidung").css("border", "1px solid red");

				check = false;
			}else{
				$("#UploaderNoidung").css("border", "1px solid #BDC7D8");
			}

			if( $.trim( $("#UploaderModelLoai").val() ).length < 1 )
			{
				$("#UploaderModelLoai").css("border", "1px solid red");

				check = false;
			}else{
				$("#UploaderModelLoai").css("border", "1px solid #BDC7D8");
			}

			if( !check )
			{
				$("#chon_file").css( "background-color", "#BDC7D8" );
			}else{

				$("#chon_file").css( "background-color", "#3B5998" );
			}

			return check;
		}

		return true;
	}

</script>