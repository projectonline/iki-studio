<legend>Thông tin</legend>
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create($model, array('type' => 'file')); ?>
<?php echo $this->element( 'message' );?>
<table class="Data">
    <tr>
        <td colspan="2">Thông tin</td>
    </tr>
    <tr>
        <td><?php echo __("Loại (*)"); ?></td>
        <td><?php echo $this->Form->input($model.'.loai', array('style' => 'width:175px', 'options' => $loai ));?></td>
    </tr>
    <tr>
        <td><?php echo __("Kiểu (*)"); ?></td>
        <td><?php echo $this->Form->input($model.'.nhan', array('style' => 'width:175px', 'options' => $nhan ));?></td>
    </tr>
    <tr>
        <td><?php echo __("Tên (*)"); ?></td>
        <td><?php echo $this->Form->input($model.'.ten', array('style' => 'width:180px', 'label' => false ));?></td>
    </tr>
    <tr>
        <td><?php echo __("Chức danh"); ?></td>
        <td><?php echo $this->Form->input($model.'.chucdanh', array('style' => 'width:180px', 'label' => false ));?></td>
    </tr>
    <tr>
        <td><?php echo __("Số điện thoại (*)"); ?></td>
        <td><?php echo $this->Form->input($model.'.dienthoai', array('style' => 'width:180px', 'label' => false )); ?></td>
    </tr>
    <tr>
        <td><?php echo __("Email"); ?></td>
        <td><?php echo $this->Form->input($model.'.email', array('style' => 'width:180px', 'label' => false )); ?></td>
    </tr>
    <tr>
        <td><?php echo __("Tên Công ty"); ?></td>
        <td><?php echo $this->Form->input($model.'.tencongty', array('style' => 'width:356px', 'label' => false )); ?></td>
    </tr>
    <tr>
        <td><?php echo __("Địa chỉ"); ?></td>
        <td><?php echo $this->Form->input($model.'.diachi', array('style' => 'width:356px', 'label' => false )); ?></td>
    </tr>
    <tr>
        <td><?php echo __("Công trình"); ?></td>
        <td><?php echo $this->Form->input($model.'.congtrinh_id', array('style' => 'width:356px', 'options' => $dsct ));?></td>
    </tr>
    <tr>
        <td><?php echo __("Hạng mục"); ?></td>
        <td><?php echo $this->Form->input($model.'.hangmuc', array('style' => 'width:356px', 'label' => false )); ?></td>
    </tr>
    <tr>
        <td colspan="2">
        <center>
             <input id="commentForm" title="Lưu" value=" Lưu " class="Button" type="submit">
        </center>
        </td>
    </tr>
</table>
<?php echo $this->Form->end(); ?>