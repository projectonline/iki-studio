<?php echo $this->Form->create($model); ?>
<div style="padding:20px">
    <div id="divChucNang">
    <?php if(isset($admin)){ ?>
        <?php echo $this->Js->link( "New", "/admin/doitacs/them/",
                    array(
                                'before'  => '$("#loading").show()',
                                'update'  => '#window_content',
                                'title'   => 'Thêm mới',
                                'class'   => 'Button',
                                'success' => "$(\"#loading\").hide();window_show();upload_iframe_window( 'dsdoitac', 'tinymce' );") );
            ?>
    <?php } ?>
    </div>

    <div id="dsdoitac" style="max-width:1000px">
        <table class="Data">
            <tr>
                <td style="width: 30px;">STT</td>
                <td style="width: 50px;">Loại</td>
                <td style="width: 50px;">Nhãn</td>
                <td>Tên</td>
                <td>Chức danh</td>
                <td style="width: 81px;">Số điện thoại</td>
                <td>Email</td>
                <td>Công ty</td>
                <td>Địa chỉ</td>
                <td>Công trình</td>
                <td>Hạng mục</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo $this->Form->input('loai', array('style' => 'width:50px', 'options' => $loai, 'onchange' => "this.form.submit()" ));?></td>
                <td><?php echo $this->Form->input('nhan', array('style' => 'width:50px', 'options' => $nhan, 'onchange' => "this.form.submit()" ));?></td>
                <td><?php echo $this->Form->input('ten', array('label' => false, 'style' => 'width:96%' ));?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
             </tr>
            <?php $i = 1; $STT = 1;
            if( $this->request->params['paging'][$model]['page'] > 1 )
            {
                $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
            } ?>
            <?php foreach($datas as $data){?>
            <tr>
                <td><?php echo $STT; ?></td>
                <td><?php echo $loai[$data[$model]["loai"]]; ?></td>
                <td><?php echo $nhan[$data[$model]["nhan"]]; ?></td>
                <td><?php echo $data[$model]["ten"]; ?></td>
                <td><?php echo $data[$model]["chucdanh"]; ?></td>
                <td><?php echo $data[$model]["dienthoai"]; ?></td>
                <td><?php echo $data[$model]["email"]; ?></td>
                <td><?php echo $data[$model]["tencongty"]; ?></td>
                <td><?php echo $data[$model]["diachi"]; ?></td>
                <td><?php echo $dsct[$data[$model]["congtrinh_id"]]; ?></td>
                <td><?php echo $data[$model]["hangmuc"]; ?></td>
                <td><?php if(isset($admin)){ ?>
                <?php echo $this->Js->link( __("Edit"), "/admin/doitacs/sua/".$data[$model]["id"],
                    array( 'before' => '$(\'#loading\').show()',
                           'update' => '#window_content', 'title' => __('Sửa đối tác'),
                           'success' => "$(\"#loading\").hide();window_show();upload_iframe_window( 'dsdoitac', 'tinymce' );"
                    ) );
                ?>
                <?php } ?>
                </td>
            </tr>
            <?php $STT++; } ?>
        </table>

    <?php echo $this->element( 'paging', array("show_info" => "đối tác", 'update' => '#mainContent')); ?>
    </div>

</div>
<?php echo $this->Form->end(); ?>