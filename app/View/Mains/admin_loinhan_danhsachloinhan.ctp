<?php if( $show_link_them ){ ?>
<div class="ThemMoiLoinhanMainRight" id="<?php echo strtoupper($model).'S';?>">
    <?php echo $this->Js->link( "New", "/admin/mains/them_loinhan_right/".$model.'/1/'.$div_update_paging,
            array(
                        'before'  => '$("#loading").show()',
                        'update'  => '#window_content',
                        'title'   => 'Thêm mới lời nhắn',
                        'success' => '$("#loading").hide();window_show();') );
    ?>
    &nbsp;|&nbsp;<a target="_blank" href="/mains/loinhan_danhsachloinhan_thongke/<?php echo $model;?>">Thống kê</a>

    <?php
            if($model == 'Loinhanpkinhdoanh' && in_array(AuthComponent::user('id'), array(21,138,27,117,438,265,560,382,557,17,589,351))){ ?>&nbsp;|&nbsp;
                <?php echo $this->Js->link( 'Nội Bộ TH', '/admin/mains/loinhan_noiboth/',
                            array(
                                        "update"  => "#loinhanpkinhdoanhs",
                                        'before'  => '$(\'#loading\').show()',
                                        'success' => "$(\"#loading\").hide();TableChucNang_active(event.delegateTarget.id, \"TableChucNangKinhdoanh\", \"loinhanpkinhdoanhs\")",
                                        "title"   => "Lời nhắn nội bộ của phòng tổng hợp" ) );
                            ?>
                &nbsp;|&nbsp;
                <?php echo $this->Js->link( 'Tổng Hợp', '/admin/mains/loinhan_danhsachloinhan/Loinhanpkinhdoanh',
                            array(
                                    "update"  => "#loinhanpkinhdoanhs",
                                    'before'  => '$(\'#loading\').show()',
                                    'success' => "$(\"#loading\").hide();TableChucNang_active(event.delegateTarget.id, \"TableChucNangKinhdoanh\", \"loinhanpkinhdoanhs\")",
                                    "title"   => "Lời nhắn của phòng tổng hợp" ) );
            } ?>
</div>
<?php } ?>

<?php if(empty($datas)){ ?>
    <em>(Hiện tại chưa có thông tin nào)</em>
<?php }else{ ?>
    <?php
       $STT = 1;
        if( $this->request->params['paging'][$model]['page'] > 1 )
        {
            $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
        }
    ?>
    <?php foreach( $datas as $data ){ ?>
        <div class="item" id="<?php echo $model."_".$data[$model]['id']; ?>">
            <div class="from ContainLinkHoverXoa">
                <?php if( $model == "Loinhannhansuhanhchanh" && isset($data[$model]["loai"]) && $data[$model]["loai"] == 1 ){ ?>
                    <img src="/img/icon/baotri_danhsachloinhan.png" />
                <?php }else{ ?>
                    <!-- <img src="/img/icon/<?php echo $controller; ?>_danhsachloinhan.png" /> -->
                    <img src="/iconiki.ico" />
                <?php } ?>

                <?php echo $STT++.". "; ?>
                <?php echo $this->element("avatar_view",array(
                        "created" => date("d/m/Y H:i", strtotime($data[$model]["created"])),
                        "dataUser" => $data["Nguoitao"],
                        'class' => 'AvatarChinh'
                ));?>
                <?php if( (strtotime($data[$model]["created"]) + 129600) > strtotime("now")){ ?><img src="/img/icon_new.png" /><?php } ?>
                <?php if(isset($admin) || AuthComponent::user('id') == $data[$model]["nguoitao"]){ ?>
                    <a class="linkHoverXoa" href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]['id']; ?>)">Del</a>
                <?php } ?>
            </div>
            <div id="loinhan_<?php echo $model."_".$data[$model]['id']; ?>" class="content" <?php if(strlen($data[$model]["noidung"])> 330) echo 'style="height: 230px; overflow: hidden;"'; ?>><?php echo  $this->Thumbnail->make_links_blank(str_replace("%20", " ", $data[$model]["noidung"])); ?></div>
                <?php if(strlen($data[$model]["noidung"])> 330){ ?>
                <a id="loinhan_xemthem_<?php echo $model."_".$data[$model]['id']; ?>" href="javascript:void(0)" onclick="xemthemloinhan('<?php echo $model."_".$data[$model]['id']; ?>',1)">More...</a>
                <a id="loinhan_thugon_<?php echo $model."_".$data[$model]['id']; ?>" hidden="hidden" href="javascript:void(0)" onclick="xemthemloinhan('<?php echo $model."_".$data[$model]['id']; ?>',2)">Close</a>
                <?php } ?>

                <!--COMMENT-->
                <div style="position:relative">
                    <div class="ThemTraLoi" style="top: -2px;">
                        <?php echo $this->Js->link( "Reply", "/admin/comments/them/".$data[$model]["id"].'/'.$model,
                            array( "update" => "#window_content",
                                   'before' => '$(\'#loading\').show()',
                                   'success' => "$(\"#loading\").hide();window_show();",
                                   "title" => "Trả lời lời nhắn" ) ); ?>
                    </div>
                </div>
                <div id="comment_<?php echo $model ?>_<?php echo $data[$model]["id"]; ?>">
                    <script type="text/javascript">
                        var url = "/admin/comments/them_success/<?php echo $data[$model]["id"].'/'.$model; ?>";
                        $.ajax({
                            url: url,
                            timeout: 15000,
                            error: error_handler,
                            success: function(html) {
                                $("#comment_<?php echo $model ?>_<?php echo $data[$model]["id"]; ?>").html(html);
                                //$("#loading").hide();
                            }
                        });
                    </script>
                </div>
            </div>
            <!--END COMMENT-->

        </div>
    <?php } ?>
    <?php echo $this->element( "paging", array( "update" => $div_update_paging ) ); ?>
<?php } ?>
<script type="text/javascript">
    $(function (){
        $('#menu_rg').tooltip();
    });

    function xemthemloinhan(div_update, xem) {
        if(xem == 1){//xem them
            $("#loinhan_" + div_update).removeAttr("style");
            $("#loinhan_xemthem_" + div_update).attr('hidden', "hidden");
            $("#loinhan_thugon_" + div_update).removeAttr("hidden");
        }else if(xem == 2){//thu gon
            $("#loinhan_" + div_update).attr('style', 'height: 230px; overflow: hidden;');
            $("#loinhan_thugon_" + div_update).attr('hidden', "hidden");
            $("#loinhan_xemthem_" + div_update).removeAttr("hidden");
        }
        return false;
    }
</script>
