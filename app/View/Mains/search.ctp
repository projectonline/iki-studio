<div style="background-color:white">

<?php echo $this->Form->create($model); ?>
<center>
	<br>	
	<h1 class="Shadow">Tìm Kiếm Thông Tin</h1><br>
	<fieldset style="border: 1px solid rgb(166, 201, 226); width: 99%; border-radius: 5px;">	 
		<legend style="display: inline-block; ">Trường tìm kiếm thông tin : </legend> 	
			<ul style ="  float: left;    margin-bottom: -7px;   margin-top: 20px;">				
				<li>
					<?php $options=array('tatca'=>' Tất cả ','loinhan'=>' Lời nhắn ', 'timfile' => "Tìm file", 'tencongtrinh' => " Tên công trình ",'nguoidung' => 'Người dùng');
                        $attributes=array('default' => 'tatca','legend'=>false,'style'=>'margin-left: 70px;' ,'onchange' => 'them_theo_doi_hd(this.id)');
                        echo $this->Form->radio("timkiem",$options,$attributes); ?>
                        <button style="margin-left: 145px;" class="Button" type="submit">Tìm</button>                       
				</li>			
			</ul>	
			<div  id="TDHD_tatca" style="  float: left;    margin-bottom: 13px;    margin-right: 25px;    margin-top: 26px;">	
				<table>
					<tr>
						<td><?php echo $this->Form->input($model.".content", array( "size" => 60, "label" => " <b>Nội dung tìm:<b> " )); ?></td>
						<td><?php echo $this->Form->input($model.".nguoitao_tatca", array( "size" => 16, "label" => " <b>Người tạo cần tìm:<b> " )); ?></td>
					</tr>
				</table>	
			</div>	
			<div  id="TDHD_loinhan" style="  float: left;    margin-bottom: 13px;    margin-right: 25px;    margin-top: 26px; display: none">	
				<table>
					<tr style=" height: 53px;">
						<td><?php echo $this->Form->input($model.".noidung_loinhan", array( "size" => 60, "label" => " <b>Nội dung tìm:<b> " )); ?></td>
						<td><?php // echo $this->Form->input($model.".content", array( "size" => 50, "label" => " <b>Nội dung tìm:<b> " )); ?></td>
						<td><?php echo $this->Form->input($model.".nguoitao_loinhan", array( "size" => 16, "label" => " <b>Người tạo cần tìm:<b> " )); ?></td>	
						
					</tr>
					<tr>						
						<td colspan="2"><?php echo $this->Form->input("loai_loinhan", array( "options" => $array_type_name,  "empty" => "---" ,"label" => " <b>File trong :<b> " )); ?></td>
					</tr>
				</table>	
			</div>	
			<div  id="TDHD_timfile" style="  float: left;    margin-bottom: 13px;    margin-right: 25px;    margin-top: 26px; display: none">	
				<table>
					<tr style=" height: 53px;">
						<td><?php echo $this->Form->input($model.".noidung_timfile", array( "size" => 50, "label" => " <b>Mô tả về file<br> (nội dung tìm file):<b> " )); ?></td>
						<td>
							<?php echo $this->Form->input($model.".ten_file", array( "size" => 50, "label" => " <b style='margin-left: 32px;'>Tên file :<b> " )); ?>
						</td>											
					</tr>
					<tr style=" height: 53px;">
						<td>
							<?php echo $this->Form->input($model.".tieude_file", array( "size" => 50, "label" => " <b  style='margin-left: 5px;'> Tiêu đề của file :   <b> " )); ?>
						</td>
						<td><?php echo $this->Form->input($model.".nguoitao_timfile", array( "size" => 16, "label" => " <b>Người tạo cần tìm :<b> " )); ?></td>						
						
					</tr>
					<tr>
						<td colspan="2">
							<?php echo $this->Form->input("loai_timfile", array( "options" => $array_typefile_name,  "empty" => "---" ,"label" => " <b style='margin-left: 10px;'>Loại lời nhắn :<b> " )); ?>
						</td>
					</tr>
				</table>	
			</div>	
			<div  id="TDHD_tencongtrinh" style="  float: left;    margin-bottom: 13px;    margin-right: 25px;    margin-top: 26px; display: none">	
				<table>
					<tr>
						<td><?php echo $this->Form->input($model.".noidung_tencongtrinh", array( "size" => 50, "label" => " <b>Nội dung tìm:<b> " )); ?></td>
						<td><?php echo $this->Form->input($model.".nguoitao_tencongtrinh", array( "size" => 16, "label" => " <b>Người tạo cần tìm:<b> " )); ?></td>
					</tr>
				</table>	
			</div>	
			<div  id="TDHD_nguoidung" style="  float: left;    margin-bottom: 13px;    margin-right: 25px;    margin-top: 26px; display: none">	
				<table>
					<tr>
						<td><?php echo $this->Form->input($model.".noidung_nguoidung", array( "size" => 50, "label" => " <b>Tên đăng nhập:<b> " )); ?></td>					
					</tr>
				</table>	
			</div>	
	</fieldset>
	<?php echo $this->element( "message" ); ?>

	<table id="table_main_search" class="CssTable" style="width:86%">
		<!--<tr>
			<td><?php //echo $this->Form->input($model.".content", array( "size" => 60, "label" => " <b>Nội dung tìm:<b> " )); ?></td>
			<td><button class="Button" type="submit">Tìm</button></td>
			<td style="width: 300px;">
				<?php //echo $this->Form->input($model.".nguoitao", array( "size" => 16, "label" => " <b>Người tạo cần tìm:<b> " )); ?>
			</td>
		</tr>-->

	<?php //if( !empty($datas) ){ ?>
		<?php 
		if(isset($user) && $user == 1)
		{
			echo $this->element("search_user");
		}elseif (isset($congtrinh) && $congtrinh == 1) {
			echo $this->element("search_congtrinh");
		}
		elseif (isset($file_upload) && $file_upload == 1) {
			echo $this->element("search_file");
		}
		elseif (isset($tatca)) {	
			if(isset($file_upload_tatca) && $file_upload_tatca ==1 )
			{
				echo $this->element("search_file");
			}	
			if(isset($congtrinh_tatca) && $congtrinh_tatca == 1)
			{
				echo $this->element("search_congtrinh");
			}		
			if(isset($user_tatca) && $user_tatca == 1)
			{
				echo $this->element("search_user");
			}
			if(isset($comment_tatca) && $comment_tatca == 1)
			{
				foreach($datas as $model => $value){ //pr($model); ?>		
				<tr>
				<td colspan="3">				
					<?php
					$sub_ctp = strtolower( $model );				
					if( strpos( $sub_ctp, 'loinhan' ) !== false)
					{						
						$sub_ctp = 'loinhan';
						echo $this->element('../Mains/search_loinhan' , array(
								'data' => $value,
								'model' => $model,
								'tieude' => $array_type_name[$model]
						) );
					}				
					else{					
						echo $this->element('../Mains/search_' . $sub_ctp, array(
								'data' => $value,
								'model' => $model,
								'tieude' => $array_type_name[$model],
					) );
					} ?>
				</td>
			</tr>
		<?php		
			}
			}
		}
		else{?>
		<center><b>Những lời nhắn tìm kiếm thấy</b></center>
		<?php
		if( !empty($datas) ){
		foreach($datas as $model => $value){ //pr($model); ?>		
		<tr>
			<td colspan="3">				
				<?php

				$sub_ctp = strtolower( $model );				
				if( strpos( $sub_ctp, 'loinhan' ) !== false)
				{
					//pr($sub_ctp); pr("aaabbbbb");
					$sub_ctp = 'loinhan';
					echo $this->element('../Mains/search_loinhan' , array(
							'data' => $value,
							'model' => $model,
							'tieude' => $array_type_name[$model]
					) );
				}				
				else{					
					echo $this->element('../Mains/search_' . $sub_ctp, array(
							'data' => $value,
							'model' => $model,
							'tieude' => $array_type_name[$model],
				) );
				} ?>
			</td>
		</tr>
		<?php		
			}
		}elseif( isset($datas)){ ?>

		<tr>
			<td colspan="2"><b><i>(Không tìm thấy dữ liệu nào.)</i></b></td>
		</tr>

	<?php } } if( isset($search_them_model) ){ ?>

		<tr id="xemthem_ketqua_timkiem">
			<td colspan="3">
				<b>>> Xem thêm kết quả tìm kiếm <a href="javascript:void(0)" onclick="xem_them_main_search()" title="Xem thêm kết quả tìm kiếm">Click</a></b>
			</td>
		</tr>

		<tr id="scroll_To">
			<td colspan="3"></td>
		</tr>
	<?php } ?>	

	</table>

</center>

<?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
function xem_them_main_search( object, model )
{
	$("#loading").show();
	$.ajax({
		url: "/mains/search_more/" + $( "#input_model_search_them" ).val() + "/" + $( "#input_id_search_them" ).val() ,
		timeout: 15000,
		error: error_handler,
		success: function(html){

			if( $.trim(html) == "")
			{
				$("#xemthem_ketqua_timkiem").remove();
				return false;
			}

			if( html == "error" )
			{
				alert("Xem thêm tìm kiếm thất bại. Vui lòng bấm F5 và thao tác lại.");

			}else{

				$( "#input_model_search_them" ).remove();
				$( "#input_id_search_them" ).remove();

				// Ghep them html vao cuoi bang
				$("#table_main_search > tbody").append(html);

				$.scrollTo( "#scroll_To", 100, {offset:-90} );

				// XOA SCROLL O TREN VA THEM SCROLL O DUOI
				$("#scroll_To").remove();
				$("#table_main_search > tbody").append('<tr id="scroll_To"><td colspan="3"></td></tr>');

				// XOA LINK CLICK O TREN VA THEM LINK CLICK O DUOI
				$("#xemthem_ketqua_timkiem").remove();
				$("#table_main_search > tbody").append('<tr id="xemthem_ketqua_timkiem"><td colspan="3"><b>>> Xem thêm kết quả tìm kiếm <a href="javascript:void(0)" onclick="xem_them_main_search()" title="Xem thêm kết quả tìm kiếm">Click</a></b></td></tr>');
				console.log('ok');

			}
			$("#loading").hide();

		}
	});
	return false;
}
</script>
<!--DIULT : THEM JAVA THEO DOI HOP DONG-->
<script type="text/javascript">
 	$(document).ready(function() { 		
 		if(document.getElementById("MainTimkiemLoinhan").checked == true ){ 			
			document.getElementById("TDHD_tatca").style.display = 'none';
			document.getElementById("TDHD_timfile").style.display = 'none';			
			document.getElementById("TDHD_tencongtrinh").style.display = 'none';
			document.getElementById("TDHD_nguoidung").style.display = 'none';		
			//document.getElementById("TDHD_loinhan").style.display = 'display';		
			$("#TDHD_loinhan").slideToggle();	
 		}
 		else if(document.getElementById("MainTimkiemTimfile").checked == true ){ 			
			document.getElementById("TDHD_tatca").style.display = 'none';
			//document.getElementById("TDHD_timfile").style.display = 'none';			
			document.getElementById("TDHD_tencongtrinh").style.display = 'none';
			document.getElementById("TDHD_nguoidung").style.display = 'none';		
			document.getElementById("TDHD_loinhan").style.display = 'none';		
			$("#TDHD_timfile").slideToggle();	
 		}
 		else if(document.getElementById("MainTimkiemTencongtrinh").checked == true ){ 			
			document.getElementById("TDHD_tatca").style.display = 'none';
			document.getElementById("TDHD_timfile").style.display = 'none';			
			//document.getElementById("TDHD_tencongtrinh").style.display = 'none';
			document.getElementById("TDHD_nguoidung").style.display = 'none';		
			document.getElementById("TDHD_loinhan").style.display = 'none';		
			$("#TDHD_tencongtrinh").slideToggle();	
 		}
 		else if(document.getElementById("MainTimkiemNguoidung").checked == true ){ 			
			document.getElementById("TDHD_tatca").style.display = 'none';
			document.getElementById("TDHD_timfile").style.display = 'none';			
			document.getElementById("TDHD_tencongtrinh").style.display = 'none';
			//document.getElementById("TDHD_nguoidung").style.display = 'none';		
			document.getElementById("TDHD_loinhan").style.display = 'none';		
			$("#TDHD_nguoidung").slideToggle();	
 		}
 		
       
    });
	function them_theo_doi_hd(id)
	{
		var radioBtn = document.getElementById(id);
		//alert(radioBtn);
		if(radioBtn.value == 'loinhan' )
		{ 
			var content = document.getElementById('MainContent').value;
			var nguoitao = document.getElementById('MainNguoitaoTatca').value;
			if(content !='')
			{
				document.getElementById('MainNoidungLoinhan').value = content
			}
			if(nguoitao !='')
			{
				document.getElementById('MainNguoitaoLoinhan').value = nguoitao
			}
			document.getElementById("TDHD_tatca").style.display = 'none';
			document.getElementById("TDHD_timfile").style.display = 'none';			
			document.getElementById("TDHD_tencongtrinh").style.display = 'none';
			document.getElementById("TDHD_nguoidung").style.display = 'none';			
			$("#TDHD_loinhan").slideToggle();	

		}else if(radioBtn.value == 'timfile')
		{
			var content = document.getElementById('MainContent').value;
			var nguoitao = document.getElementById('MainNguoitaoTatca').value;
			if(content !='')
			{
				document.getElementById('MainNoidungTimfile').value = content
			}
			if(nguoitao !='')
			{
				document.getElementById('MainNguoitaoTimfile').value = nguoitao
			}
			document.getElementById("TDHD_loinhan").style.display = 'none';
			document.getElementById("TDHD_tatca").style.display = 'none';
			document.getElementById("TDHD_tencongtrinh").style.display = 'none';
			document.getElementById("TDHD_nguoidung").style.display = 'none';			
			$("#TDHD_timfile").slideToggle();	
		}else if(radioBtn.value == 'tencongtrinh')
		{
			var content = document.getElementById('MainContent').value;
			var nguoitao = document.getElementById('MainNguoitaoTatca').value;
			if(content !='')
			{
				document.getElementById('MainNoidungTencongtrinh').value = content
			}
			if(nguoitao !='')
			{
				document.getElementById('MainNguoitaoTencongtrinh').value = nguoitao
			}
			document.getElementById("TDHD_loinhan").style.display = 'none';
			document.getElementById("TDHD_tatca").style.display = 'none';
			document.getElementById("TDHD_timfile").style.display = 'none';
			document.getElementById("TDHD_nguoidung").style.display = 'none';			
			$("#TDHD_tencongtrinh").slideToggle();	
		}
		else if(radioBtn.value == 'nguoidung')
		{
			var content = document.getElementById('MainContent').value;
			if(content !='')
			{
				document.getElementById('MainNoidungNguoidung').value = content
			}
			document.getElementById("TDHD_loinhan").style.display = 'none';
			document.getElementById("TDHD_tatca").style.display = 'none';
			document.getElementById("TDHD_tencongtrinh").style.display = 'none';
			document.getElementById("TDHD_timfile").style.display = 'none';			
			$("#TDHD_nguoidung").slideToggle();	
		}
		else if(radioBtn.value == 'tatca')
		{
			document.getElementById("TDHD_loinhan").style.display = 'none';
			document.getElementById("TDHD_timfile").style.display = 'none';
			document.getElementById("TDHD_tencongtrinh").style.display = 'none';
			document.getElementById("TDHD_nguoidung").style.display = 'none';			
			$("#TDHD_tatca").slideToggle();		
		}
		//alert(radioBtn.value);
		//	$("#TDHD_text_div").slideToggle();			
	}	
</script>