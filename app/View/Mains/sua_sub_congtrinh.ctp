<legend>CHỈNH SỬA</legend>
<?php echo $this->Form->create($model, array( 'onsubmit' => 'beginUpload()', 'type' => 'file' )); ?>
<table class="CssTable" cellpadding="0" cellspacing="1" width="99%" border="0">
<tr>
    <td>Nhập tiêu đề (*):</td>
    <td><?php echo $this->Form->input($model.".tieude", array("size" => 60, 'placeholder' => 'Xin nhập tiêu đề trước khi chọn file'));?>
        <em>(không quá 255 kí tự)</em>
    </td>
</tr>
<tr>
    <td>Mô tả về file được upload (*):</td>
    <td><?php echo $this->Form->input($model.".mota", array("rows" => 5, "cols" => 50, 'placeholder' => 'Xin nhập mô tả trước khi chọn file' ));?>
        <em>(không quá 2000 kí tự)</em>
    </td>
</tr>
<tr>
    <td>Upload File (*):</td>
    <td><?php echo $this->element( "list_files" ); ?>
        <?php echo $this->element( "upload_progress" ); ?>
    </td>
</tr>
<tr>
    <td colspan="2" class="Center">
        <input title="Lưu" value=" Lưu thông tin " type="submit" class="Button">
        <a onclick="$('#window_content').dialog( 'close' );" href="javascript:void(0)" class="Button" style="float: right;">Đóng lại</a>
    </td>
</tr>
</table>
<?php echo $this->Form->end(); ?>
