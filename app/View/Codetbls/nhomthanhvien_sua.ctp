<?php echo $this->Form->create($model); ?>
<div id="divChucNang">
	<input type="Button" title="<?php echo __('Trở về'); ?>" value=" <?php echo __('Trở về'); ?> " class="Button" id="btnadd" onclick="window.location='/<?php echo $controller;?>/nhomthanhvien'">
	<input title="<?php echo __('Lưu nhóm thành viên'); ?>" value=" <?php echo __('Lưu nhóm thành viên'); ?> " type="submit" class="Button" id="btnsearch">
</div>
<div id="headerArea"><h2>Chỉnh sửa</h2></div>

<?php echo $this->element( 'message' );?>
<table class="Data">
		<tr>
			<td><?php echo __("Tên nhóm thành viên"); ?> (*):</td>
			<td><?php echo $this->Form->input($model.'.name', array('size' => '80' ));?></td>
		</tr>
		<tr>
			<td><?php echo __("Trưởng bộ phận"); ?> (*):</td>
			<td><?php echo $this->element( 'users_search', array("field_usersearch" => "Codetbl.manager") );?></td>
		</tr>
		<tr>
			<td><?php echo __("Số thứ tự"); ?>:</td>
			<td><?php echo $this->Form->input($model.'.sort', array('size' => '5',  'style' => 'float:left' ));?></td>
		</tr>
		</table>
<?php echo $this->Form->end(); ?>