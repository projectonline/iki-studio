<?php echo $this->Form->create($model, array('type' => 'GET')); ?>
<div id="divChucNang">
	<input value=" <?php echo __('Tìm'); ?> " type="submit" class="Button" id="btnsearch">
	<input type="Button" value=" <?php echo __('Thêm'); ?> " class="Button" id="btnadd" onclick="window.location='/<?php echo $controller;?>/chudautu_them'">
</div>

<div id="headerArea"><h2>Danh sách chủ đầu tư</h2></div>

<?php if( isset( $message ) ) echo  $message ; ?>
<table class="Data">
	<tr>
		<td><?php echo __("STT"); ?></td>
		<td><?php echo __("Tên chủ đầu tư"); ?></td>
		<td><?php echo __("Thao tác"); ?></td>
	</tr>
	<tr>
		<td></td><td><?php echo $this->Form->input('name', array('size' => '65' ));?></td>
		<td></td>
		<td></td>
	 </tr>
	<?php $i = 1; $STT = 1;
		if( $this->request->params['paging'][$model]['page'] > 1 )
		{
			$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
		} ?>
	<?php foreach($datas as $data){ ?>
	<tr id="<?php echo $model; ?>_<?php echo $data[$model]['id'];?>" class="row<?php echo $i; $i = 3 - $i;?>">
		<td><?php echo $STT; ?><?php if( (strtotime($data[$model]["modified"]) + 640800) > strtotime("now") ){ ?>
			<img src="/img/icon_new.png" /><?php } ?></td>
		<td>
			<a target="_blank" href="/congtrinhs/list_all_congtrinh/cdt_name:<?php echo $data[$model]['name']; ?>">
				<?php echo $data[$model]['name']; ?>
			</a>
		</td>
		<td><?php if(isset($admin)){ ?><a href="/<?php echo $controller; ?>/chudautu_sua/<?php echo $data[$model]["id"];?>" title="<?php echo __('Chỉnh sửa chủ đầu tư này');?>"><?php echo __("Sửa"); ?></a>
		<a href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]["id"];?>)" title="<?php echo __('Xóa chủ đầu tư này');?>"><?php echo __("Xóa"); ?></a><?php } ?></td>
	</tr>
	<?php $STT++; } ?>
</table>
<?php echo $this->element('paging', array('show_info' => 'chủ đầu tư')); ?>
<?php echo $this->Form->end(); ?>