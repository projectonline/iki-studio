<legend>Thêm mới ngày nghỉ của công ty</legend>
<table class="CssTable">
	<tr>
		<td>STT</td>
		<td>Ngày</td>
		<td>Tên Ngày Nghỉ</td>
		<td>STT</td>
	</tr>
<?php $STT = 1;
			if( $this->request->params['paging'][$model]['page'] > 1 )
			{
				$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
			}
	foreach( $datas as $data ){ ?>
	<tr style="text-align:center" id="<?php echo $model."_".$data[$model]["id"]; ?>">
		<td><?php echo $STT++.". "; ?></td>
		<td><?php echo date("d/m/Y", strtotime($data[$model]["date"])); ?></td>
		<td><?php echo $data[$model]["name"]; ?></td>
		<td><a href="javascript:void(0)" onclick="xoa_ajax('Codetbl' ,<?php echo $data[$model]['id']; ?>)">Xóa</a></td>
	</tr>
<?php } ?>
</table>

<?php echo $this->element( 'paging', array( 'update' => '#window_content' ) ); ?>
<?php echo $this->Form->create($model); ?>
	<?php echo $this->element("message"); ?>
	<table class="CssTable">
		<tr>
			<td><h3> - Ngày nghỉ của công ty (*):</h3></td>
			<td><?php echo "Ngày: ".$this->Form->day($model.'.date_tmp')."&nbsp;&nbsp;&nbsp;";
				echo $this->Form->month($model.'.month_tmp', array('monthNames' => false));
				echo $this->Form->year($model.'.year_tmp', 2000, 2020); ?>
			</td>
		</tr>
		<tr>
			<td><h3> - Tên ngày nghỉ (*):</h3></td>
			<td><?php echo $this->Form->input($model.'.name',  array('size' => '40'));?>
				<em>(Ví dụ: Tết, Nghỉ 30-04, Giỗ Tổ Hùng Vương)</em></td>
		</tr>
	</table>
	<center><?php echo $this->Js->submit( 'Lưu', array('before' => '$(\'#loading\').show()','class' => 'Button',
	'success' => 'handle_response(data, \'window_content\')'
	));?></center>
<?php echo $this->Form->end(); ?>