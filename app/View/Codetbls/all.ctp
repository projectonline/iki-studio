<?php echo $this->Form->create($model); ?>

<div id="divChucNang">
	<input value=" Tìm " type="submit" class="Button" id="btnsearch">
	<a class="Button" href="/codetbls/all_them">Thêm</a>
</div>

<div id="headerArea"><h2>Danh sách Codetbl</h2></div>

<table class="Data">
	<tr>
		<td>STT</td>
		<td>Code1</td>
		<td>Code2</td>
		<td>Name</td>
		<td>Name 2</td>
		<td>Date</td>
		<td>Sort</td>
		<td>Parent</td>
		<td style="width:150px">Created</td>
		<td>Xóa</td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo $this->Form->input('code1', array('label' => false, 'style' => 'width:99%' ));?></td>
		<td><?php echo $this->Form->input('code2', array('label' => false, 'style' => 'width:99%' ));?></td>
		<td><?php echo $this->Form->input('name', array('label' => false, 'style' => 'width:99%' ));?></td>
		<td></td>
		<td></td>
	 </tr>
	<?php $STT = 1;
		if( $this->request->params['paging'][$model]['page'] > 1 )
		{
			$STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
		} ?>
	<?php foreach($datas as $data){ ?>
	<tr id="<?php echo $model; ?>_<?php echo $data[$model]['id'];?>">
		<td>
			<?php echo $STT; ?>
			<?php if( (strtotime($data[$model]["modified"]) + 640800) > strtotime("now") ){ ?>
				<img src="/img/icon_new.png" />
			<?php } ?>
		</td>
		<td><?php echo $data[$model]["code1"]; ?></td>
		<td><?php echo $data[$model]["code2"]; ?></td>
		<td><?php echo $data[$model]["name"]; ?></td>
		<td><?php echo $data[$model]["name_2"]; ?></td>
		<td><?php echo $data[$model]["date"]; ?></td>
		<td><?php echo $data[$model]["sort"]; ?></td>
		<td><?php echo $data[$model]["parent"]; ?></td>
		<td><?php if( strlen($data[$model]['modified']) > 0 ){
					echo date('d/m/Y H:i', strtotime($data[$model]['modified'])); ?> <br><em>(<?php echo date('d/m/Y H:i', strtotime($data[$model]['created'])); ?>)</em>
			<?php } ?>
		</td>
		<td>
			<?php if(AuthComponent::user('id') == 4){ ?>
				<a href="/codetbls/all_sua/<?php echo $data[$model]["id"];?>" title="Chỉnh sửa này">Sửa</a>
				<a href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model; ?>',<?php echo $data[$model]["id"];?>)" title="Xóa này">Xóa</a>
			<?php } ?>
		</td>
	</tr>
	<?php $STT++; } ?>
</table>
<?php echo $this->element('paging_search', array('show_info' => 'codetbl', 'update' => '#mainContent')); ?>
<?php echo $this->Form->end();?>