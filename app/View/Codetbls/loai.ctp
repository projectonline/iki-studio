<?php echo $this->Form->create($model, array('type' => 'GET')); ?>
<div id="divChucNang">
	<input value=" <?php echo __('Tìm'); ?> " type="submit" class="Button" id="btnsearch">
	<input type="Button" value=" <?php echo __('Thêm'); ?> " class="Button" id="btnadd" onclick="window.location='/<?php echo $controller;?>/loai_them'">
</div>
<div id="headerArea"><h2>Danh sách</h2></div>

<?php if( isset( $message ) ) echo  $message ; ?>
<table class="Data">
			<tr>
				<td>STT</td>
				<td>Tên Chính (phòng ban)</td>
				<td>Loại Công Trình</td>
				<td>Thao tác</td>
			</tr>
			<?php $i = 1; $STT = 1; ?>
			<?php foreach($datas as $data){ ?>
			<tr id="<?php echo $model; ?>_<?php echo $data[$model]['id'];?>" class="row<?php echo $i; $i = 3 - $i;?>">
				<td><?php echo $STT; ?><?php if( (strtotime($data[$model]["modified"]) + 640800) > strtotime("now") ){ ?>
					<img src="/img/icon_new.png" /><?php } ?></td>
				<td><?php echo $data[$model]['name']; ?></td>
				<td>
					<ul>
					<?php foreach( $data['LoaiCon'] as $loaicon  ){ ?>
						<li><?php echo $loaicon['name']; ?>
							<?php if(isset($admin)){ ?>
								<a href="/<?php echo $controller; ?>/loai_sua/<?php echo $loaicon["id"];?>" title="Chỉnh sửa loại công trình này">Sửa</a>
							<?php } ?>
						</li>
					<?php } ?>
					</ul>
				</td>
				<td>
				</td>
			</tr>
			<?php $STT++; } ?>
		</table>

		<!--CAC LOAI CONG TRINH CHUA CHINH SUA-->
		<?php if( isset($no_datas[0]) ){ ?>
		<table class="CssTable" cellpadding="0" cellspacing="1" width="100%" border="0">
			<tr>
				<td>Các Loại Công Trình Chưa Chỉnh Sửa</td>
				<td>Thao tác</td>
			</tr>
			<?php $i = 1; $STT = 1; ?>
			<?php foreach($no_datas as $data){ ?>
			<tr id="<?php echo $model; ?>_<?php echo $data[$model]['id'];?>" class="row<?php echo $i; $i = 3 - $i;?>">
				<td><?php echo $data[$model]['name']; ?></td>
				<td>
					<?php if(isset($admin)){ ?>
						<a href="/<?php echo $controller; ?>/loai_sua/<?php echo $data[$model]["id"];?>" title="Chỉnh sửa loại công trình này">Sửa</a>
					<?php } ?>
				</td>
			</tr>
			<?php $STT++; } ?>
		</table>
		<?php } ?>
<?php echo $this->Form->end(); ?>