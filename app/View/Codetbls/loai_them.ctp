<?php echo $this->Form->create(); ?>
<div id="headerArea"><h2>Thêm mới</h2></div>

		<?php echo $this->element( 'message' );?>
		<table class="Data">
		<tr>
			<td>Tên loại cha (nếu thêm loại cha):</td>
			<td><?php echo $this->Form->input('loai_cha', array('size' => 50 ));?></td>
		</tr>
		<tr>
			<td>Chọn tên loại cha:</td>
			<td><?php echo $this->Form->input('loai_cha_option', array('options' => $loai_cha, 'empty' => UNCHOOSE )); // 'default'=> 86, ?></td>
		</tr>
		<tr>
			<td>Tên loại con (*):</td>
			<td><?php echo $this->Form->input('loai_con', array('size' => 50 ));?></td>
		</tr>
		<tr>
			<td colspan="2"><em>(Chú ý: xin hỏi lại IT nếu bạn không hiểu loại cha, loại con là gì, cám ơn.)</em></td>
		</tr>
		<tr>
			<td colspan="2" class="Center">
				<a href="/<?php echo $controller;?>/loai" class="Button">Trở về</a>
				<input title="Lưu" value=" Lưu thông tin " type="submit" class="Button">
			</td>
		</tr>
		</table>
<?php echo $this->Form->end(); ?>