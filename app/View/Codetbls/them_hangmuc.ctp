<?php
	if( !isset( $thanhcong ) ){
?>
<fieldset>
	<legend>THÊM HẠNG MỤC</legend>
	<?php echo $this->element( 'message' );?>
	<table class="CssTable" cellpadding="0" cellspacing="1" border="0">
	<tr>
		<td><b><?php echo __("Tên hạng mục"); ?> (*):</b></td>
		<td><?php echo $this->Form->input($model.'.name', array('size' => '30' ));?></td>
	</tr>
	<tr>
		<td><b><?php echo __("Số thứ tự"); ?>:</b></td>
		<td><?php echo $this->Form->input($model.'.sort', array('size' => '5',  'style' => 'float:left' ));?></td>
	</tr>
	</table>
	<center><button class="Button" onclick="them_hangmuc();">Lưu</button></center>
</fieldset>
<?php
	}else{
		echo $this->Form->input("Loinhangiaonhanhosovt.hangmucchon", array("options" => $hangmuc,  "style" => "width:130px", "empty" => "---", "onchange" => "chonhangmuc();"));
?>
		<input type="hidden" id="idhangmuc" value="<?php echo $id; ?>" />
<?php } ?>
<script type="text/javascript">
	function them_hangmuc()
	{
		var $contain = $("#window_content_2");
		var $tenhangmuc = $( '#CodetblName', $contain );
		var $thutu = $( '#CodetblSort', $contain );

		$('#loading').show();
		$.ajax({
			url: '/codetbls/them_hangmuc/',
			type: 'POST',
			data: { name: $tenhangmuc.val(), sort: $thutu.val() },
			timeout: 120000,
			error: error_handler,
			success: function(html){
				$('#loading').hide();

				if( html.substr(1,8) != 'fieldset' ){
					$( "#dshangmuc" ).html( html );
					$('#LoinhangiaonhanhosovtHangmucchon').val( $('#idhangmuc').val() );
					chonhangmuc();
					$('#window_content_2').dialog( "close" );
				}else{
					jQuery( "#window_content_2" ).html( html );
				}
			}
		});
	}
</script>