<?php echo $this->Form->create($model); ?>
<div id="divChucNang">
	<a href="/codetbls/all" class="Button">Trở về</a>
	<input title="Lưu All" value=" Lưu All " type="submit" class="Button">
</div>

<div id="headerArea"><h2>Thêm mới</h2></div>

<?php echo $this->Session->flash();?>
<table class="Data">
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td>Code 1 (*):</td>
		<td><?php echo $this->Form->input($model.'.code1', array( 'size' => 80 ));?></td>
	</tr>
	<tr>
		<td>Code 2 (*):</td>
		<td><?php echo $this->Form->input($model.'.code2', array( 'size' => 80 ));?></td>
	</tr>
	<tr>
		<td>Name (*):</td>
		<td><?php echo $this->Form->input($model.'.name', array( 'size' => 80 ));?></td>
	</tr>
	<tr>
		<td>Name 2:</td>
		<td><?php echo $this->Form->input($model.'.name_2', array( 'size' => 80 ));?></td>
	</tr>
	<tr>
		<td>Date:</td>
		<td><?php echo $this->Form->input($model.'.date_tmp', array( 'size' => 80 ));?></td>
	</tr>
	<tr>
		<td>Sort:</td>
		<td><?php echo $this->Form->input($model.'.sort', array( 'size' => 80 ));?></td>
	</tr>
	<tr>
		<td>Parent:</td>
		<td><?php echo $this->Form->input($model.'.parent', array( 'size' => 80 ));?></td>
	</tr>

</table>
<?php echo $this->Form->end(); ?>