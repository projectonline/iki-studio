<legend>Chi tiết lịch họp</legend>

<div style="min-width:600px;max-width:800px">
    <br>
    <center><h2><?php echo $data[$model]['tieude']; ?></h1></center>
    <br>

    <table class="CssTable">
        <tr>
            <td style="width: 100px;"><b>Thời gian: </b></td>
            <td><?php echo date("d/m/Y H:i", strtotime($data[$model]["thoigian"]) ); ?></td>
        </tr>

        <tr>
            <td><b>Vị trí: </b></td>
            <td><?php echo $data[$model]["vitri"]; ?></td>
        </tr>

        <tr>
            <td><b>Chủ trì: </b></td>
            <td><?php echo $data[$model]["chutri"]; ?></td>
        </tr>
        <!-- Doan tam an
        <tr>
            <td><b>Thành phần: </b></td>
            <td><?php //echo $phongbans[$data[$model]["phongban_id"]]; ?></td>
        </tr>
        Doan tam an -->
        <tr>
            <td><b>Nội dung: </b></td>
            <td>
                <div style="max-height:500px;max-width: 665px;overflow:auto;">
                    <?php echo $data[$model]['noidung']; ?>
                </div>
            </td>
        </tr>

        <tr>
            <td><b>File</b></td>
            <td>
                <?php if( isset($data['File']['id']) ){
                    echo $this->element("link_download_file", array(
                        "id_file" => $data['File']["id"],
                        "name_file" => $data['File']["name"],
                        "dungluong_file" => $data['File']["dungluong"] ));
                } ?>
            </td>
        </tr>
    </table>

    <br>
    <em>(Người nhập: <?php echo $data['Nguoitao']['username'].', '.date('d/m/Y H:i', strtotime($data[$model]['created'])); ?>)</em>

    <?php if( isset($admin) || AuthComponent::user('id') == $data[$model]['nguoitao'] ){

        echo $this->Js->link( 'Sửa', "/lichhops/sua/".$data[$model]["id"],
                    array( 'before' => '$(\'#loading\').show()',
                           'update' => '#window_content',
                           'title' => 'Sửa BBH này',
                           'success' => '$("#loading").hide();window_show();fix_textarea_size("lichhops");upload_iframe_window( "DanhSachLichHop_td", "tinymce" )'
               ) );
    ?>
         / <a href="javascript:void(0)" onclick="xoa_ajax('Lichhop', <?php echo $data['Lichhop']['id']; ?>); $('#window_content').dialog('close');">Xóa</a>
    <?php } ?>

    <div style="text-align:right"><a class="Button" href="javascript:void(0)" onclick="$('#window_content').dialog('close');$.scrollTo( '#DanhSachLichHop', 300, {offset:-90} );">Đóng lại</a></div>

</div>