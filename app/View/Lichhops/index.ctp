<div id="LichCongTacBGD" class="demo">

	<center>
		<h2 class="Shadow">LỊCH HỌP</h2><br>
		<h2><?php echo date("d/m/Y", strtotime($tuan_dangxem[0]))." - ".date("d/m/Y", strtotime($tuan_dangxem[1]));?></h2><br>
	</center>

		<?php echo $this->Session->flash(); ?>

		<table class="DataSave" style="border-collapse:separate;border-spacing: 0.5px;">
			<tr>
				<td colspan="2" rowspan="2">Ngày tháng</td>
				<td colspan="<?php echo $count = count($phongbans); ?>">Nội dung</td>
			</tr>
			<tr>

			<?php foreach( $phongbans as $phongban ){ ?>
				<td>
					<?php echo $phongban; ?>
				</td>
			<?php } ?>
			</tr>

			<?php
				$lich_chualam = false; $homnay = strtotime(date("Y-m-d"));
				$ngay = strtotime($tuan_dangxem[0]);

				$number_day_show = round((strtotime(substr($tuan_dangxem[1],0,10)) - strtotime($tuan_dangxem[0]) +1)/86400,0);
				for( $k = $number_day_show; $k >= 0; $k-- )
				{
					$td_number = 1;
					$data_ngay = array(); if(isset($datas[$thubay]))$data_ngay = $datas[$thubay];

					$style_td = "";
					if($homnay <= $ngay)
					{
						$lich_chualam =true;

					}else{
						$style_td .= "background-color:#F5F5F5;";

					}

					$today = false;
					if($homnay == $ngay)
					{
						$today = true;
					}
					 ?>
				<tr><!--XUẤT DỮ LIỆU BUỔI SÁNG-->
					<td style="width:153px;<?php if( $today ){ ?>border-color: blue;border-top: solid 1px blue;border-right: none;<?php } ?>" rowspan="2">
						<span <?php if($today){ echo "style='color:blue'"; } ?>>
							<?php echo $day_weeks[date("N", strtotime($thubay))]."<br>".date("d/m", $ngay); ?>
						</span>
					</td>
					<td style="width:90px;<?php if( $today ){ ?>border-top: solid 1px blue;<?php } ?>">

						<?php if($homnay == $ngay){ echo "<font color='blue'>Sáng</font>"; }else{ echo "Sáng"; } ?>
					</td>

					<?php

					$compare_count = 0; $count = count($phongbans);
					foreach( $phongbans as $key_id_phongban => $phongban ){

						$compare_count += 1;
					?>
						<!-- Xuất ra nhiệm vụ của phongban nếu trùng id-->
						<td <?php if(!isset($data_ngay[0][$key_id_phongban])){ ?> onclick="run_them_lich('<?php echo $thubay."/".$key_id_phongban; ?>')"<?php } ?> class="ChinhSua ContainLinkHover LichCongTacTdColor<?php echo $td_number++;?>" style="position:relative;<?php echo $style_td; ?><?php if( $today ){ ?>border-top: solid 1px blue;<?php if( $compare_count == $count ){ ?>border-right-color:blue;<?php } ?><?php } ?>">

							<div class="ContainLinkHover">
								<?php
									$str_tmp_echo = $str_tmp = "";

									if(isset($data_ngay[0][$key_id_phongban]))
									{

										foreach($data_ngay[0][$key_id_phongban] as $data_noidung_phongban_sang)
										{
											$thoigian = date("H:i",strtotime($data_noidung_phongban_sang["thoigian"]));
											if($lich_chualam)
											{
												$str_tmp .= "<div id='string_".$model."_".$data_noidung_phongban_sang['id']."'><font color='red'>".$thoigian."</font> :<br>";
												$str_tmp_echo .= "<div id='".$model."_".$data_noidung_phongban_sang['id']."'><font color='red'>".$thoigian."</font> :<br>";
											}else{
												$str_tmp .= "<div id='string_".$model."_".$data_noidung_phongban_sang['id']."'><b>".$thoigian."</b> :<br>";
												$str_tmp_echo .= "<div id='string_".$model."_".$data_noidung_phongban_sang['id']."'><b>".$thoigian."</b> :<br>";
											}

											if(strlen($data_noidung_phongban_sang["vitri"])>0)
											{
												$str_tmp .= "<b>Vị trí: </b>".$data_noidung_phongban_sang["vitri"]."<br>";
											}

											if(strlen($data_noidung_phongban_sang["thanhphan"])>0)
											{
												$thanhphan = explode(",", $data_noidung_phongban_sang["thanhphan"]);
												$str_tmp .= "<b>T/phần: </b>";
												foreach($thanhphan as $key => $subthanhphan)
												{
													$thanhphan[$key] = "<font color='#4F81BD'>".$subthanhphan."</font>";
												}
												$str_tmp .= implode(", ", $thanhphan)."<br>";

											}

											$str_tmp_echo .= $data_noidung_phongban_sang["tieude"]."<br></div>";
											$str_tmp .= "<b>Tiêu đề: </b>".$data_noidung_phongban_sang["tieude"]."<br>";

											if( strlen($data_noidung_phongban_sang["noidung"]) < 100 )
											{

												$str_tmp .= "<b>Nội dung: </b>".$data_noidung_phongban_sang["noidung"];
											}else{

												$str_tmp .= "<b>Nội dung: </b>".substr(strip_tags($data_noidung_phongban_sang["noidung"]), 0, 100 ).'...';
												$str_tmp .= $this->Js->link( "chi tiết", "/lichhops/xem_chitiet/".$data_noidung_phongban_sang["id"],
													array( "update" => "#window_content",
														   'before' => '$(\'#loading\').show()',
														   'success' => "$(\"#loading\").hide();window_show();",
														   "title" => "Xem chi tiết Biên Bản Họp này",
														   'escape' => false ) )."<br>";
											}

											if( isset($data_noidung_phongban_sang['File']['id']) ){
												$str_tmp .= '<b>File: </b>'.$this->element("link_download_file", array(
													"id_file" => $data_noidung_phongban_sang['File']["id"],
													"name_file" => $data_noidung_phongban_sang['File']["name"],
													"dungluong_file" => $data_noidung_phongban_sang['File']["dungluong"]
												))."<br>";
											}


											$str_tmp .= "<em>(".$data_noidung_phongban_sang["Nguoitao"]["username"].", ".date("d/m/Y H:i", strtotime($data_noidung_phongban_sang["modified"])).")</em>";

											$str_tmp .= "<br>".$this->Js->link( "<i>Sửa</i>", "/lichhops/sua/".$data_noidung_phongban_sang["id"],
													array( "update" => "#window_content",
														   'before' => '$(\'#loading\').show()',
														   'success' => "$(\"#loading\").hide();window_show();",
														   "title" => "Sửa lịch họp này",
														   'escape' => false ) )." | ";

											$str_tmp .= '<a href="javascript:void(0)" onclick="xoa_ajax('."'".$model."'".','.$data_noidung_phongban_sang['id'].'); $('."'".'#string_'.$model."_".$data_noidung_phongban_sang['id']."'".').remove();" title="Xóa lịch họp này">Xóa</a>';

											$str_tmp .= "<hr><br></div>";
										}
										echo $str_tmp_echo; ?>

										<div class="ShowThongTin linkHover" style="left: -8px;top:-10px;min-width:169px;max-width:420px">
											<?php echo $str_tmp; ?>
											<?php echo $this->Js->link( "<img src='/img/plus_icon.png'> Thêm",
													"/".$controller."/them/".$thubay."/".$key_id_phongban,
													array(
														'before' => '$(\'#loading\').show()',
														'update' => '#window_content',
												   		'success' => '$("#loading").hide();window_show();',
												   		'title' => 'Thêm mới lịch họp',
												   		'escape' => false
											) );?>
										</div>
							<?php } else{ ?>

								<?php /*echo $this->Js->link( "<img src='/img/plus_icon.png'>",
										"/".$controller."/them/".$thubay."/".$key_id_phongban,
										array(
											'before' => '$(\'#loading\').show()',
											'update' => '#window_content',
									   		'success' => '$("#loading").hide();window_show();',
									   		'title' => 'Thêm mới lịch họp',
									   		'escape' => false
								) );*/ ?>
							<?php } ?>
							</div>
						</td>
					<?php } ?>
				</tr>
				<tr><!--XUẤT DỮ LIỆU BUỔI CHIỀU-->
					<td style="<?php if( $today ){ ?>border-bottom: solid 1px blue;<?php } ?>">
						<?php if($homnay == $ngay){ echo "<font color='blue'>Chiều</font>"; }else{ echo "Chiều"; } ?>
					</td>

					<?php $td_number=1; $compare_count = 0; $count = count($phongbans);

					foreach( $phongbans as $key_id_phongban => $phongban )
					{
						$compare_count += 1;

			 		?>
					 	<!-- Xuất ra nhiệm vụ của phongban nếu trùng id-->
						<td <?php if(!isset($data_ngay[1][$key_id_phongban])){ ?> onclick="run_them_lich('<?php echo $thubay."/".$key_id_phongban; ?>')"<?php } ?> class="ChinhSua ContainLinkHover LichCongTacTdColor<?php echo $td_number++;?>" style="<?php echo $style_td; ?><?php if( $today ){ ?>border-bottom: solid 1px blue;<?php if( $compare_count == $count ){ ?>border-right-color:blue;<?php } ?><?php } ?>">
							<div class="ContainLinkHover">

								<?php
									$str_tmp_echo = $str_tmp = "";

									if(isset($data_ngay[1][$key_id_phongban])){

										foreach($data_ngay[1][$key_id_phongban] as $data_noidung_phongban_chieu)
										{
											$thoigian = date("H:i",strtotime($data_noidung_phongban_chieu["thoigian"]));
											if($lich_chualam)
											{
												$str_tmp .= "<div id='string_".$model."_".$data_noidung_phongban_chieu['id']."'><font color='red'>".$thoigian."</font> :<br>";
												$str_tmp_echo .= "<div id='".$model."_".$data_noidung_phongban_chieu['id']."'><font color='red'>".$thoigian."</font> :<br>";
											}else{
												$str_tmp .= "<div id='string_".$model."_".$data_noidung_phongban_chieu['id']."'><b>".$thoigian."</b> :<br>";
												$str_tmp_echo .= "<div id='string_".$model."_".$data_noidung_phongban_chieu['id']."'><b>".$thoigian."</b> :<br>";
											}

											if(strlen($data_noidung_phongban_chieu["vitri"])>0)
											{
												$str_tmp .= "<b>Vị trí: </b>".$data_noidung_phongban_chieu["vitri"]."<br>";
											}

											if(strlen($data_noidung_phongban_chieu["thanhphan"])>0)
											{
												$thanhphan = explode(",", $data_noidung_phongban_chieu["thanhphan"]);
												$str_tmp .= "<b>T/phần: </b>";
												foreach($thanhphan as $key => $subthanhphan)
												{
													$thanhphan[$key] = "<font color='#4F81BD'>".$subthanhphan."</font>";
												}
												$str_tmp .= implode(", ", $thanhphan)."<br>";

											}

											$str_tmp_echo .= $data_noidung_phongban_chieu["tieude"]."<br></div>";
											$str_tmp .= "<b>Tiêu đề: </b>".$data_noidung_phongban_chieu["tieude"]."<br>";

											if( strlen($data_noidung_phongban_chieu["noidung"]) < 100 )
											{

												$str_tmp .= "<b>Nội dung: </b>".$data_noidung_phongban_chieu["noidung"];
											}else{

												$str_tmp .= "<b>Nội dung: </b>".substr(strip_tags($data_noidung_phongban_chieu["noidung"]), 0, 100 ).'...';
												$str_tmp .= $this->Js->link( "chi tiết", "/lichhops/xem_chitiet/".$data_noidung_phongban_chieu["id"],
													array( "update" => "#window_content",
														   'before' => '$(\'#loading\').show()',
														   'success' => "$(\"#loading\").hide();window_show();",
														   "title" => "Xem chi tiết Biên Bản Họp này",
														   'escape' => false ) )."<br>";
											}

											if( isset($data_noidung_phongban_chieu['File']['id']) ){
												$str_tmp .= '<b>File: </b>'.$this->element("link_download_file", array(
													"id_file" => $data_noidung_phongban_chieu['File']["id"],
													"name_file" => $data_noidung_phongban_chieu['File']["name"],
													"dungluong_file" => $data_noidung_phongban_chieu['File']["dungluong"]
												))."<br>";
											}

											$str_tmp .= "<em>(".$data_noidung_phongban_chieu["Nguoitao"]["username"].", ".date("d/m/Y H:i", strtotime($data_noidung_phongban_chieu["modified"])).")</em>";

											$str_tmp .= "<br>".$this->Js->link( "<i>Sửa</i>", "/lichhops/sua/".$data_noidung_phongban_chieu["id"],
													array( "update" => "#window_content",
														   'before' => '$(\'#loading\').show()',
														   'success' => "$(\"#loading\").hide();window_show();fix_textarea_size('lichhops');",
														   "title" => "Sửa lịch họp này",
														   'escape' => false ) )." | ";

											$str_tmp .= '<a href="javascript:void(0)" onclick="xoa_ajax('."'".$model."'".','.$data_noidung_phongban_chieu['id'].'); $('."'".'#string_'.$model."_".$data_noidung_phongban_chieu['id']."'".').remove();" title="Xóa lịch họp này">Xóa</a>';

											$str_tmp .= "<hr><br></div>";
										}
										echo $str_tmp_echo; ?>

										<div class="ShowThongTin linkHover" style="left: -8px;top:-10px;min-width:169px;max-width:420px">
											<?php echo $str_tmp; ?>
											<?php echo $this->Js->link( "<img src='/img/plus_icon.png'> Thêm",
													"/".$controller."/them/".$thubay."/".$key_id_phongban,
													array(
														'before' => '$(\'#loading\').show()',
														'update' => '#window_content',
												   		'success' => '$("#loading").hide();window_show();',
												   		'title' => 'Thêm mới lịch họp',
												   		'escape' => false
											) );?>
										</div>

								<?php } else{ ?>

										<?php /*echo $this->Js->link( "<img src='/img/plus_icon.png'>",
												"/".$controller."/them/".$thubay."/".$key_id_phongban,
												array(
													'before' => '$(\'#loading\').show()',
													'update' => '#window_content',
											   		'success' => '$("#loading").hide();window_show();',
											   		'title' => 'Thêm mới lịch họp',
											   		'escape' => false
										) );*/ ?>

								<?php } ?>
							</div>
						</td>
					<?php } ?>
				</tr>
			<?php $ngay = strtotime('+1 day',$ngay); $thubay = date("Y-m-d", $ngay); } ?>
			<tr>
				<td colspan="2"></td>

				<?php foreach( $phongbans as $phongban ){ ?>
				<td style="width:220px">
					<?php echo $phongban; ?>
				</td>
				<?php } ?>
			</tr>
		</table><br>
		<div id="paging_lichhops" class="paging">
			Tháng:&nbsp;<?php echo $this->Form->month("Thang.select",array("onchange" => "get_tuan_cua_thang(this.value)")); ?>
			&nbsp;&nbsp;&nbsp;
			<?php foreach($tuans as $key => $tuan){
				$title = date("d/m/Y", strtotime($tuan[0])).'->'.date("d/m/Y", strtotime($tuan[1]));
				if(substr($tuan_dangxem[1],0,10) == $tuan[1]){
					echo '<span title="'.$title.'" class="current">Tuần '.($key+1).'</span>'.' '.date("d/m", strtotime($tuan[0])).' - '.date("d/m", strtotime($tuan[1])).' ';
				}else{
					echo '<span title="'.$title.'"><a href="/lichhops/index/'.$tuan[0].'/'.$tuan[1].'">Tuần '.($key+1).'</a></span>';
				}
				echo "&nbsp;|&nbsp;";
			}
			  echo "của tháng <font color='red'>".$thang."</font>"; ?>
		</div>
</div>

<script type="text/javascript">

	function run_them_lich(link)
	{
		$.ajax({
			beforeSend:function (XMLHttpRequest) {
				$('#loading').show()
			},
			dataType:"html",
			success:function (data, textStatus) {
				$("#window_content").html(data);
				$("#loading").hide();
				window_show();

				fix_textarea_size("lichhops");

			},
			url:"/lichhops/them/" + link
		});

		return false;
	}

	function get_tuan_cua_thang(thang)
	{
		if(thang.length < 1)return false;
		var url = "/lichhops/get_tuan/<?php echo $nam; ?>-"+thang+"-01/javascript";
		$('#loading').show();
		$.ajax({url: url,timeout: 15000,
			error: error_handler,
			success: function(html){
				$("#paging_lichhops").html(html);
				$('#loading').hide();
			}
		});
	}
	//$(function() {$("[title]").tooltip();$(".demo").tooltip();});
</script>