<legend>Thêm mới lịch họp</legend>

<?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create($model, array( 'type' => 'file' )); ?>

<div style="width:645px" id="LichhopThemForm_update_if_error">


    <table class="CssTable" border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td>Tiêu đề (*):</td>
            <td><?php echo $this->Form->input($model.'.tieude', array('style' => 'width:512px'));?></td>
        </tr>
        <tr>
            <td>Ngày (*):</td>
            <td><?php echo $this->Form->input($model.'.ngay_tmp', array('style' => 'width:148px; float: left' ));?>
                <?php echo $this->element("img_pop_calendar"); ?>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="width:100px">Giờ (*):</td>
            <td><?php echo $this->Form->hour($model.'.ngay', true, array('style' => 'width: 55px; margin-right: 5px;'))." giờ, "; // 'empty' => 'Giờ',
                    echo $this->Form->minute($model.'.ngay', array('interval' => 15,'style' => 'width: 62px;')); //  'empty' => 'Phút',
                ?>&nbsp;phút
            </td>
        </tr>
        <tr>
            <td>Vị trí (*):</td>
            <td><?php echo $this->Form->input($model.'.vitri', array('style' => 'width:514px' ));?></td>
        </tr>
        <tr>
            <td>Chủ trì (*):</td>
            <td><?php echo $this->Form->input($model.'.chutri', array('style' => 'width:514px' ));?></td>
        </tr>
        <!-- Doan tam an
        <tr>
            <td>Thành phần (*):</td>
            <td><?php //echo $this->Form->input($model.'.phongban_id', array('style' => 'width:156px', 'options' => $phongbans ));?></td>
        </tr>
        Doan tam an -->
        <tr>
            <td>Nội dung (*):</td>
            <td><?php echo $this->Form->input($model.'.noidung', array('id' => $controller.'_textarea_id', 'rows' => 5, 'style' => 'width:522px'));?></td>
        </tr>
        <tr>
            <td>File BBH (*):</td>
            <td><input type="file" name="data[File][]" multiple="multiple" /></td>
        </tr>
        <!-- Doan tam an
        <tr>
            <td colspan="2">
                Thông báo cho: <?php //echo $this->element("giaoviec"); ?>
            </td>
        </tr>
        Doan tam an -->
        <tr>
            <td colspan="2">
                <center>
                    <input id="commentForm" title="Lưu" value=" Lưu lịch họp " class="Button" type="submit">
                </center>

                <input style="float:right;width:60px" title="Đóng lại" value=" Đóng lại " onclick="$('#window_content').dialog( 'close' );" class="Button">
            </td>
        </tr>

    </table>
</div>


<?php echo $this->Form->end(); ?>

<?php echo $this->element('active_tinymce'); ?>