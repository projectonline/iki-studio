<div id="DanhSachLichHop" class="ContainData" style="border:1px solid #CDCDCD;position:relative;margin:0">
    <div class="legend tintucs ActiveTask_tt" style="float:left">Weekly Schedule</div>
    <div id="tintucs_cover" class="term_r scroll">

        <?php echo $this->Session->flash(); ?>

        <?php if( isset( $datas[0] ) ){ ?>
        <ul>
            <?php $STT = 1;
                if( $this->request->params['paging'][$model]['page'] > 1 )
                {
                    $STT = ($this->request->params['paging'][$model]['page']-1)*$this->request->params['paging'][$model]['limit'] + 1;
                }
            ?>
            <?php foreach( $datas as $data ){ ?>
            <li id="<?php echo $model."_".$data[$model]["id"]; ?>">

                <?php echo $STT++; ?>.&nbsp;

                <?php echo $this->Js->link( $data[$model]['tieude'], "/admin/lichhops/xem_chitiet/".$data[$model]["id"],
                        array( "update" => "#window_content",
                               'before' => '$(\'#loading\').show()',
                               'success' => "$(\"#loading\").hide();window_show();",
                               "title" => "Xem chi tiết Biên Bản Họp này",
                               'escape' => false ) ); ?>
                &nbsp;<em>(<?php echo $all_user_qlda[$data[$model]["nguoitao"]]['username']; ?>)</em>

                <?php if( (strtotime($data[$model]["created"]) + (DAY*3) ) >= strtotime("now") ) {  ?>
                    <img src="/img/icon_new.png" />
                <?php } ?>

            </li>
            <?php } ?>
        </ul>

        <?php }else{ ?>
            <br><br><br><center><i>(không tìm thấy dữ liệu nào)</i></center>
        <?php } ?>
    </div>

    <div id="ThemLichHop">
        <?php echo $this->Js->link( "New",
                    "/admin/lichhops/them",
                    array( "update" => "#window_content",
                           'before' => '$(\'#loading\').show()',
                           'success' => "$(\"#loading\").hide();window_show();upload_iframe_window( 'DanhSachLichHop_td', 'tinymce' )",
                           "title" => "Thêm BBH mới",
                           'escape' => false ) )."<br>";
        ?>
    </div>

    <?php echo $this->element( 'paging', array( 'update' => 'DanhSachLichHop_td' ) ); ?>
</div>

<script type="text/javascript">
function lichhops_thay_doi_phong_ban()
{
    $("#loading").show();
    $.ajax({
        url: "/admin/lichhops/danhsach/" + $("#LichhopPhongbanId").val(),
        timeout : 120000,
        success: function(html){

            $("#DanhSachLichHop_td").html( html );

            $("#loading").hide();

        },error: error_handler
    });
}
</script>