<div id="DanhSachLichHop" class="ContainData" style="border:1px solid #CDCDCD;position:relative;margin:0">
<div class="legend tintucs ActiveTask_tt" style="float:left">Weekly Schedule</div>
<div id="tintucs_cover" class="term_r scroll">
    <?php echo $this->Session->flash(); ?>
    <?php if( isset( $tuans[0] ) ){ ?>
        <ul>
        <?php foreach( $tuans as $tuan ){ ?>
            <li id="<?php echo $model."_tuan_".$tuan["stt"]; ?>">
            <div style="<?php if(date('d/m/Y') == date('d/m/Y', strtotime($tuan["ngay"]))) echo 'color: red;'; ?>">
            <?php echo "<b>".$tuan["thu"]."</b>: ".date('d/m/Y', strtotime($tuan["ngay"])); ?>
            <?php echo $this->Js->link( "New", "/admin/lichhops/them_ngay/".$tuan["ngay"],
                    array(
                                'before'  => '$("#loading").show()',
                                'update'  => '#window_content',
                                'title'   => 'Thêm mới',
                                'success' => "$(\"#loading\").hide();window_show();upload_iframe_window( 'lichhop_".$tuan["ngay"]."', 'tinymce' );") );
            ?>
            </div>
            <div id="lichhop_<?php echo $tuan["ngay"]; ?>" class="traloi">
            <?php if( isset( $tuan["lich"][0] ) ){ $stt = 0; ?>
                <?php foreach( $tuan["lich"] as $lich ){ $stt = $stt + 1; ?>
                    <div id="<?php echo $model."_".$lich['Lichhop']['id']; ?>" class="div1NoiDungTraLoi ContainLinkHoverXoa">
                    <?php echo $stt.". ".$lich['Lichhop']['tieude']." _ ".$lich['Lichhop']['noidung']." _ ".$lich['Lichhop']['chutri']; ?>
                    <?php if(isset($admin)){ ?>
                        <em class="linkHoverXoa" style="margin-left : 44px;"><?php echo $this->Js->link( __("Edit"), "/admin/lichhops/sua_ngay/".$lich['Lichhop']['id']."/".$tuan["ngay"],
                            array( 'before' => '$(\'#loading\').show()',
                                   'update' => '#window_content', 'title' => __('Sửa lịch này'),
                                   'success' => "$(\"#loading\").hide();window_show();upload_iframe_window( 'lichhop_".$tuan["ngay"]."', 'tinymce' );"
                                   ) );
                        ?>| <a href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model ?>',<?php echo $lich[$model]["id"] ?>)">Del</a></em>
                    <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
            </div>
            </li>
        <?php } ?>
        </ul>
    <?php } ?>
    <hr>
    <?php if( isset( $tuantieps[0] ) ){ ?>
        <ul>
        <?php foreach( $tuantieps as $tuan ){ ?>
            <li id="<?php echo $model."_tuan_".$tuan["stt"]; ?>">
            <?php echo "<b>".$tuan["thu"]."</b>: ".date('d/m/Y', strtotime($tuan["ngay"])); ?>
            <?php echo $this->Js->link( "New", "/admin/lichhops/them_ngay/".$tuan["ngay"],
                    array(
                                'before'  => '$("#loading").show()',
                                'update'  => '#window_content',
                                'title'   => 'Thêm mới',
                                'success' => "$(\"#loading\").hide();window_show();upload_iframe_window( 'lichhop_".$tuan["ngay"]."', 'tinymce' );") );
            ?>
            <div id="lichhop_<?php echo $tuan["ngay"]; ?>" class="traloi">
            <?php if( isset( $tuan["lich"][0] ) ){ $stt = 0; ?>
                <?php foreach( $tuan["lich"] as $lich ){ $stt = $stt + 1; ?>
                    <div id="<?php echo $model."_".$lich['Lichhop']['id']; ?>" class="div1NoiDungTraLoi ContainLinkHoverXoa">
                    <?php echo $stt.". ".$lich['Lichhop']['tieude']." _ ".$lich['Lichhop']['noidung']." _ ".$lich['Lichhop']['chutri']; ?>
                    <?php if(isset($admin)){ ?>
                        <em class="linkHoverXoa" style="margin-left : 44px;"><?php echo $this->Js->link( __("Edit"), "/admin/lichhops/sua_ngay/".$lich['Lichhop']['id']."/".$tuan["ngay"],
                            array( 'before' => '$(\'#loading\').show()',
                                   'update' => '#window_content', 'title' => __('Sửa lịch này'),
                                   'success' => "$(\"#loading\").hide();window_show();upload_iframe_window( 'lichhop_".$tuan["ngay"]."', 'tinymce' );"
                                   ) );
                        ?>| <a href="javascript:void(0)" onclick="xoa_ajax('<?php echo $model ?>',<?php echo $lich[$model]["id"] ?>)">Del</a></em>
                    <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
            </div>
            </li>
        <?php } ?>
        </ul>
    <?php } ?>
</div>
</div>