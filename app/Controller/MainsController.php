<?php
class MainsController extends AppController
{
    var $name = 'Mains';

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function admin_index($loai = 1)
    {
        // de dong nay tren dau de check Session timeout
        if ($this->request->is('ajax')) {echo '';exit;}

        //160617 Doan them
        if($loai == 'home'){
            $loai = 1;
        }
        $this->set('loai', $loai);

        // MỤC LỜI NHẮN BÊN TAY TRÁI TRANG CHỦ
        $this->set('congtrinhs', $this->requestAction('/admin/congtrinhs/lists/'.$loai, ['return']));
    }

    public function admin_them_loinhan_right($model, $show_link_them = 0, $div_update_paging = '')
    {
        $controller = Inflector::tableize($model);
        if (!empty($this->data) && $this->request->is('Ajax')) {
            $this->loadModel($model);
            $save = $this->data;
            if ($this->{$model}->save($save)) {
                return $this->redirect('/admin/mains/loinhan_danhsachloinhan/' . $model . '/' . $show_link_them . '/' . $div_update_paging);
            }
        }

        $this->set('controller', $controller);
        $this->set('model', $model);
        if ($div_update_paging == '5') {
            $div_update_paging = 'loinhannhansuhanhchanhs_dkx';

        } elseif (($div_update_paging == '' || is_numeric($div_update_paging)) && $show_link_them != '6') {
            $div_update_paging = $controller;
        } elseif ($model == 'Loinhanketoan' && $show_link_them == '6') {
            $div_update_paging = 'loinhanketoans_nb';
        } elseif ($model == 'Loinhanpkinhdoanh' && $show_link_them == '6') {
            $div_update_paging = 'loinhanpkinhdoanhs_nb';
        } elseif ($model == 'Loinhanvibim' && $show_link_them == '6') {
            $div_update_paging = 'loinhanvibims_nb';
        }
        $this->set('div_update_paging', $div_update_paging);

    }

    function admin_loinhan_danhsachloinhan($model, $show_link_them = 0, $div_update_paging = '')
    {
        $this->loadModel($model);
        $controller = Inflector::tableize($model);
        $this->set('controller', $controller);
        if($model == 'Loinhanketoan' && $show_link_them =='6'){
            $cond[$model.'.trangthai'] = 5;
        }else{
            $cond[$model.'.trangthai'] = 4;
        }
        if( $model == 'Loinhannhansuhanhchanh')
        {
            if(is_numeric($div_update_paging)){
                $cond[$model.'.loai'] = $div_update_paging ;
            }else{
                $cond[$model.'.loai <>'] = 5;
            }
            //$div_update_paging = $controller;
        }
        $limit = 10;

        $fields = array('id', 'nguoitao', 'noidung','created', 'comment_count','trangthai');
        $this->paginate = array(
            'fields' => $fields,
            'conditions' => $cond,
            'order' => $model.'.modified desc',
            'limit' => $limit,
            'contain' => array('Nguoitao', 'Comment' => array('Nguoitao') ),
        );

        $this->set( 'datas', $this->paginate($model) );

        $controller = Inflector::tableize($model);
        $this->set('controller', $controller);
        $this->set('model', $model);

        $this->set('show_link_them', $show_link_them);

        if( $div_update_paging == '' || is_numeric($div_update_paging))
        {
            $div_update_paging = $controller;
        }

        $this->set('div_update_paging', $div_update_paging);
    }

    public function admin_xoa_ajax($model, $id)
    {
        $this->loadModel($model);
        $this->$model->id = $id;
        if (!$this->request->isAjax() || !$this->$model->exists() || !is_numeric($this->Auth->user('id'))) {
            exit;
        }

        $save['id']         = $id;
        $save['trangthai']  = 9;
        $this->$model->name = $model;
        if ($this->$model->save($save, false)) {
            echo 'ok';
        }
        exit;
    }
}