<?php
class CommentsController extends AppController {
    var $name = 'Comments';

    var $components = array( 'Session', 'Common', 'Auth', 'Search.Prg', 'Cmfile' );

    function beforeRender( )
    {
        // goi den before filter cha
        parent::beforeRender();

//		$this->helpers[] = 'AjaxMultiUpload.Upload';
    }

    public $presetVars = array(
        array('field' => 'tieude', 'type' => 'fulltext')
    );

    function admin_search($model, $model_id )
    {
        // CONDITION
        $this->Prg->commonProcess('Comment');
        $cond = $this->Comment->parseCriteria($this->passedArgs);
        $tmp['Comment'] = $this->request->params['named'];
        $this->request->data = $tmp;

        $cond['Comment.trangthai'] = 4;

        if ( !isset($cond['no_model']) )
        {
            $cond['Comment.model']   = $model;
            $cond['Comment.item_id'] = $model_id;
        }else{
            unset($cond['no_model']);
        }


        $this->paginate = array(
//			'fields' => array('Comment.*'),
            'conditions' => $cond,
            'contain' => array('Nguoitao'),
            'limit' => 5,
            'order' => 'Comment.id desc'
        );
        $this->set('datas', $this->paginate());
    }

    function admin_index($page = 1, $id_last = 0 )
    {
        $cond = array( 'Comment.trangthai' => 4);
        if ($id_last > 0 && is_numeric($id_last))
        {
            $cond['Comment.id <'] = $id_last;
        }

        $datas = $this->Comment->find('all', array(
            'fields'     => array('Comment.*'),
            'conditions' => $cond,
            'contain'    => array('Nguoitao'),
            'limit'      => 50,
            'order'      => 'Comment.id desc'
        ));
        $this->set('datas', $datas);
        $this->set('page', $page);

        if ($this->request->is('ajax') )
        {
            $this->set('ajax', true);
        }
    }

    function admin_sua_comment_ajax($id, $model){
        $this->loadModel('Comment');
        if (!empty($this->data)) {

            $save = $this->data;
            $save['Comment']['id']    = $id;
            if ($this->Comment->save($save, false )){
                die(nl2br($save['Comment']['noidung']));
            }

        }
        $this->data = $this->Comment->find('first', array(
            'fields'     => array('id', 'noidung', 'noidung_attach', 'item_id'),
            'conditions' => array('id' => $id),
            'contain'    => false
        ));

        $noidung_attach = $this->data['Comment']['noidung_attach'];

        if ($model == 'Task') {
            $this->loadModel('Task');
            $tmp = $this->Task->find('first', array(
                'fields'     => array('id', 'congtrinh_id'),
                'conditions' => array('id' => $tmp['Comment']['item_id']),
                'contain'    => false
            ));
            $this->set('congtrinh_id', $tmp['Task']['congtrinh_id']);
        }
        $this->set('id', $id);

        if (strlen($noidung_attach) > 0) {
            $tmp = explode('<br>', str_replace("\n", "<br>", $noidung_attach));
            unset($tmp[0]);
            $files = array();
            foreach ($tmp as $value) {
                $tmp1 = explode('</a>', $value);
                $file_size = trim(str_replace(array('<em>(', ')</em>'), '', $tmp1[1]));
                $file = str_replace(array('<b>', '</b>','<br>'), '', $tmp1[0]);
                $file = explode('>', $file);
                $file_name = $file[1];
                $file_id = explode('title', $file[0]);
                $file_id = explode('/uploaders/download/', $file_id[0]);
                $file_id = str_replace("' ", '', $file_id[1]);

                $files[] = array($file_id, $file_name, $file_size);
            }
            $this->set('show_noidung_attach', $files);
        }
    }

    //diult them sua de chuyen ve tin nhan chinh
    function admin_sua($congtrinh_id, $item_id, $model, $traloi_id){
        $this->loadModel($model);
        $con_nc = array(
            $model.'.trangthai'    => 4,
            $model.'.quantrong'    => 1,
            $model.'.congtrinh_id' => $congtrinh_id,
        );
        $dsctqt = $this->$model->find("list",array(
            'fields'     => array('id','tieude'),
            'contain'    => false,
            'conditions' => $con_nc
        ));
        $this->set('dsctqt', $dsctqt);

        $div_update = 'Comment_'.$model.'_'.$item_id;
        if (!empty($this->data))
        {
            $data = $this->data;
            if (isset($data['Comment']['congtrinh_id']) && $data['Comment']['congtrinh_id'] !=0)
            {
                $con_nc[$model.'.id'] = $data['Comment']['congtrinh_id'];
                $count_comment = $this->$model->find("first",array(
                    'fields'     => array('comment_count',),
                    'contain'    =>false,
                    'conditions' => $con_nc
                ));
                $save['id'] = $traloi_id; $save['item_id'] = $data['Comment']['congtrinh_id'];
                if ($this->Comment->save($save, false)){
                    $save_comment['id'] =  $data['Comment']['congtrinh_id'];
                    $save_comment['comment_count'] = $count_comment[$model]['comment_count'] + 1;
                    $this->Task->save($save_comment, false);
                    $div_update = $model.'_'.$data['Comment']['congtrinh_id'];
                }
            }
        }
        $this->set('update_div', $div_update);// DIV update la ten controller
        $this->set('traloi_id',$traloi_id);
        $this->layout = 'ajax';
    }
    //diult ket thuc them loi nha

    function admin_them($item_id, $model)
    {
        $this->set('model', $model);

        if (!empty($this->data))
        {
            $save = $this->data;
            $save['Comment']['model'] = $model;
            $save['Comment']['item_id'] = $item_id;
            $save['Comment']['trangthai'] = 4;

            $save['File'] = $this->move_file_to_app_upload($this->data);

            if( !empty($save['File'][0]))
            {
                foreach ($save['File'] as $key => $value) {
                    $save['File'][$key]['item_id'] = $item_id;
                    $save['File'][$key]['congtrinh_id'] = $item_id;
                }

            }else{
                unset($save['File']);
            }

            $this->Comment->create();
            if ($this->Comment->saveAll($save) ){
                $this->loadModel($model);
                $this->$model->save(['id' => $item_id, 'comment_count' => 1], false);
                return $this->redirect( '/admin/comments/them_success/'.$item_id . '/'. $model );
            }
        }

        $this->set('item_id', $item_id);

        $this->layout = 'ajax';
    }

    public function admin_them_success($item_id, $model)
    {
        $cond = array('Comment.trangthai' => 4,
                        'Comment.model' => $model,
                        'Comment.item_id' => $item_id);
        $this->paginate = array(
            'conditions' => $cond,
            'order' => 'Comment.modified desc',
            'limit' => 10,
            'contain' => array(
                'Nguoitao',
                'File'
        ));
        $this->set('item_id', $item_id);
        $this->set('model', $model);
        $this->set('div_update', 'comment_'.$model.'_'.$item_id);
        $this->set('data', $this->paginate());

        $this->layout = 'ajax';

    }

//160616 Doan them
    function admin_them_congtrinh($item_id, $model)
    {
        $this->set('model', $model);

        if (!empty($this->data))
        {
            $save = $this->data;
            $save['Comment']['model'] = $model;
            $save['Comment']['item_id'] = $item_id;
            $save['Comment']['trangthai'] = 4;

            $save['File'] = $this->move_file_to_app_upload($this->data);

            if( isset($save['File'][0]))
            {
                foreach ($save['File'] as $key => $value) {
                    $save['File'][$key]['item_id'] = $save['Comment']['tab'];
                    $save['File'][$key]['congtrinh_id'] = $item_id;
                }

            }else{
                unset($save['File']);
            }

            $this->Comment->create();
            if ($this->Comment->saveAll($save) ){
                $this->loadModel($model);
                $this->$model->save(['id' => $item_id, 'comment_count' => 1], false);
                return $this->redirect( '/admin/comments/danhsach/'.$item_id );
            }
        }

        $this->set('item_id', $item_id);

        $tabs[4] = "Process";
        $tabs[1] = "Input";
        $tabs[2] = "Output";
        //$tabs[3] = "Original File";
        $tabs[5] = "MOM";
        $tabs[7] = "Legal Docs.";
        $tabs[8] = "Reference";
        $tabs[9] = "Site Picture";
        //$tabs[10] = "Previous Backup";
        $tabs[6] = "Contract";
        $this->set('tabs', $tabs);

        $this->layout = 'ajax';
    }

    public function admin_danhsach($congtrinh_id = null, $tab = 0)
    {
        $cond = array('Comment.trangthai' => 4,
                        'Comment.model' => 'Congtrinh',
                        'Comment.item_id' => $congtrinh_id);
        if($tab != 0){
            $cond['Comment.tab'] = $tab;
        }
        $this->paginate = array(
            'conditions' => $cond,
            'order' => 'Comment.modified desc',
            'limit' => 10,
            'contain' => array(
                'Nguoitao',
                'File'
        ));

        $this->set('data', $this->paginate());
        $this->set('tab', $tab);
        $this->set('congtrinh_id', $congtrinh_id);
        $this->set('div_update', 'comment_Congtrinh_'.$congtrinh_id);

        $this->layout = 'ajax';

    }
//160616 Doan them ---

    function _get_user_giaoviec($item_id, $mymodel )
    {
        $result = array();

        // lay danh sach user da duoc thong bao truoc do
        $this->loadModel('Giaoviec');
        $tmp = $this->Giaoviec->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                'Giaoviec.model_nhanlai' => $mymodel,
                'Giaoviec.id_nhanlai'    => $item_id,
                'Giaoviec.nguoitao'      => $this->Auth->user('id'),
                'Giaoviec.trangthai'     => 4
            ),
            'contain' => false,
            'order' => 'id desc'
        ));

        if ( isset($tmp['Giaoviec']) )
        {
            $this->loadModel('GiaoviecsUser');
            $result = $this->GiaoviecsUser->find('list', array(
                'fields' => array('id', 'user_id'),
                'conditions' => array(
                    'GiaoviecsUser.giaoviec_id' => $tmp['Giaoviec']['id']
                ),
                'contain' => false
            ));
            $result = array_unique($result);
            if ( count($result) > 80 )
            {
                return array( 100 => 1);
            }else{

                // LAY RA NHUNG NGUOI COMMENT SAU NAY VA THEM VAO
                $comment = $this->Comment->find('first', array(
                    'fields' => array('id', 'created'),
                    'conditions' => array(
                        'Comment.trangthai' => 4,
                        'Comment.item_id'   => $item_id,
                        'Comment.model'     => $mymodel,
                        'Comment.nguoitao'  => $this->Auth->user('id')
                    ),
                    'order' => 'id desc'
                ));

                if ( isset($comment['Comment']))
                {
                    $cond = array(
                        'Comment.trangthai'   => 4,
                        'Comment.item_id'     => $item_id,
                        'Comment.model'       => $mymodel,
                        'Comment.created >='  => $comment['Comment']['created'],
                        'Comment.nguoitao <>' => $this->Auth->user('id')
                    );
                }else{
                    $cond = array(
                        'Comment.trangthai'   => 4,
                        'Comment.item_id'     => $item_id,
                        'Comment.model'       => $mymodel,
                        'Comment.nguoitao <>' => $this->Auth->user('id')
                    );
                }

                $comment = $this->Comment->find('all', array(
                    'fields'     => array('id', 'nguoitao'),
                    'conditions' => $cond,
                    'group'      => 'nguoitao',
                    'limit'      => 50
                ));

                if (isset($comment[0]))
                    $result = array_unique(array_merge(Set::extract('/Comment/nguoitao', $comment), $result));
            }

        }else{

            // lay ra danh sach tat ca nhung nguoi da tung tra loi cho loi nhan nay
            $cond = array(
                'Comment.trangthai' => 4,
                'Comment.item_id'   => $item_id,
                'Comment.model'     => $mymodel,
                'Comment.nguoitao <>' => $this->Auth->user('id')
            );

            $comment = $this->Comment->find('all', array(
                'fields' => array('id', 'nguoitao'),
                'conditions' => $cond,
                'group' => 'nguoitao',
                'limit' => 50
            ));

            if ( isset($comment[0]) )
                $result = array_unique(Set::extract('/Comment/nguoitao', $comment));

            $this->loadModel($mymodel);
            $nguoitaoloinhans = $this->{$mymodel}->find('first', array(
                'fields' => array('id', 'nguoitao'),
                'conditions' => array(
                    $mymodel.'.id' => $item_id
                ),
                'contain' => false
            ));
            $result[] = $nguoitaoloinhans[$mymodel]['nguoitao'];
        }

        return $result;
    }

    //diult chinh sua, vaf them ngay 25/11/2013

    function admin_next_comments($id_next, $model, $item_id,$kiemtra , $congtrinh_id)
    {

        if ($this->request->is('Ajax') && is_numeric($id_next) && is_string($model))
        {
            $this->set('datas', $this->Comment->find('all', array(
                'fields'     => array('id', 'noidung','noidung_attach', 'created', 'nguoitao'),
                'order'      => 'Comment.id desc',
                'conditions' => array(
                    'Comment.id <'      => $id_next,
                    'Comment.item_id'   => $item_id,
                    'Comment.model'     => $model,
                    'Comment.trangthai' => 4
                ),
                'contain' => array('Nguoitao'),
                'limit' => 3
            )));
            // lay danh sach loi nhan trong tasks cua cong trinh duoc danh dau quan trong
            if (isset($congtrinh_id) && $congtrinh_id !=0)
            {
                $this->loadModel($model);
                $con_nc = array(
                    $model.'.trangthai'    => 4,
                    $model.'.quantrong'    => 1,
                    $model.'.congtrinh_id' => $congtrinh_id,
                );
                $dsctqt = $this->$model->find("list",array(
                    'fields'     => array('id','tieude',),
                    'contain'    => false,
                    'conditions' => $con_nc
                ));
                $this->set('dsctqt', $dsctqt);
            }

            $this->set('model_loinhan', $model);
            $this->set('kiemtra', $kiemtra);
            $this->set('congtrinh_id', $congtrinh_id);
            $this->set('item_id', $item_id);

        }else{
            exit;
        }
    }
    function admin_next_comments_all($id_next, $model, $item_id,$kiemtra , $congtrinh_id)
    {

        if ($this->request->is('Ajax') && is_numeric($id_next) && is_string($model))
        {
            $this->set('datas', $this->Comment->find('all', array(
                'fields'     => array('id', 'noidung','noidung_attach', 'created', 'nguoitao', 'like', 'like_people', 'quantrong'),
                'order'      => 'Comment.id desc',
                'conditions' => array(
                    'Comment.id <'      => $id_next,
                    'Comment.item_id'   => $item_id,
                    'Comment.model'     => $model,
                    'Comment.trangthai' => 4
                ),
                'contain' => array('Nguoitao'),
            )));
            // lay danh sach loi nhan trong tasks cua cong trinh duoc danh dau quan trong
            if (isset($congtrinh_id) && $congtrinh_id !=0)
            {
                $this->loadModel($model);
                $con_nc = array(
                    $model.'.trangthai'    => 4,
                    $model.'.quantrong'    => 1,
                    $model.'.congtrinh_id' => $congtrinh_id,
                );
                $dsctqt = $this->$model->find("list",array(
                    'fields'     => array('id','tieude',),
                    'contain'    => false,
                    'conditions' => $con_nc
                ));
                $this->set('dsctqt', $dsctqt);
            }

            $this->set('model_loinhan', $model);
            $this->set('kiemtra', $kiemtra);
            $this->set('congtrinh_id', $congtrinh_id);
            $this->set('item_id', $item_id);

        }else{
            exit;
        }
    }
    //diult ket thuc them va chinh sua ngay 25/11/2013

    function admin_all_comment_quantrong($model, $item_id )
    {
        if ($this->request->is('Ajax') && is_numeric($item_id) && is_string($model))
        {
            $this->set('datas', $this->Comment->find('all', array(
                'fields' => array('id', 'noidung', 'created', 'nguoitao', 'like', 'like_people', 'quantrong'),
                'order' => 'Comment.id desc',
                'conditions' => array(
                    'Comment.item_id'   => $item_id,
                    'Comment.model'     => $model,
                    'Comment.trangthai' => 4,
                    'Comment.quantrong' => 1
                ),
                'contain' => array('Nguoitao')
            )));
        }else{
            echo 'Có lỗi trong quá trình xử lý. Thao tác của bạn không hợp lệ.'; exit;
        }
    }

    function admin_click_like($model, $id, $type )
    {
        if ( !is_numeric($id) && !$this->request->is('Ajax') )exit;

        $data = $this->Comment->find('first', array(
            'fields' => array('id', 'like', 'like_people', 'modified', 'model'),
            'conditions' => array('Comment.id' => $id),
            'contain' => false,
        ));

        if ( !isset($data['Comment']['id']) )exit;

        $save['id'] = $id;
        $save['modified'] = $data['Comment']['modified'];

        if ($data['Comment']['like_people'] == '')
        {
            $save['like'] = 1;
            $save['like_people'] = $this->Auth->user('username');
        }else{
            if ($type == 1 )
            {
                $save['like'] = $data['Comment']['like'] + 1;
                $tmp = explode('_._', $data['Comment']['like_people']);
                $tmp[] = $this->Auth->user('username');
                $save['like_people'] = implode('_._', $tmp);
            }else{

                // unlike
                $save['like'] = $data['Comment']['like'] - 1;
                $save['like_people'] = str_replace($this->Auth->user('username'), '', $data['Comment']['like_people']);
                $save['like_people'] = str_replace('_.__._', '_._', $save['like_people']);
            }
        }

        if ($this->Comment->save($save) )
        {
            echo 'ok';
            $this->Comment->updateCounter($data['Comment']['model']);
        }
        exit;
    }

}
