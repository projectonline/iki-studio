<?php
class LichhopsController extends AppController {

    var $name = 'Lichhops';
    var $components = array( 'Common', 'Cmfile', 'Auth' );

    function beforeFilter( )
    {
        // goi den before filter cha
        parent::beforeFilter();
    }

    function admin_index($fromday = null, $today = null)
    {
        $ts = $this->_week_range($fromday, $today);
        $this->set('thubay', substr($ts[0],0,10));

        $datas = $this->Lichhop->find('all', array(
            'fields' => array( 'id','tieude', 'thanhphan', 'vitri', 'noidung', 'thoigian', 'phongban_id', 'nguoitao', 'modified' ),
            'conditions' => array(
                'Lichhop.trangthai' => 4,
                'Lichhop.thoigian >=' => $ts[0],
                'Lichhop.thoigian <=' => $ts[1]
            ),
            'contain' => array(
                'Nguoitao', 'File'
            ),
            'order' => 'Lichhop.thoigian asc'
        ));

        $tmp = array();
        foreach($datas as $data)
        {
            $key_ngay = substr($data['Lichhop']['thoigian'],0,10);

            // kiểm tra xem là buổi sáng hay chiều
            $am_pm = 0;
            if(date('a',strtotime($data['Lichhop']['thoigian'])) == 'pm')$am_pm = 1;

            $data['Lichhop']['Nguoitao'] = $data['Nguoitao'];
            $data['Lichhop']['File'] = $data['File'];
            $tmp[$key_ngay][$am_pm][$data['Lichhop']['phongban_id']][] = $data['Lichhop'];
        }
        $datas = $tmp;
        $this->set('datas', $datas);

        $phongbans = $this->Lichhop->Phongban->find('list',array(
            'conditions' => array(
                'code1' => 'usr',
                'code2' => 'grp',
                'trangthai' => 4
            ),
            'fields' => array('id', 'name'),
            'order' => 'name asc'
        ));
        //unset($phongbans[521], $phongbans[522], $phongbans[315], $phongbans[520], $phongbans[979], $phongbans[626]);
        $this->set('phongbans', $phongbans);


        $this->set('day_weeks', array('','Thứ hai','Thứ ba','Thứ tư','Thứ năm','Thứ sáu','Thứ bảy'));

        // LẤY 4 TUẦN CỦA THÁNG ĐANG XEM
        $tuans = array();
        $this->get_tuan($fromday);
        $this->set('tuan_dangxem',$ts);
    }

    function admin_danhsach( $phongban_id = 0 )
    {
        $cond = array(
            'Lichhop.trangthai' => 4,
            'Lichhop.hienthi' => 1
        );

        if( is_numeric($phongban_id) && $phongban_id > 0 )
        {
            $cond['Lichhop.phongban_id'] = $phongban_id;
        }

        $this->set('phongban_id', $phongban_id);

        $this->paginate = array(
            'fields' => array('id', 'tieude', 'noidung', 'nguoitao', 'created'),
            'conditions' => $cond,
            'order' => 'Lichhop.created desc',// id
            'limit' => 8,
            'contain' => array('Nguoitao','File')
        );
        $this->set('datas', $this->paginate());

        $phongbans = $this->Lichhop->Phongban->find('list',array(
            'conditions' => array(
                'code1' => 'usr',
                'code2' => 'grp',
                'trangthai' => 4
            ),
            'fields' => array('id', 'name'),
            'order' => 'name asc',
            'contain' => false
        ));

        $this->set('phongbans', $phongbans);

        $this->layout = 'ajax';
    }

    function admin_xem_chitiet( $id )
    {
        if( !$this->request->is('ajax') || !is_numeric($id))exit;

        $this->set('data', $this->Lichhop->find('first', array(
            'conditions' => array(
                'Lichhop.id' => $id,
                'Lichhop.trangthai' => 4
            ),
            'fields' => array(
                'id', 'thoigian', 'tieude', 'noidung', 'vitri', 'thanhphan', 'thanhphan_num',
                'created', 'chutri', 'phongban_id','nguoitao'
            ),
            'contain' => array(
                'Nguoitao',
                'File'
            )
        )));

        $phongbans = $this->Lichhop->Phongban->find('list',array(
            'conditions' => array(
                'code1' => 'usr',
                'code2' => 'grp',
                'trangthai' => 4
            ),
            'fields' => array('id', 'name'),
            'order' => 'name asc'
        ));

        $this->set('phongbans', $phongbans );
    }

    function _week_range($fromday = null, $today = null)
    {
        $ts = strtotime('now');
        if(isset($fromday))
        {
            return array($fromday.' 00:00:00', $today.' 23:59:59');
        }
        //$date = date('Y-m-d', strtotime('-'.($tuan*7).'  days'));
        //$ts = strtotime($date);
        $ts = strtotime('now');
        //if(date('N', $ts) == 1)$start = $ts;
        $start =(date('w', $ts) == 1) ? $ts : strtotime('last monday', $ts);
        if(date('w', $ts) == 0)$start = strtotime('next monday', $ts);

        return array(date('Y-m-d', $start).' 00:00:00', date('Y-m-d', strtotime('next saturday', $start)).' 23:59:59');
    }

    function admin_get_tuan($ngay_dang_xem = null, $javascript = null)
    {
        if(!isset($ngay_dang_xem))
        {
            $tuan = date('Y-m').'-01';
            $time_tuan = strtotime($tuan);
            if(date('N', $time_tuan) == 7)$tuan = date('Y-m').'-02';
            $tuan1 = date('Y-m-d',strtotime('next saturday', $time_tuan));

            $thang = date('m'); $nam = date('Y');
        }else{
            $time_tuan = strtotime($ngay_dang_xem);
            $tuan = date('Y-m', $time_tuan).'-01';
            if(date('N', strtotime($tuan)) == 7)$tuan = date('Y-m', $time_tuan).'-02';

            $tuan1 = date('Y-m-d',strtotime('next saturday', strtotime($tuan)));

            $thang = date('m', strtotime($ngay_dang_xem)); $nam = date('Y', strtotime($ngay_dang_xem));
        }
        $tuans[] = array($tuan, $tuan1);
        for($i=0;$i<4;$i++)
        {
            $tuan = date('Y-m-d',strtotime('next monday', strtotime($tuan))); $tuan1 = date('Y-m-d',strtotime('next saturday', strtotime($tuan)));
            $tuans[] = array($tuan, $tuan1);
        }
        $this->set('tuans', $tuans);

        $this->set('thang',$thang);
        $this->set('nam',$nam);

        if(!isset($javascript))return $tuans;
    }

    //160617 Doan them
    function admin_congviec_tuan()
    {
        $stt = 0;
        //thu: 0:sun, 1:mom, 6:satur
        $day = 1;
        $Year = date("Y");
        $thu = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        for($i = 1; $i < 7; $i++){
            $stt = $stt + 1;
            $day = $i;
            $Week = date("W");
            $DateByNumberOfWeek  = date('Y-m-d', strtotime($Year . "W". $Week  . $day));
            //$tuan[$thu[$i]] = $DateByNumberOfWeek;
            $datas = $this->Lichhop->find('all', array(
            'fields' => array( 'id','tieude', 'noidung', 'chutri', 'thanhphan', 'vitri', 'thoigian', 'nguoitao', 'modified' ),
                'conditions' => array(
                    'Lichhop.trangthai' => 4,
                    'Lichhop.thoigian' => $DateByNumberOfWeek
                ),
                'contain' => array(
                    'Nguoitao'
                ),
                'order' => 'Lichhop.id asc'
            ));
            $tuans[] = array("stt" => $stt, "thu" => $thu[$i], "ngay" => $DateByNumberOfWeek, 'lich' => $datas);

            $Week = date("W") + 1;
            $DateByNumberOfWeek  = date('Y-m-d', strtotime($Year . "W". $Week  . $day));
            //$tuantiep[$thu[$i]] = $DateByNumberOfWeek;
            $datas = $this->Lichhop->find('all', array(
            'fields' => array( 'id','tieude', 'noidung', 'chutri', 'thanhphan', 'vitri', 'thoigian', 'nguoitao', 'modified' ),
                'conditions' => array(
                    'Lichhop.trangthai' => 4,
                    'Lichhop.thoigian' => $DateByNumberOfWeek
                ),
                'contain' => array(
                    'Nguoitao'
                ),
                'order' => 'Lichhop.id asc'
            ));
            $tuantieps[] = array("stt" => $stt, "thu" => $thu[$i], "ngay" => $DateByNumberOfWeek, 'lich' => $datas);
        }

        $this->set('tuans', $tuans);
        $this->set('tuantieps', $tuantieps);
    }

    public function admin_danhsach_ngay($ngay = null)
    {
        $datas = $this->Lichhop->find('all', array(
            'fields' => array( 'id','tieude', 'noidung', 'chutri', 'thanhphan', 'vitri', 'thoigian', 'nguoitao', 'modified' ),
                'conditions' => array(
                    'Lichhop.trangthai' => 4,
                    'Lichhop.thoigian' => $ngay
                ),
                'contain' => array(
                    'Nguoitao'
                ),
                'order' => 'Lichhop.id asc'
            ));

        $this->set('datas', $datas);
        $this->set('ngay', $ngay);

        $this->layout = 'ajax';
    }

    function admin_them_ngay($ngay)
    {
        if(!empty($this->data))
        {
            $this->Lichhop->create();
            if ($this->Lichhop->save($this->data) ){
                $this->redirect( '/admin/lichhops/danhsach_ngay/'.$ngay );
            }
        }

        $this->set('ngay', $ngay);

        $this->layout = 'ajax';
    }

    function admin_sua_ngay( $id, $ngay ) {
        if(empty($this->data)){
            if(!is_numeric( $id ))exit;
            $cond = array('Lichhop.trangthai' => 4,'Lichhop.id' => $id);
            $tmp = $this->Lichhop->find( 'first',array(
                'contain' => false,
                'conditions' => $cond
            ));

            if(empty($tmp))exit;

            $this->data = $tmp;
        }else{
            $data = $this->data['Lichhop'];
            $data['id'] = $id;
            if ($this->Lichhop->save($data) ){
                $this->redirect( '/admin/lichhops/danhsach_ngay/'.$ngay );
            }
        }
        $this->set('id', $id);
        $this->set('ngay', $ngay);
        $this->layout = 'ajax';
    }
    //160617 Doan them ---
}
