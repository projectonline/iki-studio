<?php
class UploaderUsersController extends AppController{
	var $name='UploaderUsers';

//	var $helpers = array( 'Html', 'Form', 'Js', 'Session', 'Thumbnail', 'Cache', 'Paginator', 'Time' );

	function beforeFilter()
	{
		parent::beforeFilter();

		// alow initDB
	}

	function index()
	{
		// lay ra cac cong ty duoc tui theo doi
		$tmp = $this->UploaderUser->find('list', array(
			'fields' => array('id', 'congty_id'),
			'conditions' => array(
				'UploaderUser.user_id' => $this->Auth->user('id'),
				'UploaderUser.trangthai' => 4,
				'UploaderUser.loai' => 1
			),
			'contain' => false
		));
		$this->loadModel('Codetbl');
		$datas = $this->Common->html_decode($this->Codetbl->find('list',array(
			'conditions' => array(
				'id' => $tmp
			),
			'fields' => array('id', 'name'),
			'order' => 'sort asc',
			'contain' => false
		)));
		$this->set('congty', $datas);

		// Theo dõi user xem có upload file đầy đủ hay không
		$list_id_user = $this->UploaderUser->find('all', array(
			'fields' => array('id', 'user_id', 'solanvipham'),
			'conditions' => array(
				'UploaderUser.trangthai' => 4,
				'UploaderUser.loai' => 0, // 0 la nhung nguoi bi theo doi
				'UploaderUser.congty_id' => $tmp
			),
			'contain' => array(
				'User'
			)
		));

		$datas = array();
		if( !empty($list_id_user) )
		{
			$this->loadModel('Uploader');

			// GET ngay
			$homnay = strtotime('now');
			if( isset($this->data['UploaderUser']) )
			{
				$homnay = strtotime($this->Common->string2date($this->data['UploaderUser']['q_ngay']).' 00:00:00');
				$homqua = $homnay;
				if( date('N', strtotime('-1 day', $homnay)) == 7 )
				{
					$homkia = strtotime('-2 days', $homnay);
				}else{
					$homkia = strtotime('-1 day', $homnay);
				}

			}else{
				if( date('N', strtotime('-1 day', $homnay)) == 7 )
				{
					$homqua = strtotime('-2 days', $homnay);
				}else{
					$homqua = strtotime('-1 day', $homnay);
				}

				$tmpdate['UploaderUser']['q_ngay'] = date("d/m/Y", $homqua);
				$this->data = $tmpdate;
			}

			$this->set('homqua', $homqua);

			foreach( $list_id_user as $data )
			{
				$tmp = $this->Uploader->find('all', array(
					'fields' => array('id','name','dungluong','nguoitao'),
					'conditions' => array(
						'Uploader.trangthai' => 4,
						'Uploader.nguoitao' => $data['UploaderUser']['user_id'],
						'DATE(Uploader.created)' => date('Y-m-d', $homqua)
					),
					'contain' => false
				));
				$datas[$data['UploaderUser']['user_id']][0] = $tmp;
				$datas[$data['UploaderUser']['user_id']][1] = $data['User'];
				$datas[$data['UploaderUser']['user_id']][2] = $data['UploaderUser'];
			}

		}else{
			$this->set('homqua', strtotime('-1 day', strtotime('now')));
		}

		$this->set('datas', $datas);
	}

	function chon_congty()
	{
		$this->loadModel('Codetbl');
		$datas = $this->Common->html_decode($this->Codetbl->find('list',array(
			'conditions' => array(
				'code1' => 'congtrinhs',
				'code2' => 'congty',
				'trangthai' => 4
			),
			'fields' => array('id', 'name'),
			'order' => 'sort asc',
			'contain' => false
		)));

		$this->set('datas', $datas);
	}

	function them_user_theo_doi($congty_id)
	{
		if( !$this->request->is('ajax') )exit;

		$this->set('congty_id', $congty_id);

		$save = array();

		if( isset($this->data['UploaderUser']['username']) )
		{
			$username = $this->data['UploaderUser']['username'];
			$save['loai'] = 0;

		}elseif( isset($this->data['UploaderUser']['username1']) )
		{
			$username = $this->data['UploaderUser']['username1'];
			$save['loai'] = 1;
		}

		if( isset($username) && !empty($this->data) )
		{
			$tmp = $this->UploaderUser->Nguoitao->find('first', array(
				'fields' => array('id', 'username'),
				'conditions' => array(
					'Nguoitao.username' => $username,
					'Nguoitao.trangthai' => 4,
				),
				'contain' => false
			));

			if( isset($tmp['Nguoitao']) )
			{
				$tmp_tontai = $this->UploaderUser->find('first', array(
					'fields' => array('id'),
					'conditions' => array(
						'UploaderUser.user_id' => $tmp['Nguoitao']['id'],
						'UploaderUser.trangthai' => 4,
						'UploaderUser.loai' => $save['loai']
					),
					'contain' => false
				));
				if( !isset($tmp_tontai['UploaderUser']) )
				{
					$save['user_id'] = $tmp['Nguoitao']['id'];
					$save['congty_id'] = $congty_id;

					if($this->UploaderUser->save( $save ))
					{
						$this->Session->setFlash('Thêm thành công', 'default', array('class' => 'message_success'));
						//$this->redirect('/uploader_users'); exit;
					}else{
						$this->set('message_error', 'Có lỗi trong quá trình lưu. Vui lòng bấm F5 và thử lại.');
					}
				}else{
					$this->Session->setFlash('Thành viên này đã được theo dõi rồi.', 'default', array('class' => 'message_error'));
				}
			}else{
//				$this->Session->setFlash('Lưu công trình thành công', 'default', array('class' => 'message_success'));
				$this->Session->setFlash('Tên thành viên không tồn tại.', 'default', array('class' => 'message_error'));
			}
		}

		$tmp = $this->UploaderUser->find('all', array(
			'fields' => array('id'),
			'conditions' => array(
				'UploaderUser.trangthai' => 4,
				'UploaderUser.loai' => 0,
				'UploaderUser.congty_id' => $congty_id
			),
			'contain' => array(
				'User'
			)
		));
		$this->set('datas1', $tmp);

		$tmp = $this->UploaderUser->find('all', array(
			'fields' => array('id'),
			'conditions' => array(
				'UploaderUser.trangthai' => 4,
				'UploaderUser.loai' => 1,
				'UploaderUser.congty_id' => $congty_id
			),
			'contain' => array(
				'User'
			)
		));
		$this->set('datas2', $tmp);
	}

	function thay_doi_so_lan_vi_pham( $id, $plus = 1 )
	{
		if( !$this->request->is('ajax') || !is_numeric($id) )exit;


		$tmp = $this->UploaderUser->find('first', array(
			'fields' => array('id', 'solanvipham'),
			'conditions' => array(
				'UploaderUser.id' => $id
			),
			'contain' => false
		));

		$this->UploaderUser->id = $id;
		$solanvipham = $tmp['UploaderUser']['solanvipham'] + 1;
		if( $plus )
		{
			$solanvipham = $tmp['UploaderUser']['solanvipham'] - 1;
		}
		if( $this->UploaderUser->saveField('solanvipham', $solanvipham) )
		{
			echo $solanvipham;
		}
		exit;
	}
}
