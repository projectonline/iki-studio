<?php
class TintucsController extends AppController{
	var $name = 'Tintucs';

	function beforeFilter( )
	{
		// goi den before filter cha
		parent::beforeFilter();
	}

	function admin_lists(){

		$data = array();

		$tmp = $this->Tintuc->find('list', array(
				'conditions' => array(
					'Tintuc.trangthai' => 4,
					'Tintuc.modified >=' => date('Y-m-d', (strtotime('now') - DAY*3) ).' 00:00:00'
				),
				'fields' => array('id', 'id'),
				'contain' => false,
			));

		if( !empty($tmp) )
		{
			$id_rand = array_rand($tmp);

			$data = $this->Tintuc->find('first', array(
				'conditions' => array(
					'Tintuc.id' => $id_rand
				),
				'fields' => array('id', 'tieude', 'noidung', 'created', 'nguoitao'),
				'contain' => false
			));
		}

		// neu khong co tin moi thi random toan bo tin cu~
		if( !isset($data['Tintuc']) )
		{
			$tmp = $this->Tintuc->find('list', array(
				'conditions' => array(
					'Tintuc.trangthai' => 4
				),
				'fields' => array('id', 'id'),
				'contain' => false,
			));

			$id_rand = array_rand($tmp);

			$data = $this->Tintuc->find('first', array(
				'conditions' => array(
					'Tintuc.id' => $id_rand
				),
				'fields' => array('id', 'tieude', 'noidung', 'created', 'nguoitao'),
				'contain' => false
			));
			$this->set('random_all', 1);
		}
		//$this->lists_more();

		$this->set( 'data', $data );
		$this->admin_lists_more();
	}

	function admin_chitiet( $id ){

		$data = $this->Tintuc->find('first', array(
			'conditions' => array(
				'Tintuc.id' => $id
			),
			'fields' => array('id', 'tieude', 'noidung', 'created'),
			'contain' => array('Nguoitao')
		));

		$this->set( 'data', $data );
	}

	function admin_lists_more()
	{
		$this->paginate = array(
			'fields' => array('id', 'tieude', 'created', 'nguoitao'),
			'conditions' => array(
				'Tintuc.trangthai' => 4
			),
			'order' => 'Tintuc.id desc',
			'limit' => 5,
			'contain' => false
		);
		$this->set( 'datas', $this->paginate() );
	}

	function admin_them()
	{
		if(!empty($this->data))
		{
			$data = array();
			$data['tieude'] = $this->Common->html($this->data['Tintuc']['tieude']);
			$data['noidung'] = $this->data['Tintuc']['noidung'];

			$this->Tintuc->create();
			if($this->Tintuc->save($data))
			{
				$this->redirect('/admin');
			}
		}
	}

	function admin_sua( $id ) {
		if(empty($this->data)){
			if(!is_numeric( $id ))exit;
			$cond = array('Tintuc.trangthai' => 4,'Tintuc.id' => $id);
			$tmp = $this->Tintuc->find( 'first',array(
				'contain' => false,
				'conditions' => $cond
			));

			if(empty($tmp))exit;

			$tmp['Tintuc']['tieude'] = $this->Common->html_decode($tmp['Tintuc']['tieude']);
			$this->data = $tmp;
		}else{
			$data['id'] = $id;
			$data['tieude'] = $this->Common->html($this->data['Tintuc']['tieude']);
			$data['noidung'] = str_replace('em>', 'i>', $this->data['Tintuc']['noidung']);
			if($this->Tintuc->save($data))
			{
				$this->redirect('/admin');
			}
		}
	}
}