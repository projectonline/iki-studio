<?php //160712 Doan them
class DoitacsController extends AppController
{
    var $name       = 'Doitacs';
    var $components = array('Common', 'Cmfile', 'Auth', 'Search.Prg', 'Cookie');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $check_permission = false;
        if(in_array($this->Auth->user('id'), array( 1, 2, 3)))
        {
            $check_permission = true;
        }
        $this->set('check_permission', $check_permission);
    }

    public $presetVars = array(
        array('field' => 'ten', 'type' => 'fulltext'),
        array('field' => 'dienthoai', 'type' => 'like'),
        array('field' => 'loai', 'type' => 'value'),
        array('field' => 'nhan', 'type' => 'value')
    );

    function admin_index( $type_parent = null )
    {
        $this->Prg->commonProcess('Doitac');
        $cond = $this->Doitac->parseCriteria($this->passedArgs);
        $tmp['Doitac'] = $this->request->params['named'];
        $this->request->data = $tmp;

        if(isset($cond['Doitac.loai']) && $cond['Doitac.loai'] == 0){
            unset($cond['Doitac.loai']);
        }
        if(isset($cond['Doitac.nhan']) && $cond['Doitac.nhan'] == 0){
            unset($cond['Doitac.nhan']);
        }
        $cond['Doitac.trangthai'] = 4;
        $this->paginate = array(
            'conditions' => $cond,
            'order' => 'Doitac.ten asc',
            'limit' => 20,
            'contain' => false
        );
        $this->set('datas',$this->paginate());

        $loai[0] = "---";
        $loai[1] = "Client";
        $loai[2] = "Partner";
        $this->set('loai', $loai);

        $nhan[0] = "---";
        $nhan[1] = "Cá nhân";
        $nhan[2] = "Công ty";
        $this->set('nhan', $nhan);

        $this->loadModel('Congtrinh');
        $con_nc = array(
            'Congtrinh.trangthai'    => 4
        );
        $dsct = $this->Congtrinh->find("list",array(
            'fields'     => array('id','tieude'),
            'contain'    => false,
            'conditions' => $con_nc
        ));
        $dsctd[0] = "---";
        $dsct = $dsctd + $dsct;
        $this->set('dsct', $dsct);
    }

    function admin_danhsach()
    {
        $cond['Doitac.trangthai'] = 4;
        $this->paginate = array(
            'conditions' => $cond,
            'order' => 'Doitac.ten asc',
            'limit' => 20,
            'contain' => false
        );
        $this->set('datas',$this->paginate());

        $loai[0] = "---";
        $loai[1] = "Client";
        $loai[2] = "Partner";
        $this->set('loai', $loai);

        $nhan[0] = "---";
        $nhan[1] = "Cá nhân";
        $nhan[2] = "Công ty";
        $this->set('nhan', $nhan);

        $this->loadModel('Congtrinh');
        $con_nc = array(
            'Congtrinh.trangthai'    => 4
        );
        $dsct = $this->Congtrinh->find("list",array(
            'fields'     => array('id','tieude'),
            'contain'    => false,
            'conditions' => $con_nc
        ));
        $dsctd[0] = "---";
        $dsct = $dsctd + $dsct;
        $this->set('dsct', $dsct);

        $this->layout = 'ajax';
    }

    function admin_them()
    {
        if(!empty($this->data))
        {
            //$this->Doitac->create();
            //if ($this->Doitac->save($this->data) ){
                $this->redirect( '/admin/doitacs/danhsach' );
            //}
        }

        $loai[1] = "Client";
        $loai[2] = "Partner";
        $this->set('loai', $loai);

        $nhan[1] = "Cá nhân";
        $nhan[2] = "Công ty";
        $this->set('nhan', $nhan);

        $this->loadModel('Congtrinh');
        $con_nc = array(
            'Congtrinh.trangthai'    => 4
        );
        $dsct = $this->Congtrinh->find("list",array(
            'fields'     => array('id','tieude'),
            'contain'    => false,
            'conditions' => $con_nc
        ));
        $dsctd[0] = "---";
        $dsct = $dsctd + $dsct;
        $this->set('dsct', $dsct);

        $this->layout = 'ajax';
    }

    function admin_sua($id)
    {
        if(empty($this->data)){
            if(!is_numeric( $id ))exit;
            $cond = array('Doitac.trangthai' => 4,'Doitac.id' => $id);
            $tmp = $this->Doitac->find( 'first',array(
                'contain' => false,
                'conditions' => $cond
            ));

            if(empty($tmp))exit;

            $this->data = $tmp;
        }else{
            $data = $this->data['Doitac'];
            $data['id'] = $id;
            if ($this->Doitac->save($data) ){
                $this->redirect( '/admin/doitacs/danhsach' );
            }
        }
        $this->set('id', $id);

        $loai[1] = "Client";
        $loai[2] = "Partner";
        $this->set('loai', $loai);

        $nhan[1] = "Cá nhân";
        $nhan[2] = "Công ty";
        $this->set('nhan', $nhan);

        $this->loadModel('Congtrinh');
        $con_nc = array(
            'Congtrinh.trangthai'    => 4
        );
        $dsct = $this->Congtrinh->find("list",array(
            'fields'     => array('id','tieude'),
            'contain'    => false,
            'conditions' => $con_nc
        ));
        $dsctd[0] = "---";
        $dsct = $dsctd + $dsct;
        $this->set('dsct', $dsct);

        $this->layout = 'ajax';
    }
}
