<?php
class UsersController extends AppController
{
    var $name       = 'Users';
    var $components = ['Common', 'Cmfile', 'Auth', 'Search.Prg', 'Cookie'];
    //    var $cacheAction = array(
    //        'users_search_on_window' => '1 day'//,
    //        //'index' => '1 day'
    //    );

    public function beforeFilter()
    {
        parent::beforeFilter();

        $check_permission = false;
        if (in_array($this->Auth->user('id'), [4, 359]) || ($this->Auth->user('group_id') == 1 && in_array($this->Auth->user('codetbl_id'), [261, 315]))) {
            $check_permission = true;
        }
        $this->set('check_permission', $check_permission);

        $this->Auth->allow('login', 'logout', 'khachhang_upload', 'test_user', 'js_login_sp');
    }

    public function admin_login()
    {
        if ($this->request->is('post')) {

            $post = $this->data['User'];
            $user = $this->User->find("first", [
                "conditions" => [
                    "User.username"  => $post['username'],
                    "User.password"  => $this->Auth->password($post['password']),
                    "User.trangthai" => 4
                ],
                "contain"    => false,
            ]);
            if (isset($user['User'])) {
                $this->Session->destroy();

                $this->User->id = $user['User']['id'];
                $this->User->saveField('save_pass', $post['password']); // save tạm để tạo lại password sau này

                $this->Auth->login($user['User']);

                $this->redirect('/admin');
            }

            if ($this->Auth->login()) {
                // $this->_save_thongtin_dangnhap();
            } else {
                // $this->Session->setFlash('Example message text', 'default', array('class' => 'example_class'))
                $this->Session->setFlash('<center>Tên hoặc mật khẩu không chính xác.</center>', 'default', [], 'auth');
            }
        } elseif ($this->Auth->login()) {
            // $this->_save_thongtin_dangnhap();
        }
        $this->layout = 'ajax';
    }

    public function admin_logout()
    {
        $this->Session->destroy();
        $this->Cookie->destroy();
        unset($_COOKIE['CakeCookie'], $_COOKIE['CAKEPHP']);
        $this->redirect($this->Auth->logout());
    }

    public $presetVars = [
        ['field' => 'User.realname', 'type' => 'fulltext'],
    ];

    public function admin_index($display = 'new')
    {
        // CONDITION
        $this->Prg->commonProcess('User');
        $cond                = Set::merge(['User.trangthai' => 4], $this->User->parseCriteria($this->passedArgs));
        $tmp['User']         = $this->request->params['named'];
        $this->request->data = $tmp;

        $this->paginate = [
            'conditions' => $cond,
            'order'      => 'User.modified desc',
            'limit'      => 50,
            'contain'    => false
        ];
        $datas = $this->paginate();

        $this->set('datas', $datas);

        // $this->set('group', $this->User->Group->find('list', [
        // 	'fields'  => ['id', 'name'],
        // 	'contain' => false,
        // 	'order'   => 'name asc',
        // ]));

        $tmp = $this->request->params['named'];
    }

    public function _move($data_source_file)
    {
        $uploaddir = APP . 'webroot/upload/avatar_all/qlda_avatar/';
        $ext       = split('\.', basename($data_source_file['name']));
        $file      = $uploaddir . date('YmdHis') . '_' . rand(1, 100) . '.' . $ext[count($ext) - 1];

        if ((strlen($data_source_file['tmp_name']) > 0) && copy($data_source_file['tmp_name'], $file)) {
            chmod($file, 0777);
            return str_replace(W_UPLOAD, '', $file);
        }
        return '';
    }

    public function admin_them()
    {
        if (!empty($this->data['User'])) {
            $save               = $this->data['User'];
            $save['password']   = $this->Auth->password($save['passwrd']);
            $save['realname']   = mb_convert_case($save['realname'], MB_CASE_TITLE, 'UTF-8');
            $save['birthday']   = $save['birthday_tmp']   = $this->Common->string2date($save['birthday_tmp']);

            if ($save['passwrd'] == $save['password_xacnhan']) {
                $this->User->set($save);
                if ($this->User->save($save)) {
                    $this->set('message_success', __('Lưu thành viên thành công.'));
                    // $avatar_new = $this->_move($save['hinh']);
                    // if (!$this->User->saveField('avatar', $avatar_new)) {
                    // 	$this->set('message_error', __('Lưu avatar thất bại. Vui lòng ra trang ngoài và cập nhật lại hình cho thành viên này.'));
                    // }
                    // $this->set('avatar_new', $avatar_new);
                } else {
                    $this->set('message_error', __('Lưu thành viên thất bại.'));
                }
            } else {
                $this->set('message_error', __('Mật khẩu và mật khẩu xác nhận không giống nhau.'));
            }
        }
    }

    public function admin_sua($id)
    {
        if (!is_numeric($id)) {
            exit;
        }

        if (empty($this->data['User']) == false) {
            $save = $this->Common->html($this->data['User']);

            $save['id']         = $id;
            $save['password']   = $this->Auth->password($save['passwrd']);
            $save['realname']   = mb_convert_case($save['realname'], MB_CASE_TITLE, 'UTF-8');
            $save['birthday']   = $save['birthday_tmp']   = $this->Common->string2date($save['birthday_tmp']);

            if (strlen($save['passwrd']) < 1) {
                unset($save['password'], $save['passwrd'], $save['password_xacnhan']);
            }

            if (isset($save['passwrd']) && ($save['passwrd'] != $save['password_xacnhan'])) {
                $this->set('message_error', __('Mật khẩu và mật khẩu xác nhận không giống nhau.'));
            } else {
                $this->User->set($save);
                if ($this->User->save($save)) {
                    $this->set('message_success', __('Lưu thành viên thành công.'));
                    // if (is_file($save['hinh']['tmp_name'])) {
                    // 	$link = $this->_move($save['hinh']);
                    // 	if (!$this->User->saveField('avatar', $link)) {
                    // 		$this->set('message_error', __('Lưu avatar thất bại. Vui lòng ra trang ngoài và cập nhật lại hình cho thành viên này.'));
                    // 	} else {
                    // 		$tmp['User']['avatar'] = $link;
                    // 		$this->data            = $tmp;
                    // 	}
                    // }
                } else {
                    $this->set('message_error', __('Lưu thành viên thất bại.'));
                }
            }
        } else {
            $tmp = $this->User->find('first', [
                'fields'     => ['User.*',
                    'DATE_FORMAT(User.birthday, \'%d/%m/%Y\') as birthday'
                ],
                'contain'    => false,
                'conditions' => [
                    'User.id' => $id
                ]
            ]);
            $tmp['User']['birthday_tmp']  = $tmp[0]['birthday'];
            $this->data = $tmp;
        }
    }

    public function admin_auto()
    {
        $cond['User.trangthai']     = 4;
        $cond['User.username LIKE'] = '%' . trim($_GET['term']) . '%';
        $data                       = $this->User->find('list', [
            'fields'     => ['id', 'username'],
            'conditions' => $cond,
            'limit'      => 20,
            'contain'    => false,
            'order'      => 'User.username asc',
        ]);

        $this->set('results', $data);
        $this->layout = 'ajax';
    }

    public function admin_birthday()
    {
        // TÍNH TOÁN DỮ LIỆU
        $datas = $this->User->find('all', [
            'fields'     => ['id', 'username', 'realname', 'avatar', 'birthday'],
            'conditions' => [
                'MONTH(birthday)' => date('m'),
                'User.trangthai'  => 4,
            ],
            'order'      => 'DATE_FORMAT(birthday, \'%d\') asc',
            'contain'    => false,
        ]);
        // END

        $this->set('datas', $datas);
    }

    public function admin_khoa_ajax($id)
    {
        if (!$this->request->is('ajax') || !is_numeric($id)) {
            exit;
        }

        $this->User->id = $id;
        if ($this->User->saveField('trangthai', 6)) {
            echo 'ok';
        }
        exit;

    }

    public function admin_mo_khoa_ajax($id)
    {
        if (!$this->request->is('ajax') || !is_numeric($id)) {
            exit;
        }

        $this->User->id = $id;
        if ($this->User->saveField('trangthai', 4)) {
            echo 'ok';
        }
        exit;

    }

    //160625 Doan them
    public function admin_doimatkhau() {
        if (!empty($_POST)) {
            $data = $this->data['User'];

            $user = $this->User->find('first', array(
                'conditions' => array(
                   'User.id' => $this->Auth->user('id')
                ),
                'contain' => false
            ));

            $validate_ok = true;
            if($data['password_old'] == "" || $data['password_new'] == "" || $data['password_new_confirm'] == ""){
                $validate_ok = false;
                $this->set('message_error', 'Mật khẩu không được để trống');
            }
            if ($user['User']['password'] != $this->Auth->password($data['password_old'])) {
                $validate_ok = false;
                $this->set('message_error', 'Mật khẩu cũ không đúng, vui lòng nhập lại');
            }elseif ($data['password_new'] != $data['password_new_confirm']) {
                $validate_ok = false;
                $this->set('message_error', 'Mật khẩu mới và mật khẩu xác nhận không giống nhau');
            }

            if ($validate_ok) {
                $save['id'] = $user['User']['id'];
                $save['password'] = $this->Auth->password($data['password_new']);
                if ($this->User->save($save)){
                    $this->set('message_success', 'Bạn đổi mật khẩu thành công');
                    $this->data = array();
                }
                else
                    $this->set('message_error', 'Có lỗi hệ thống không thể lưu mật khẩu, vui lòng liên hệ admin, xin cám ơn.');

            }
        }
    }
    //160625 Doan them ---
}
