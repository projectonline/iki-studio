<?php
class FrontsController extends AppController
{
    var $name = 'Fronts';
    var $layout = 'front';

    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->Auth->allow();
    }

    public function index()
    {
        // 
    }

    public function about()
    {
        // 
    }

    public function news()
    {
        // 
    }

    public function gallery()
    {
        // 
    }

    public function purpose()
    {
        // 
    }
    
    public function contact()
    {
        // 
    }
}