<?php
class CmfileComponent extends Component{

	function move_to($source_file, $dest_folder, $file_name, $copy = '')
	{
		// KIEM TRA TRUOC KHI MOVE


		if(!is_dir( $dest_folder ))
		{
			mkdir( $dest_folder ); chmod( $dest_folder, 0777 );
		}

		// Phân thư mục ra thành từng tháng
		$dest_folder = $dest_folder.date( 'Ym' ).'/';
		if(!is_dir( $dest_folder ))
		{
			mkdir( $dest_folder ); chmod( $dest_folder, 0777 );
		}

		if( !file_exists( $source_file ) || !is_writable( $dest_folder ))return false;

		$new_file = $dest_folder.$file_name;

		if($copy == 'copy' )
		{
			if( copy($source_file, $new_file))
			{
				unlink($source_file);
				chmod( $new_file, 0777 );
				return str_replace( DIR_UPLOAD, '', $new_file );
			}
		}else{
			if( move_uploaded_file($source_file, $new_file))
			{
				chmod( $new_file, 0777 );
				return str_replace( DIR_UPLOAD, '', $new_file );
			}
		}


		return false;
	}

	function move($source_file, $dest_folder, $username)
	{
		if(!is_dir( $dest_folder ))
		{
			mkdir( $dest_folder ); chmod( $dest_folder, 0777 );
		}

		// Phân thư mục ra thành từng tháng
		$dest_folder = $dest_folder.date( 'Ym' ).'/';
		if(!is_dir( $dest_folder ))
		{
			mkdir( $dest_folder ); chmod( $dest_folder, 0777 );
		}

		$new_file = $dest_folder.$username.'_'.basename( $source_file );

		// copy file to new location
		if(strpos($dest_folder, '/large/'))
		{
			$dir_upload = DIR_UPLOAD;
		}else{
			$dir_upload = APP_DIR_UPLOAD;
		}

		if(!is_file($source_file))$source_file = $dir_upload.$source_file;

		// kiểm tra file tồn tại trước khi copy
		if( filesize( $source_file ) < 4  || !file_exists( $source_file ) || !is_writable( $dest_folder ))return false;

		copy( $source_file, $new_file );chmod( $new_file, 0777 );unlink( $source_file );

		if( filesize( $new_file ) < 4 )return false;

		// nếu move vào tmp thì ko cần xóa gốc APP
		if(strpos($new_file, '/tmp/') === false)
		{
			return str_replace($dir_upload,'',$new_file);
		}else{
			return $new_file;
		}

	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( SONY ) ( Nov 4, 2008 )
	//***************************************************************//
//	function move( $source_file = null, $dest_folder = null )
//	{
//		// check dieu kien vao
//		if( file_exists( $source_file ) == false
//		|| is_dir( $dest_folder ) == false
//		|| is_writable( $dest_folder ) == false )
//		{
//			return false;
//		}
//
//		$new_file = $dest_folder .  basename( $source_file );
//		// copy file to new location
//		copy( $source_file,  $new_file );
//		chmod( $new_file, 0777 );
//		unlink( $source_file );
//		if( strpos( $new_file, 'webroot' ) ){
//			return  str_replace( SMALL, '', $new_file ); // APP.'webroot/upload/'
//		}else{
//			return  str_replace( SMALLFILE, '', $new_file ); // APP.'upload/'
//		}
//
//	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( SONY ) ( Nov 4, 2008 )
	//***************************************************************//
//	function get_file_type( $file  = null )
//	{
//		if( file_exists( $file ) == true )
//		{
//			return exec( 'file -b ' . escapeshellarg ( $file ), $a ) ;
//		}
//		else
//		{
//			return null;
//		}
//
//	}
}
