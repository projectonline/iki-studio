<?php
class FormatComponent extends Component{
	function cut_string( $string = null, $flag = true, $length = TITLE_MAX_LENGTH, $ending = '...', $num_row = 1 )
	{
		if( $flag == true )
		{
			$string = $this->html_decode(trim( $string ));
			if( !is_numeric( $length ) ) $length  = TITLE_MAX_LENGTH;
			if( mb_strlen( $string ) > $length )
			{
				if(strpos(' ', $string) == false ){ $string .= ' '; }

				APP::import( 'Helper', 'Text' );
				$text = new TextHelper( );
				if( $num_row == 1 )
				{
					$string1 = $text->truncate( $string, $length, $ending, false, true );
					$length1 = mb_strlen( $string1 );
					if( $length1 < 4 )
					{
						$string1 = mb_substr( $string, 0, $length - 3 ).$ending;
					}
					$string = $string1;
				}else // == 2
				{
					$strtmp = $string;

					// lay ra doan chuoi thu 1
					$str1 = $this->cut_string( $strtmp, TRUE, $length, '' );
					$leng_string1 = mb_strlen( $str1 );

					// neu nhu còn thừa thì mới cắt thành đoạn thứ 2
					if( mb_strlen( $string ) > $leng_string1 )
					{
						// cat ra doan chuoi 2
						$strtmp_end = mb_substr( $strtmp, $leng_string1 );
						$str2 = $this->cut_string( $strtmp_end, TRUE, $length );
					}else
					{
						$str2 = '';
					}
					$string = $str1.'<br>'.$str2;
				}
			}
		}elseif( $flag == false ){
			if( mb_strlen( $string ) > $length )
			$string = mb_substr( $string, 0, $length ) . '...' ;
		}
		return $string;
	}

	//***************************************************************//
	// Function :
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( BiBi.BaoNam ) ( May 10, 2011 )
	//***************************************************************//
	function _SEO( $str )
	{
		if( strlen( $str ) == 0) return $str;
			$str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
			$str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
			$str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
			$str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
			$str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
			$str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
			$str = preg_replace('/(đ)/', 'd', $str);
			$str = preg_replace('/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'A', $str);
			$str = preg_replace('/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'E', $str);
			$str = preg_replace('/(Ì|Í|Ị|Ỉ|Ĩ)/', 'I', $str);
			$str = preg_replace('/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'O', $str);
			$str = preg_replace('/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'U', $str);
			$str = preg_replace('/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'Y', $str);
			$str = preg_replace('/(Đ)/', 'D', $str);
		return $str;
	}

	//***************************************************************//
	// Function :  ucwords_vn
	//---------------------------------------------------------------//
	// Params   :
	//---------------------------------------------------------------//
	// Return   :
	//---------------------------------------------------------------//
	// Memo     :
	//---------------------------------------------------------------//
	// Version  :  Add ( namnb ) ( Nov 11, 2009 )
	//***************************************************************//
	function ucwords_vn($string = null)
	{
		$string = str_replace(array('-', '(', ')', '( ', ' )'), array(' - ', ' (', ') ', '(', ')'),$string);

		$arr_tmp = split(' ', $string);

		foreach( $arr_tmp as $key => $tmp )
		{
			if( strlen( $tmp ) > 0 )
			{
				$from = 0; $str_tmp = '';
				if( mb_substr($tmp, 0, 1) == '(')
				{
					$from = 1;
					$str_tmp = mb_substr($tmp, 0, 1);
				}
				$arr_tmp[$key] = $str_tmp.mb_convert_case(mb_substr($tmp, $from, 1), MB_CASE_TITLE, 'UTF-8').mb_substr($tmp, ($from+1));
			}
		}

		$string = implode(' ', $arr_tmp);
		return $string;
	}
}
