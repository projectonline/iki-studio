<?php
class UploadersController extends AppController
{
	var $name = 'Uploaders';
	var $components = array( 'Common', 'Cmfile', 'Auth' );

	var $mimeType = array(
		'ai' => 'application/postscript', 'bcpio' => 'application/x-bcpio', 'bin' => 'application/octet-stream',
		'ccad' => 'application/clariscad', 'cdf' => 'application/x-netcdf', 'class' => 'application/octet-stream',
		'cpio' => 'application/x-cpio', 'cpt' => 'application/mac-compactpro', 'csh' => 'application/x-csh',
		'csv' => 'application/csv', 'dcr' => 'application/x-director', 'dir' => 'application/x-director',
		'dms' => 'application/octet-stream', 'doc' => 'application/msword', 'drw' => 'application/drafting',
		'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'one' => 'application/onenote',
		'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'dvi' => 'application/x-dvi', 'dwg' => 'application/acad', 'dxf' => 'application/dxf',
		'dxr' => 'application/x-director', 'eot' => 'application/vnd.ms-fontobject', 'eps' => 'application/postscript',
		'exe' => 'application/octet-stream', 'ez' => 'application/andrew-inset',
		'flv' => 'video/x-flv', 'gtar' => 'application/x-gtar', 'gz' => 'application/x-gzip',
		'bz2' => 'application/x-bzip', '7z' => 'application/x-7z-compressed', 'hdf' => 'application/x-hdf',
		'hqx' => 'application/mac-binhex40', 'ico' => 'image/vnd.microsoft.icon', 'ips' => 'application/x-ipscript',
		'ipx' => 'application/x-ipix', 'js' => 'application/x-javascript', 'latex' => 'application/x-latex',
		'lha' => 'application/octet-stream', 'lsp' => 'application/x-lisp', 'lzh' => 'application/octet-stream',
		'man' => 'application/x-troff-man', 'me' => 'application/x-troff-me', 'mif' => 'application/vnd.mif',
		'ms' => 'application/x-troff-ms', 'nc' => 'application/x-netcdf', 'oda' => 'application/oda',
		'otf' => 'font/otf', 'pdf' => 'application/pdf',
		'pgn' => 'application/x-chess-pgn', 'pot' => 'application/mspowerpoint', 'pps' => 'application/mspowerpoint',
		'ppt' => 'application/mspowerpoint', 'ppz' => 'application/mspowerpoint', 'pre' => 'application/x-freelance',
		'prt' => 'application/pro_eng', 'ps' => 'application/postscript', 'roff' => 'application/x-troff',
		'scm' => 'application/x-lotusscreencam', 'set' => 'application/set', 'sh' => 'application/x-sh',
		'shar' => 'application/x-shar', 'sit' => 'application/x-stuffit', 'skd' => 'application/x-koan',
		'skm' => 'application/x-koan', 'skp' => 'application/x-koan', 'skt' => 'application/x-koan',
		'smi' => 'application/smil', 'smil' => 'application/smil', 'sol' => 'application/solids',
		'spl' => 'application/x-futuresplash', 'src' => 'application/x-wais-source', 'step' => 'application/STEP',
		'stl' => 'application/SLA', 'stp' => 'application/STEP', 'sv4cpio' => 'application/x-sv4cpio',
		'sv4crc' => 'application/x-sv4crc', 'svg' => 'image/svg+xml', 'svgz' => 'image/svg+xml',
		'swf' => 'application/x-shockwave-flash', 't' => 'application/x-troff',
		'tar' => 'application/x-tar', 'tcl' => 'application/x-tcl', 'tex' => 'application/x-tex',
		'texi' => 'application/x-texinfo', 'texinfo' => 'application/x-texinfo', 'tr' => 'application/x-troff',
		'tsp' => 'application/dsptype', 'ttf' => 'font/ttf',
		'unv' => 'application/i-deas', 'ustar' => 'application/x-ustar',
		'vcd' => 'application/x-cdlink', 'vda' => 'application/vda', 'xlc' => 'application/vnd.ms-excel',
		'xll' => 'application/vnd.ms-excel', 'xlm' => 'application/vnd.ms-excel', 'xls' => 'application/vnd.ms-excel',
		'xlw' => 'application/vnd.ms-excel', 'zip' => 'application/zip','rar' => 'application/x-rar', 'aif' => 'audio/x-aiff', 'aifc' => 'audio/x-aiff',		
		'aiff' => 'audio/x-aiff', 'au' => 'audio/basic', 'kar' => 'audio/midi', 'mid' => 'audio/midi',
		'midi' => 'audio/midi', 'mp2' => 'audio/mpeg', 'mp3' => 'audio/mpeg', 'mpga' => 'audio/mpeg',
		'ra' => 'audio/x-realaudio', 'ram' => 'audio/x-pn-realaudio', 'rm' => 'audio/x-pn-realaudio',
		'rpm' => 'audio/x-pn-realaudio-plugin', 'snd' => 'audio/basic', 'tsi' => 'audio/TSP-audio', 'wav' => 'audio/x-wav',
		'asc' => 'text/plain', 'c' => 'text/plain', 'cc' => 'text/plain', 'css' => 'text/css', 'etx' => 'text/x-setext',
		'f' => 'text/plain', 'f90' => 'text/plain', 'h' => 'text/plain', 'hh' => 'text/plain', 'htm' => 'text/html',
		'html' => 'text/html', 'm' => 'text/plain', 'rtf' => 'text/rtf', 'rtx' => 'text/richtext', 'sgm' => 'text/sgml',
		'sgml' => 'text/sgml', 'tsv' => 'text/tab-separated-values', 'tpl' => 'text/template', 'txt' => 'text/plain',
		'xml' => 'text/xml', 'avi' => 'video/x-msvideo', 'fli' => 'video/x-fli', 'mov' => 'video/quicktime',
		'movie' => 'video/x-sgi-movie', 'mpe' => 'video/mpeg', 'mpeg' => 'video/mpeg', 'mpg' => 'video/mpeg',
		'qt' => 'video/quicktime', 'viv' => 'video/vnd.vivo', 'vivo' => 'video/vnd.vivo', 'gif' => 'image/gif',
		'ief' => 'image/ief', 'jpe' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'jpg' => 'image/jpeg',
		'pbm' => 'image/x-portable-bitmap', 'pgm' => 'image/x-portable-graymap', 'png' => 'image/png',
		'pnm' => 'image/x-portable-anymap', 'ppm' => 'image/x-portable-pixmap', 'ras' => 'image/cmu-raster',
		'rgb' => 'image/x-rgb', 'tif' => 'image/tiff', 'tiff' => 'image/tiff', 'xbm' => 'image/x-xbitmap',
		'xpm' => 'image/x-xpixmap', 'xwd' => 'image/x-xwindowdump', 'ice' => 'x-conference/x-cooltalk',
		'iges' => 'model/iges', 'igs' => 'model/iges', 'mesh' => 'model/mesh', 'msh' => 'model/mesh',
		'silo' => 'model/mesh', 'vrml' => 'model/vrml', 'wrl' => 'model/vrml',
		'mime' => 'www/mime', 'pdb' => 'chemical/x-pdb', 'xyz' => 'chemical/x-pdb');


	function beforeFilter( )
	{
//		$index_changed = 'Filedata'; // 'uploadfile'
//		if ($this->request->action == 'index' &&
//			is_uploaded_file( $this->request->params['form']['uploadfile']['tmp_name'] )) {
//			$this->Session->id($this->request->params['pass'][0]);
//			$this->Session->start();
//		}
		parent::beforeFilter( );
		//$this->Auth->allowedActions = array('*');
		//$this->Auth->allowedActions = array('download','index');
		$this->Auth->allowedActions = array('download','index','zipFilesAndDownload');

		//$this->Auth->allow('*');
	}
	function index( )
	{
		$index_changed = 'Filedata'; // 'uploadfile'
		if(isset( $_FILES[$index_changed] ) && $_FILES[$index_changed]['error'] == 0 )
		{
			$uploaddir = APP.'tmp/upload/';
			$ext = split( '\.', basename($_FILES[$index_changed]['name']) );
			$file = $uploaddir . date('Y_m_d_H_i_s').'_rand_'.rand( 1, 9999 ).'.'.$ext[count($ext)-1];
			$size = $_FILES[$index_changed]['size'];
			if($size > (1048576*(MAX_SIZE_UPLOAD+5)))
			{
				// echo 'error file size > 1 MB';
				echo 'Lỗi kích thước file > '.MAX_SIZE_UPLOAD.'MB';
				unlink($_FILES[$index_changed]['tmp_name']);
				exit;
			}
			if (move_uploaded_file($_FILES[$index_changed]['tmp_name'], $file)) {
				if( !file_exists($file) || filesize($file) < 5 )
				{
					echo 'File upload bị lỗi hoặc dung lượng quá nhỏ. Bạn vui lòng upload lại. Chân thành cám ơn bạn.';
				}else{
					$str = 'success__.__<input class=\'FileValue\' name=\'data[FileUrl][]\' type=\'hidden\' value=\''.$file.'\'/>';
					$str .= '<input class=\'FileName\' name=\'data[FileName][]\' type=\'hidden\' value=\''.basename($_FILES[$index_changed]['name']).'\'/>';
					echo $str;
				}
			} else {
				echo $_FILES[$index_changed]['error'].'_move_upload_file'; //.' --- '.$_FILES[$index_changed]['tmp_name'].' %%% '.$file.'($size)';
			}
		}else{
			echo 'Không tồn tại FILE upload hoặc error != 0';
		}
		exit;
	}

	function download($id = null)
	{		
		if(is_string($id) && !is_numeric($id))
			$id = base64_decode($id);
		if(!is_numeric($id))exit;
		//if(!is_numeric($id_item) || !is_string($controller))exit;
		//if(!is_numeric($id))die;

		$cond = array('Uploader.id' => $id);
		$this->data = $this->Uploader->find('first', array(
			'contain'    => false,
			'conditions' => $cond
		) );

		if( !isset( $this->data['Uploader'] ) )
		{
			echo 'Ban dang vao mot duong dan khong ton tai. Vui long lien he lai nguoi cung cap duong dan (link).'; exit;
		}

		if( $this->data['Uploader']['trangthai'] == 9 )
		{
			echo 'File da bi nguoi upload xoa, vui long lien he lai nguoi upload. Cam on.'; exit;
		}

		$user = $this->Auth->user('id');

		// KIEM TRA VIEC CHO PHEP DOWNLOAD RA NGOAI CON HOP LE HAY KHONG
		$download_tu_do = false;
		if( $this->data['Uploader']['download_tu_do'] == 1 )
		{
			if( strlen($this->data['Uploader']['download_tu_do_time']) > 0 )
			{
				if( (strtotime($this->data['Uploader']['download_tu_do_time']) + DAY*15) > strtotime('now') )
				{
					$download_tu_do = true;
				}
			}
		}

		if( !$download_tu_do && !is_numeric($user))
		{
			$number = substr($this->data['Uploader']['controller'], 0, 2);
			if(!(is_numeric($number) || in_array($this->data['Uploader']['controller'], array( 'users' )) ))
			{
				echo 'Ban phai dang nhap roi moi co the download. Neu van khong duoc vui long lien he IT - diult: 0989.289.470 . Cam on rat nhieu.'; exit;
			}
		}


		$filename = DIR_UPLOAD.$this->data['Uploader']['file'];
		if (!file_exists($filename)) {
			echo 'FILE DA BI MAT, vui long lien he nguoi upload file nay hoac lien he IT - diult: 0989.289.470 . Cam on rat nhieu.'; exit;
		}

		// addition by Jorg Weske
		$file_extension = strtolower(substr(strrchr($filename,'.'),1));

		$date_folder_dl = date( 'Y_m_d_H_i' ); // chuyen folder
		

		if( isset($this->mimeType[$file_extension]) )
		{
			//$this->set('cache', '3 days');
			$this->set('download', true);
			$this->set('name', str_replace('.'.$file_extension, '', $this->data['Uploader']['name']));
			$this->set('id', basename($this->data['Uploader']['file']) );
			$this->set('path', DIR_UPLOAD.dirname($this->data['Uploader']['file']).DS );
			//echo mime_content_type(DIR_UPLOAD.$this->data['Uploader']['file']);
			$this->set('extension', $file_extension);

			$this->viewClass = 'Media';
			$this->autoLayout = false;
		}else{
			// Phân thư mục ra thành từng tháng
			$dest_folder = SYMLINK_DOWNLOAD_FILE.$date_folder_dl.'/';

			$name_file = str_replace($file_extension, '', $this->_SEO($this->data['Uploader']['name']));		
			
			if(!in_array($file_extension, array('rar','zip')))
			{				
				$name_file .= '_archive.zip';
				$zip = new ZipArchive();

				//create the file and throw the error if unsuccessful
				$archive_file_name = APP.'tmp/archive/'.$name_file;
				if ($zip->open($archive_file_name, ZIPARCHIVE::CREATE )!==TRUE) {
					exit('cannot open <'.$archive_file_name.'>\n');
				}
				//add each files of $file_name array to archive
				$zip->addFile(DIR_UPLOAD.$this->data['Uploader']['file'],$this->data['Uploader']['name']);
				$zip->close();
				chmod($archive_file_name, 777);
			}else{
				$name_file .= '.'.$file_extension;
				$archive_file_name = DIR_UPLOAD.$this->data['Uploader']['file'];
			}

			if(!is_dir( $dest_folder ))
			{
				if(!mkdir( $dest_folder ))
				{
					echo 'System thong bao: co loi trong qua trinh download file. Vui long lien he IT de sua ngay cho ban: 0989.289.470. ' .
						'Cam on that nhieu. Error ZIP: mkdir '.date( 'Ym' ).' folder false.'; exit;
				}
				chmod( $dest_folder, 0777 );
			}

			if(!file_exists($dest_folder.$name_file))
			{
				if(!symlink( $archive_file_name, $dest_folder.$name_file ))
				{
					echo 'System thong bao: co loi trong qua trinh download file. ' .
						'Vui long lien he IT de sua ngay cho ban: 0989.289.470. Cam on that nhieu. Error ZIP: symlink false.'; exit;
				}
			}

			$this->redirect(DOWNLOAD_FROM_WEBROOT_SYMLINK.$date_folder_dl.'/'.$name_file);
			exit;
		}
	}

	function _SEO( $str )
	{
		if( strlen( $str ) == 0) return $str;
		$str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
		$str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
		$str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
		$str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
		$str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
		$str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
		$str = preg_replace('/(đ)/', 'd', $str);
		$str = preg_replace('/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'A', $str);
		$str = preg_replace('/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'E', $str);
		$str = preg_replace('/(Ì|Í|Ị|Ỉ|Ĩ)/', 'I', $str);
		$str = preg_replace('/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'O', $str);
		$str = preg_replace('/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'U', $str);
		$str = preg_replace('/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'Y', $str);
		$str = preg_replace('/(Đ)/', 'D', $str);
		$str = preg_replace('/( )/', '-', $str);
		$str = preg_replace('/[^0-9a-zA-Z_\-]/', '', $str);
		return $str;
	}

	function zipFilesAndDownload($id_item,$controller)
	{
		if(is_string($id_item))
			$id_item = base64_decode($id_item);
		if(!is_numeric($id_item) || !is_string($controller))exit;

		//create the object
		$zip = new ZipArchive();

		//create the file and throw the error if unsuccessful
		$archive_file_name = APP.'tmp/archive/'.date('Y_m_d_H_i_s').'_archive.zip';
		if ($zip->open($archive_file_name, ZIPARCHIVE::CREATE )!==TRUE) {
			exit('cannot open <$archive_file_name>\n');
		}

		//$this->loadModel('Uploader');
		$file = $this->Uploader->find('all', array('contain' => false,'fields' => array('name', 'file'),
			'conditions' => array('Uploader.trangthai' => 4, 'Uploader.item_id' => $id_item, 'Uploader.controller' => $controller)));

		$count = count($file);
		if($count < 1)exit;// thoát nếu nhỏ hơn 5 fil0

		//add each files of $file_name array to archive
		foreach($file as $subfile)
		{
			$zip->addFile(DIR_UPLOAD.$subfile['Uploader']['file'],$subfile['Uploader']['name']);
		}
		//$zip->close();
		chmod($archive_file_name, 777);

//		$file = $archive_file_name;
//		$data = file_get_contents($file);
//		$size = filesize($file);
//		if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
//		{
//			header('Content-Type: application/force-download');
//		}else{
//			header('Content-Type: application/get-the-correct-mime-type');
//		}
//		header('Content-Length: '. $size);
//		header('Content-disposition: attachment; filename='' . $count .'_files_qlda_spaceaa_com_'.date('dmYHis').'.zip'');
//		echo $data; exit;

		//then send the headers to foce download the zip file

		// Phân thư mục ra thành từng tháng
		$dest_folder = SYMLINK_DOWNLOAD_FILE.date( 'Ym' ).'/';
		if(!is_dir( $dest_folder ))
		{
			if(!mkdir( $dest_folder ))
			{
				echo 'System thong bao: co loi trong qua trinh download file. Vui long lien he IT de sua ngay cho ban: 0989.289.470. Cam on that nhieu. Error ZIP: mkdir '.date( 'Ym' ).' folder false.'; exit;
			}
			chmod( $dest_folder, 0777 );
		}

		if(!file_exists($dest_folder.basename($archive_file_name)))
		{
			if(!symlink( $archive_file_name, $dest_folder.basename($archive_file_name) ))
			{
				echo 'System thong bao: co loi trong qua trinh download file. Vui long lien he IT de sua ngay cho ban: 0989.289.470. Cam on that nhieu. Error ZIP: symlink false.'; exit;
			}
		}

		$this->redirect(DOWNLOAD_FROM_WEBROOT_SYMLINK.date( 'Ym' ).'/'.basename($archive_file_name));
		exit;

//		header('Content-type: application/zip');
//		header('Content-Disposition: attachment; filename='.$count.'_files_qlda_spaceaa_com.zip');
//		header('Pragma: no-cache');
//		header('Expires: 0');
//		readfile($archive_file_name);
//		exit;
	}

	function history( $username = '' )
	{
		$cond['Uploader.controller <>'] = 'file_ketoans';
		if( $username != '' )
		{
			$cond['Nguoitao.username LIKE'] = '%'.trim($username).'%';
		}
		$option_loai = array('khachhang' => 'Khách Hàng Up','users' => 'Cá Nhân', 'bienbans' => 'Biên Bản', 'bantinhs' => 'Bản Tính',
			'giaodichvts' => 'TMinh - DToán','congvans' => 'Công Văn','khaosats' => 'Khảo Sát', 'phaply' => 'Pháp Lý (HT)',
			'thuviens' => 'Thư Viện (HT)','banvevts' => 'Bản Vẽ',
			'ddabanvevts' => 'Bản Vẽ (GS)', 'congvandens' => 'Công Văn Đến (GS)','congvandivts' => 'Bản Vẽ (GS)',
			'ddabienbans' => 'Biên Bản (GS)', 'ddacongvans' => 'Công Văn (GS)', 'ddaphaplyvts' => 'Pháp Lý (GS)',
			'ddathuviens' => 'Thư Viện (GS)', 'nghiemthuvts' => 'Nghiệm Thu (GS)', 'kiemdinhthinghiems' => 'KĐTNghiệm (GS)',
			'ddahinhcongtrinhs' => 'Hình Chụp GS'
		);
		$this->set('option_loai', $option_loai);

		// TÍNH TOÁN DỮ LIỆU

		$this->paginate = array(
			'fields' => array('controller', 'nguoitao', 'id', 'name', 'created', 'dungluong'),
			'conditions' => $cond,
			'order' => 'Uploader.id desc',
			'limit' => 15,
			'contain' => array('Nguoitao'),
		);
		$this->set('datas', $this->paginate());

		$this->set('loais', $this->_get_loai_model_upload());
	}

	function _get_loai_model_upload()
	{
		return array( 'Phaplyvt' => 'Pháp Lý',
			'Bienban' => 'V.Bản - B.Bản',
			'Giaodichvt' => 'T.Minh - D.Toán',
			'Khaosat' => 'Khảo Sát',
			'Banvevt' => 'Bản Vẽ',
			'Bantinh' => 'Bản Tính',
			'Ddahinhcongtrinh' => 'Hình Chụp GS',
			'users' => 'Upload file cá nhân');
	}

	function upload_file_congtrinh()
	{
		if( !isset($this->data['Congtrinh']) )exit;
		$loai = $this->_get_loai_model_upload();

		$check_save = true;

		if( !is_numeric($this->data['Congtrinh']['id']) )
		{
			$check_save = false;
			$this->Uploader->validationErrors['congtrinh'] = 'Vui lòng chọn công trình';
		}else{
			$save['congtrinh_id'] = $this->data['Congtrinh']['id'];
		}

		if( strlen(trim($this->data['Uploader']['noidung'])) < 1 )
		{
			$check_save = false;
			$this->Uploader->validationErrors['noidung'] = 'Vui lòng ghi nội dung file upload';
		}else{
			$save['tieude'] = $save['mota'] = $this->Common->html($this->data['Uploader']['noidung']);
		}

		if( !isset($loai[$this->data['Uploader']['loai']]) )
		{
			$check_save = false;
			$save['File'] = array();
			$this->Uploader->validationErrors['loai'] = 'Vui lòng chọn loại';
		}else{
			$model = $this->data['Uploader']['loai'];
			$controllers = ($model != 'users')?Inflector::tableize($model):$model;

			$move_return = $this->move_file_to_app_upload($this->data, $controllers );
			$save['File'] = $move_return[0];
		}

		if( !isset($save['File'][0]) )
		{
			$check_save = false;
			$this->set('message_error', 'File bị lỗi hoặc bạn chưa upload file. Vui lòng upload lại.');
		}elseif( $move_return[1] > 0 )
		{
			$check_save = false;
			$this->set('message_error', 'Có file bị lỗi hoặc dung lượng quá nhỏ nên đã bị hệ thống loại bỏ. Vui lòng chọn lại.');
		}

		if( $model == 'users' )// upload file cá nhân
		{
			if( !isset($save['File'][0]) )
			{
				$this->set('message_error', 'File bị lỗi hoặc bạn chưa upload file. Vui lòng upload lại.');
			}else{
				$this->Uploader->create();
				if( $this->Uploader->saveAll($save['File']) )
				{
					$this->redirect('/uploaders/history/');
				}else{
					$this->set('message_error', 'Lưu thất bại.');
				}
			}
		}else{ // Lưu file bình thường
			if($check_save){

				$this->loadModel($model);
				if($this->$model->saveAll($save))
				{
					$this->Uploader->updateCounter('Congtrinh');
					$this->redirect('/uploaders/history/');
				}else{
					$this->set( 'get_file_tmp', $this->move_file_return_to_tmp($save['File']) );
					$this->set('message_error', 'Lưu thất bại.');
				}

			}else{
				$this->set( 'get_file_tmp', $this->move_file_return_to_tmp($save['File']) );
				$this->set('message_error', 'Vui lòng điền đầy đủ thông tin để upload.');
			}
		}

		$this->set('loais', $loai );
	}

	function open_my_file ()
	{
		// ======================== UPLOAD FILE ======================
		$loai = $this->_get_loai_model_upload();
		$this->set('loais', $loai );

		if(!empty($this->data)) {

			$model = $this->data['Uploader']['loai'];
			$controllers = ($model != 'users')?Inflector::tableize($model):$model;


			$save = array();

			$check_save = true;

			if( !is_numeric($this->data['Congtrinh']['id']) )
			{
				$check_save = false;
				$this->Uploader->validationErrors['congtrinh'] = 'Vui lòng chọn công trình';
			}else{
				$save['congtrinh_id'] = $this->data['Congtrinh']['id'];
			}

			if( strlen(trim($this->data['Uploader']['noidung'])) < 1 )
			{
				$check_save = false;
				$this->Uploader->validationErrors['noidung'] = 'Vui lòng ghi nội dung file upload';
			}else{
				$save['tieude'] = $save['mota'] = $this->Common->html($this->data['Uploader']['noidung']);
			}

			if( !isset($loai[$this->data['Uploader']['loai']]) )
			{
				$check_save = false;
				$save['File'] = array();
				$this->Uploader->validationErrors['loai'] = 'Vui lòng chọn loại';
			}else{
				$model = $this->data['Uploader']['loai'];
				$controllers = ($model != 'users')?Inflector::tableize($model):$model;
			}

			if( $model == 'users' )// upload file cá nhân
			{
				$save['File'] = $this->move_file_to_app_upload($this->data, $controllers );
				if( !isset($save['File'][0]) )
				{
					$this->set('message_error', 'File bị lỗi hoặc bạn chưa upload file. Vui lòng upload lại.');
				}else{
					$this->Uploader->create();
					if( $this->Uploader->saveAll($save['File']) )
					{
						//$this->redirect('/uploaders/history/');
						$this->set('message_success', 'Upload file thành công.');
					}else{
						$this->set('message_error', 'Lưu thất bại.');
					}
				}
			}else{ // Lưu file bình thường
				if($check_save){

					$save['File'] = $this->move_file_to_app_upload($this->data, $controllers );

					$this->loadModel($model);
					if($this->{$model}->saveAll($save))
					{
						//http://192.168.1.44/congtrinhs/view/246/tasks/#scrollsubcontent
						$this->set('message_success',
							'Upload file vào '.$loai[$model].' thành công. Xem <a target="_blank" href="/congtrinhs/view/'.
							$save['congtrinh_id'].'/'.$controllers.'/#scrollsubcontent">chi tiết</a> công trình.');

						//$this->redirect('/uploaders/history/');
					}else{
						$this->set('message_error', 'Lưu thất bại. Vui lòng upload lại.');
					}

				}else{
					$this->set('message_error', 'Vui lòng điền đầy đủ thông tin để upload.');
				}
			}
		}

		// =============================== GET FILE ======================
		$this->paginate = array(
			'fields' => array('id', 'name', 'file', 'dungluong', 'created'),
			'contain' => false,
			'conditions' => array(
				'Uploader.trangthai' => 4,
				'Uploader.nguoitao' => $this->Auth->user('id')
			),
			'contain' => false,
			'order' => 'Uploader.id desc',
			'limit' => 10
		);
		$this->set('datas', $this->paginate() );

		$this->layout = 'ajax';
	}

	function attach_file ($congtrinh_id = '')
	{
		$this->set('congtrinh_id', $congtrinh_id );

		if( !is_numeric($congtrinh_id) )
		{
			$congtrinh_id = 246;
		}

		$this->loadModel('Congtrinh');
		$tmp = $this->Congtrinh->find('first', array(
			'conditions' => array('Congtrinh.id' => $congtrinh_id),
			'fields' => array('id', 'tieude'),
			'contain' => false
		));
		$this->set('congtrinh', $tmp );
		$this->set('congtrinh_id', $congtrinh_id );

		if(!empty($this->data))
		{
			$loai = $this->data['Uploader']['model_loai'];

			if( $loai == 'User' )
			{
				$save = $this->move_file_to_app_upload($this->data, 'users' );

				if( isset($save[0]['name']) ){
					$this->Uploader->create();
					if( $this->Uploader->saveAll($save) )
					{
						$this->Session->setFlash('Lưu thành công.', 'default', array('class' => 'message_success'));
					}else{
						$this->Session->setFlash('Uploader: Hệ thống lưu bị trục trặc. Vui lòng thử lại.', 'default', array('class' => 'message_error'));
					}
				}
			}else{

				$save = array();
				$save['CongtrinhsFile'] = $this->Common->html($this->data['Uploader']);
				$save['CongtrinhsFile']['mota'] = $save['CongtrinhsFile']['noidung'];
				$save['CongtrinhsFile']['loai'] = $loai;
				$save['File'] = $this->move_file_to_app_upload($this->data, 'congtrinhs_files' );

				$this->loadModel('CongtrinhsFile');
				if( isset($save['File'][0]) )
				{
					$this->CongtrinhsFile->create();
					if( $this->CongtrinhsFile->saveAll($save) )
					{
						$this->Uploader->updateCounter();
						$this->Session->setFlash('Lưu thành công.', 'default', array('class' => 'message_success'));
					}else{
						$this->Session->setFlash('Hệ thống lưu bị trục trặc. Vui lòng thử lại.', 'default', array('class' => 'message_error'));
					}
				}
			}
		}

		$this->set('option_loais', array(
			'khachhang' => 'Khách Hàng Up', 'users' => 'Cá Nhân', 'bienban' => 'Biên Bản', 'bantinh' => 'Bản Tính',
			'giaodich' => 'TMinh - DToán','khaosat' => 'KS Địa Hình','khaosatdc' => 'KS Địa Chất', 'phaply' => 'Pháp Lý (HT)',
			'thuvien' => 'Thư Viện (HT)','banve' => 'Bản Vẽ',
			'congvanden' => 'Công Văn Đến','congvandi' => 'Công Văn đi',
			'kcs' => 'KCS','buttichcdt' => 'Bút tích CĐT','hopdong' => 'Hợp Đồng',
			 'nghiemthu' => 'Nghiệm Thu', 'kiemdinhthinghiem' => 'KĐTNghiệm',
			'ddahinhcongtrinhs' => 'Hình Chụp GS', 'nhatkycongtruong' => 'Nhật ký công trường'
		) );


		$this->paginate = array(
			'fields' => array('id', 'name', 'file', 'dungluong', 'created', 'controller'),
			'contain' => false,
			'conditions' => array(
				'Uploader.trangthai' => 4,
				'Uploader.nguoitao' => $this->Auth->user('id')
			),
			'contain' => false,
			'order' => 'Uploader.id desc',
			'limit' => 5
		);
		$this->set('datas', $this->paginate() );
	}

	function remove_file_upload_ajax( )
	{
		if( !$this->request->is('ajax') || !isset($_POST['file']))exit;

		$dir = APP.'tmp/upload/'.AuthComponent::user('id');
		if( is_dir($dir) )
		{
			$command = 'cd '.$dir.'; rm -f "'.trim($_POST['file']).'"';
			exec($command);
			echo 'ok';
		}
		exit;
	}

	function chon_file_xuat( $model, $id, $congtrinh_id, $file_xuat = 1 )
	{
		if( !$this->request->is('ajax') || !is_numeric($id))exit;

		$save['id'] = $id;
		$save['file_xuat'] = $file_xuat;
		$save['congtrinh_id'] = $congtrinh_id;

		if( $this->Uploader->save($save, false) )
		{
			$this->Uploader->updateCounter($model);
			echo 'ok';
		}

		exit;
	}

	function chon_download_outside( $id )
	{
		if( !$this->request->is('ajax') || !is_numeric($id))exit;

		$save['id'] = $id;
		$save['download_tu_do'] = 1;
		$save['download_tu_do_time'] = date('Y-m-d', strtotime('now') + DAY*10);

		if( $this->Uploader->save($save, false) )
		{
			echo 'ok';
		}

		exit;
	}

	function ngay_xuat( $id )
	{
		if( !$this->request->is('ajax') || !is_numeric($id) )exit;

		$this->set('id', $id);

		if( !empty($this->data) )
		{
			if( strlen($this->data['Uploader']['ngay_xuat_tmp']) > 0)
			{
				$ngay_xuat = $this->Common->string2date($this->data['Uploader']['ngay_xuat_tmp']);

				$this->Uploader->id = $id;
				if( $this->Uploader->saveField('ngay_xuat', $ngay_xuat) )
				{
					$this->set('ngay_xuat', $this->data['Uploader']['ngay_xuat_tmp']);

					$this->render('ngay_xuat_success');
				}
			}
		}
	}

	function view($id = null){
		if (!is_numeric($id)) {
			echo 'Link file khong ton tai, vui long dien them ID file'; exit;
		}

		$this->loadModel('Uploader');
		$file = $this->Uploader->find('first', array(
			'conditions' => array('Uploader.id' => $id),
			'contain'    => false
		));

		if (!isset($file['Uploader'])) {
			echo 'FILE DA BI MAT, vui long lien he nguoi upload file nay hoac lien he IT - diult: 0989.289.470 . Cam on rat nhieu.'; exit;
		}

		// check file exist or not
		$file = $file['Uploader'];
		$file_path = DIR_UPLOAD.$file['file'];
		if (!file_exists($file_path)) {
			echo 'FILE DA BI MAT, vui long lien he nguoi upload file nay hoac lien he IT - diult: 0989.289.470 . Cam on rat nhieu.'; exit;
		}
		$this->set('file', $file);

		// check file_temp exists or not, if not, we upload file to cloud and create file_temp
		$file_temp = 'upload/tmp/view_office_online/temp_view_'.$file['id'].'.php';
		if (!file_exists(WWW_ROOT.$file_temp)){

			// check if file is not uploaded to cloud, we will upload it to onedrive
			if (!$file['cloud_id'] || strlen($file['cloud_id']) < 3) {

				// load auth app info
				$this->loadModel('FilesOnedriverAuth');
				$onedrive_auth = $this->FilesOnedriverAuth->find('first', array(
					'conditions' => array('FilesOnedriverAuth.id' => 1),
					'contain'    => false
				));
				$onedrive_auth = $onedrive_auth['FilesOnedriverAuth'];

				// Define security credentials for your app.
				define("client_id", $onedrive_auth['client_id']);
				define("client_secret", $onedrive_auth['client_secret']);
				define("callback_uri", $onedrive_auth['callback_uri']);
				define("onedrive_base_url", $onedrive_auth['onedrive_base_url']);
				require_once('UploadersSkydrive.php');

				// get token for app access into account live.com
				$this->loadModel('FilesOnedriverToken');
				$access_token = $this->FilesOnedriverToken->find('first', array(
					'conditions' => array('FilesOnedriverToken.id' => 1),
					'contain'    => false
				));
				$access_token = $access_token['FilesOnedriverToken'];

				// check token is expired or not
				if (time() > (int)$access_token['expires_in']) { // Token needs refreshing. Refresh it and then return the new one.
					$refreshed = skydrive_auth::refresh_oauth_token($access_token['refresh_token']);
					$refreshed['id']         = $access_token['id'];
					$refreshed['expires_in'] = time() + (int)$refreshed['expires_in'];

					// update old token into new token
					if ($this->FilesOnedriverToken->save($refreshed))
						$token = $refreshed['access_token'];
					else{
						echo '$this->FilesOnedriverToken->save failed, please copy this and send to IT. Thanks'; die;
					}
				} else
					$token = $access_token['access_token']; // Token currently valid. Return it.

				// use this token to upload file to onedrive
				try {
					$sd = new skydrive($token);
					$response = $sd->put_file($onedrive_auth['folder_id'], $file_path, $token);

					$save                 = [];
					$save['id']           = $id;
					$save['cloud_id']     = $file['cloud_id'] = $response['id'];
					$save['cloud_source'] = $response['source'];
					$save['cloud_time']   = strtotime('now');
					if (!$this->Uploader->save($save)) {
						echo 'Error: if (!$this->Uploader->save($save)) { cloud_id cloud_source';
						die;
					}
				} catch (Exception $e) {
					// An error occured, print HTTP status code and description.
					echo "Error: ".$e->getMessage();
					exit;
				}
			}

			// then we view it on browser
			$app = array(
				'xls'  => 'Excel',
				'xlsx' => 'Excel',
				'doc'  => 'Word',
				'docx' => 'Word',
				'pptx' => 'PowerPoint',
				'ppt'  => 'PowerPoint',
				'pdf'  => 'Word'
			);
			$ext = strtolower(substr(strrchr(basename($file_path),'.'),1));

			$arr_file = explode('.', $file['cloud_id']);

			$data = '<?php
			session_start();
			ini_set("max_input_time", 9000);
			ini_set("memory_limit", "-1");
			ini_set("max_execution_time", 900);
			ini_set("display_errors", 0);
			$domain = file_get_contents("https://onedrive.live.com/view.aspx?cid='.$arr_file[1].'&resid='.urlencode($arr_file[2]).'&app='.$app[$ext].'&authkey=!AHO4JCN6jFLOprY&wdo=1");
			echo $domain;
			?>';
			file_put_contents(WWW_ROOT.$file_temp, $data);
		}

		$this->set('file_temp', $file_temp);
		$this->layout = 'onedrive';
	}

	public function view_download($id, $type = 'new'){
		if (!is_numeric($id)) {
			echo 'Link file khong ton tai, vui long dien them ID file'; exit;
		}

		$this->loadModel('Uploader');
		$file = $this->Uploader->find('first', array(
			'conditions' => array('Uploader.id' => $id),
			'contain'    => false
		));

		if (!isset($file['Uploader'])) {
			echo 'FILE DA BI MAT, vui long lien he nguoi upload file nay hoac lien he IT - diult: 0989.289.470 . Cam on rat nhieu.'; exit;
		}

		// check file exist or not
		$file = $file['Uploader'];
		$file_path = DIR_UPLOAD.$file['file'];
		if (!file_exists($file_path)) {
			echo 'FILE DA BI MAT, vui long lien he nguoi upload file nay hoac lien he IT - diult: 0989.289.470 . Cam on rat nhieu.'; exit;
		}

		$save['id']               = $file['id'];
		$save['cloud_time']       = strtotime('now');
		$save['cloud_type']       = $type;
		$save['cloud_up_user_id'] = $this->Auth->user('id');

		$this->set('type', $type);

		$this->set('result', 0);
		if ($this->Uploader->save($save))
			$this->set('result', 1);
	}
}