<?php
class AppController extends Controller{

	var $helpers = array( 'Html', 'Form', 'Js', 'Session', 'Thumbnail', 'Paginator', 'Number' );

	var $components = array( 'Auth', 'Session', 'Common', 'RequestHandler', 'Cookie');// , 'Search.Prg', //'Security',

	function isAuthorized($user)
	{
		return true;
	}

	function _paginatorURL() {

		$search_URL = array();
		if( is_array($this->request->query) )
			$search_URL = array('', '?' => http_build_query($this->request->query));
		$this->set('search_URL', $search_URL );
	}

	function beforeFilter()
	{
		// Tro ve trang chu khi nguoi dung save ma bi mat session
		// VI SMILE CHO PHEP KHACH HANG KHONG CAN DANG NHAP LEN LOAI TRU TH SMILE
		if( isset($this->request['params']['controller']) && substr($this->request['params']['controller'], 0, 5) != 'smile' )
		{
			if( $this->request->is('ajax') && is_numeric( $this->Auth->user('id') ) )
			{
				echo '<script type="text/javascript">location.href = "/";</script>';exit;
			}
		}
		// end

		// DICH CAKEPHP RA TIENG VIET
		Configure::write('Config.language', 'vi' );

		$this->Auth->authenticate = array(
				'Form' => array(
					'fields' => array('username' => 'username', 'password' => 'password'),
					'userModel' => 'User',
					'scope' => array('User.trangthai' => 4))
		);

		$this->Auth->authorize = 'Controller';
		$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'admin' => true);
		$this->Auth->loginRedirect = '/admin';
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login', 'admin' => true);
		$this->Auth->authError = '';
		$this->Auth->loginError = '<center>Đăng nhập sai tên hoặc mật khẩu.</center>';
		$this->Auth->autoRedirect = false;
//		$this->Auth->autoRedirect = true;

		$this->_paginatorURL();

		if(is_numeric($this->Auth->user('id')))
		{
			if($this->Auth->user( 'group_id' ) == 1 || $this->Auth->user( 'id' ) == 4 || $this->Auth->user( 'id' ) == 351)
				$this->set( 'admin', 1 );
			if(in_array($this->Auth->user('id'),array(4,351)) && Configure::read('debug') == 0){ Configure::write('debug',1);}
		}

		// Để ở beforeFilter vì sẽ rewrite ở trong từng controller riêng dc
		$this->set('model', $this->modelClass);
		$this->set('controller', Inflector::tableize($this->modelClass));

		$this->_get_tongnhanvien_cacphongban();

	}

	// Save Session toan bo nhan vien de khong phai search ra nguoi tao nguoi sua nua
	function _get_tongnhanvien_cacphongban()
	{
		//diult khoa lai
		if( !$this->Session->check('all_user_qlda') ){

			$this->loadModel('User');
			$user_tmp = $this->User->find('all', array(
				'fields' => array('id', 'username', 'avatar', 'skype', 'realname', 'tel', 'line_dienthoai', 'trangthai', 'codetbl_id'),
				// 'conditions' => array(
				// 	'User.trangthai' => 4
				// ),
				'contain' => false,
			));

			$all_user_qlda = $tmp = array();
			foreach( $user_tmp as $value )
			{
				$all_user_qlda[$value['User']['id']] = $value['User'];

				if( $value['User']['trangthai'] != 4 )continue;

				if( !isset($tmp[$value['User']['codetbl_id']]) )$tmp[$value['User']['codetbl_id']] = 0;
				$tmp[$value['User']['codetbl_id']] += 1;
			}

			$this->Session->write('all_user_qlda', $all_user_qlda);

			$this->Session->write('tongnhanvien_phongban', $tmp); // dung trong whoisonline.ctp Elements

			$this->loadModel('Codetbl');
			$data_phongban = $this->Codetbl->find('list', array(
				'conditions' => array(
					'Codetbl.code1' => 'usr',
					'Codetbl.code2' => 'grp',
					'Codetbl.trangthai' => 4
				),
				'fields' => array('id', 'name'),
				'contain' => false
			));
			$this->Session->write( 'phongban_ALL', $data_phongban);

		// diult khoa lai
		}else{
			$data_phongban = $this->Session->read('phongban_ALL');
			$all_user_qlda = $this->Session->read('all_user_qlda');
		}

		$this->set( 'phongban', $data_phongban );
		$this->set( 'all_user_qlda', $all_user_qlda );
	}

	function beforeRender( )
	{
		if($this->request->is('ajax')) {
			$this->layout = 'ajax';
		}
	}

	function move_file_to_app_upload( $data_file, $controller = '' )
	{
		if(!isset($data_file['File']))return array( array(), 0 );

		if( $controller == '' )$controller = Inflector::tableize($this->modelClass);

		APP::import('Component', 'Cmfile');
		$Cmfile = new CmfileComponent(new ComponentCollection());

		$tmp_all = array();
		$tmp = array('controller' => $controller );

		// CHECK IMAGE EXT
		$image_extensions_allowed = array('jpg', 'jpeg', 'png', 'gif', 'bmp');

		if (isset($data_file['File'])) {
			foreach( $data_file['File'] as $value )
			{
				if(!isset($value['error']) || $value['error'] != 0 )continue;
				$tmp['dungluong'] = $value['size'];

				$ext = split( '\.', basename($value['name']) );
				$file = $this->Auth->user('username').'_'.date('Y_m_d_H_i_s').
							'_rand_'.rand( 1, 9999 ).'.'.$ext[count($ext)-1];

				// CHECK IMAGE
				if( $controller == 'ddahinhcongtrinhs' )
				{
					if( !in_array( strtolower($ext[count($ext)-1]), $image_extensions_allowed ) )
					{
						continue;
					}
				}

				$tmp['file'] = $Cmfile->move_to( $value['tmp_name'], APP.'upload/'.$controller.'/', $file );

				if( !$tmp['file'] )continue;
				$tmp['name'] = $value['name'];

				$tmp_all[] = $tmp;
			}
		}
		return $tmp_all;
	}
}
