<?php
class CongtrinhsController extends AppController
{
    var $name = 'Congtrinhs';

    var $components = array( 'Session', 'Common', 'Auth', 'Search.Prg', 'Cmfile' ); //, 'Ofc' );

    function beforeFilter()
    {
        parent::beforeFilter();
        $check_permission = false;
        if(in_array($this->Auth->user('id'), array( 4, 138,351)))
        {
            $check_permission = true;
        }
        $this->set('check_permission', $check_permission);

        // alow initDB
        //$this->Auth->allow('*');
    }

    public $presetVars = array(
        array('field' => 'macongtrinh', 'type' => 'like'),
        array('field' => 'tieude', 'type' => 'value'),
        array('field' => 'type', 'type' => 'value'),
        array('field' => 'tinhthanh', 'type' => 'value'),
        array('field' => 'vitri', 'type' => 'value'),
        array('field' => 'name', 'type' => 'like'),
        array('field' => 'congtythuchien_id', 'type' => 'value'),
        array('field' => 'nhom', 'type' => 'value'),
        array('field' => 'hangmuc', 'type' => 'value'),
        array('field' => 'tilephantramxong', 'type' => 'value'),
        array('field' => 'co_hd', 'type' => 'value'),
        array('field' => 'ngaytao', 'type' => 'query')
    );

    function admin_index( $type_parent = null )
    {
        $this->Prg->commonProcess('Congtrinh');
        $cond = $this->Congtrinh->parseCriteria($this->passedArgs);
        $tmp['Congtrinh'] = $this->request->params['named'];
        $this->request->data = $tmp;

        $cond['Congtrinh.trangthai'] = 4;
        $this->paginate = array(
            'fields' => array( 'Congtrinh.id', 'Congtrinh.macongtrinh', 'Congtrinh.tieude', 'Congtrinh.mota', 'Congtrinh.loai', 'Congtrinh.modified'),
            'conditions' => $cond,
            'order' => 'Congtrinh.modified desc',
            'limit' => 10,
            'contain' => false
        );
        $this->set('datas',$this->paginate());
    }

    function admin_lists($loai = 1)//loại 1: Project thường; loại 2: Pursued Project (đang theo đuổi)
    {
        $this->paginate = array(
            'fields' => array('id', 'macongtrinh', 'tieude', 'mota', 'created', 'nguoitao', 'nguoisua'),
            'conditions' => array('Congtrinh.trangthai' => 4, 'Congtrinh.loai' => $loai),
            'order' => 'Congtrinh.modified desc',
            'limit' => 16,
            'contain' => array(
                'Nguoitao',
                'Comment' => array('id', 'created', 'noidung', 'noidung_attach', 'nguoitao'),
        ));
        $this->set('datas', $this->paginate());
        $this->set('link_them_loinhan', 'New');
        $this->set('div_update', 'tasks');
        //160617 Doan them
        $this->set('loai', $loai);
    }

    function admin_them(){
        $save = $this->data['Congtrinh'];
        $save['trangthai']  = 4;
        $save['nguoitao'] = $save['nguoisua'] = $this->Auth->user('id');
        if (is_numeric($save['id']) && $save['id'] >= 1) {
            # code...
        }else{
            unset($save['id']);
            $this->Congtrinh->create();
        }
        if ($this->Congtrinh->save($save, false)) {
            echo $this->requestAction('/admin/congtrinhs/lists/'.$save['loai'], ['return']);
        }
        die;
    }

    //160615 Doan them
    public function admin_view($id = null) {
        if (!$this->Congtrinh->exists($id)) {
            throw new NotFoundException(__('Invalid congtrinht'));
        }
        $options = array('conditions' => array('Congtrinh.' . $this->Congtrinh->primaryKey => $id));
        $this->set('congtrinh', $this->Congtrinh->find('first', $options));
    }
}