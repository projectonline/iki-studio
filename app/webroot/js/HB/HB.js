function HB_setting(congty_id)
	{
		// get effect type from
		selectedEffect = "slide";

		// most effect types need no options passed by default
		options = {};
		// some effects have required parameters
		if ( selectedEffect === "scale" ) {
			options = { percent: 0 };
		} else if ( selectedEffect === "size" ) {
			options = { to: { width: 950, height: 0 } };
		}

		// run the effect
		//$( ".background_smiles1" ).hide( selectedEffect, options, 100, callback );

		// some effects have required parameters
		if (selectedEffect === "scale") {
			options = {percent: 200};
		} else if (selectedEffect === "size") {
			options = { to: { width: 950, height: 0 } };
		}
		$(".HB","#"+congty_id).show("slide", options,800, callback );
	}

	function callback()
	{
		$(".warning").fadeIn("fast");
	};

	function HB_sua_save(id,congty_id)
	{
		var $data = $("#HB_Namtaikhoavt").serialize();
		$("#loading").show();
		$.ajax({
			url: "/namtaikhoavts/Update/"+id,
			type: "POST",
			data: $data,
			timeout: 120000,
			error: error_handler,
			success: function(html){
				$("#loading").hide();
				if(html.substr(1,6) == "legend")
				{
					$("#"+congty_id,"#HB_window").html( html );
					$(".HB").show();
					$(".warning").show();
					$("#HB_window_content").dialog("close");
				}
				else
					$("#HB_window_content").html(html);
			}
		});
	}
	
	// Kiemtra_namtaikhoa_id

	function HB_kiemtra_namtaikhoa_id_comments(nam, kiemtra_namtaikhoa_id)
	{
		if( kiemtra_namtaikhoa_id != $("#NamtaikhoavtKiemtraNamtaikhoaId").val() )
			HB_comments_callback(nam, kiemtra_namtaikhoa_id);

	}

	function HB_comments_callback(nam, kiemtra_namtaikhoa_id)
	{
		//$("#loading").show();
		$.ajax({
				url: "/namtaikhoavts/get_comments/"+nam+"/"+kiemtra_namtaikhoa_id,
				type: "POST",
				timeout: 120000,
				error: error_handler,
				success: function(html){
					//$("#loading").hide();
					$("#HB_comment_container").html(html);
				}
			});
	}

	function HB_them_save(congty_id,nam)
	{
		var $data = $("#HB_Namtaikhoavt").serialize();
		$("#loading").show();
		$.ajax({
			url: "/namtaikhoavts/Add/"+congty_id+"/"+nam,
			type: "POST",
			data: $data,
			timeout: 120000,
			error: error_handler,
			success: function(html){
				$("#loading").hide();
				if(html.substr(1,6) == "legend")
				{
					$("#"+congty_id,"#HB_window").html( html );
					$(".HB").show();
					$(".warning").show();
					$("#HB_window_content").dialog("close");
				}
				else
				{
					$("#HB_window_content").html(html);
				}
			}
		});
	}
	
	function HB_handler()
	{
		$('#HB_window_content' ).prev('.ui-widget-header').css({
			'background-image':'url(/img/HB/banner_HB_large.png)',
			'background-repeat':'no-repeat',
			'background-color':'#3B5998',
			'background-position':'left'});
	}