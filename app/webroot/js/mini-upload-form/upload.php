<?php

// A list of permitted file extensions
$allowed = array('png', 'jpg', 'gif','zip');

if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

	if(!in_array(strtolower($extension), $allowed)){
		echo '{"status":"error extension"}';
		exit;
	}

	if(move_uploaded_file($_FILES['upl']['tmp_name'], '/var/www/namnb/app/webroot/mini-upload-form/uploads/'.$_FILES['upl']['name'])){
		echo '{"status":"success"}';
		exit;
	}else{
		echo '{"status":"error move_uploaded_file fail"}';
		exit;
	}
}

echo '{"status":"error upload"}';
exit;