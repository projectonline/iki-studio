function them( loai, loaiparent, id_loai, parent )
{
	var diachi = $('#diachifolder', '#folder').val().replace(/\&/g, '~');
	if( loai == 0 ){
		$url = '/folders/themfolder/'+loaiparent+'/'+id_loai+'/'+parent;
	}else{
		$url = '/folders/themfile/'+loaiparent+'/'+id_loai+'/'+parent+ '/'+ check_click; // namnb them check check_click
	}
	$("#loading").show();
	$.ajax({
		url:$url,
		async:true,
		type:'post',
		data: { diachi: diachi },
		complete:function(request, json) {
			$('#window_content').html(request.responseText);
			window_show();
			$("#loading").hide();
		}
	});
}

function vaofolder( loaiparent, id_loai, parent, div, click_back )
{
	var $data = $('#DS_Folder_File').serialize() + "&data%5Bdiachi%5D=" + $('#diachifolder', '#'+div).val().replace(/\&/g, '~');
	$url = '/folders/danhsachfolder/' + loaiparent + '/' + id_loai + '/' + parent + '/' + div;
	if( div == 'folderfilesearch' ){
		$tring = $.trim($('#searchfolder', '#'+div).val());
		$data = $data + "&data%5Btim%5D=" + $tring;
	}
	if( click_back == "click_back" || click_back == "back" ){
		$data = $data + "&data%5Bback%5D=1";
		if( div == 'folderfilesearch' ){
			$duongdan = $('#diachifolder', '#'+div).val();
			if( $duongdan.split('/').length <= 3 ){
				$url = '/folders/search/' + loaiparent + '/' + id_loai + '/' + div + '/' + $tring;
				div = 'window_content';
			}
		}
	}

	$("#loading").show();
	$.ajax({
		url: $url,
		async:true,
		type:'post',
		data: $data,
		complete:function(request, json) {
			$('#'+div).html(request.responseText);
			$("#loading").hide();

			//$.History.go("/" + check_click);
			if( div == "folder" ){
				if( click_back == "click_back" )
				{
					check_click--;
				}else{
					check_click++;
				}
				location.hash = '' + check_click;
			}

		}
	});
}

var check_click = 0;

$(function(){

	$.History.bind(function(state){

		//if( check_click )
		//{
		//	console.log('click back .....' + check_click);
			//$("#backButton").click();
		//}

		var string_hash = location.hash;

		console.log(check_click);

		if( (string_hash.substr(1,1) != check_click) && string_hash.substr(1,1) != "-" && check_click != 0)
		{
			$("#backButton").click();
		}

	});
  // Bind the event.
  /*$(window).hashchange( function(){
	// Alerts every time the hash changes!

		console.log("ok2");
		//$("#backButton").click();
		//location.hash = 'foo' + Math.floor((Math.random()*1000)+1);
		console.log("ok");
  })

  // Trigger the event (useful on page load).
  $(window).hashchange();*/

	$('#searchThongTin').hide();

});


function xoa( id, loai, loaiparent, id_loai )
{
	if( !confirm("Bạn có chắc chắn muốn xóa không?") )return false;

	if( loai == 0 ){
		$url = '/folders/xoafolder/'+id+'/'+loaiparent+'/'+id_loai;
	}else{
		$url = '/folders/xoafile/'+id;
	}
	$('#loading').show();
	$.ajax({
		url: $url,
		timeout: 120000,
		error: error_handler,
		success: function(html){
			if( html == "ok" )
			{
				if( loai == 0 ){
					$("#folder_"+id ).fadeOut();
				}else{
					$("#file_"+id ).fadeOut();
				}
			}else{
				alert("Có lỗi trong quá trình thực hiện. Vui lòng nhấn F5 thao tác lại.");
			}
			$('#loading').hide();
		}
	});
}

function doiten( id, loai )
{
	$("#loading").show();
	$.ajax({
		url:'/folders/doiten/'+id+'/'+loai,
		async:true,
		type:'post',
		complete:function(request, json) {
			$('#window_content').html(request.responseText);
			window_show();
			$("#loading").hide();
		}
	});
}

function downloadfile( id )
{
	document.location.href = '/uploaders/download/'+id;
}

function laycopy( id, ten, loai, div )
{
	$('.Title_ThongBaoCaNhan').show();

	if( loai == 0 ){
		ten = 'Folder: '+ten;
		chon = 'Folder_Folder_File_'+id;
		name_id = 'data[id][Folder][]';
		name_ten = 'data[ten][Folder][]';
		$style = $("#folderhinhten_"+id, '#'+div);
		$cut = $("#foldercut_"+id, '#'+div);
	}else{
		ten = 'File: '+ten;
		chon = 'File_Folder_File_'+id;
		name_id = 'data[id][Uploader][]';
		name_ten = 'data[ten][Uploader][]';
		$style = $("#filehinhten_"+id, '#'+div);
		$cut = $("#filecut_"+id, '#'+div);
	}

	$('#Folder_File_DiaChi').html('<input type="hidden" name="data[diachicu]" value="'+$("#diachifolder", "#folder").val().replace(/\&/g, "~")+'" />');

	$('#Folder_File').append(
		'<span id="'+chon+'">' + ten +
		'<img style="cursor:pointer" src="/img/icon/toggle_delete.png" onclick="xoa_chonlay('+ id +','+ loai +');" title="Bỏ" />' + ' ; ' +
		'<input type="hidden" name="'+name_id+'" value="'+id+'" />' +
		'<input type="hidden" name="'+name_ten+'" value="'+ten+'" />' +
		'<br></span>'
	);

	$style.attr("style", "border-style:solid;border-width:1px;border-color:blue;opacity:0.5;");
	$style.attr("class", "folderfilehinhtencut");
	$cut.hide();
}

function xoa_chonlay( id, loai )
{
	if( loai == 0 ){
		$("#Folder_Folder_File_"+id).remove();
		$style = $("#folderhinhten_"+id);
		$cut = $("#foldercut_"+id);
	}else{
		$("#File_Folder_File_"+id).remove();
		$style = $("#filehinhten_"+id);
		$cut = $("#filecut_"+id);
	}
	$style.attr("style", "border-style:solid;border-width:1px;border-color:#CDCDCD;");
	$style.attr("class", "folderfilehinhten");
	$cut.show();
	return false;
}

function xoahet(  )
{
	$('#Folder_File').html('');
	$('.Title_ThongBaoCaNhan').hide();
	$('.folderfilehinhtencut').attr("class","folderfilehinhten");
	$(".folderfilehinhten").attr("style", "border-style:solid;border-width:1px;border-color:#CDCDCD;");
	$('.folderfilecut').show();
	$('#paste').hide();
	return false;
}

function movefolderfile( loaiparent, id_loai, parent, loai )
{
	var $data = $('#DS_Folder_File').serialize() + "&data%5Bdiachi%5D=" + $('#diachifolder', '#folder').val().replace(/\&/g, '~');

	$('#loading').show();
	$.ajax({
		url: '/folders/movefolderfile/'+loaiparent+'/'+id_loai+'/'+parent+'/'+loai,
		type: 'POST',
		data: $data,
		timeout: 120000,
		error: error_handler,
		success: function(html){
			if( loai == 0 ){//paste
				$('#folder').html(html);
			}
			$("#loading").hide();
			xoahet();
		}
	});
}

function chonfolderfile( id, loai, div )
{
	if( loai == 0 ){
		$style = $("#folderhinhten_"+id, '#'+div);
		$menu = $("#menufolder_"+id, '#'+div);
	}else{
		$style = $("#filehinhten_"+id, '#'+div);
		$menu = $("#menufile_"+id, '#'+div);
	}
	$(".folderfilehinhten", '#'+div).attr("style", "border-style:solid;border-width:1px;border-color:#CDCDCD;");
	$style.attr("style", "border-style:solid;border-width:1px;border-color:blue;");

	$(".menufolderfile", '#'+div).attr("style", "display:none;");
	$(".menufolderfile", '#'+div).attr("class", "menufolderfile");
	$menu.attr("style", "top:-70px;left:2px;");
	$menu.attr("class", "menufolderfile AvatarView linkHover");
}

function chitietfile( id, win )
{
	$("#loading").show();
	$.ajax({
		url: '/folders/chitietfile/'+id,
		async:true,
		type:'post',
		complete:function(request, json) {
			if( win == 1 ){
				$('#window_content').html(request.responseText);
				window_show();
			}else{
				$('#window_content_2').html(request.responseText);
				window_show_2();
			}
			$("#loading").hide();
		}
	});
}