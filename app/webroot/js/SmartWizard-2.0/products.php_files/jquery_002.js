/*
 * smartLike plugin v0.91
 *
 * jQuery Like Feeder Plugin
 * 
 * Dips  
 * 
 * http://tech-laboratory.blogspot.com
 * http://techlaboratory.wordpress.com
 */
 
(function($){
    $.fn.smartLike = function(options) {
        var options = $.extend({}, $.fn.smartLike.defaults, options);

        return this.each(function() {
                var obj = $(this);
                var slFeedAnchor = $("a", obj);
                var slFeedResult = $("div", obj);
                var processingReq = false;
                var projId = slFeedAnchor.attr('id');
      		      
                $(slFeedAnchor).bind("click", function(e){
                    if(processingReq){
                      // Processing previous click, let it finish it first
                      // to avoid processing multiple and continuous clicks
                      return false;
                    }else{
                      processingReq = true;
                      // If result is higher length width increment by 20px
                      $.ajax({
                            url: "services/serv_likeIt.php",
                            type: "POST",
                            data: ({ser_action : 2,project_id:projId}),
                            dataType: "text",
                            success: function(msg){
                              if(msg!='FAILURE'){
                                showResult(msg);
                              }
                            }
                         }
                      );
                     
                      return false;                      
                    }
                });
                
                function showResult(res){
                  var likedText ="Liked";
                  var likCntrl = $("<span></span>").html(likedText).hide();
                  $(slFeedAnchor).html('').addClass("liked").append(likCntrl);
                  likCntrl.fadeIn("slow",reActivate);
                  
                  var resCntrl = $("<span></span>").html(res).hide();
                  slFeedResult.html('').append(resCntrl);
                  resCntrl.fadeIn("slow",reActivate);

                  return true;
                }

                function reActivate(){
                  processingReq = false;
                }
        });  
    };  
 
    $.fn.smartLike.defaults = {   
          transitionEffect:'fade' // Effect on navigation, none/fade/slide
    };

})(jQuery);
