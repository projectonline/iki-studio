/**
 * @author John Noel <john.noel@rckt.co.uk>
 * @copyright rckt 2011 <http://www.rckt.co.uk>
 * @package MuseumsSheffield
 */

/**
 * Scroller plugin
 * Turns a list into a scroller / carousel / slider / whatever
 *
 * @author John Noel <john.noel@rckt.co.uk>
 * @copyright rckt http://www.rckt.co.uk/
 * @version 2.0
 * @package MuseumsSheffield
 * @todo Allow choosing of pagination CSS classes
 * @todo Allow external calling of advance / retreat
 * @todo Allow irregularly sized slides
 */
(function($) {
	$.fn.scroller = function(option) { 
		option = $.extend({}, $.fn.scroller.option, option);
		
		return this.each(function() {
			var $this = $(this),
				transitioning = false,
				slideCount = $(option.slidesSelector, $this).length;
			
			option.paginationBuild &= (slideCount > option.slidesPerScroll);
			
			// add pagination
			if(option.paginationBuild)
			{
				var pageCount = Math.ceil(slideCount / option.slidesPerScroll);
				var pages = '';
				for(var i = 1; i <= pageCount; i++)
				{
					pages += '<li>'+i+'</li>';
				}
				
				$('<div class="pagination" />')
					.append('<span class="previous">Previous</span>')
					.append('<span class="next">Next</span>')
					.append('<ul>'+pages+'</ul>')
					.appendTo($this);
			}
			
			if(option.pagination)
			{
				$('.pagination li', $this).first().addClass('on');
			}
			
			// "square off" scroller
			if((slideCount % option.slidesPerScroll) != 0)
			{
				var p = $(option.containerSelector, $this);
				var toInsert = $(option.slidesSelector, $this)
					.slice(0 - ((Math.ceil(slideCount / option.slidesPerScroll) * option.slidesPerScroll) - slideCount))
					.each(function() {
						p.append(this.cloneNode(false)); // not a deep copy
					});
			}
			
			// absolutise
			var h = 0;
			$(option.slidesSelector, $this).each(function(idx) {
				$(this).css({
					position: 'absolute',
					top: 0, left: ($(this).outerWidth(true)*idx)+'px'
				});
				
				h = ($(this).outerHeight(true) > h) ? $(this).outerHeight(true) : h;
			});
			
			$(option.containerSelector, $this).css({ position: 'relative', height: h+'px' });
			
			// advance
			var advance = function(count) {
				if(!transitioning)
				{
					transitioning = true;
				
					// pagination
					if(option.pagination)
					{
						var next = $('.pagination li.on', $this).nextAll();
						var n = (next.length < count) ?
							$('.pagination li', $this).eq(1 - Math.abs(next.length - count)) :
							next.eq(count - 1); // 0 indexed
						
						n.addClass('on').siblings().removeClass('on');
					}
					
					var slides = $(option.slidesSelector, $this);
					// grab the first slidesPerScroll slides
					var cloneableSlides = $(option.slidesSelector, $this).slice(0, option.slidesPerScroll * count);
					var w = slides.outerWidth(true);
					
					// append them and set their left offset
					$(option.containerSelector, $this).append(cloneableSlides.clone().each(function(idx) {
						$(this).css({ left: (w * (slides.length + idx)) });
					}));
					
					// shouldn't use slides variable as slides have been added
					$(option.slidesSelector, $this).animate({
						left: '-=' + ((w * option.slidesPerScroll) * count)
					}, 700, function() {
						transitioning = false;
						cloneableSlides.remove();
						
						initHomeCarouselHover();
						
					});
				} // !transitioning
				
				
				
			}; // advance()
			
			// retreat
			var retreat = function(count) {
				if(!transitioning)
				{
					transitioning = option.slidesPerScroll;
					
					if(option.pagination)
					{
						// pagination
						var prev = $('.pagination li.on', $this).prevAll();
						var p = (prev.length < count) ?
							$('.pagination li', $this).eq(prev.length - count) :
							prev.eq(count - 1);
						p.addClass('on').siblings().removeClass('on');
					}
					
					// grab the final slidesPerScroll slides
					var cloneableSlides = $(option.slidesSelector, $this).slice(0 - (option.slidesPerScroll * count));
					var w = cloneableSlides.outerWidth(true);
					
					// clone and prepend them, setting their left offset correctly
					$(option.containerSelector, $this).prepend(cloneableSlides.clone().each(function(idx) {
						$(this).css({ left: 0 - (w * (cloneableSlides.length - idx)) });
					}));
					
					$(option.slidesSelector, $this).animate({
						left: '+=' + ((w * option.slidesPerScroll) * count)
					}, 700, function() {
						transitioning = false;
						cloneableSlides.remove();
						
						initHomeCarouselHover();
					});
				} // !transitioning
			}; // retreat()
			
			if(option.pagination)
			{
				$('.pagination .next', $this).click(function(evt) { evt.stopPropagation(); advance(1); });
				$('.pagination .previous', $this).click(function(evt) { evt.stopPropagation(); retreat(1); });
				$('.pagination li', $this).click(function(evt) {
					evt.stopPropagation();
					var $elem = $(evt.currentTarget);
					
					var target = $elem.prevAll().length;
					var current = $elem.siblings('.on').prevAll().length;
					
					// check to make sure not navigating to same element
					if(!$elem.hasClass('on'))
					{
						// by default advance
						var diff = target - current;
						var nDiff = diff;
						
						// but if moving over half of the items, retreat instead
						if(Math.abs(diff) > Math.floor($elem.siblings().andSelf().length / 2))
						{
							nDiff = Math.abs(nDiff) - $elem.siblings().andSelf().length;
							nDiff *= (diff < 0) ? -1 : 1;
						}
						
						if(nDiff > 0)
						{
							advance(nDiff);
						}
						else
						{
							retreat(Math.abs(nDiff));
						}
					}
				});
			} // option.pagination
		});
	};
	
	$.fn.scroller.option = {
		slidesPerScroll: 1, // how many slides per click
		slidesSelector: 'ul.slides li', // how to select the slides (scoped to the element)
		containerSelector: 'ul.slides', // how to select the slide container
		paginationBuild: true, // whether to build the pagination or not
		pagination: true // whether to operate pagination at all
	};
})(jQuery);

$(window).load(function() {
	$('.scroller.threeSlides').scroller({ slidesPerScroll: 3 });
	$('.scroller.twoSlides').scroller({ slidesPerScroll: 2 });
	$('.scroller.oneSlide').scroller({ slidesPerScroll: 1 });
	$('#socialMediaStrip .flickr, #sidebarRight .flickr').scroller({
		slidesPerScroll: 1,
		slidesSelector: 'ul li',
		containerSelector: 'ul',
		paginationBuild: false
	});
});


// Track outbound clicks and file downloads
$('a').click( function(e) {
	//return false;
	if (_gaq) {
		var l = $(e.currentTarget).attr('href');
		if (l.substr(0,4) == 'http') {			
			_gaq.push(['_trackEvent', 'External link', 'Click', l]);
			return;	
		}
		var parts = l.split('.'),
			ext = parts[parts.length-1].toLowerCase(),
			toTrack = ['pdf','doc','mp3','ppt'];		
		if (parts.length > 1 && $.inArray(ext, toTrack) > -1) {
			_gaq.push(['_trackEvent', 'File download', ext, l]);
			return;		
		}
	}
	
});


// Discover more
// need to wait for the window to load rather than the dom due to images no longer having fixed sizes (grid system)
$(window).load(function() {
	var dm = $('#discoverMore'),
		originalText = dm.find('a.button:first').html();
	dm.removeClass('hidden');
	$('#discoverMoreNav', dm).append($('#sitemap ul')[0].cloneNode(true));
		
	$('.button', dm).click(function(evt) {
		evt.preventDefault();
		var $t = $(evt.currentTarget);

		if(parseInt(dm.css('top')) < 0) // opening
		{ 
			dm.animate({
				top: 0
			}, 400, 'linear', function() {
				$t.fadeOut(250, function() {
					$t.html('Close <span></span>');
					$t.addClass('open');
					$t.fadeIn(250);
				});
			});
			
			// Track interactions
			if (_gaq) {
				_gaq.push(['_trackEvent', 'Explore bar', 'Open']);	
			}
			
		}
		else
		{
			dm.animate({
				top: 30 - dm.outerHeight()
			}, 400, 'linear', function() { 
					$t.fadeOut(250, function() {
					$t.html(originalText);
					$t.removeClass('open');
					$t.fadeIn(250);
				})
			});
			
			// Track interactions
			if (_gaq) {
				_gaq.push(['_trackEvent', 'Explore bar', 'Close']);	
			}
		}
	});
												
	$('#discoverMore').css({top: 30 - dm.outerHeight()});
	
	// re-align the discover more bar on window resize
	var resizeTimeout = null;
	$(window).resize(function() {
		clearTimeout(resizeTimeout);
		resizeTimeout = setTimeout(function() {
			var dm = $('#discoverMore');
			dm.css({top: 30 - dm.outerHeight()});
		}, 200);
	});
});

// Captions
$('.caption').each(function() {
	var caption = $(this);
	caption.css({bottom: 0 - caption.outerHeight()+'px'});
	caption.data('timer', null);
	
	caption.parent().mouseover(function(evt) {
		clearTimeout(caption.data('timer'));
		caption.data('timer', null);
		
		if(caption.data('timer') == null)
		{
			caption.animate({
				bottom: 0
			}, 'fast');
		}
	}).mouseout(function(evt) {
		if(caption.data('timer') == null)
		{
			caption.data('timer', setTimeout(function() {
				caption.animate({
					bottom: 0 - caption.outerHeight()+'px'
				}, 'fast', function() { caption.data('timer', null); });
			}, 100));
		}
	});
});

// Logos
$('#logo img').mouseover(function(evt) {
	var $this = $(this);
	$this.attr('src', $this.attr('src').replace(/logo-museumssheffield(-.*?)?\.png/, 'logo-museumssheffield$1-on.png'));
}).mouseout(function(evt) {
	var $this = $(this);
	$this.attr('src', $this.attr('src').replace(/logo-museumssheffield(-.*?)?-on.png/, 'logo-museumssheffield$1.png'));
});

// placeholder
if(!Modernizr.input.placeholder)
{
	var i = $('input[placeholder]');
	i.val(i.attr('placeholder')).focus(function(evt) {
		if($(this).val() == $(this).attr('placeholder'))
		{
			$(this).val('');
		}
	}).blur(function(evt) {
		if($(this).val() == '')
		{
			$(this).val($(this).attr('placeholder'));
		}
	});
}


/* Easing functions */
/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuint',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158; 
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */
 
 
  // Event Filtering functions
  $( function() { 
    $('#eventFilterUpdateButton').hide();
    $('#eventFilter input').change( function() {
      $(this).parents('form').submit();  
    });
	$('#eventFilter label').hover( function() { $(this).toggleClass('hover'); } );
  });
  
  
  
  // JavaScript Document


var ww = $(window).width();
function setLayout() {
	ww = $(window).width();
	console.log(ww);
	if(ww < 500) {
		$('#mainWrapper').css('width', '100%');
		$('#header').css('width', '100%');
		/*$('#header').css('position', 'fixed');
		$('#header').css('top', '0');*/
		//$('#header').css('background', '#FFF');
		
		$('.fadein img').css('width', '100%');
		
		$('#titleBar').css('width', '100%');
		$('#mainContainer').css('width', '100%');
		$('#contentLeftPanel').css('width', '99%');
		
		$('#contentRightPanel').css('width', '99%');
		
		$('.contentText').css('width', '90%');
		$('.contentText img').hide();
		
		$('.clientWrap table').css('width', '100%');
		
		$('#contentRightPanel').hide();
		$('#contentLeftPanel').show();
		$('#goBack').hide();
	} else if(ww > 500 && ww < 1030) {
		$('#mainWrapper').css('width', '100%');
		$('#header').css('width', '100%');
		/*$('#header').css('position', 'fixed');
		$('#header').css('top', '0');*/
		//$('#header').css('background', '#FFF');
		
		$('#titleBar').css('width', '100%');
		$('#mainContainer').css('width', '100%');
		$('#contentLeftPanel').css('width', '220px');
		var rightPanelWidth = ww - 225;
		$('#contentRightPanel').css('width', rightPanelWidth+'px');
		
		$('.contentText').css('width', '90%');
		$('.contentText img').hide();
		
		$('.clientWrap table').css('width', '100%');
		
		$('#contentLeftPanel').show();
		$('#contentRightPanel').show();
		$('#goBack').hide();
	} else {
		$('#mainWrapper').css('width', '1150px');
		$('#header').width();
		$('#titleBar').width();
		$('#mainContainer').width();
		$('#contentLeftPanel').width();
		$('#contentRightPanel').width();
		
		$('.contentText').width();
	}
}
	