function TSan_auto_user( field_auto )
{
	$( "#" + field_auto ).autocomplete({
		source: "/users/auto",
		select: function( event, ui ) {
			$( "#" + field_auto ).val( ui.item.label );
			return false;
		}
	});
}

function TSan_auto_ten_mau( field_auto )
{
	$( "#" + field_auto ).autocomplete({
		source: "/taisans/auto_ten_mau",
		select: function( event, ui ) {
			$( "#" + field_auto ).val( ui.item.label );
			return false;
		}
	});
}


function TSan_Dexuatmua_Baogia()
{
	$("#TSan_Dexuatmua_Baogia form").iframePostForm
	({
		iframeID: "TSan_iframe-post-form",
		post : function ()
		{
			$("#loading").show();
		},
		complete : function (response)
		{
			if( response == "ok" )
			{
				location.reload(true); return false;
			}
			$("#loading").hide();
			$("#TSan_Dexuatmua_Baogia").html(response);
		}
	});
}

function TSan_Nhatkybaotri_them()
{
	$("#window_content form").iframePostForm
	({
		iframeID: "TSan_iframe-post-form_Nhatkybaotri_them",
		post : function ()
		{
			$("#loading").show();
		},
		complete : function (response)
		{
			if( response == "ok" )
			{
				location.reload(true); return false;
			}
			$("#loading").hide();
			$("#window_content").html(response);
		}
	});
}

// ============ NHAPTAISAN ===== LUAN CHUYEN TAI SAN CHO NGUOI NHAN ======================
function TSan_nhaptaisan_luanchuyen_checkform()
{
	if( $("#TaisanId").attr("id") == undefined )
	{
		$("#TSan_thongbao").html("Bạn vui lòng nhập và lưu thông tin tài sản trước khi luân chuyển.");
		$("#TSan_thongbao").attr("class", "").addClass("message_error").fadeIn().delay(3000).fadeOut(
			function(){
				$("#loading").hide();
			}
		);
		;
		return false;

	}else{
		$("#TaisansLuanchuyenTaisanId").val( $("#TaisanId").val() );
	}

	return true;
}

// function TSan_goi_script_nhaptaisan_luanchuyen()
// {
// 	TSan_nhaptaisan_luanchuyen();

// 	$( "#TaisansLuanchuyenNguoisudung" ).autocomplete({
// 		source: "/users/auto",
// 		select: function( event, ui ) {
// 			$( "#TaisansLuanchuyenNguoisudung" ).val( ui.item.label );
// 			return false;
// 		}
// 	});
// }

// function TSan_nhaptaisan_luanchuyen()
// {
// 	$("#TSan_nhaptaisan_luanchuyen form").iframePostForm
// 	({
// 		iframeID: "TSan_iframe-post-form_nhaptaisan_luanchuyen",
// 		post : function ()
// 		{
// 			$("#loading").show();
// 		},
// 		complete : function (response)
// 		{
// 			$("#loading").hide();
// 			$("#TSan_nhaptaisan_luanchuyen").html(response);

// 			TSan_goi_script_nhaptaisan_luanchuyen();
// 		}
// 	});
// }

//
// ============ THANH LY TAI SAN ===========================
function TSan_goi_script_thanhly()
{
	TSan_thanhly();

	$( "#TaisanNguoithanhly", "#window_content" ).autocomplete({
		source: "/users/auto",
		select: function( event, ui ) {
			$( "#TaisanNguoithanhly" ).val( ui.item.label );
			return false;
		}
	});
}

function TSan_thanhly()
{
	// TSan_thanhly
	$("#window_content form").iframePostForm
	({
		iframeID: "TSan_iframe-post-form_TSan_thanhly_window_content",
		post : function ()
		{
			$("#loading").show();
		},
		complete : function (response)
		{
			$("#loading").hide();
			$("#window_content").html(response);

			TSan_goi_script_thanhly();
		}
	});
}

// ============ THEM TAI SAN MOI ======================================

// ============ LUANCHUYENTAISAN ===== LUAN CHUYEN TAI SAN CHO NGUOI NHAN ======================
function TSan_goi_script_luanchuyen_chuyen()
{
	TSan_luanchuyen_chuyen();

	$( "#TaisansLuanchuyenNguoisudung" ).autocomplete({
		source: "/users/auto",
		select: function( event, ui ) {
			$( "#TaisansLuanchuyenNguoisudung" ).val( ui.item.label );
			return false;
		}
	});
}

function TSan_luanchuyen_chuyen()
{
	$("#TSan_luanchuyen_chuyen form").iframePostForm
	({
		iframeID: "TSan_iframe-post-form_TSan_luanchuyen_chuyen",
		post : function ()
		{
			$("#loading").show();
		},
		complete : function (response)
		{
			$("#loading").hide();
			$("#TSan_luanchuyen_chuyen").html(response);

			TSan_goi_script_luanchuyen_chuyen();
		}
	});
}

function TSan_luanchuyen_duyet( field, id_luanchuyen, object )
{
	if( $(object).val().trim() == "" )
	{
		return false;
	}

	$("#loading").show();
	$.ajax({
		url: "/taisans/luanchuyen_duyet_click/" + field + "/" + id_luanchuyen + "/" + $(object).val() ,
		timeout: 15000,
		error: error_handler,
		success: function(html){

			$("#loading").hide();

			if( html == "ok" )
			{
				$("#TSan_luanchuyen_duyet_click").html("Lưu thành công");
				$("#TSan_luanchuyen_duyet_click").attr("class", "").addClass("message_success").fadeIn().delay(3000).fadeOut();
			}else{
				$("#TSan_luanchuyen_duyet_click").html("Lưu thất bại. Có lỗi trong quá trình xử lý.");
				$("#TSan_luanchuyen_duyet_click").attr("class", "").addClass("message_error").fadeIn().delay(3000).fadeOut();
			}
			return false;
		}
	});

	return false;
}


// ============ THEM TAI SAN MOI ======================================
function TSan_goi_script_nhaptaisan_them()
{
	// TSan_nhaptaisan_them();

	TSan_auto_ten_mau('TaisanTen');

	TSan_auto_user( "TaisanNguoimua" );

	TSan_auto_user( "TaisanNguoisudung" );

	TSan_auto_user( "TaisansLuanchuyenNguoisudung" );

	$( "#TaisanNhacungcap" ).autocomplete({
		source: "/taisans/nhacungcap_auto",
		select: function( event, ui ) {
			$( "#TaisanNhacungcap" ).val( ui.item.label );
			return false;
		}
	});
}

function TSan_lay_phongban()
{
	// lay phongban theo congty
	$("#loading").show();
	$.ajax({
		url: "/taisans/get_phongban_maso/" + $("#TaisansLuanchuyenCongtyId").val() + '/key_id',
		timeout: 15000,
		error: error_handler,
		success: function(html){

			$("#loading").hide();

			$("#TaisansLuanchuyenPhongbanId").html( $("#TaisanPhongbanTmp", html).html());

			return false;
		}
	});

	return false;
}

// function TSan_lay_STT( congty )
// {
// 	if( congty == "congty" )
// 	{
// 		// lay phongban theo congty
// 		$("#loading").show();
// 		$.ajax({
// 			url: "/taisans/get_phongban_maso/" + $("#TaisanCongty").val(),
// 			timeout: 15000,
// 			error: error_handler,
// 			success: function(html){

// 				$("#loading").hide();

// 				$("#TaisanPhongban").html( $("#TaisanPhongbanTmp", html).html());

// 				// apply vao cac list box khac
// 				TSan_lay_phongban();

// 				return false;
// 			}
// 		});

// 	}

// 	$("#TaisanLoaitaisanId").prop("selectedIndex", $("#TaisanLoai").prop("selectedIndex"));
// 	$("#TaisansLuanchuyenPhongbanId").prop("selectedIndex", $("#TaisanPhongban").prop("selectedIndex"));
// 	$("#TaisansLuanchuyenCongtyId").prop("selectedIndex", $("#TaisanCongty").prop("selectedIndex"));

// 	return false;
// }

function TSan_lay_STT( congty )
{
	if( congty == "congty" )
	{
		// lay phongban theo congty
		$("#loading").show();
		$.ajax({
			url: "/taisans/get_phongban_maso/" + $("#TaisanCongty").val(),
			timeout: 15000,
			error: error_handler,
			success: function(html){

				$("#loading").hide();

				$("#TaisanPhongban").html( $("#TaisanPhongbanTmp", html).html());

				TSan_lay_STT_khi_co_phongban( congty );

				return false;
			}
		});
	}else{

		TSan_lay_STT_khi_co_phongban( congty );
	}
}

function TSan_lay_STT_khi_co_phongban( congty )
{
	var string = $("#TaisanNam").val() + "." + $("#TaisanCongty").val() + "." + $("#TaisanPhongban").val() + "." + $("#TaisanLoai").val();

	$("#loading").show();
	$.ajax({
		url: "/taisans/laysothutu/" + string,
		timeout: 15000,
		error: error_handler,
		success: function(html){

			$("#loading").hide();

			$("#TaisanStt").val(html);

			// chon congty, phongban, loaitaisan mac dinh cho cac muc ben duoi.

			// var i = $("#TaisanLoai option:selected").index();
			// $("#TaisanLoaitaisanId").val( $("#TaisanLoai").val() );
			//var i = $("#TaisanLoaitaisanId option[value='USD']").index();
			$("#TaisanLoaitaisanId").prop("selectedIndex", $("#TaisanLoai").prop("selectedIndex"));
			$("#TaisansLuanchuyenPhongbanId").prop("selectedIndex", $("#TaisanPhongban").prop("selectedIndex"));
			$("#TaisansLuanchuyenCongtyId").prop("selectedIndex", $("#TaisanCongty").prop("selectedIndex"));

			if( congty == "congty" )
			{
				TSan_lay_phongban();
			}

			return false;
		}
	});

	return false;
}

// function TSan_nhaptaisan_them()
// {
// 	$("#TSan_nhaptaisan_them form").iframePostForm
// 	({
// 		iframeID: "TSan_iframe-post-form_nhaptaisan_them",
// 		post : function ()
// 		{
// 			$("#loading").show();
// 		},
// 		complete : function (response)
// 		{
// 			$("#loading").hide();
// 			$("#TSan_nhaptaisan_them").html(response);

// 			TSan_goi_script_nhaptaisan_them();
// 		}
// 	});
// }

function TSan_taidanhgia( field, id_taisan, object )
{
	if( $(object).val().trim() == "" )
	{
		return false;
	}

	$("#loading").show();
	$.ajax({
		url: "/taisans/taidanhgia_them",
		timeout: 15000,
		type: 'POST',
		data: { field: field, id_taisan:id_taisan, value: $(object).val().trim(), nam: $('#TaisanNamYear').val(), quy: $('#TaisanQuy').val(), id_taidanhgia: $("#TaisanIdTaidanhgiaVualuu" + id_taisan).val() },
		error: error_handler,
		success: function(html){

			$("#loading").hide();

			if( html.substr(0,2) == "ok" )
			{
				if( $("#TaisanIdTaidanhgiaVualuu" + id_taisan).val().length > 0 )
				{
					$("#TSan_thongbao").html("Cập nhật thành công");
				}else{
					$("#TSan_thongbao").html("Lưu thành công");
				}

				$("#TSan_thongbao").attr("class", "").addClass("message_success").fadeIn().delay(3000).fadeOut();

				// Ghi nho ID vua luu thanh cong
				$("#TaisanIdTaidanhgiaVualuu" + id_taisan).val(html.replace("ok", ""));

			}else{
				$("#TSan_thongbao").html("Lưu thất bại. Có lỗi trong quá trình xử lý.");
				$("#TSan_thongbao").attr("class", "").addClass("message_error").fadeIn().delay(3000).fadeOut();
			}
			return false;
		}
	});

	return false;
}

function TSan_dexuatmua_duyet( field, id_dexuat, object )
{
	if( $(object).val().trim() == "" )
	{
		return false;
	}

	$("#loading").show();
	$.ajax({
		url: "/taisans/dexuatmua_duyet_click/" + field + "/" + id_dexuat + "/" + $(object).val() ,
		timeout: 15000,
		error: error_handler,
		success: function(html){

			$("#loading").hide();

			if( html == "ok" )
			{
				$("#TSan_dexuatmua_duyet_click").html("Lưu thành công");
				$("#TSan_dexuatmua_duyet_click").attr("class", "").addClass("message_success").fadeIn().delay(3000).fadeOut();
			}else{
				$("#TSan_dexuatmua_duyet_click").html("Lưu thất bại. Có lỗi trong quá trình xử lý.");
				$("#TSan_dexuatmua_duyet_click").attr("class", "").addClass("message_error").fadeIn().delay(3000).fadeOut();
			}
			return false;
		}
	});

	return false;
}

function TSan_nhaptaisan_duyet( field, id_luanchuyen, object )
{
	if( $(object).val().trim() == "" )
	{
		return false;
	}

	$("#loading").show();
	$.ajax({
		url: "/taisans/nhaptaisan_duyet_click/" + field + "/" + id_luanchuyen + "/" + $(object).val() ,
		timeout: 15000,
		error: error_handler,
		success: function(html){

			$("#loading").hide();

			if( html == "ok" )
			{
				$("#TSan_nhaptaisan_duyet_click").html("Lưu thành công");
				$("#TSan_nhaptaisan_duyet_click").attr("class", "").addClass("message_success").fadeIn().delay(3000).fadeOut();
			}else{
				$("#TSan_nhaptaisan_duyet_click").html("Lưu thất bại. Có lỗi trong quá trình xử lý.");
				$("#TSan_nhaptaisan_duyet_click").attr("class", "").addClass("message_error").fadeIn().delay(3000).fadeOut();
			}
			return false;
		}
	});

	return false;
}

// $(function(){

// 		$( "#TaisansDexuatTenTmp" ).autocomplete({
// 			source: "/taisans/auto_taisan",
// 			select: function( event, ui ) {
// 				$( "#TaisansDexuatTenTmp" ).val( ui.item.label );
// 				return false;
// 			}
// 		});
// 	});

// function Tsan_dexuatmua_baogia_load(taisan_id)
// {
// 	$("#loading").show();
// 	$.ajax({
// 		url: "/taisans/dexuatmua_baogia/" + taisan_id,
// 		timeout: 15000,
// 		error: error_handler,
// 		success: function(html){

// 			$("#TSan_Dexuatmua_Baogia").html(html);

// 			$("#loading").hide();

// 			return false;
// 		}
// 	});

// 	return false;
// }

function Tsan_TaisansYkien_xemthem( loai )
{
	$("#loading").show();

	var id_next = $("#TaisansYkienIdMore").val();

	if( loai == undefined )loai = 'dexuat';

	$.ajax({
		url: "/taisans/ykien_xemthem/" + id_next + "/" + loai,
		timeout : 120000,
		success: function(html){

			if( html != "" )
			{
				$("#TaisansYkien_xemthem").append(html);

				$("#TaisansYkienIdMore").val($("#TaisansYkienIdMore").val() - 3);

			}else{
				$("#TaisansYkien_xemthem_alink").html("<div id='Tsan_het_ykien' style='text-align: center; color: green;'>Không còn ý kiến nào cả</div>");

				$("#Tsan_het_ykien").delay(3000).fadeOut();
			}

			$("#loading").hide();


		},error: error_handler
	});
}

function TSan_chon_danhmuc_phongban(congty_id)
{
	$("#loading").show();

	$.ajax({
		url: "/taisans_nccs/danhmuc_phongban/" + congty_id,
		timeout : 120000,
		error: error_handler,
		success: function(html){

			$("#TSan_danhmuc_phongban").html(html);

			$("#loading").hide();

		}
	});

	return false;
}