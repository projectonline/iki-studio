//var J = $.noConflict();
/*
delete x // Removes the variable x
delete x[i] // Removes the element with index i from array x
delete x.p // Removes the property p from object / array x

Sắp xếp phần tử của mảng số trong javascript

Giả sử bạn có mảng sau:
Mã:
var myarray=[25, 8, 7, 41]

1. Sắp xếp tăng dần
Mã:
myarray.sort(function(a,b){return a - b});
//Kq [7, 8, 25, 41]

2. Sắp xếp giảm dần
Mã:
myarray.sort(function(a,b){return b - a});
//Kq [41, 25, 8, 7]

*/
//========= HAM XU LY LOI CUA AJAX REQUEST ==========//
function error_handler(XMLHttpRequest, textStatus, errorThrown) {
	$("#loading").hide();
	// alert( "Có lỗi trong quá trình xử lý. Vui lòng F5 và thao tác lại!");
}


// Xóa tất cả các input, checkbox của form
function clear_form(myform) {
	$(':input', '#' + myform).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
}

function checkbox_all(value, class_add) {
	$(":checkbox." + class_add).attr("checked", value);
}

function checkall_observe(value, id_checkall, class_check) {
	var $checkall = $("#" + id_checkall);
	if ($checkall.attr("checked") && !value) {
		$checkall.attr("checked", false);

	} else if (value) {
		var check = true;
		$(":checkbox." + class_check).each(function() {
			if (!$(this).attr("checked")) check = false;
		});
		$checkall.attr("checked", check);
	}
}

/*
function textarea_resize(contain)
{
$("textarea", contain ).autosize();
}
*/
$(function() {

	// Use this example, or...
	// $('a.lightbox').lightBox();
	//bind_calendarimage(); // get vị trí con trỏ chuột cho calendar

	// Accordion
	//jQuery("#accordion").accordion( { autoHeight: false } );//
	//jQuery("#accordion_two").accordion( { autoHeight: false } );//

	var li_contain = $("#Left_MainTabs li:eq(2)");
	if (li_contain.hasClass("ActiveTask")) {
		$("#TabsGiamSatThiNghiem li:eq(1)").click();
	}

	// KHỞI TẠO DIALOG
	$("#window_content").dialog({
		autoOpen: false,
		//show: "blind",
		//hide: "drop",
		width: "auto"
	});

	// KHỞI TẠO DIALOG 2
	$("#window_content_2").dialog({
		autoOpen: false,
		//show: "blind",
		//hide: "drop",
		width: "auto"
	});

	// KHỞI TẠO DIALOG 3
	$("#window_content_3").dialog({
		autoOpen: false,
		//show: "blind",
		//hide: "drop",
		width: "auto"
	});

	// KHỞI TẠO DIALOG 4
	$("#window_content_4").dialog({
		autoOpen: false,
		//show: "blind",
		//hide: "drop",
		width: "auto"
	});

	//textarea_resize('body');

	//$(document).tooltip();

});

$.fx.speeds._default = 1000;

function window_show() {

	var selector = $("#window_content");

	//textarea_resize("#window_content");

	var title = "Nhập thông tin";

	var legend = $("legend", selector);
	if (legend.length > 0) {
		title = legend.html();
		legend.remove();
	}

	selector.dialog({
		title: title
	});

	selector.dialog("open");

	/*
$( "#window_content" ).dialog({
	autoOpen: false,
	show: "blind",
	hide: "explode"
});
*/

	var textarea = $("textarea:first", "#window_content");
	if (textarea.attr("id") != undefined) {
		textarea.focus();
	}
	return false;
}

function window_show_2() //  Show Window choose User
{
	var selector = $("#window_content_2");

	//textarea_resize("#window_content_2");

	var title = "Nhập thông tin";
	if ($("legend", selector).length > 0) {
		title = $("legend", selector).html();
		$("legend", selector).remove()
	}
	selector.dialog({
		title: title
	});

	selector.dialog("open");

	selector.prev(".ui-widget-header").css("background", "none").css("background-color", "#6F89B3");


	return false;
}

function window_show_3() //  Show Window choose User
{
	var selector = $("#window_content_3");

	//textarea_resize("#window_content_3");

	var title = "Nhập thông tin";
	if ($("legend", selector).length > 0) {
		title = $("legend", selector).html();
		$("legend", selector).remove()
	}
	selector.dialog({
		title: title
	});

	selector.dialog("open");

	selector.prev(".ui-widget-header").css({
		color: "#000000",
		background: "-moz-linear-gradient(center top , #F5F5F5, #F1F1F1) repeat scroll 0 0 #F5F5F5",
		border: "1px solid #DCDCDC"
	});

	//selector.prev(".ui-widget-header").css("background", "none").css("background-color", "#444444");

	return false;
}

function window_show_4() {

	var selector = $("#window_content_4");

	//textarea_resize("#window_content_4");

	var title = "Nhập thông tin";

	var legend = $("legend", selector);
	if (legend.length > 0) {
		title = legend.html();
		legend.remove();
	}

	selector.dialog({
		title: title
	});

	selector.dialog("open");

	var textarea = $("textarea:first", "#window_content");
	if (textarea.attr("id") != undefined) {
		textarea.focus();
	}
	return false;
}

//HB
//Window_show use only for HB (Heart Business)
function window_show_HB() {
	$("#HB_window").show();

	var width_HB = $("#HB_window").width();
	var width_Document = $(window).width();

	$("#HB_window").css({
		"left": ((width_Document - width_HB) / 2)
	});
}

function window_show_5() //  Show Window choose User
{
	var selector = $("#window_content_2");

	//textarea_resize("#window_content_2");

	var title = "Nhập thông tin";
	if ($("legend", selector).length > 0) {
		title = $("legend", selector).html();
		$("legend", selector).remove()
	}
	selector.dialog({
		title: title
	});

	selector.dialog("open");

	selector.prev(".ui-widget-header").css("background", "none").css("background-color", "#6F89B3");


	return false;
}
//End HB

// USER GIAO VIEC
// click luc open chon nguoi giao viec
function open_user_search_window_2(group) {
	var url = "/users/users_search_on_window";
	if (group != undefined) url = url + "/" + group;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#window_content_2").html(html);
			$("#loading").hide();
			window_show_2();
			$("#window_content_2").parent("div").css("left", 584 + Math.floor(($(window).width() - 1060) / 2));

			// REMOVE SEND ALL FROM LOI NHAN BEN TAY TRAI
			if ($("#hide_send_all", "#window_content").attr("id") == "hide_send_all") {
				$("#div_send_all", "#window_content_2").remove();
			}
		}
	});
}

var mang_tmp = [];

function chon_users_search_2(loai, object_a_clicked, id_td) {
	// Gán html vào thẻ window 1 chọn user
	// $("#users_search_giaoviec").append( $( "#td_user_" + loai + "_" + id_td, "#window_content_2").html() );
	// diult dong code ngay 06/10/2013
	var tmp = $("#users_search_giaoviec");

	if ($("#span_id_user_giaoviec_all", tmp).attr("id") != undefined) {
		$("#span_id_user_giaoviec_all", tmp).remove();
	}

	if ($(object_a_clicked).is(":checked")) {
		tmp.append($("#td_user_" + loai + "_" + id_td, "#window_content_2").html());

		if (loai == "thongbao") {
			data_thanhvien_thongbao[id_td] = id_td;
		} else {
			data_thanhvien_phanhoi[id_td] = id_td;
		}
	} else {
		// XOA CHON
		var contain = $("#users_search_giaoviec");

		$("img#img_xoa_" + loai + "_" + id_td, contain).click();

		if (loai == "thongbao") {
			data_thanhvien_thongbao[id_td] = 0;
		} else {
			data_thanhvien_phanhoi[id_td] = 0;
		}
	}
	return false;
	//diult dong ngay 20/09/2014
	//Ket thuc dong code bgay 06/11/2013


	//diult code chinh sua , them ngay 06/11/2013
	/*var tam = 0 ;

var id_td;
if(id_td == 9)
{
	 id_td = ["9","174","151","27","493","482"];
}
else if(id_td == 117){
		id_td = ["151","152","410","117"];
}else if(id_td == 27){
		id_td = ["27","493","174"];
}else if(id_td == 339){
		id_td = ["339","482","174","129","317"];
}else if(id_td == 22){
		id_td = ["22","339","482","174"];
}else if(id_td == 255){
		id_td = ["255","151","410"];
}else if(id_td == 14){
		id_td = ["14","415","480"];
}else if(id_td == 345){
		id_td = ["345","152","409"];
}else if(id_td == 382){
		id_td = ["382","152","409"];
}else if(id_td == 347){
		id_td= ["347","481","152"];
}else if(id_td == 466){
		id_td = ["466","481","152"];
}else if(id_td == 261){
		id_td = ["261","14"];
}else if(id_td == 446){
		id_td = ["446","14"];
}else if(id_td == 129){
		id_td = ["129","14","339"];
}else if(id_td == 317){
		id_td = ["317","339"];
}
else
{
	 id_td = [id_td];
}

var tmp = $("#users_search_giaoviec");

if( $("#span_id_user_giaoviec_all",tmp ).attr("id") != undefined )
{
	$("#span_id_user_giaoviec_all",tmp ).remove();
}

if( $(object_a_clicked).is(":checked") )
{

	for(id_tdd in id_td)
	{
		tam = 0;

		for(tam_tmp in mang_tmp)
		{
			if(mang_tmp[tam_tmp] == id_td[id_tdd])
			{
				tam = 1;
				break;
			}
		}
		if (tam != 1)
		{
			tmp.append( $( "#td_user_" + loai + "_" + id_td[id_tdd], "#window_content_2").html() );

			if( loai == "thongbao" )
			{
				data_thanhvien_thongbao[id_td[id_tdd]] = id_td[id_tdd];
			}else{
				data_thanhvien_phanhoi[id_td[id_tdd]] = id_td[id_tdd];
			}
			mang_tmp.push(id_td[id_tdd]);
		}
		//alert (mang_tmp);
	}

}else{
	// XOA CHON

	var contain = $("#users_search_giaoviec");
	for(id_tdd in id_td)
	{
		for(tam_tmp in mang_tmp)
		{
			if(mang_tmp[tam_tmp] == id_td[id_tdd])
			{
				var i = mang_tmp.indexOf(mang_tmp[tam_tmp]);
				if(i != -1)
				{
					mang_tmp.splice(i, 1);
				}
				break;
			}
		}
		$("img#img_xoa_"+ loai +"_" + id_td[id_tdd], contain ).click();
		if( loai == "thongbao" )
		{
			data_thanhvien_thongbao[id_td[id_tdd]] = 0;
		}else{
			data_thanhvien_phanhoi[id_td[id_tdd]] = 0;
		}
	}
}
return false;*/

	// diult ket thuc them, sua code ngay 06/11/2013
}

function chon_users_search_2_all(type, object) {
	$("td.Td" + type + " :checkbox", "#data_click_group_user").each(function() {
		if ($(object).is(":checked")) {
			if (!$(this).is(":checked")) {
				$(this).click();
			}
		} else {
			if ($(this).is(":checked")) {
				$(this).click();
			}
		}

	});
	return false;
}

function stick_checkbox_all_user_in_group(group, object) {
	if ($(object).is(":checked")) {
		show_all_user_in_group(group, $("#click_on_name_group_" + group), "stick_checkbox_all_user_in_group");
	} else {
		show_all_user_in_group(group, $("#click_on_name_group_" + group), "stick_checkbox_all_user_in_group_nochecked");
	}

}

function show_all_user_in_group(group, object, click_thong_bao) {
	var element;
	$("li a", "#UlChonUser").each(function() {

		// lay ra a.href dang active
		if ($(this).attr("class") == "active") {
			element = $(this);
		}

		$(this).removeClass('active');

	});
	$(object).addClass('active');

	var div = $("#data_click_group_user");

	if (!$(object).data('cache')) {
		$("#loading").show();
		$.ajax({
			url: "/admin/admin/users/users_search_on_window/" + group + "/" + "from_js",
			timeout: 15000,
			error: error_handler,
			success: function(html) {
				$("#loading").hide();

				// store to cache
				if ($.trim(div.html()) != "") {
					element.data('cache', div.html());
					$(object).data('cache', html)
				}

				// update new data
				div.html(html);

				check_user_thongbao_is_checked();

				// KIEM TRA CO PHAI GOI TU function stick_checkbox_all_user_in_group()
				if (click_thong_bao == "stick_checkbox_all_user_in_group") {
					var UserThongBaoTatca = $("#UserThongBaoTatca");
					if (!UserThongBaoTatca.is(":checked")) {
						UserThongBaoTatca.click();
					}
				} else if (click_thong_bao == "stick_checkbox_all_user_in_group_nochecked") {
					var UserThongBaoTatca = $("#UserThongBaoTatca");
					if (UserThongBaoTatca.is(":checked")) {
						UserThongBaoTatca.click();
					}
				}
			}
		});


	} else {

		div.html($(object).data('cache'));

		// CHECK AGAIN;
		check_user_thongbao_is_checked();

		// KIEM TRA CO PHAI GOI TU function stick_checkbox_all_user_in_group()
		if (click_thong_bao == "stick_checkbox_all_user_in_group") {
			var UserThongBaoTatca = $("#UserThongBaoTatca");
			if (!UserThongBaoTatca.is(":checked")) {
				UserThongBaoTatca.click();
			}
		} else if (click_thong_bao == "stick_checkbox_all_user_in_group_nochecked") {
			var UserThongBaoTatca = $("#UserThongBaoTatca");
			if (UserThongBaoTatca.is(":checked")) {
				UserThongBaoTatca.click();
			}
		}
	}

	return false;
}

function check_user_thongbao_is_checked() {
	var tmp_ob = "";
	var tmp = "";

	// THONG BAO
	// kiem tra check_all
	var check_all = true;
	$("td.TdThongBao", "#data_click_group_user").each(function() {

		tmp_ob = $("input:checkbox", this);
		tmp = tmp_ob.attr("id");
		tmp = tmp.replace("UserTb", "");

		if (data_thanhvien_thongbao[tmp] == tmp) {
			tmp_ob.attr("checked", true);
		} else {
			tmp_ob.attr("checked", false);
			check_all = false;
		}

	});
	if (check_all) {
		$("#UserThongBaoTatca", "#data_click_group_user").attr("checked", true);
	}

	// PHAN HOI
	check_all = true;
	$("td.TdPhanHoi", "#data_click_group_user").each(function() {

		tmp_ob = $("input:checkbox", this);
		tmp = tmp_ob.attr("id");
		tmp = tmp.replace("UserPh", "");

		if (data_thanhvien_phanhoi[tmp] != undefined) {
			tmp_ob.attr("checked", true);
		} else {
			tmp_ob.attr("checked", false);
			check_all = false;
		}

	});
	if (check_all) {
		$("#UserPhanHoiTatca", "#data_click_group_user").attr("checked", true);
	}
}

/*
function xoa_chon_user_giaoviec(id_span_user, contain)
{
if( contain != undefined )
{
	$("#span_id_user_giaoviec_" + id_span_user, "#" + contain).remove();
}else{
	$("#span_id_user_giaoviec_" + id_span_user).remove();
}

return false;
}
*/
function xoa_chon_user_giaoviec(object) {
	if (object == "all") {
		$("#span_id_user_giaoviec_all").remove();
	} else {
		$(object).parent("span").remove();
	}
}

// click len nhom thanh vien
/*
function toggle_plus_minus_data(group, object, contain)
{
var imgclass = $(object).attr("class");
if( imgclass == "PlusActive" )
{
	$(".MinusActive", "#" + contain).each(function(){
		$(this).attr("class", "PlusActive").attr("src", "/img/icon/toggle_plus.png");
	});
	$(object).attr("src", "/img/icon/toggle_minus.png");
	$(object).attr("class", "MinusActive");

	$("#loading").show();
	$.ajax({
		url: "/admin/users/users_search_on_window/" + group + "/" + "from_js",
		timeout: 15000,
		error: error_handler,
		success: function(html){
			$("#loading").hide();
			$("#data_click_group_user").html(html);
		}
	});
}else{
	$(object).attr("src", "/img/icon/toggle_plus.png");
	$(object).attr("class", "PlusActive");
}
}
*/
function gui_giaoviec_tatca_moinguoi() {
	data_thanhvien_thongbao = new Array();
	data_thanhvien_phanhoi = new Array();

	$("#users_search_giaoviec").html(
		'<span id="span_id_user_giaoviec_all"><b style="color:red">Gửi thông báo đến tất cả mọi người !</b>' +
		'<input type="hidden" value="1" name="data[Nguoinhanloinhan][check_thongbao_gv_all]">' +
		'&nbsp;&nbsp;<img style="cursor:pointer" src="/img/icon/toggle_delete.png" onclick="xoa_chon_user_giaoviec(' + "'all'" + ')" title="Bỏ" />' +
		'</span>'
	);
	$("#window_content_2").dialog('close');
}

// END GIAO VIEC

//var pagemouseX = 0; var pagemouseY = 0;
//function bind_calendarimage()
//{
//	$("body").mousemove(function(e){
//		pagemouseX = e.pageX; pagemouseY = e.pageY;
//	});
//}

// SỬ DỤNG ĐỂ CLICK CHỌN NHIỀU NGÀY CÙNG MỘT LÚC, trong file popcalendar.js
function get_date_main_js(day, month, year) {
	var table = $("#tableAddDay", "#window_content");
	if (table.attr("id") == undefined) return false;
	var model = $("#input_tmp_model", "#window_content").val();
	var index_key = day + "" + month + "" + year;
	var string = "<tr id='choose_day_" + index_key + "'><td><input type='hidden' name=data[" + model + "][choosengaynghi][" + index_key + "][] value='" + year + "-" + (month + 1) + "-" + day + "' >";

	var dk_dicongtac_id = $("#LoinhandicongtacNoidung", "#window_content").attr("id");

	var string_tmp1 = "Nghỉ ngày";
	var string_tmp2 = "Sáng và Chiều";
	var string_tmp3 = "Sáng";
	var string_tmp4 = "Chiều";
	if (dk_dicongtac_id == "LoinhandicongtacNoidung") {
		string_tmp1 = "Không check vân tay";
		string_tmp2 = "Vào và Ra";
		string_tmp3 = "Không bấm Vào";
		string_tmp4 = "Không bấm Ra";
	}

	string += string_tmp1 + ": <b>" + day + "/" + (month + 1) + "/" + year + "</b></td>";
	string += '<td><input type="radio" checked="checked" value="2" style="margin-left:10px;" id="' + model + 'Sanghoacchieu2' + index_key + '" name="data[' + model + '][choosengaynghi][' + index_key + '][]">';
	string += '<label style="font-weight:200" for="' + model + 'Sanghoacchieu2' + index_key + '">' + string_tmp2 + '</label>';
	string += '<input type="radio" value="0" style="margin-left:10px;" id="' + model + 'Sanghoacchieu0' + index_key + '" name="data[' + model + '][choosengaynghi][' + index_key + '][]">';
	string += '<label style="font-weight:200" for="' + model + 'Sanghoacchieu0' + index_key + '">' + string_tmp3 + '</label>';
	string += '<input type="radio" value="1" style="margin-left:10px;" id="LoinhannghiphepSanghoacchieu1' + index_key + '" name="data[' + model + '][choosengaynghi][' + index_key + '][]">';
	string += '<label style="font-weight:200" for="' + model + 'Sanghoacchieu1' + index_key + '">' + string_tmp4 + '</label></td>';
	string += '<td><a href="javascript:void(0)" onclick="$(\'#choose_day_' + index_key + '\').remove();return false;">Bỏ</a></td>';
	string += "</tr>";
	table.append(string);
	$("#pcIDcalendar").css("visibility", "visible");
	return false;
}

function change_calendar(event) {
	var $mydiv = $("#pcIDcalendar");
	var style = $mydiv.attr("style");
	$mydiv.attr("style", style + ";z-index:9999;").css({
		top: event.pageY,
		left: event.pageX
	});

	$mydiv = $("#pcIDselectMonth");
	style = $mydiv.attr("style");
	$mydiv.attr("style", style + ";z-index:9999;");

	$mydiv = $("#pcIDselectYear");
	style = $mydiv.attr("style");
	$mydiv.attr("style", style + ";z-index:9999;");
}

function tooltip() {
	$("a.tooltip").easyTooltip({
		useElement: "item"
	});
}


// bat su kien click
/*var target = $(this).attr( "target" );
if( target == "_blank" ){
window.open( href, target );
}else{
location.href = href;
}*/

function upload_iframe_window(div_update, tinymce) {
	// TSan_thanhly
	$("#window_content form").iframePostForm({
		iframeID: "TSan_iframe-post-form_window_content",
		post: function() {
			$("#loading").show();
		},
		complete: function(response) {
			$("#loading").hide();

			handle_response(response, div_update);

			upload_iframe_window(div_update);

			// $("#window_content").html(response);
		}
	});
}

function handle_response(html, div_update) {
	// nếu dữ liệu trả về thông báo ko có lỗi thì cập nhật vào div_update
	if ($("<div>").html(html).find("form").length > 0 && $("<div>").html(html).find("form").attr("id") != "search_data") {
		$("#window_content").html(html);

	} else {

		if (html == "ok") {

			location.reload(true);
			return false;

		} else {

			$("#" + div_update).html(html);
			$('#window_content').dialog("close");
		}

	}

	$("#loading").hide();
}

function handle_response_comment(html, div_update) {
	// nếu dữ liệu trả về thông báo ko có lỗi thì cập nhật vào div_update
	if ($("<div>").html(html).find("form").length > 0 && $("<div>").html(html).find("form").attr("id") != "search_data") {
		if (div_update.substr(0, 7) == "Comment") {
			$("#window_content").html(html);
		} else {

			$("#window_content_3").html(html);
		}

	} else {

		if (html == "ok") {

			location.reload(true);
			return false;

		} else {

			if (div_update.substr(0, 7) == "Comment") {
				$('#window_content').dialog("close");
			} else {
				$('#window_content_3').dialog("close");
			}
			$("#" + div_update).prepend(html);
			console.log("#" + div_update);
			// $('#window_content').dialog( "close" );
		}

	}

	$("#loading").hide();
}

//function xemthem_alltraloi_tasks(id_task, limit)
/*function xemthem_comments( id_next, model, model_id, div_update_more )
{
var url = "/comments/next_comments/" + id_next + '/' + model + '/' + model_id;

$("#loading").show();
$.ajax({
	url: url,
	timeout : 120000,
	success: function(html){

		// ==== div_update_more ===
		var div_update = "";
		if( div_update_more == undefined )
		{
			div_update = "#Comment_"+ model + "_" + model_id;
			$('#window_content').dialog( "close" );
		}else{
			div_update = "#" + div_update_more  + "Comment_"+ model + "_" + model_id;
			$('#window_content_3').dialog( "close" );
		}
		// end

		$(div_update).append(html);

		var link_view_more = $(div_update + "_link_view_more");
		var num_total = link_view_more.attr('rel2') - 3;

		if( num_total <= 0)
		{
			link_view_more.parent("div").remove();
		}else{
			var id_last = $( div_update + " > div:last").attr('rel_id');

			// ==== div_update_more ===
			if( div_update_more == undefined )
			{
				 link_view_more.attr("onclick", "xemthem_comments("+ id_last +", '" +model+ "', " + model_id + ")" );
			}else{
				 link_view_more.attr("onclick", "xemthem_comments("+ id_last +", '" +model+ "', " + model_id + ", '" + div_update_more + "')" );
			}
			// end


			link_view_more.html("xem thêm (" + num_total + ")");

			link_view_more.attr('rel2', num_total);
		}

		$("#loading").hide();

		var link_view_more_search_comment = $(div_update + "_link_view_more_" + "search_comment");
		link_view_more_search_comment.show();

	},error: error_handler
});
}*/

//function xemthem_alltraloi_tasks(id_task, limit)
// diult them va chinh sua de chuyen loi nhan (comment) sang loi nhawn quan trong
//function xemthem_comments( id_next, model, model_id, kiemtra,congtrinh_id, div_update_more )
function xemthem_comments(id_next, model, model_id, kiemtra, congtrinh_id, div_update_more) {
	if (kiemtra == 2) {
		var url = "/comments/next_comments_caolanh/" + id_next + '/' + model + '/' + model_id + '/' + kiemtra + '/' + congtrinh_id;
	} else {
		var url = "/comments/next_comments/" + id_next + '/' + model + '/' + model_id + '/' + kiemtra + '/' + congtrinh_id;
	}
	//var url = "/comments/next_comments/" + id_next + '/' + model + '/' + model_id + '/' + kiemtra + '/' + congtrinh_id;

	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 120000,
		success: function(html) {

			// ==== div_update_more ===
			var div_update = "";
			if (div_update_more == undefined) {
				div_update = "#Comment_" + model + "_" + model_id;
				if ($(div_update).attr("id") == undefined) {
					div_update = "xemthem_thongtin_Comment_Loinhan_" + model_id;
				}
				// $('#window_content').dialog( "close" );
			} else {
				div_update = "#" + div_update_more + "Comment_" + model + "_" + model_id;
				// $('#window_content_3').dialog( "close" );
			}
			// end
			console.log(div_update);
			$(div_update).append(html);

			var link_view_more = $(div_update + "_link_view_more");
			var num_total = link_view_more.attr('rel2') - 3;

			if (num_total <= 0) {
				link_view_more.parent("div").remove();
			} else {
				var id_last = $(div_update + " > div:last").attr('rel_id');

				// ==== div_update_more ===
				if (div_update_more == undefined) {
					link_view_more.attr("onclick", "xemthem_comments(" + id_last + ", '" + model + "', " + model_id + ", '" + congtrinh_id + "' ," + kiemtra + ")");
				} else {
					link_view_more.attr("onclick", "xemthem_comments(" + id_last + ", '" + model + "', " + model_id + ", '" + kiemtra + "', " + congtrinh_id + ", '" + div_update_more + "')");
				}
				/*if( div_update_more == undefined )
			{
				 link_view_more.attr("onclick", "xemthem_comments("+ id_last +", '" +model+ "', " + model_id + ")" );
			}else{
				 link_view_more.attr("onclick", "xemthem_comments("+ id_last +", '" +model+ "', " + model_id + ", '" + div_update_more + "')" );
			}*/
				// end


				link_view_more.html("xem thêm (" + num_total + ")");

				link_view_more.attr('rel2', num_total);
			}

			$("#loading").hide();

			var link_view_more_search_comment = $(div_update + "_link_view_more_" + "search_comment");
			link_view_more_search_comment.show();

		},
		error: error_handler
	});
}

function xemthem_comments_hienhet(id_next, model, model_id, kiemtra, congtrinh_id, div_update_more) {
	var url = "/comments/next_comments_all/" + id_next + '/' + model + '/' + model_id + '/' + kiemtra + '/' + congtrinh_id;
	
	//var url = "/comments/next_comments/" + id_next + '/' + model + '/' + model_id + '/' + kiemtra + '/' + congtrinh_id;

	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 120000,
		success: function(html) {

			// ==== div_update_more ===
			var div_update = "";
			if (div_update_more == undefined) {
				div_update = "#Comment_" + model + "_" + model_id;
				if ($(div_update).attr("id") == undefined) {
					div_update = "xemthem_thongtin_Comment_Loinhan_" + model_id;
				}
				// $('#window_content').dialog( "close" );
			} else {
				div_update = "#" + div_update_more + "Comment_" + model + "_" + model_id;
				// $('#window_content_3').dialog( "close" );
			}
			// end
			console.log(div_update);
			$(div_update).append(html);

			var link_view_more = $(div_update + "_link_view_more");
			var num_total = link_view_more.attr('rel2') - 3;

			if (num_total <= 0) {
				link_view_more.parent("div").remove();
			} else {
				var id_last = $(div_update + " > div:last").attr('rel_id');

				// ==== div_update_more ===
				if (div_update_more == undefined) {
					link_view_more.attr("onclick", "xemthem_comments(" + id_last + ", '" + model + "', " + model_id + ", '" + congtrinh_id + "' ," + kiemtra + ")");
				} else {
					link_view_more.attr("onclick", "xemthem_comments(" + id_last + ", '" + model + "', " + model_id + ", '" + kiemtra + "', " + congtrinh_id + ", '" + div_update_more + "')");
				}
				/*if( div_update_more == undefined )
			{
				 link_view_more.attr("onclick", "xemthem_comments("+ id_last +", '" +model+ "', " + model_id + ")" );
			}else{
				 link_view_more.attr("onclick", "xemthem_comments("+ id_last +", '" +model+ "', " + model_id + ", '" + div_update_more + "')" );
			}*/
				// end


				link_view_more.html("xem thêm (" + num_total + ")");

				link_view_more.attr('rel2', num_total);
			}

			$("#loading").hide();

			var link_view_more_search_comment = $(div_update + "_link_view_more_" + "search_comment");
			link_view_more_search_comment.show();

		},
		error: error_handler
	});
}
// ket thuc them code ngay 25/11/2013

function handle_response_listallcongtrinh(html, div_update, append) {
	// nếu dữ liệu trả về thông báo ko có lỗi thì cập nhật vào div_update
	if ($(".submit", html).length > 0 || $("input[type$='submit']", html).length > 0) {
		$("#window_content").html(html);
	} else if (append == undefined) {
		if (html == "ok") {
			$('#window_content').dialog("close");
			location.reload(true);
		} else {
			$("#" + div_update).html(html);
			$('#window_content').dialog("close");
		}
	} else { // còn không thì show dữ liệu thông báo lỗi trên popup window content
		$("#" + div_update).append(html);
		$('#window_content').dialog("close");
	}
	$("#loading").hide();
}

function handle_response_no_window(html, div_success, div_error) {
	// nếu dữ liệu trả về thông báo ko có lỗi thì cập nhật vào div_update
	if ($(html).find("#error_form").attr("class") != undefined) {
		$("#" + div_error).html(html);
	} else {
		$("#" + div_success).html(html);
	}
	$("#loading").hide();
}

function handle_response_giaoviec(html, controller, id) {
	// nếu dữ liệu trả về thông báo ko có lỗi thì cập nhật vào div_update
	if ($(".submit", html).length > 0 || $("input[type$='submit']", html).length > 0) {
		$("#window_content").html(html);
		/*$("#Giaoviec_"+id).fadeOut();
	if($("#content_thongbao_canhan table.CssTable tr:visible").length == 1)
	{
		$('#ThongBaoCaNhan').hide();
	}*/
	} else {
		if (controller == "ddatasks" || controller == "hntasks" || controller == "cnmttasks") {
			$(".LiTabs").each(function() {
				$(this).removeClass("ActiveTask");
			});

			if (controller == "ddatasks") {
				$("li.LiTabs:eq(1)").addClass("ActiveTask");
			} else if (controller == "hntasks") {
				$("li.LiTabs:eq(2)").addClass("ActiveTask");
			} else {
				$("li.LiTabs:eq(3)").addClass("ActiveTask");
			}
			controller = "tasks";
		}
		$("#" + controller).html(html);

		$('#window_content').dialog("close");
		$("#Giaoviec_" + id).fadeOut();
		if ($("#content_thongbao_canhan table.CssTable tr:visible").length == 1) {
			$('#ThongBaoCaNhan').hide();
		}
		$.scrollTo("#scroll_" + controller, 300, {
			offset: -90
		});
	}
	$("#loading").hide();
}

//diult them ngay 10/07/2014
function confirm_giaoviec_od(id, type_stick, loai) {
	var url = "/giaoviecs/confirm_giaoviec_od/" + id + "/" + type_stick + "/" + loai;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#Giaoviec_" + id).remove();

				if ($("#content_thongbao_canhan table.CssTable tr:visible").length == 0) {
					$('#ThongBaoCaNhanBtn').hide();
					$("#ThongBaoChiTiet").hide("slow");
				}
			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function confirm_giaoviec_send_all(id) {
	var url = "/giaoviecs/confirm_giaoviec_send_all/" + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#Giaoviec_" + id).remove();

				if ($("#content_thongbao_canhan table.CssTable tr:visible").length == 0) {
					$('#ThongBaoCaNhanBtn').hide();
					$("#ThongBaoChiTiet").hide("slow");
				}
			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function confirm_giaoviec_quantrong(id) {
	var url = "/giaoviecs/confirm_giaoviec_quantrong/" + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#Giaoviec_" + id).remove();

				if ($("#content_thongbao_canhan table.CssTable tr:visible").length == 0) {
					$('#ThongBaoCaNhanBtn').hide();
					$("#ThongBaoChiTiet").hide("slow");
				}
			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function confirm_comment_quantrong(id) {
	var url = "/giaoviecs/confirm_giaoviec_quantrong/" + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#Giaoviec_Quantrong_" + id).remove();
			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function confirm_quantrong_gobo(id) {
	var url = "/giaoviecs/confirm_quantrong_gobo/" + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#Giaoviec_" + id).remove();

				if ($("#content_thongbao_canhan table.CssTable tr:visible").length == 0) {
					$('#ThongBaoCaNhanBtn').hide();
					$("#ThongBaoChiTiet").hide("slow");
				}
			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function confirm_congtrinh_send_all(id) {
	var url = "/tamsteps/confirm_congtrinh_send_all/" + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#Thongbao_Congtrinh_" + id).remove();

				if ($("#content_thongbao_canhan table.CssTable tr:visible").length == 0) {
					$('#ThongBaoCaNhanBtn').hide();
					$("#ThongBaoChiTiet").hide("slow");
				}
			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function confirm_giaoviec_dalamxong(username, id) {
	var url = "/giaoviecs/confirm_giaoviec_dalamxong/" + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#confirm_dalamxong_" + id).html("<i>(đang chờ <b>" + username + "</b> tắt)</i>");
			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function confirm_giaoviec_stickxong(id, type_stick) {
	var url = "/giaoviecs/confirm_giaoviec_stickxong/" + id + "/" + type_stick;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				if (type_stick == 1) {
					$("#Giaoviec_" + id).fadeOut();
				} else {
					$("#nguoitao_confirm_dalamxong_" + id).html("<i>(chưa được phản hồi)</i>");
					$("#Giaoviec_" + id).attr("class", "RowYellowInform");
				}

			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function handle_response_upload_file_congtrinh(html) {
	if (html.substr(0, 10) == "<fieldset>") {
		$("#upload_file_congtrinh").html(html);
	} else {
		$("#historyuploadfiles").html(html);
	}
	$("#loading").hide();
}

function run_to_history_upload() {
	$.scrollTo("#historyuploadfiles", 300, {
		offset: -80
	});
	$("#upload_file_congtrinh").slideDown();
}

function checkbox_allgroup(value, class_add) {
	$(":checkbox." + class_add).attr("checked", value);
}

function checkbox_all(value, div_contain) {
	$(":checkbox", "#" + div_contain).attr("checked", value);
}

function open_search_user(div_update, group) {
	var url = "/users/users_search/" + div_update;
	if (group != undefined) url = url + "/" + group;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#window_content").html(html);
			window_show();
			$("#loading").hide();
		}
	});
}

function open_search_user_2(div_update, group) {
	var url = "/users/users_search_2/" + div_update;
	if (group != undefined) url = url + "/" + group;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#window_content_2").html(html);
			window_show_2();
			$("#loading").hide();
		}
	});
}

function chon_users_search(div_update, id) {
	var thutu = 0;
	var tmp = "";

	$("input", "#" + div_update).each(function() {
		if (thutu == 0) {
			$(this).val(id);
			thutu = 1;
		} else {
			tmp = $("#chon_user_" + id, "#window_content").html();
			$(this).val(tmp);
			$("#tieudeloinhan").val(tmp);
		}
	});
	$('#window_content').dialog("close");
}

function chon_users_search_th2(div_update, id) {
	var thutu = 0;
	$("input", "#" + div_update).each(function() {
		if (thutu == 0) {
			$(this).val(id);
			thutu = 1;
		} else {
			$(this).val($("#chon_user_" + id, "#window_content_2").html());
		}
	});
	$("#window_content_2").dialog('close');
}

function chon_congtrinh_search_2(div_update, id) {
	var thutu = 0;
	$("input", "#" + div_update).each(function() {
		if (thutu == 0) {
			$(this).val(id);
			thutu = 1;
		} else {
			$(this).val($("#chon_user_" + id, "#window_content_2").html());
		}
	});
	$('#window_content_2').dialog("close");
}

function tim_vitri(id, kothem_tinhthanh) {
	if (id.length < 1) return false;
	if (kothem_tinhthanh != undefined) id = id + "/0";

	var url = '/congtrinhs/vitri/' + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			$("#tim_vitri").html(html);
		}
	});
}

function get_quanhuyen(value, div_update) {
	if (value.length < 1) {
		if (div_update == undefined) {
			$("#update_quanhuyen").html("");
		} else {
			$("#" + div_update).html("");
		}
		return false;
	}

	var url = '/congtrinhs/get_quanhuyen/' + value;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (div_update == undefined) {
				$("#update_quanhuyen").html(html);
			} else {
				$("#" + div_update).html(html);
			}

			$("#loading").hide();
		}
	});
}

function window_search_congtrinh(input_update, object) {
	$("#loading").show();
	var url = "/congtrinhs/search_congtrinhs/" + input_update;

	var data = new Array();
	if (object != undefined) {
		data = $(object).parents('form:first').serialize();
	}

	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		type: 'post',
		data: data,
		success: function(html) {
			$("#window_content").html(html);
			window_show();
			$("#loading").hide();
		}
	});
}

function window_search_congtrinh_2(input_update, object) {
	$("#loading").show();
	var url = "/congtrinhs/search_congtrinhs_2/" + input_update;

	var data = new Array();
	if (object != undefined) {
		data = $(object).parents('form:first').serialize();
	}

	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		type: 'post',
		data: data,
		success: function(html) {
			$("#window_content_2").html(html);
			window_show_2();
			$("#loading").hide();
		}
	});
}

function sua_comment_ajax(id, model) {
	$("#loading").show();
	$.ajax({
		url: '/admin/comments/sua_comment_ajax/' + id + "/" + model,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			$("#window_content").html(html);
			window_show();
		}
	});
}

function xoa_ajax(model, id, model_cache) {
	if (!confirm("Bạn có chắc chắn muốn xóa không?")) return false;
	if (model_cache != undefined)
		model_cache = "/" + model_cache;
	else
		model_cache = "";
	$("#loading").show();
	$.ajax({
		url: '/admin/mains/xoa_ajax/' + model + "/" + id + model_cache,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "ok")
				$("#" + model + "_" + id).remove();
			else
				alert("Có lỗi trong quá trình xóa. Vui lòng bấm F5 và thao tác lại. Lỗi: " + html);
		}
	});
}

//diult them ngay 21/11/2013
function chon_quantrong(model, id, model_cache) {
	quantrong_chon = "quantrong";
	if (!confirm("Bạn có chắc chắn muốn chọn làm tin quan tọng không ?")) return false;
	if (model_cache != undefined) {
		model_cache = "/" + model_cache;
	} else {
		model_cache = "";
	}
	$("#loading").show();
	$.ajax({
		url: '/admin/mains/chon_quantrong/' + model + "/" + id + model_cache,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "ok") {
				$("#" + quantrong_chon + "_" + id).fadeOut();
				window.location.reload();
			} else {
				alert("Có lỗi trong quá trình xác nhận. Vui lòng bấm F5 và thao tác lại. Lỗi: " + html);
			}
		}
	});
} // diult ket thuc code ngay 21/12/2013
function mo_xoa_ajax(model, id, model_cache) {
	if (!confirm("Bạn có chắc chắn muốn mở khóa không?")) return false;
	if (model_cache != undefined) {
		model_cache = "/" + model_cache;
	} else {
		model_cache = "";
	}
	$("#loading").show();
	$.ajax({
		url: '/admin/mains/mo_xoa_ajax/' + model + "/" + id + model_cache,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "ok") {
				$("#" + model + "_" + id).fadeOut();
				//$("#"+model+"_"+id + "_click_unlock").hide();
				//$("#"+model+"_"+id + "_click_lock").show();
				//$("#"+model+"_"+id + "_img_lock").hide();
			} else {
				alert("Có lỗi trong quá trình xóa. Vui lòng bấm F5 và thao tác lại. Lỗi: " + html);
			}
			return false;
		}
	});
	return false;
}

function xemthem_alltraloi(model, model_loinhan, id_loinhan, limit) {
	var url = "/mains/listall_traloi/" + model + "/" + model_loinhan + "/" + id_loinhan;
	if (limit != undefined) {
		url = url + "/" + limit;
	}
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 120000,
		success: function(html) {
			var $model_traloi = $("#" + model + "_" + id_loinhan);
			$model_traloi.html(html);
			$("#loading").hide();
			$(".TRALOILOINHAN", "#" + model_loinhan + "_" + id_loinhan).toggle();
			$.scrollTo($model_traloi, 300, {
				offset: -90
			});
		},
		error: error_handler
	});
}

function tim_search(form) {
	if ($(form).attr("id") != undefined) {

		$("#loading").show();
		$(form).submit();
	}
}

function click_search(btn_search) {
	$("#loading").show();
	$("#" + btn_search).click();
}

function list_all_congtrinh_an_ko_thongke(id, trangthai) {
	if (!confirm("Bạn có chắc chắn không?")) return false;
	$("#loading").show();
	$.ajax({
		url: '/admin/congtrinhs/list_all_congtrinh_an_ko_thongke/' + id + "/" + trangthai,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html != "ok") {
				alert("Có lỗi trong quá trình xóa. Vui lòng bấm F5 và thao tác lại. Lỗi: " + html);
				return false;
			}

			if (trangthai == 5) { // ẩn đi
				$("#Congtrinh_" + id).fadeOut();
			} else if (trangthai == 4) { // hiện lại
				$("#Congtrinh_" + id + " td:first").html('<span style="color:green"><i>Hiện thành công</i></span>');
			}
		}
	});
}

function click_tabs_task(object) {
	if ($(object).hasClass("ActiveTask")) return false;
	$("#loading").show();
	$.ajax({
		url: $(object).attr("link"),
		timeout: 120000,
		error: error_handler,
		success: function(html) {
			$(".LiTabs").each(function() {
				$(this).removeClass("ActiveTask");
			});
			$(object).addClass("ActiveTask");
			$("#loading").hide();
			$("#tasks").html(html);
		}
	});
}

function click_tabs_tintuc(object,tmp) {
	$("#loading").show();
	$.ajax({
		url: $(object).attr("link"),
		timeout: 120000,
		error: error_handler,
		success: function(html) {
			$(".legend").each(function() {
				$(this).removeClass("ActiveTask_tt");
			});
			if(tmp == 2)
			{
				$(".quidinhs").addClass("ActiveTask_tt");
			}else{
				$(".tintucs").addClass("ActiveTask_tt");
			}
			$("#loading").hide();
			$("#tintucs_cover").html(html);
		}
	});
}

function show_xemnhanh_task() {
	$('.xemnhanh_detail_task').show();
}

function hide_xemnhanh_task() {
	$('.xemnhanh_detail_task').hide();
}

function show_xemnhanh() {
	//if($('#xemnhanh').attr('style') != 'display:none'){

	//$('#xemnhanh').hide();
	$('#xemnhanh_detail').show();
	$('#xemnhanh').attr("style", "color:#3B9865");
	//}
}

function hide_xemnhanh() {
	$('#xemnhanh_detail').hide();
	$('#xemnhanh').removeAttr("style");
	//$('#xemnhanh').show();
}

function get_avatar_gopy(object, link_name_image) {
	$("#emo img").removeClass("active");
	$(object).find("img").addClass("active");
	$("#TraloigopyvtAvatarLink").val("/img/smiley/" + link_name_image);
	$("#avatar_gopy_new").attr("src", "/img/smiley/" + link_name_image);
}

function send_comment_gopy(id) {

	var contain = $("#form_gopy_comment");
	var content = $("#TraloigopyvtNoidung", contain).val();
	if (content.length == 0) {
		alert("Vui lòng nhập nội dung bình luận.");
		$("#TraloigopyvtNoidung", contain).focus();
		return false;
	}
	$("#loading").show();
	$.ajax({
		url: "/admin/gopyvts/them_traloi/" + id,
		timeout: 15000,
		error: error_handler,
		type: "POST",
		data: {
			noidung: content,
			avatar_link: $("#TraloigopyvtAvatarLink", contain).val()
		},
		success: function(html) {
			$("#loading").hide();
			if (!html.indexOf("error_save")) {
				alert("Có lỗi trong quá trình gửi bình luận. Vui lòng bấm F5 và thao tác lại.");
			} else {
				var comment_gopy_all = $("#comment_gopy_all");
				comment_gopy_all.html(html);
				$("#TraloigopyvtNoidung", contain).val("");
				$.scrollTo(comment_gopy_all, 300, {
					offset: -90
				});
			}
		}
	});
}
/* ********************  MỞ KHUNG CHỌN NHỮNG NGƯỜI NÀO TĂNG CA */
function click_chon_user_tangca(group_id) {
	$("#loading").show();
	$.ajax({
		url: "/admin/loinhantangcavts/click_chon_user_tangca/" + group_id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#window_content").html(html);
				window_show();
			}
		}
	});
}

function click_chon_user_tangca_add_tr(id_tr) {
	$("#user_dangky_tangca").append("<tr>" + $("#tr_user_" + id_tr).html() + "</tr>");
	$("#user_dangky_tangca tr:last").find("input:hidden").attr("name", "data[Nguoinghiphep][" + $("table#user_dangky_tangca tr").length + "]");
	return false;
}

function xoa_chon_user_tangca(object_tr) {
	if ($("table#user_dangky_tangca tr").length > 1) {
		$(object_tr).parent("td").parent("tr").remove(); // tagName
		//$(object_tr).filter("tr").remove("slow");
	} else {
		alert("Phải có ít nhất một người tăng ca. Bạn có thể chọn thêm người khác rồi xóa người này.");
	}
	return false;
}
/*function duyet_tangca(id){
$("#loading").show();$.ajax({url: "/admin/loinhantangcavts/duyet_tangca/"+id,
	timeout: 15000,error: error_handler,
	success: function(html){
		$("#loading").hide();
		if(html == "error"){
			alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
		}else{
			$("#cover_duyet_tangca_"+id).html(html);
		}
	}
});
}*/
function duyet_tangca(id, duyet_hay_ko_duyet) {
	var td = 0;
	if (duyet_hay_ko_duyet == 1) {
		td = document.getElementById("LoinhantangcavtDanhgiatp" + id).value;
		if (td == '') {
			td = 0;
		}
	}
	$("#loading").show();
	$.ajax({
		url: "/admin/loinhantangcavts/duyet_tangca/" + id + "/" + td + "/" + duyet_hay_ko_duyet,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				//$("#cover_duyet_nghiphep_"+id).html(html);
				$("#Thongbao_Loinhantangcavt_" + id).remove();
			}
		}
	});
}

function click_user_duyet_tangca(id, user_id, username) {
	$("#loading").show();
	$.ajax({
		url: "/admin/loinhantangcavts/duyet_tangca/" + id + "/" + user_id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#cover_duyet_tangca_" + id).html(html);
			}
			$('#window_content').dialog("close");
		}
	});
}
/* KHÔNG DUYỆT TĂNG CA CỦA NGƯỜI THỨ 1 */
function ko_duyet_tangca1(id) {
	$("#loading").show();
	$.ajax({
		url: "/admin/loinhantangcavts/ko_duyet_tangca1/" + id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#cover_duyet_tangca_" + id).html(html);
			}
		}
	});
}

/* DUYỆT TĂNG CA CỦA NGƯỜI THỨ 2 */
function duyet_tangca2(id, user_id1, user_id2) {
	$("#loading").show();
	$.ajax({
		url: "/admin/loinhantangcavts/duyet_tangca2/" + id + "/" + user_id1 + "/" + user_id2,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#cover_duyet_tangca_" + id).html(html);
			}
		}
	});
}

function ko_duyet_tangca2(id, user_id1, user_id2) {
	$("#loading").show();
	$.ajax({
		url: "/admin/loinhantangcavts/ko_duyet_tangca2/" + id + "/" + user_id1 + "/" + user_id2,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#cover_duyet_tangca_" + id).html(html);
			}
		}
	});
}

/*
function click_user_bo_duyet_tangca(id){
$("#loading").show();$.ajax({url: "/admin/loinhantangcavts/bo_duyet_tangca/"+id,
	timeout: 15000,error: error_handler,
	success: function(html){
		$("#loading").hide();
		if(html == "error"){
			alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
		}else{
			$("#cover_duyet_tangca_"+id).html(html);
		}
	}
});
}*/

/* QUAN LY DAU THAU */
function qldt_chuyen_congty(value, controller) {
	location.href = "/" + controller + "/index/" + value;
}

function show_thongtinlienquan(myobject, update, index_show, contain) {
	var href = $(myobject).attr("href");
	$("#loading").show();
	$.ajax({
		url: href,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			$("#" + update).html(html); //.attr("style", "border: 1px solid #989898;width:100%");
			$(".ImageLineNgang img", "#" + contain).hide();
			if (index_show != undefined) {
				$("#NamNgang" + index_show).show();
				//$(".ImageLineNgang:eq("+index_show+") img").show();
			}
		}
	});
}

function handle_response_dauthauvt_maudon_upload(html) {
	if ($(".submit", html).length > 0 || $("input[type$='submit']", html).length > 0) {
		$("#don_maudon_upload").html(html);
	} else {
		$("#don_tatca_maudon").html(html);
		$("#file_uploadQueue", "#don_maudon_upload").html("");
		$("#QuanlydauthauvtsDondexuatGhichuFile", "#don_maudon_upload").val("");
	}
	$("#loading").hide();
}

/* Lịch công tác */
/*
function handle_response_lichcongtacs(html, div_update)
{
// nếu dữ liệu trả về thông báo ko có lỗi thì cập nhật vào div_update
if($(".submit", html).length > 0 || $("input[type$='submit']", html).length > 0)
{
	$("#window_content").html(html);
}else{
	var tmp = html.split("__.__"); // phân biệt công việc của ngươi nào
	var tmp1 = []; // [“xin chào”, 100, “bạn là ai thế?”, 0.911];
				   // {key1: value1, key2: value2, ...};
	for (var x in tmp){
		if(tmp[x].length > 0){
			tmp1 = tmp[x].split("__0.0__");
			if($("#"+tmp1[0]).html() == "Văn phòng công ty" )$("#"+tmp1[0]).html("");
			$("#"+tmp1[0]).append(tmp1[1]);
		}
	}
	$('#window_content').dialog( "close" );
	//$.scrollTo( "#"+div_update, 300, {offset:-90} );
}
$("#loading").hide();
}*/
function sua_lichcongtacs(id) {
	var url = "/lichcongtacs/sua/" + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			$("#window_content").html(html);
			window_show();
		}
	});
	return false;
}
//diult them cho lịch đào tạo
function sua_daotaochitiets(id, chinhanh) {
	var url = "/daotaochitiets/sua/" + id + "/" + chinhanh;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#window_content").html(html);
			window_show();
			$("#loading").hide();
		}
	});
	return false;
}

function sua_lichpkds(id) {
	var url = "/lichpkds/sua/" + id;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#window_content").html(html);
			window_show();
			$("#loading").hide();
		}
	});
	return false;
}

function xoa_tintucs(id) {
	if (!confirm("Bạn có chắc chắn muốn xóa không?")) return false;
	$("#loading").show();
	$.ajax({
		url: '/admin/tintucs/xoa/' + id,
		timeout: 120000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#tintucs_" + id).fadeOut();
			} else {
				alert("Có lỗi trong quá trình thực hiện. Vui lòng nhấn F5 thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

function duyet_nghiphep(id, duyet_hay_ko_duyet) {
	$("#loading").show();
	$.ajax({
		url: "/admin/loinhannghipheps/duyet_nghiphep/" + id + "/" + duyet_hay_ko_duyet,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#cover_duyet_nghiphep_" + id).html(html);
			}
		}
	});
}

function click_submit_from(object, div_update) {
	$("#loading").show();
	var url = $(object).attr("href");
	var data = $(object).parents('form:first').serialize();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		type: 'post',
		data: data,
		success: function(html) {
			if (div_update == "add_tr") // thêm tr vào bảng
			{
				$("#add_tr_submid_ajax tr:last").remove();
				$("#add_tr_submid_ajax").append(html);
			} else {
				$("#" + div_update).html(html);
			}
			$("#loading").hide();
		}
	});
	return false;
}

function click_them_qldt_nhansu(object, div_update) {
	$("#loading").show();
	var url = $(object).attr("href");
	var data = $(object).parents('form:first').serialize();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		type: 'post',
		data: data,
		success: function(html) {
			var tmp = html.split("_._");
			$("#add_tr_submid_ajax tr:last").remove();
			$("#add_tr_submid_ajax").append(html);
			$("#loading").hide();
		}
	});
	return false;
}

function show_hide_themmoi_loinhan_main_index(type) {
	if (type == "show") {
		$("#id_themmoi").hide();
		$("#id_form_them").slideDown();
	} else {
		$("#id_form_them").hide();
		$("#id_themmoi").show();
	}
	return false;
}

function show_hide_themmoi_ctr_main_index(type) {
	if (type == "show") {
		$("#id_themmoi", "#NhatKyCongTruong").hide();
		$("#id_form_them", "#NhatKyCongTruong").slideDown();
	} else {
		$("#id_form_them", "#NhatKyCongTruong").slideUp(function() {
			$("#id_themmoi", "#NhatKyCongTruong").show();
		});
	}
	return false;
}

function them_loinhan(object) {
	$("#loading").show();
	var url = $(object).attr("href");

	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		type: "post",
		data: $(object).parents("form:first").serialize(),
		success: function(html) {
			$("#tasks").html(html);

			$.scrollTo("#tasks", 300, {
				offset: -90
			});
			$("#loading").hide();
		}
	});
	return false;
}

function xem_them_main_search(object, model) {
	$("#loading").show();
	$.ajax({
		url: "/admin/mains/search_more/" + model,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "error") {
				alert("Xem thêm tìm kiếm thất bại. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#table_main_search").append(html);
				$(object).remove();
				$.scrollTo("#model_more_" + model, 300, {
					offset: -90
				});
			}
			$("#loading").hide();
		}
	});
	return false;
}

// BangTinhCong
function send_thong_bao(object, id) {
	$("#loading").show();
	var url = "/bangtinhcongs/send_thong_bao/" + id;
	var data = $(object).parents('form:first').serialize();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		type: 'post',
		data: data,
		success: function(html) {
			var tmp = html.split("_._");
			if (tmp[0] == "ok") {
				$("#update_send_ok_" + id).html(tmp[1]);
			} else {
				alert("Error: có lỗi xảy ra, error=" + html);
			}
			$("#loading").hide();
		}
	});
}

function xoa_bangtinhcongfile(id, thangnam) {
	if (!confirm("Bạn có chắc chắn muốn xóa bảng lương bên dưới không?")) return false;
	$("#loading").show();
	var url = "/bangtinhcongs/xoa_bangtinhcongfile/" + id + "/" + thangnam;
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html == "ok") {
				$("#button_submit_search").click();
				return false;
			}

			var tmp = html.split("_._");
			if (tmp[0] == "ok") {
				$("#upload_bangtinhcong").html(tmp[1]);
			} else {
				alert("Error: có lỗi xảy ra, error=" + html);
			}
			$("#loading").hide();
		}
	});
}

function chinhsua_hoanthanh(id, hoanthanh) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congviecpkds/chinhsua_hoanthanh/" + id + "/" + hoanthanh,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#nhatkycongtruongs_main_index").html(html);
			}
			$("#loading").hide();
		}
	});
	return false;
}

function poll_gopy() {
	if ($("#poll-results").length > 0) {
		animateResults();
	}
}

function animateResults() {
	$("#result-table-poll .div-result-poll").each(function() {
		var percentage = $(this).attr("title");
		$(this).css({
			width: "0%"
		}).animate({
			width: percentage
		}, 1000);
	});
}

function animateProgress(div_id_wrapper) {
	$("#" + div_id_wrapper + " .progress-result").each(function() {
		var percentage = $(this).attr("title");
		$(this).css({
			width: "0%"
		}).animate({
			width: percentage
		}, 1000);
	});
}

function disable_double_click_download(object) {
	var href = $(object).attr("href");
	if (href == "javascript:void(0)") return false;
	location.href = href;
	$(object).attr("href", "javascript:void(0)");
	var timer = null;
	timer = window.setTimeout(function() {
		$(object).attr('href', href);
		window.clearTimeout(timer);
	}, 5000);
}
// NHAT KY CONG TRUONG
function show_hide_nhatkycongtruong(object, id_congtrinnh) {
	if ($(object).attr("src") == "/img/icon/toggle_plus.png") {
		$(object).attr("src", "/img/icon/toggle_minus.png")
	} else {
		$(object).attr("src", "/img/icon/toggle_plus.png")
	}
	$("#data_nhatkycongtrinh_" + id_congtrinnh).slideToggle();
}

function click_tabs_inside_ddatask(object) {
	if ($(object).hasClass("ActiveTask")) return false;
	$("#loading").show();
	$.ajax({
		url: $(object).attr("link"),
		timeout: 120000,
		error: error_handler,
		success: function(html) {
			$(".LiTabs", "#tasks").each(function() {
				$(this).removeClass("ActiveTask");
			});
			$(object).addClass("ActiveTask");
			$("#loading").hide();
			$("#update_tab_ddatasks").html(html);
		}
	});
}

function them_congtrinh_nkct(controller, loai_giamsat_tn) {
	var id_congtrinh = $("#id_congtrinh", "#NhatKyCongTruong").val();
	if (id_congtrinh == undefined) alert("Vui lòng chọn công trình trước khi bấm Thêm Công Trình");
	$("#loading").show();
	// $("input[name='data[Ddatask][loai_giamsat_tn]']:checked").val()
	$.ajax({
		url: "/admin/nhatkycongtruongs/them_congtrinh/" + controller + "/" + id_congtrinh + "/" + loai_giamsat_tn,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "error") {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			} else {
				$("#tasks").html(html);
			}
			$("#loading").hide();
		}
	});
	return false;
}

function loc_loinhan_theonhom(object, controller) {
	$("#loading").show();
	$.ajax({
		url: "/admin/" + controller + "/danhsachloinhan/0/" + $(object).val(),
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loinhannhansuhanhchanhs").html(html);
			$("#loading").hide();
		}
	});
	return false;
}

function hien_nguoinhan_giaoviec(id) {
	$("#hien_nguoinhan_giaoviec_" + id).html($("#hien_nguoinhan_giaoviec_tmp_" + id).html());
	return false;
}

function hien_noidung_giaoviec(id) {
	$("#hien_noidung_giaoviec_" + id).html($("#hien_noidung_giaoviec_tmp_" + id).html());
	return false;
}

function xem_giaoviec_xacnhan(stick, id) {
	$("#loading").show();
	$.ajax({
		url: "/admin/giaoviecs/xem_giaoviec_xacnhan/" + id + "/" + stick,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#xem_giaoviec_xacnhan_" + id).html(html);
			$("#loading").hide();
		}
	});
	return false;
}

function ketoan_sua_tile_chiphi(field, id, id_chiphi) {
	$("#loading").show();

	$("#window_content").html("<div class=\"input text ChiPhi\"><b>Nhập giá trị </b><br>" +
		"<input type=\"text\" size=\"20\" id=\"value_from_client\" " +
		" value=\"" + $("#" + field + "_" + id).html() + "\">" +
		"<input type=\"hidden\" value=\"" + id + "\" id=\"congtrinh_id\" />" +
		"<input type=\"hidden\" value=\"" + id_chiphi + "\" id=\"id_chiphi\" />" +
		"<br><br><center>" +
		"<input onclick=\"ketoan_sua_tile_chiphi_save('" + field + "', '" + id + "', '" + id_chiphi + "')\" class=\"Button\" value=\"Lưu\" style=\"width:20px\" /></center>" +
		"</div>");

	$("#loading").hide();
	window_show();

	$('#ov_contain').css({
		top: $("#gc_vattu_685").position().top,
		left: ($("#gc_vattu_685").position().left - $(window).width())
	});
}

function ketoan_sua_tile_chiphi_save(field, id, id_chiphi) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs/list_all_congtrinh_save_ketoan_chiphi/" + field + "/" + id + "/_" + id_chiphi + "_/" + $("#value_from_client").val(),
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#" + field + "_" + id).html(html);
			$("#loading").hide();
			$('#window_content').dialog("close");
		}
	});
	return false;
}

function list_all_congtrinh_chon_users(id_congtrinh, user_id) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs/list_all_congtrinh_nguoilayve/" + id_congtrinh + "/" + user_id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html != "ok") alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			$("#Nguoilayve_" + id_congtrinh).html($("#list_all_congtrinh_nguoilayve_getuser_" + user_id).html());
			$("#loading").hide();
			window_show();
		}
	});
	return false;
}

function click_like(model, id, count_like, type, controller) {
	$("#loading").show();

	if (controller == undefined) controller = "loinhanytuongs";

	$.ajax({
		url: "/admin/" + controller + "/click_like/" + model + "/" + id + "/" + type,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html == "ok") {
				if (type == 1) // like
				{
					html = "<span class=\"LikeCount\">Có <span class=\"NumCount\">" + (count_like + 1) + "</span> người thích</span> <a title=\"Bỏ thích nội dung này\" class=\"ClickLike\" href=\"javascript:void(0)\" onclick=\"click_like('" + model + "', " + id + "," + (count_like + 1) + ", 0,'" + controller + "')\">Bỏ thích</a>";
				} else {
					html = "<span class=\"LikeCount\">Có <span class=\"NumCount\">" + (count_like - 1) + "</span> người thích</span> <a title=\"Thích nội dung này\" class=\"ClickLike\" href=\"javascript:void(0)\" onclick=\"click_like('" + model + "', " + id + "," + (count_like - 1) + ", 1,'" + controller + "')\">Thích</a>"
				}
				$("#" + model + "_liked_" + id).html(html);
			} else {
				alert("Có lỗi hệ thống xảy ra. Vui lòng F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
	return false;
}

function xem_chitiet_sangtao(id, object_xem) {
	$("#xem_chitiet_sangtao_" + id).slideToggle(function() {
		if ($(object_xem).text() == "xem") {
			$(object_xem).text("thu lại");
		} else {
			$(object_xem).text("xem");
		}
	});
}

/*

for (i = 0; i < 1000; i++) {
var myList = $('.myList');
myList.append('This is list item ' + i);
}

*/

function show_thongtin_loinhan_main(controller, model) {
	var contain = $("#" + controller + "_hidethongtin");
	var div_data = $("#" + controller, contain);

	var url = "/mains/loinhan_danhsachloinhan/" + model;

	if (div_data.html().length > 1) {
		contain.hide().slideDown();
	} else {
		$("#loading").show();

		$.ajax({
			url: url,
			timeout: 15000,
			error: error_handler,
			type: 'GET',
			success: function(html) {
				div_data.html(html);
				$("#loading").hide();

				$("#" + controller + "_SpanMenuHorizon").remove();
				contain.slideDown();
			}
		});
	}
}

function show_thongtin_loinhan(controller) {
	var contain = $("#" + controller + "_hidethongtin");
	var div_data = $("#" + controller, contain);

	var url = "/" + controller + "/danhsachloinhan";
	if (controller == "historyuploadfiles") {
		url = "/uploaders/history";
	}

	if (div_data.html().length > 1) {
		contain.hide().slideDown();
	} else {
		$("#loading").show();

		$.ajax({
			url: url,
			timeout: 15000,
			error: error_handler,
			type: 'GET',
			success: function(html) {
				div_data.html(html);
				$("#loading").hide();

				$("#" + controller + "_SpanMenuHorizon").remove();
				contain.slideDown();
			}
		});
	}
}

function open_my_file_upload() {
	$("#loading").show();
	window.open('/uploaders/open_my_file', 'Danh Sách File Tôi Upload', 'location=no,status=no,scrollbars=no,width=800,height=444');
}

// iframe old, ko dung nua
/*
function chon_attach_file_nay(id_file)
{
//window.parent.document
//popup_window = window.open("");
//.....
//popup_window.close ();

if( $("#contain_attach_file", window.opener.document).attr('id') == undefined )
{
	alert('Không tìm thấy mục để attach file vào. Vui lòng thao tác lại.');
	javascript:self.close();
}

$("#contain_attach_file", window.opener.document).append( $("#chon_attach_file_nay_td_" + id_file).html()  );
$("#chon_attach_file_nay_td_click_" + id_file).html("<center><em>(đã chọn)</em></center>");
}
*/
// new window
function chon_attach_file_nay(id_file, object) {
	// opener_contain_attach_file = parent.$("#contain_attach_file"); // khong dung iframe nua
	var opener_contain_attach_file = $("#contain_attach_file");

	if ($(object).is(":checked")) {
		// kiem tra xem nguoi dung da chon file nay chua
		var tmp = $("#chon_attach_file_nay_" + id_file, opener_contain_attach_file);
		if (tmp.attr("id") == undefined) {
			$(opener_contain_attach_file).append($("#chon_attach_file_nay_div_" + id_file).html());
		}

	} else {

		// xoa bo file attach
		$("#chon_attach_file_nay_" + id_file, opener_contain_attach_file).remove();
	}

	return false;
}

function xoa_chon_attach_file(id_file) {
	$("#chon_attach_file_nay_" + id_file, "#contain_attach_file").remove();
	return false;
}

function check_morong() {
	var div_content = $(".content", "#window_content");

	var div_morong = $("#morong", "#window_content");

	if ($("#window_content").height() > 600 && div_content.height() > 100) {
		//div_content.css({"height":"50","overflow":"hidden"});
	} else {

		// div_morong.remove();
	}

	div_morong.remove();
	// $( "#morong a", "#window_content" ).show();

	// $( "a#xemthem", div_morong).toggle(
	// 	 function(){
	// 		 $(this).text("... Thu gọn ...");
	// 		 div_content.css({"height":"auto","overflow":"visible"});
	// 	 },
	// 	 function(){
	// 		 $(this).text("... Mở rộng...");
	// 		 div_content.css({"height":"50","overflow":"hidden"});
	// 	 }
	// );

	return true;
}

function congtrinhs_congviecs_homnay(date, sangchieu) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_congviecs/congviec_moingay/" + date + '/' + sangchieu,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#window_content_2").html(html);
			window_show_2();
			$("#loading").hide();
		}
	});
}

function congtrinhs_congviecs_them(date, sangchieu) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_congviecs/them/" + date + '/' + sangchieu,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#window_content").html(html);
			window_show();
			$("#loading").hide();
		}
	});
}

function congtrinhs_congviecs_them_homnay(sangchieu) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_congviecs/them_homnay/" + sangchieu,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			var tmp = $("#them_moi", "#window_content_4");
			tmp.hide();
			tmp.html(html).show();
			$("#loading").hide();
		}
	});
}

function show_edit_hai_long_kh_save(id) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_congviecs/show_edit_hai_long_kh/" + id + "/" + $("#CongtrinhsCongviecHaiLongKh_" + id).val(),
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#loading").hide();

			var td = $("#TdEdithai_long_kh_" + id);
			td.attr("rel", 0);

			td.html(html);
		}
	});

	return false;
}

function show_edit_hai_long_kh(id, hai_long_kh) {
	var td = $("#TdEdithai_long_kh_" + id);
	if (td.attr("rel") == 1) {
		return false;
	}

	var text_tmp = td.text();

	var tmp = '<select style="width:43px" id="CongtrinhsCongviecHaiLongKh_' + id + '" onchange="show_edit_hai_long_kh_save( ' + id + ' )">';

	if (text_tmp == "A") {
		tmp += '<option value="1" selected="selected">A</option>';
	} else {
		tmp += '<option value="1">A</option>';
	}

	if (text_tmp == "B") {
		tmp += '<option value="2" selected="selected">B</option>';
	} else {
		tmp += '<option value="2">B</option>';
	}

	if (text_tmp == "C") {
		tmp += '<option value="3" selected="selected">C</option>';
	} else {
		tmp += '<option value="3">C</option>';
	}

	if (text_tmp == "Không hài lòng") {
		tmp += '<option value="4" selected="selected">Không hài lòng</option>';
	} else {
		tmp += '<option value="4">Không hài lòng</option>';
	}

	tmp += '</select>';

	td.attr("rel", 1).html(tmp);

	return false;
}

function show_edit(id, danhgia, type) {

	var td = $("#TdEditDanhgia_" + id + "_" + type);
	if (td.attr("rel") == 0) {
		var txt = td.html();
		var string = '<input type="text" id="tmp_id" style="width:35px;" val="" onblur="close_edit( ' + id + ', this,' + type + ' )">';

		td.html(string);

		td.attr("rel", 1);

		var td_input = $("#tmp_id", td);
		td_input.val(txt);
		td_input.focus();
		td_input[0].setSelectionRange(0, 0);
		//td_input.focus().val(td_input.val());

		td_input.bind('keypress', function(e) {
			if (e.keyCode == 13) {
				td_input.unbind('keypress');
				close_edit(id, td_input, type);
			}
		});
	}
	return false;
}

function close_edit(id, object, type) {

	var td_input_value = $(object).val();
	td_input_value = parseInt(td_input_value.replace("%", ""))

	if (td_input_value.length == 0) td_input_value = "0";


	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_congviecs/save_edit_danhgia/" + id + "/" + type + "/" + td_input_value,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			if (html == "ok") {
				var td = $("#TdEditDanhgia_" + id + "_" + type).attr("rel", 0);
				td.html(td_input_value);

				if (td_input_value > 0)
					$(".CTCV_bo_qua", "#CongtrinhsCongviec_" + id).show();
				else
					$(".CTCV_bo_qua", "#CongtrinhsCongviec_" + id).hide();
			} else
				alert("Có lỗi trong quá trình Lưu, bạn vui lòng bấm F5 và thao tác lại. Cám ơn.");
		}
	});

	return false;
}

function tao_thao_luan(congtrinhs_congviecs_id, user_id) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_congviecs/show_danhgia_comments_of_user/" + user_id + "/" + congtrinhs_congviecs_id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#loading").hide();
			$("#CMRomchatrieng_" + user_id).html(html);
		}
	});

	return false;
}

function get_info_congtrinhs_congviecs_danhgia() {
	$.ajax({
		url: "/admin/congtrinhs_congviecs/show_danhgia",
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html == "hide_ROM") {
				$("#show_ROM").remove();
				return false;
			} else if (html == "no_data") {
				return false;
			} else {
				$("#window_content").html(html);
				window_show();
			}

		}
	});
}

function congtrinhs_congviecs_run_change_phongban(object) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_congviecs/danhsach_dongdoi/" + $(object).val(),
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#window_content").html(html);
			$("#loading").hide();
			window_show();
		}
	});
}
// END CONG TRINH-CONG VIEC

// uploaders/history
function search_uploaders_history() {
	$("#loading").show();
	$.ajax({
		url: "/admin/uploaders/history/" + $("#UploaderUsername").val(),
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#historyuploadfiles").html(html);
			$("#loading").hide();
		}
	});
}

// danhgianhanviendiems/danhgia
function dgnvd_show_edit(k, type) {
	if (type == "mark1") {
		var tong = $("#tongNV");
	} else if (type == "mark2") {

		var tong = $("#tongQL");
	} else {

		var tong = $("#tongNV");
	}

	var td = $("#TdDgnvd_" + k + type);

	if (td.attr("rel") == 0) {
		var txt = $.trim(td.html());

		if (txt != "") {
			tong.html((parseInt(tong.html()) - parseInt(txt)));
		}

		td.html('<input type="text" id="tmp_id" style="width:35px;" value="' + txt + '" onblur="dgnvd_close_edit( ' + $("#id_Dgnvd_" + k).val() + ',' + k + ', this,' + "'" + type + "'" + ' )">');

		td.attr("rel", 1);
		var td_input = $("#tmp_id", td);
		td_input.focus();
	}

	return false;
}

function dgnvd_close_edit(id, k, object, type) {
	var td_input_value = $(object).val();

	if (type == "mark1") {
		var tong = $("#tongNV");
	} else if (type == "mark2") {

		var tong = $("#tongQL");
	} else {

		var tong = $("#tongNV");
	}

	var td = $("#TdDgnvd_" + k + type);

	if (td_input_value == "") {
		td.attr("rel", 0);
		td.html("");
		return false;
	}

	$("#loading").show();
	$.ajax({
		url: "/admin/danhgianhanviendiems/save_edit_danhgia/" + id + "/" + td_input_value + "/" + k + "/" + $("#DanhgianhanviendiemThangMonth").val() + "/" + $("#DanhgianhanviendiemNamYear").val() + "/" + type + "/" + $("#userid").val(),
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#loading").hide();

			if (html.length > 3) {
				var getData = $.parseJSON(html);

				if (getData[0] == "ok") {
					td.html(td_input_value);
					td.attr("rel", 0);
					$("#id_Dgnvd_" + k).val(getData[1]);

					tong.html((parseInt(tong.html()) + parseInt(td_input_value)));

				} else {
					alert("Có lỗi trong quá trình Lưu, bạn vui lòng bấm F5 và thao tác lại. Cám ơn.");
				}
			} else {
				td.attr("rel", 0);
				td.html("");
			}
		}
	});

	return false;
}

function thongtin_dgnvd_show_edit(id_div, id_thongtin, field) {
	var div = $("#" + id_div);

	if (div.attr("rel") == 0) {
		var txt = br2nl_js($.trim(div.html()));
		var width = div.width() - 14;

		var textarea = '<textarea id="tmp_id" style="width:' + width + 'px" textarea cols="80" rows="5" onblur="thongtin_dgnvd_close_edit( ' + "'" + id_div + "'" + ',' + id_thongtin + ',' + "'" + field + "'" + ', this )"></textarea>';
		if (txt[3] != ".") {
			textarea = '<textarea id="tmp_id" style="width:' + width + 'px" textarea cols="80" rows="5" onblur="thongtin_dgnvd_close_edit( ' + "'" + id_div + "'" + ',' + id_thongtin + ',' + "'" + field + "'" + ', this )">' + txt + '</textarea>';
		}

		div.html(textarea);

		div.attr("rel", 1);
		$("#tmp_id").focus();
	}

	return false;
}

function thongtin_dgnvd_close_edit(id_div, id_thongtin, field, object) {
	var div_textarea_value = $.trim($(object).val());

	var div = $("#" + id_div);

	/*
if( div_textarea_value == "" )
{
	div.attr("rel", 0);
	div.html("....... ........ ..........<br>....... ........ ........");
	return false;
}
*/
	$("#loading").show();
	$.ajax({
		url: "/admin/danhgianhanviendiems/save_edit_danhgia_thongtin/" + id_thongtin + "/" + $("#DanhgianhanviendiemThangMonth").val() + "/" + $("#DanhgianhanviendiemNamYear").val() + "/" + field + "/" + $("#userid").val(),
		timeout: 15000,
		error: error_handler,
		type: "POST",
		data: {
			content: div_textarea_value
		},
		success: function(html) {

			$("#loading").hide();
			if (html == "ok") {
				div.html(nl2br_js(div_textarea_value));
				div.attr("rel", 0);
			} else {
				alert("Có lỗi trong quá trình Lưu, bạn vui lòng bấm F5 và thao tác lại. Cám ơn.");
			}
		}
	});

	return false;
}

function nl2br_js(myString) {
	return myString.replace(/\n/g, '<br />');
}

function br2nl_js(myString) {
	myString = myString.replace(/\n/g, '');
	return myString.replace(/<br>/g, '\n');
}

// mains/index
function TableChucNang_active(object, table_chucnang) {
	$("table#" + table_chucnang + " td").removeClass("Active");
	$("#" + object).parent("td").addClass("Active");

}

function task_load_hd(object, congtrinh_id) {
	if ($(object).attr("loaded") == "1") {
		return false;
	}

	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs/task_load_hd/" + congtrinh_id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#loading").hide();

			$(object).attr("loaded", 1);

			var div_showthongtin = $(".ShowThongTin", object);
			div_showthongtin.html(html + div_showthongtin.html());

			return false;
		}
	});

	return false;
}

function task_load_so_nguoi_thamgia(object, congtrinh_id) {
	if ($(object).attr("loaded") == "1") {
		$(".ShowThongTin", object).show();
		return false;
	}

	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_congviecs/task_load_so_nguoi_thamgia/" + congtrinh_id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#loading").hide();

			$(object).attr("loaded", 1);

			var div_showthongtin = $(".ShowThongTin", object);
			div_showthongtin.html(html).show();

			return false;
		}
	});

	return false;
}

// ======= Upload Ajax Element ==================
function remove_file_upload_ajax(object, file_name) {
	if (!confirm("Bạn có chắc chắn muốn xóa file: '" + file_name + "'")) {
		return false;
	}
	$("#loading").show();
	$.ajax({
		url: "/admin/uploaders/remove_file_upload_ajax",
		timeout: 15000,
		type: "POST",
		data: {
			file: file_name
		},
		error: error_handler,
		success: function(html) {

			$("#loading").hide();

			if (html == "ok") {
				$(object).parent("li").remove();
			} else {
				alert("Có lỗi trong quá trình xóa. Error:" + html);
			}

		}
	});

	return false;
}

function list_all_congtrinh_chinhsua_goi(functionvt, id) {
	if (functionvt == "heso" || functionvt == "name_hopdong" || functionvt == "name_hopdong" || functionvt == "giatrihopdong" || functionvt == "giatriquyettoan" || functionvt == "giatriquyettoan") {
		$("#loading").show();
		$.ajax({
			url: "/admin/congtrinhs/task_load_thongtin_congtrinh_sua/" + id,
			timeout: 15000,
			error: error_handler,
			success: function(html) {

				$("#loading").hide();
				$("#window_content").html(html);
				window_show();
			}
		});
	} else {

		$("#loading").show();
		$.ajax({
			url: "/admin/congtrinhs/list_all_congtrinh_" + functionvt + "/" + id,
			timeout: 15000,
			error: error_handler,
			success: function(html) {

				$("#loading").hide();
				$("#window_content").html(html);
				window_show();
			}
		});
	}


}

function show_nhap_ngay_tienve(id) {
	$("#loading").show();
	$.ajax({
		url: '/admin/congtrinhs/vao_list_theo_doi_ngay/' + id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#loading").hide();
			$("#window_content").html(html);
			window_show();
		}
	});
}

function vao_list_theo_doi_nga_an_theo_doi(id) {
	if (!confirm("Bạn có chắc chắn muốn ẩn không?")) return false;

	$("#loading").show();
	$.ajax({
		url: '/admin/congtrinhs/vao_list_theo_doi_nga_an_theo_doi/' + id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#loading").hide();

			if (html == "ok") {
				$("#tr_theo_doi_hd_" + id).fadeOut();
			} else {
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
		}
	});
}

function task_load_thongtin_congtrinh(id) {
	var tmp = $("#div_task_load_thongtin_congtrinh_" + id);

	if (tmp.html() != "") {
		return false;
	}

	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs/task_load_thongtin_congtrinh/" + id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#loading").hide();
			tmp.html(html);

		}
	});
}

function fix_textarea_size(id_textarea) {
	setTimeout('$("#' + id_textarea + '_textarea_id_ifr", "#window_content").attr("style", "width: 522px; height: 100px; display: block;");', 1500)
}
//diult chuong trinh smiles
function window_search_congtrinh_smiles(input_update, ngonngu, object) {

	$("#loading").show();
	var url = "/congtrinhs/search_congtrinhs_smiles/" + input_update + "/" + ngonngu;

	var data = new Array();
	if (object != undefined) {
		data = $(object).parents('form:first').serialize();
	}

	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		type: 'post',
		data: data,
		success: function(html) {
			$("#window_content").html(html);
			window_show();
			$("#loading").hide();
		}
	});
}

function chon_congtrinh_search_smiles(div_update, ngonngu, id) {
	if (ngonngu == 1) {
		var thutu = 0;
		$("input", "#" + div_update).each(function() {
			if (thutu == 0) {
				$(this).val(id);
				thutu = 1;
			} else {
				$(this).val($("#chon_user_" + id, "#window_content").html());
			}
		});
		$('#window_content').dialog("close");
	} else {

		if ($("#chon_user_" + id, "#window_content").html() == '') {
			$("#loading").show();
			var url = "/congtrinhs/update_tenanhvan/" + id;
			//var $soluong = $( '#XuathanghoavtSoluong', $contain );
			$.ajax({
				url: url,
				timeout: 15000,
				error: error_handler,
				type: 'post',
				success: function(html) {
					$("#window_content").html(html);
					window_show();
					$("#loading").hide();
				}
			});
		} else {
			var thutu = 0;
			$("input", "#" + div_update).each(function() {
				if (thutu == 0) {
					$(this).val(id);
					thutu = 1;
				} else {
					$(this).val($("#chon_user_" + id, "#window_content").html());
				}
			});
		}
		$('#window_content').dialog("close");
	}
}

function HB_update_tinnhan() {
	var contain = $("#HB_window");
	if (contain.attr("id") != undefined) {
		$("#HB_footer img", "#HB_window").attr("src", "/img/HB/drop_icon.jpeg").attr("title", "Xem tin nhắn");
		$("#tinnhan", "#HB_window").addClass("active");
		$("#tinnhan", "#HB_window").fadeIn();
	}
}

function chon_file_xuat(model, file_id) {
	$("#loading").show();

	$.ajax({
		url: "/admin/uploaders/chon_file_xuat/" + model + "/" + file_id + "/" + $("#CongtrinhId").val(),
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html == "ok") {
				$("#file_xuat_" + file_id).css("opacity", 0.3).attr("onclick", "return false;");

				$("#file_xuat_span_" + file_id).fadeIn();
				setTimeout(function() {
					$("#file_xuat_span_" + file_id).fadeOut();
				}, 4000);

				$("#file_xuat_number").html((parseInt($("#file_xuat_number").text()) + 1));
			}
			$("#loading").hide();
		}
	});
}

function xoa_chon_file_xuat(model, file_id) {
	$("#loading").show();

	$.ajax({
		url: "/admin/uploaders/chon_file_xuat/" + model + "/" + file_id + "/0/0",
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html == "ok") {
				$("#Uploader_" + file_id).fadeOut();

				$("#file_xuat_number").html((parseInt($("#file_xuat_number").text()) - 1));
			}
			$("#loading").hide();
		}
	});
}

function xoa_chon_file_duyet(model, file_id,congtrinh_id) {
	$("#loading").show();

	$.ajax({
		url: "/admin/uploaders/chon_file_xuat/" + model + "/" + file_id + "/"+congtrinh_id+"/1",
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html == "ok") {
				$("#Uploader_" + file_id).fadeOut();

				$("#file_xuat_number").html((parseInt($("#file_xuat_number").text()) - 1));
			}
			$("#loading").hide();
		}
	});
}
function chon_file_duyet(model, file_id,congtrinh_id) {

	$("#loading").show();
	$.ajax({
		url: "/admin/uploaders/chon_file_xuat/" + model + "/" + file_id + "/"+congtrinh_id+"/2",
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				$("#Uploader_" + file_id).fadeOut();
			}
			$("#loading").hide();
		}
	});
}

function chon_download_outside(file_id,controller) {
	$("#loading").show();

	$.ajax({
		url: "/admin/uploaders/chon_download_outside/" + file_id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html == "ok") {
				$("#download_outside_" + file_id).css("opacity", 0.3);

				//$("#file_xuat_span_" + file_id).html("File này đã có thể download ngoài, copy: <span style='color: rgb(51, 0, 255);'>http://qlda.spaceaa.com/uploaders/download/" + file_id + "</span>").fadeIn();
				$("#file_xuat_span_" + file_id).html("File này đã có thể download ngoài, copy:<input type='text' style = 'width:310px' onclick ='copy_link(" + file_id + ")' id =" + file_id + " value = http://qlda.spaceaa.com/uploaders/zipFilesAndDownload/" +  btoa(file_id) +"/"+controller+ ">").fadeIn();
				setTimeout(function() {
					$("#file_xuat_span_" + file_id).fadeOut();
				}, 12000);
				//diult them ngay 29/10/2013 chon link de copy
				copy_link(file_id);
			}
			$("#loading").hide();
		}
	});
}

function chon_download_outside_copy(file_id) {
	$("#loading").show();

	$.ajax({
		url: "/admin/uploaders/chon_download_outside/" + file_id,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html == "ok") {
				$("#download_outside_" + file_id).css("opacity", 0.3);

				//$("#file_xuat_span_" + file_id).html("File này đã có thể download ngoài, copy: <span style='color: rgb(51, 0, 255);'>http://qlda.spaceaa.com/uploaders/download/" + file_id + "</span>").fadeIn();
				$("#file_xuat_span_" + file_id).html("File này đã có thể download ngoài, copy:<input type='text' style = 'width:310px' onclick ='copy_link(" + file_id + ")' id =" + file_id + " value = http://qlda.spaceaa.com/uploaders/download/" +  btoa(file_id) + ">").fadeIn();
				setTimeout(function() {
					$("#file_xuat_span_" + file_id).fadeOut();
				}, 12000);
				//diult them ngay 29/10/2013 chon link de copy
				copy_link(file_id);
			}
			$("#loading").hide();
		}
	});
}


//diult : them ngay 29/10/2013
function copy_link(file_id) {

	var elem = document.getElementById(file_id);
	if (typeof elem !== 'undefined' && elem !== null) {
		document.getElementById(file_id).select();
	}

}
// diult : ket thuc ham diult them

function congtrinh_congviecs_get_file_attach(id, date) {
	if ($("#attach_file_congtrinhs_congviecs").attr("id") == undefined) return false;

	var url = "/congtrinhs_congviecs/attach_file/" + id;
	if (date != undefined) url = "/congtrinhs_congviecs/attach_file/" + id + "/" + date;

	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			$("#attach_file_congtrinhs_congviecs").html(html);
		}
	});
}

function chon_nam_ngay_nghiphep(nam) {
	// lay phongban theo congty
	$("#loading").show();
	$.ajax({
		url: "/admin/loinhannghipheps/ngaynghi/" + nam,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			$("#window_content").html(html);

			window_show();

			$("#loading").hide();

			return false;
		}
	});

	return false;
}



function kiem_tra_tinnhan_moi() {
	var id_giaoviec_last = 0;
	if ($("#id_giaoviec_last").attr("id") != undefined) {
		id_giaoviec_last = $("#id_giaoviec_last").val();
	}

	$.ajax({
		url: "/admin/giaoviecs/kiemtra_giaoviec_moi/" + id_giaoviec_last,
		timeout: 15000,
		error: error_handler,
		success: function(html) {

			if (html != "") {
				$("#id_giaoviec_last").remove();

				// Khởi tạo table đầu tiên
				if (id_giaoviec_last == 0) {
					$("#content_thongbao_canhan").append('<table class="CssTable"><tbody></tbody></table>');
				}

				var tmp = html.split("tach_data_._tach_data");

				$("tbody", "#content_thongbao_canhan").append(tmp[0]);
				$("#content_thongbao_canhan").append(tmp[1]);

				// $('#ThongBaoCaNhanBtn').hide();$("#ThongBaoChiTiet").hide("slow");
				$('#ThongBaoCaNhanBtn').show();
				$("#ThongBaoChiTiet").show();

				var contain = $("#ThongBaoChiTiet");
				$("#content_thongbao_canhan", contain).scrollTop($("#content_thongbao_canhan", contain)[0].scrollHeight);

				if (!window_focus) {
					alert('Bạn có thông báo cá nhân mới bên dưới!');
				}
			}

			return false;
		}
	});

	return false;
}

function khoi_tao_set_interval() {
	window.setInterval("kiem_tra_tinnhan_moi()", 60000);
}

var window_focus;

function khoi_tao_kiemtra_loinhan_moi() {
	$(window).focus(function() {

		window_focus = true;

	}).blur(function() {

		window_focus = false;

	});

	khoi_tao_set_interval();

	// setTimeout('khoi_tao_set_interval()', 6000);
}

function upload_file_ketoan() {
	$("#Them_file_ketoan form").iframePostForm({
		iframeID: "TSan_iframe-post-form-upload_file_ketoan",
		post: function() {
			$("#loading").show();
		},
		complete: function(response) {
			$("#loading").hide();
			$("#window_content").html(response);
		}
	});
}


function listallcongtrinhchinhsua(functionvt, id) {
	list_all_congtrinh_chinhsua_goi(functionvt, id);
}

// $(document).one('click',function() {
// 	setInterval(function() { $('body').append('has focus? ' + window_focus + '<br>'); }, 1000);
// });

//diult them de lam cau cao lanh
function chon_file_view(file_id, value) {
	$("#loading").show();
	$.ajax({
		url: "/admin/congtrinhs_files/chon_file_view/" + file_id + "/" + value,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				if (value == 1) {
					$("#view_" + file_id).text("Không public ra ngoài");

					//$("#view_img_" + file_id).css("opacity", 0.3).attr("onclick", "return false;");
					$("#view_" + file_id).attr("onclick", "chon_file_view(" + file_id + ",0)");
					$("#view_img_" + file_id).attr("onclick", "chon_file_view(" + file_id + ",0)", "style", "opacity: 0.3; height: 15px;width: 15px;");
					$("#view_img_" + file_id).attr("style", "opacity: 0.3; height: 15px;width: 15px;");
					$("#file_xuat_span_" + file_id).fadeIn();
					setTimeout(function() {
						$("#file_xuat_span_" + file_id).fadeOut();
					}, 4000);
				} else {

					$("#view_" + file_id).text("Public ra ngoài");
					$("#view_" + file_id).attr("onclick", "chon_file_view(" + file_id + ",1)");
					$("#view_img_" + file_id).attr("onclick", "chon_file_view(" + file_id + ",1)");
					$("#view_img_" + file_id).attr("style", "opacity: 1.5; height: 15px;width: 15px;");
					$("#file_xuat_span_" + file_id).fadeIn();
					setTimeout(function() {
						$("#file_xuat_span_" + file_id).fadeOut();
					}, 4000);
				}
			}
			$("#loading").hide();
		}
	});
}

function chon_comment_view(file_id, item_id, value) {
	//alert(item_id);
	$("#loading").show();
	$.ajax({
		url: "/admin/admin/comments/chon_comment_view/" + file_id + "/" + item_id + "/" + value,
		timeout: 15000,
		error: error_handler,
		success: function(html) {
			if (html == "ok") {
				if (value == 1) {
					$("#comment_img_" + file_id).attr("onclick", "chon_comment_view(" + file_id + "," + item_id + ",0)", "style", "opacity: 0.3; height: 15px;width: 15px;");
					$("#comment_img_" + file_id).attr("style", "opacity: 0.3; height: 15px;width: 15px;");
					$("#file_xuat_span_" + file_id).fadeIn();
					setTimeout(function() {
						$("#file_xuat_span_" + file_id).fadeOut();
					}, 4000);
				} else {

					$("#comment_img_" + file_id).attr("onclick", "chon_comment_view(" + file_id + "," + item_id + ",1)");
					$("#comment_img_" + file_id).attr("style", "opacity: 1.5; height: 15px;width: 15px;");
					$("#file_xuat_span_" + file_id).fadeIn();
					setTimeout(function() {
						$("#file_xuat_span_" + file_id).fadeOut();
					}, 4000);
				}
			}
			$("#loading").hide();
		}
	});
}
