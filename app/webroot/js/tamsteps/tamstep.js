//OD8Steps
//option: col => 'xac_nhan_nguoi_phu_trach_trinh_duyet
//option: value => 1 || 0
//option: trangthai => 0,1,2,4
function confirm_thongbao_send_all(congtrinh_id, col, value, stt, giaoviec_id, trangthai)
{
	var url = "/tamsteps/confirm_thongbao_send_all/"+congtrinh_id+"/"+col+"/"+value+"/0/"+giaoviec_id+"/"+trangthai;
	if($("#frm_lydo_"+giaoviec_id) != "undefined")
		data = $("#frm_lydo_"+giaoviec_id).serialize();
	else
		data = null;
	$("#loading").show();
	$.ajax({
		url: url,
		data: data,
		type: "POST",
		timeout: 15000,
		error: error_handler,
		success: function(html){
			if( html == "ok" )
			{
				//Hiện thông báo khi user click xác nhận
				if(trangthai == 0)
				{
					nguoitao = $("#nguoitao_"+giaoviec_id).val();
					window_show_OD("Xác nhận của bạn đã được gửi đến <b>" + nguoitao);

					//Tự động tắt sau 3s
					setTimeout(function(){$("#OD_err").hide();}, 3000);
				}

				$("#TamstepsGiaoviec_"+giaoviec_id+"_"+stt).remove();

				if($("#OD_content_thongbao_canhan table.CssTable tr:visible").length == 1)
				{
					$("#OD_ThongBaoCaNhanBtn").remove();
					$("#OD_ThongBaoChiTiet").hide("slow");
				}
				if($("#content_thongbao_canhan table.CssTable tr:visible").length == 1)
				{
					$("#ThongBaoCaNhanBtn").remove();
					$("#ThongBaoChiTiet").hide("slow");
				}
			}else{
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}

//view: lists
//Thông báo cho user đã sắp tới thời gian hoàn thành tiến độ công việc được giao
//Lưu xác nhận là đã hoàn thành
function confirm_thongbao_tiendo(id, congtrinh_id, value, stt)
{
	var url = "/tamsteps/confirm_thongbao_tiendo/"+id+"/"+congtrinh_id+"/"+value;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html){
			if( html == "ok" )
			{
				if(value != 0)
				{
					nguoitao = $("#nguoitao_"+id).val();
					window_show_OD("Xác nhận của bạn đã được gửi đến <b>" + nguoitao);

					//Tự động tắt sau 3s
					setTimeout(function(){$("#OD_err").hide();}, 3000);
				}

				$("#Tamstep_"+id+"_"+stt).remove();

				if($("#OD_content_thongbao_canhan table#OD_thongbaocongtrinh tr[id]").length == 0)
					$("#OD_content_thongbao_canhan table#OD_thongbaocongtrinh").remove();

				if($("#OD_content_thongbao_canhan table.CssTable tr:visible").length == 0)
				{
					$("#OD_ThongBaoCaNhanBtn").remove();
					$("#OD_ThongBaoChiTiet").hide("slow");
				}
			}else{
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			}
			$("#loading").hide();
		}
	});
}



//view: daumoithongtin, chunhiemduan, nguoiphutrachketoan
function update_xac_nhan(congtrinh_id, field, c, giaoviec_id)
{
	$.ajax({
		url: "/tamsteps/confirm_thongbao_send_all/" + congtrinh_id + "/" + field + "/1/0/" + giaoviec_id + "/0",
		type: 'POST',
		timeout: 120000,
		success: function(html){
			if(html != "ok")
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
			else
			{
				$("."+c).html("Đã xác nhận");
				$("#"+c).val("1");
				window_show_OD("Hệ thống đã lưu xác nhận của bạn.");

				//Tự động tắt sau 3s
				setTimeout(function(){$("#OD_err").hide();}, 3000);
			}
		}
	});
}

function change_user(congtrinh_id, field,user_id,trangthai)
{
	$.ajax({
		url: "/tamsteps/confirm_thongbao_send_all/" + congtrinh_id + "/" + field + "/0/" + user_id + "/0/" + trangthai,
		timeout: 120000,
		success: function(html)
		{
			if(html != "ok")
				alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");
		}
	});
}




//Upload bằng ajax
function ajax_upload(id_div_window, id_frame, id_div_update)
{
	$(id_div_window+" form").iframePostForm
	({
		iframeID: id_frame,
		post : function ()
		{
			$("#loading").show();
		},
		complete : function (response)
		{
			$("#loading").hide();
			$(id_div_update).html(response);
		}
	});
}



function tim_dichvuthuchien(id)
{

	if(id.length < 1)return false;
	var url = '/tamsteps/dichvuthuchien/' + id + '/1';
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html){
			$("#loading").hide();
			$("#OD_tim_dichvuthuchien" ).html(html);

			//Update ngày 3/1/2013
			themthongtin('themthongtin');
		}
	});
}

function get_quanhuyen(value, div_update)
{
	if(value.length < 1)
	{
		if( div_update == undefined )
		{
			$("#update_quanhuyen" ).html("");
		}else{
			$("#" + div_update ).html("");
		}
		return false;
	}

	var url = '/congtrinhs/get_quanhuyen/' + value;
	$("#loading").show();
	$.ajax({url: url,
		timeout: 15000,error: error_handler,
		success: function(html){

			if( div_update == undefined )
			{
				$("#update_quanhuyen" ).html(html);
			}else{
				$("#" + div_update ).html(html);
			}

			$("#loading").hide();
		}
	});
}

//Dialog error
function window_show_OD(err)
{
	var url = "/tamsteps/error/" + err;
	$("#loading").show();
	$.ajax({url: url,
		timeout: 15000,
		success: function(html){
			$("#loading").hide();
			$("#OD_err_window").html(html);
			$("#OD_err_window").show();
		}
	});
}

//viethq

//Format number
function number_format( number, decimals, dec_point, thousands_sep ) {
	var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
	var d = dec_point == undefined ? "," : dec_point;
	var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
	var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;

	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function OD_xoa_ajax_files(model,id,model_cache)
{
	console.log("test");
	if(!confirm("Bạn có chắc chắn muốn xóa file?"))return false;
	if(model_cache != undefined){model_cache = "/"+model_cache;}else{ model_cache = ""; }
	$("#loading").show();$.ajax({url: '/mains/xoa_ajax/'+model+"/"+id+model_cache,
		timeout: 15000,error: error_handler,
		success: function(html){
			$("#loading").hide();
			if(html == "ok"){ location.reload(true); return false; }
			else{ alert("Có lỗi trong quá trình xử lý. Vui lòng bấm F5 và thao tác lại.");}
		}
	});
}

function open_chi_phi(congtrinh_id, phanbietcp)
{
	if (phanbietcp == 1)
	{
		var url = "/tamsteps/them_chiphichia/" + congtrinh_id;
		$("#loading").show();
		$.ajax({
			url: url,
			timeout: 15000,
			error: error_handler,
			success: function(html){
				$("#window_content_2").html(html);
				$("#loading").hide();
				window_show_2();
			}
		});
	}
}
function check_user_thongbao_is_checked_2()
{
	var tmp_ob = "";
	var tmp = "";

	// THONG BAO
	// kiem tra check_all
	var check_all = true;
	$("td.TdThongBao", "#data_click_group_user").each(function(){

		tmp_ob = $("input:checkbox", this);
		tmp = tmp_ob.attr("id");
		tmp = tmp.replace("UserTb", "");
		if( data_thanhvien_thongbao[tmp] == tmp )
		{
			tmp_ob.attr("checked", true);
		}else{
			tmp_ob.attr("checked", false);
			check_all = false;
		}

	});
	if( check_all )
	{
		$("#TamstepThongBaoTatca", "#data_click_group_user").attr("checked", true);
	}
}

function open_user_search_window_3(phanquyen_id, quyenhan, congtrinh_id, group)
{
	var url = "/tamsteps/users_search_on_window/" + phanquyen_id + "/" + quyenhan + "/" + congtrinh_id ;
	if(group != undefined )url = url +"/" +group;
	$("#loading").show();
	$.ajax({
		url: url,
		timeout: 15000,
		error: error_handler,
		success: function(html){
			$("#window_content_2").html(html);
			$("#loading").hide();
			window_show_2();
			$("#window_content_2").parent("div").css("left", 584 + Math.floor(($(window).width() - 1060)/2));

			// REMOVE SEND ALL FROM LOI NHAN BEN TAY TRAI
			if( $("#hide_send_all", "#window_content").attr("id") == "hide_send_all" )
			{
				$("#div_send_all", "#window_content_2").remove();
			}
		}
	});
}
function show_all_user_in_group_2(phanquyen_id, quyenhan, congtrinh_id, group, object, click_thong_bao)
{
	var element;
	$("li a", "#UlChonUser").each(function(){

		// lay ra a.href dang active
		if( $(this).attr("class") == "active" )
		{
			element = $(this);
		}

		$(this).removeClass('active');

	});
	$(object).addClass('active');

	var div = $("#data_click_group_user");

	if(!$(object).data('cache'))
	{
		$("#loading").show();
		$.ajax({
			url: "/tamsteps/users_search_on_window/" + phanquyen_id + "/" + quyenhan + "/" + congtrinh_id + "/" + group + "/" + "from_js",
			timeout: 15000,
			error: error_handler,
			success: function(html){
				$("#loading").hide();

				// store to cache
				if( $.trim(div.html()) != "" )
				{
					element.data('cache', div.html());
					$(object).data('cache', html)
				}

				// update new data
				div.html(html);

				check_user_thongbao_is_checked_2();

				// KIEM TRA CO PHAI GOI TU function stick_checkbox_all_user_in_group()
				if( click_thong_bao == "stick_checkbox_all_user_in_group" )
				{
					var UserThongBaoTatca = $("#TamstepThongBaoTatca");
					if( !UserThongBaoTatca.is(":checked") )
					{
						UserThongBaoTatca.click();
					}
				}else if( click_thong_bao == "stick_checkbox_all_user_in_group_nochecked" )
				{
					var UserThongBaoTatca = $("#TamstepThongBaoTatca");
					if( UserThongBaoTatca.is(":checked") )
					{
						UserThongBaoTatca.click();
					}
				}
			}
		});


	}
	else{

		div.html($(object).data('cache'));

		// CHECK AGAIN;
		check_user_thongbao_is_checked_2();

		// KIEM TRA CO PHAI GOI TU function stick_checkbox_all_user_in_group()
		if( click_thong_bao == "stick_checkbox_all_user_in_group" )
		{
			var UserThongBaoTatca = $("#TamstepThongBaoTatca");
			if( !UserThongBaoTatca.is(":checked") )
				UserThongBaoTatca.click();
		}
		else if( click_thong_bao == "stick_checkbox_all_user_in_group_nochecked" )
		{
			var UserThongBaoTatca = $("#TamstepThongBaoTatca");
			if( UserThongBaoTatca.is(":checked") )
			{
				UserThongBaoTatca.click();
			}
		}
	}

	return false;
}
function xoa_chon_user_giaoviec_2(object)
{
	if( object == "all" )
	{
		$("#span_id_user_giaoviec_all").remove();
	}else{
		var id_obj = $(object).attr("id");
		var s = id_obj.split("_");
		var id = s[s.length-1];
		$.ajax({
			url: "/tamsteps/xoa_user/" + id,
			timeout: 120000,
			type: "POST",
			before: "$(\"#loading\").show();",
			success: function(html)
			{
				$("#loading").hide();
			}
		});
		$(object).parent("span").remove();
	}
}
function chon_users_search_OD(loai, object_a_clicked, id_td, phanquyen_id, quyenhan, congtrinh_id)
{
	// $("#users_search_giaoviec").append( $( "#td_user_" + loai + "_" + id_td, "#window_content_2").html() );

	var tmp = $("#users_search_giaoviec_"+phanquyen_id+"_"+quyenhan);

	if( $("#span_id_user_giaoviec_all",tmp ).attr("id") != undefined )
	{
		$("#span_id_user_giaoviec_all",tmp ).remove();
	}

	if( $(object_a_clicked).is(":checked") )
	{
		$.ajax({
			url : "/tamsteps/them_user/"+congtrinh_id+"/"+phanquyen_id+"/"+quyenhan+"/"+id_td,
			timeout: 120000,
			type: "POST",
			before: "$(\"#loading\").show();",
			success: function(html){
				if(parseInt(html) != 0)
				{
					tmp.append( $( "#td_user_" + loai + "_" + id_td, "#window_content_2").html() );
					var contain = $("#users_search_giaoviec_"+phanquyen_id+"_"+quyenhan);
					$( "#img_xoa_" + loai + "_" + id_td, contain).attr("id", "#img_xoa_" + loai + "_" + html);
				}
				$("#loading").hide();
			}
		});
		if( loai == "thongbao" )
		{
			data_thanhvien_thongbao[id_td] = id_td;
		}else{
			data_thanhvien_phanhoi[id_td] = id_td;
		}
	}
	else
	{
		// XOA CHON
		var contain = $("#users_search_giaoviec_"+phanquyen_id+"_"+quyenhan);
		$("img[rel]", contain).each(function(){
			if($(this).attr("rel") == id_td)
			{
				xoa_chon_user_giaoviec_2(this);
				$(this).click();
			}
		});
		if( loai == "thongbao" )
		{
			data_thanhvien_thongbao[id_td] = 0;
			$("#TamstepThongBaoTatca", "#data_click_group_user").attr("checked", false)
		}else{
			data_thanhvien_phanhoi[id_td] = 0;
		}
	}

	return false;
}

function stick_checkbox_all_user_in_group_2( phanquyen_id, quyenhan, congtrinh_id, group, object )
{

	if( $(object).is(":checked") )
	{
		show_all_user_in_group_2(phanquyen_id, quyenhan, congtrinh_id, group, $("#click_on_name_group_" + group), "stick_checkbox_all_user_in_group");
	}else{
		show_all_user_in_group_2(phanquyen_id, quyenhan, congtrinh_id, group, $("#click_on_name_group_" + group), "stick_checkbox_all_user_in_group_nochecked");
	}

}

function chon_users_search_2_all_2(type, object, phanquyen, quyenhan, congtrinh_id)
{
	$( "td.Td" + type + " :checkbox", "#data_click_group_user" ).each( function(){
		if( $(object).is(":checked") )
		{
			if( !$(this).is(":checked") )
			{
				$(this).click();
			}
		}else{
			if( $(this).is(":checked") )
			{
				$(this).click();
			}
		}

	});
	return false;
}
//End OD8Steps