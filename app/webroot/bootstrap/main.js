function show_div_result(html){
    $("#saved-success").hide();
    $("#saved-failed").hide();
    if (html == "ok") {
        $("#saved-success").html("Saved successful").show();
        setTimeout('$("#saved-success").hide()', 3000);

    }else{
        $("#saved-failed").html("Error: "+html).show();
        setTimeout('$("#saved-failed").hide()', 10000);
    }
}