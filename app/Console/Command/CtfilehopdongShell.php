<?php
class CtfilehopdongShell extends AppShell {

	public $uses = array('CongtrinhsHopdong', 'Uploader');

	public function main() {

		$this->out('bat dau updateAll ....');

		$array_tmp = array(
			'fields' => array('id', 'congtrinh_id'),
			'conditions' => array(
				'CongtrinhsHopdong.trangthai' => 4
			),
			'limit' => 1000,
			'contain' => false
		);

		$datas = $this->CongtrinhsHopdong->find( 'all', $array_tmp );

		foreach( $datas as $data )
		{

			$field = array('Uploader.item_id' => $data['CongtrinhsHopdong']['congtrinh_id']);
			$cond = array('Uploader.controller' => 'congtrinhs_hopdongs', 'Uploader.item_id' => $data['CongtrinhsHopdong']['id']);

			// SAVE Chamcong 9
			$this->Uploader->belongsTo = array();
			if( !$this->Uploader->updateAll( $field, $cond ) )
			{
				$this->out('Update that bai.');
			}
		}

		$this->out('Ket thuc.');

		exit;
	}
}
