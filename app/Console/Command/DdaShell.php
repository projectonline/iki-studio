<?php
class DdaShell extends AppShell {

	public $uses = array(
		'Ddabanvevt','Banvevt',
		'Ddabienban','Bienban',
		'Ddaphaplyvt','Phaplyvt',
		'Ddabaocaothang', 'CongtrinhsBct',
		'Uploader'
	);

	public function main() {

		$this->out('bat dau copy ....');

		$args = array(
			'Phaplyvt' => 'Ddaphaplyvt',
			'Banvevt' => 'Ddabanvevt',
			'Bienban' => 'Ddabienban',
			'CongtrinhsBct' => 'Ddabaocaothang'
		);

		foreach( $args as $key => $model )
		{
			$datas = $this->{$model}->find('all', array(
				'contain' => false
			));

			foreach( $datas as $data  )
			{
				$save = $data[$model];
				unset($save['id']);
				$this->{$key}->create();
				if( $this->{$key}->save($save) )
				{
					$id_save = $this->{$key}->id;
					$field = array('Uploader.controller' => '"'.Inflector::tableize($key).'"', 'Uploader.item_id' => $id_save );
					$cond = array('Uploader.controller' => Inflector::tableize($model), 'Uploader.item_id' => $data[$model]['id']);

					// update lai File
					if( !$this->Uploader->updateAll( $field, $cond ) ){

						$this->out('update that bai - '.$key);
					}
				}

			}
//
//				if()
//
//
//			$field = array('Chamcong.trangthai' => 9,'Chamcong.nguoisua' => $this->Auth->user( 'id'));
//			$cond = array('Chamcong.trangthai' => 4,'Chamcong.chamcongfilevt_id' => $id);
//			// SAVE Chamcong 9
//			$this->Chamcong->belongsTo = array();
//			if( $this->Chamcong->updateAll( $field, $cond ) ){
//
//
//				$data[$key1]['model'] = $get_model_loinhan[$key];
//				$data[$key1]['item_id'] = $data[$key1][$value];
//
//				if( !is_numeric($data[$key1]['item_id']) || strlen($data[$key1]['noidung']) < 2)
//				{
//					//$this->out( print_r($data[$key1]) );
//					unset($data[$key1]);
//					continue;
//				}
//
//				$data[$key1]['tmp_model_traloi'] = $key;
//				$data[$key1]['tmp_id_traloi'] = $data[$key1]['id'];
//
//				unset($data[$key1][$value], $data[$key1]['id']);

		}

		$this->out('Ket thuc.');

		exit;
	}

}
