<?php
class AlbumShell extends AppShell {

	public $uses = array('Album', 'AlbumsImg');
	public $components = array( 'ImageResizer' );

	public function main() {

		$this->out('In this->args....');
//		App::import('Component','Auth');
//		$this->Auth=& new AuthComponent(null);

//		App::import('Component', 'ImageResizer');
//		$ImageResizer =& new ImageResizerComponent(null);

		$tmp = $this->AlbumsImg->find('all', array(
			'conditions' => array(
				'AlbumsImg.url_small' => '',
				'AlbumsImg.trangthai' => 4
			),
			'contain' => false
		));

		if( isset($tmp[0]) )
		{
			APP::import('Component', 'ImageResizer');
			$ImageResizer = new ImageResizerComponent(new ComponentCollection());

			foreach( $tmp as $url_img )
			{
				$this->out('begin to copy to url_small AlbumsImg -- album_id = '.$url_img['AlbumsImg']['album_id'].'... -- id=' .$url_img['AlbumsImg']['id'].'... -- id=' .$url_img['AlbumsImg']['name'] );

				// COPY to small
				$img_real_tmp = APP.'webroot'.$url_img['AlbumsImg']['url_real'];
				$img_small_tmp = APP.'webroot/upload/avatar_all/albums/'.$url_img['AlbumsImg']['album_id'].'/img_small/'.basename($img_real_tmp);
				if( !copy($img_real_tmp, $img_small_tmp) )
				{
					$this->out( 'False to copy from' );
					$this->out( $img_real_tmp );
					$this->out( $img_small_tmp );
				}

				$ImageResizer->resizeImage( $img_small_tmp, array('maxWidth' => 850, 'maxHeight' => 650) );

				$this->AlbumsImg->id = $url_img['AlbumsImg']['id'];
				if( !$this->AlbumsImg->saveField('url_small', str_replace(APP.'webroot', '', $img_small_tmp)) )
				{
					$this->out( 'error saveField AlbumsImg -> id = '.$url_img['AlbumsImg']['album_id'] );
				}
			}

			$this->AlbumsImg->updateCounter('Album');
		}

		$this->out('Ket thuc.');

		exit;
	}
}
