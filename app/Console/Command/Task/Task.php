<?php
class Task extends AppModel {
	var $name = 'Task';
	var $validate = array(
		'tieude' => array(
			'dk1' => array(
				 'rule' => array( 'notEmpty' ),
				 'require' => true,
				 'message' => 'Vui lòng nhập tiêu đề'
			),
			'dk2' => array(
				'rule' => array( 'maxLength', 255 ),
				'allowEmpty' => true,
				'message' => 'Tiêu đề không quá 255 kí tự'
			)
		),
		'noidung' => array(
			'dk1' => array(
				 'rule' => array( 'notEmpty' ),
				 'require' => true,
				 'message' => 'Vui lòng nhập nội dung'
			),
			'dk2' => array(
				'rule' => array( 'maxLength', 4000 ),
				'allowEmpty' => true,
				'message' => 'Nội dung không quá 4000 kí tự'
			)
		),
		'congtrinh' => array(
			'dk1' => array(
				 'rule' => array( 'notEmpty' ),
				 'require' => true,
				 'message' => 'Vui lòng chọn công trình'
			)
		),
		'chiutrachnhiem' => array(
			'dk2' => array(
				'rule' => array( 'my_numeric'),
				'message' => 'Người chịu trách nhiệm không hợp lệ. Vui lòng chọn lại.'
			)
		),
		'thoigianketthuc_tmp' => array(
			'dk1' => array(
				 'rule' => array( 'my_notEmpty' ),
				 'message' => 'Vui lòng chọn ngày kết thúc'
			),
			'dk2' => array(
				'rule' => array( 'date' ),
				'allowEmpty' => true,
				'message' => 'Ngày không hợp lệ. Vui lòng chọn lại'
			)
		)
	);

	function my_numeric()
	{
		if((strlen($this->data['Task']['chiutrachnhiem']) > 0 && !is_numeric($this->data['Task']['chiutrachnhiem'])) ||
		(strlen($this->data['Task']['thoigianketthuc_tmp']) > 0 && strlen($this->data['Task']['chiutrachnhiem']) < 1 ) )return false;
		return true;
	}

	function my_notEmpty()
	{
		if(strlen($this->data['Task']['chiutrachnhiem']) > 0 && strlen($this->data['Task']['thoigianketthuc_tmp']) < 1)return false;
		return true;
	}

	var $belongsTo = array(
		'Nguoitao' => array('className' => 'User',
					'fields' => array('id', 'username','avatar', 'realname', 'codetbl_id'),
					'foreignKey' => 'nguoitao',
			),
		'Nguoichiutrachnhiem' => array('className' => 'User',
					'fields' => array('id', 'username','avatar', 'realname', 'codetbl_id'),
					'foreignKey' => 'chiutrachnhiem',
			),
		'Congtrinh' =>
			array('className' => 'Congtrinh',
				'foreignKey' => 'congtrinh_id',
				'counterCache' => true,
				'counterScope' => array('Task.trangthai' => 4)
		 )
	);
	var $hasMany = array(
		'Traloitask' =>
			array('className' => 'Traloitask',
					'foreignKey' => 'task_id',
					'conditions' => array('Traloitask.trangthai' => 4),
					'fields' => array( 'id', 'created','noidung', 'nguoitao','created' ),
					'order' => 'Traloitask.id desc', 'limit' => 5
			)
	);

	var $hasAndBelongsToMany = array(
		'Receiver' =>
			 array(
			 'className' => 'User',
			 'fields' => array('username'),
			 'joinTable' => 'tasks_users',
			 'foreignKey' => 'task_id',
			 'associationForeignKey' => 'user_id'
		)
	);
}
