<?php
/**
 *
 * Dual-licensed under the GNU GPL v3 and the MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2012, Suman (srs81 @ GitHub)
 * @package       plugin
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 *                and/or GNU GPL v3 (http://www.gnu.org/copyleft/gpl.html)
 */

class UploadHelper extends AppHelper {


	public function view ($model, $id, $edit=false) {

		require_once (ROOT . DS . APP_DIR . "/Plugin/AjaxMultiUpload/Config/bootstrap.php");
		$dir = Configure::read('AMU.directory');
		if (strlen($dir) < 1) $dir = "files";

		$lastDir = $this->last_dir ($model, $id);
		$directory = WWW_ROOT . DS . $dir . DS . $lastDir;
		$baseUrl = Router::url("/") . $dir . DS . $lastDir;
		$files = glob ("$directory/*");
		$str = "<dt>" . __("Files") . "</dt>\n<dd>";
		$count = 0;
		$webroot = Router::url("/") . "ajax_multi_upload";
		foreach ($files as $file) {
			$type = pathinfo($file, PATHINFO_EXTENSION);
			$filesize = $this->format_bytes (filesize ($file));
			$f = basename($file);
			$url = $baseUrl . "/$f";
			if ($edit) {
				$baseEncFile = base64_encode ($file);
				$delUrl = "$webroot/uploads/delete/$baseEncFile/";
				$str .= "<a href='$delUrl'><img src='" . Router::url("/") .
					"ajax_multi_upload/img/delete.png' alt='Delete' /></a> ";
			}
			$str .= "<img src='" . Router::url("/") . "ajax_multi_upload/img/fileicons/$type.png' /> ";
			$str .= "<a href='$url'>" . $f . "</a> ($filesize)";
			$str .= "<br />\n";
		}
		$str .= "</dd>\n";
		return $str;
	}

	// namnb
	public function vt_view() {

		require_once (ROOT . DS . APP_DIR . "/Plugin/AjaxMultiUpload/Config/bootstrap.php");

		$files = glob (APP.'tmp/upload/'.AuthComponent::user('id')."/*");
		$str = "<dt>" . __("Files") . "</dt>\n<dd>";

		$count = 0;

		$str = '<ul class="qq-upload-list">';
		foreach ($files as $file) {
			$type = pathinfo($file, PATHINFO_EXTENSION);
			$filesize = $this->format_bytes (filesize ($file));
			$f = basename($file);

			$str .= '<li class="qq-upload-success">'.
				'<span class="qq-upload-file">'.$f.'</span>'.
				'<span class="qq-upload-size" style="display: inline; ">'.$filesize.'</span>'.
				'<a class="qq-upload-remove" href="javascript:void(0)" onclick="remove_file_upload_ajax(this, '."'".$f."'".')" style="">Delete</a>'.
				'<span class="qq-upload-failed-text">Failed</span>'.
			'</li>';
		}
		$str .= '</ul>';
		return $str;
	}

	public function vt_edit( $view = false ) {
		require_once (ROOT . DS . APP_DIR . "/Plugin/AjaxMultiUpload/Config/bootstrap.php");

		$dir = APP.'tmp/upload/'.AuthComponent::user('id');
		$str = '';

		if( $view )
		{
			$str .= $this->vt_view();
		}else{
			if( is_dir($dir) )
			{
				$command = 'cd '.APP.'tmp/upload/; rm -rf '.AuthComponent::user('id');
				exec($command);
			}

			mkdir( $dir );
			chmod( $dir, 0777 );
		}

		$tmp = <<<END
			<link rel="stylesheet" type="text/css" href="/ajax_multi_upload/css/fileuploader.css" />
			<script src="/ajax_multi_upload/js/fileuploader.js" type="text/javascript"></script>
			<div id="AjaxMultiUpload">
				<noscript>
					 <p>Please enable JavaScript to use file uploader.</p>
				</noscript>
			</div>
			<script src="/ajax_multi_upload/js/fileuploader.js" type="text/javascript"></script>
			<script type="text/javascript">
				function createUploader(){
					var uploader = new qq.FileUploader({
						element: document.getElementById('AjaxMultiUpload'),
						action: '/ajax_multi_upload/uploads/upload',
						debug: true
					});
				}
				\$(function(){
					createUploader();
				});
			</script>
END;
		return $tmp.$str;
	}
	// end namnb

	public function edit ($model, $id) {

		require_once (ROOT . DS . APP_DIR . "/Plugin/AjaxMultiUpload/Config/bootstrap.php");

		$str = $this->view ($model, $id, true);
		// Replace / with underscores for Ajax controller

		$str .= <<<END
			<br /><br />
			<link rel="stylesheet" type="text/css" href="$webroot/css/fileuploader.css" />
			<script src="$webroot/js/fileuploader.js" type="text/javascript"></script>
			<div id="AjaxMultiUpload">
				<noscript>
					 <p>Please enable JavaScript to use file uploader.</p>
				</noscript>
			</div>
			<script src="$webroot/js/fileuploader.js" type="text/javascript"></script>
			<script type="text/javascript">
				function createUploader(){
					var uploader = new qq.FileUploader({
						element: document.getElementById('AjaxMultiUpload'),
						action: '/ajax_multi_upload/uploads/upload',
						debug: true
					});
				}
				\$(function(){
					createUploader();
				});
			</script>
END;
		return $str;
	}

	// The "last mile" of the directory path for where the files get uploaded
	function last_dir ($model, $id) {
		return $model . "/" . $id;
	}

	// From http://php.net/manual/en/function.filesize.php
	function format_bytes($size) {
		$units = array(' B', ' KB', ' MB', ' GB', ' TB');
		for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
		return round($size, 2).$units[$i];
	}
}
