<?php
/**
 *
 * Dual-licensed under the GNU GPL v3 and the MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2012, Suman (srs81 @ GitHub)
 * @package       plugin
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 *                and/or GNU GPL v3 (http://www.gnu.org/copyleft/gpl.html)
 */

class UploadsController extends AjaxMultiUploadAppController {

	public $name = "Upload";
	public $uses = null;

	// list of valid extensions, ex. array("jpeg", "xml", "bmp")
	var $allowedExtensions = array();

	function upload($dir=null) {

		require_once (ROOT . DS . APP_DIR . "/Plugin/AjaxMultiUpload/Config/bootstrap.php");

		$sizeLimit = MAX_SIZE_UPLOAD;
		$dir = APP.'tmp/upload/'.AuthComponent::user('id').'/';

		$this->layout = "ajax";
		Configure::write('debug', 0);

		$uploader = new qqFileUploader($this->allowedExtensions,$sizeLimit);
		$result = $uploader->handleUpload($dir);
		$this->set("result", htmlspecialchars(json_encode($result), ENT_NOQUOTES));

		// LUU FILE
	}

	/**
	 *
	 * delete a file
	 *
	 * Thanks to traedamatic @ github
	 */
	public function delete($file = null) {
		if(is_null($file)) {
			$this->Session->setFlash(__('File parameter is missing'));
			$this->redirect($this->referer());
		}
		$file = base64_decode($file);
		if(file_exists($file)) {
			if(unlink($file)) {
				$this->Session->setFlash(__('File deleted!'));
			} else {
				$this->Session->setFlash(__('Unable to delete File'));
			}
		} else {
			$this->Session->setFlash(__('File does not exist!'));
		}

		$this->redirect($this->referer());
	}
}

?>
