<?php
/**
 * Minify Controller
 *
 * Classe responsável pela compressão de arquivos javascript e css.
 *
 * @package		app.Controller
 */
class MinifyController extends Controller {

	public $name = 'Minify';

	/**
	 * Take care of any minifying requests.
	 * The import is not defined outside the class to avoid errors if the class is read from the console.
	 *
	 * @return void
	 */
	public function index() {

		// namnb
//		http://cakephp.lighthouseapp.com/projects/42648/tickets/1

		// You need to use the PluginName.ClassName syntax for loading anything from plugins.

		//App::import('Vendor', 'TestPlugin.AnotherWelcome', array('file' => 'Welcome'.DS.'welcome.php'));

		App::import('Vendor', 'Minify.AnotherWelcome', array('file' => 'minify'.DS.'index.php'));

//		App::import('Minify.Vendor', 'minify' . DS . 'index');

		$this->response->statusCode('304');
		exit;
	}

}
?>