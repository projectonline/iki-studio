<?php
class Thuvien extends AppModel {
	var $name = 'Thuvien';
	var $validate = array(
		'tieude' => array(
			'dk1' => array(
				 'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng nhập tiêu đề'
			),
			'dk2' => array(
				'rule' => array( 'maxLength', 255 ),
				'allowEmpty' => true,
				'message' => 'Tiêu đề không quá 255 kí tự'
			)
		),
		'mota' => array(
			'dk1' => array(
				 'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng nhập mô tả'
			),
			'dk2' => array(
				'rule' => array( 'maxLength', 2000 ),
				'allowEmpty' => true,
				'message' => 'Nội dung không quá 2000 kí tự'
			)
		)
	);
	var $belongsTo = array(
			'Nguoitao' =>
				array('className' => 'User',
						'foreignKey' => 'nguoitao',
						'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id'),
						'conditions' => '',

						'order' => '',
						'counterCache' => ''
				),
		   'Type' =>
				array('className' => 'Codetbl',
						//'primaryKey' => 'value',
						'foreignKey' => 'type',
						'conditions' => 'Type.code1 = \'thv\' and Type.code2 = \'top\'',

						'order' => '',
						'counterCache' => ''
				),
			'TypeLeft' =>
				array('className' => 'Codetbl',
						//'primaryKey' => 'value',
						'foreignKey' => 'typeleft',
						'conditions' => 'TypeLeft.code1 = \'thv\' and TypeLeft.code2 = \'lef\'',

						'order' => '',
						'counterCache' => ''
				)
	);

	var $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'conditions' => array(
				'Comment.trangthai' => 4,
				'Comment.model' => 'Thuvien'
			),
			'foreignKey' => 'item_id',
			'dependent' => true,
			'fields' => array('id', 'created', 'noidung', 'noidung_attach', 'nguoitao'),
			'order' => 'Comment.id desc',
			'limit' => 3
		),
		'File' =>
		 array('className'     => 'Uploader',
			   'conditions'    => array('File.controller' => 'thuviens', 'File.trangthai <>' => 9),
			   'order'         => 'File.id desc',
			   'foreignKey'    => 'item_id',
			   'dependent'     => true
		 )
  );
}
