<?php
class Lichhop extends AppModel {
	var $name = 'Lichhop';
	var $validate = array(
		'tieude' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),

				'message' => 'Vui lòng nhập tiêu đề'
			),
			'dk2' => array(
				'rule' => array( 'maxLength', 255 ),
				'allowEmpty' => true,
				'message' => 'Tiêu đề không quá 255 kí tự'
			)
		)//,
		// 'noidung' => array(
			// 'dk1' => array(
			// 	 'rule' => array( 'notEmpty' ),
			// 	 'message' => 'Vui lòng nhập nội dung'
			// ),
			// 'dk2' => array(
			// 	'rule' => array( 'maxLength', 100000 ),
			// 	'allowEmpty' => true,
			// 	'message' => 'Nội dung không quá 100000 kí tự'
			// )
		// ),
	);
	var $belongsTo = array(
		'Nguoitao' => array(
			'className' => 'User',
			'foreignKey' => 'nguoitao',
			'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id')
		  ),
		'Phongban' => array(
			'className' => 'Codetbl',
			'foreignKey' => 'phongban_id',
			'fields' => array('id', 'name')
		)
	);

	var $hasOne = array(
		'File' => array(
			'className'  => 'Uploader',
			   'conditions' => array('File.controller' => 'lichhops', 'File.trangthai <>' => 9),
			   'order'      => 'File.name ASC',
			   'foreignKey' => 'item_id',
			   'fields' => array('id', 'name', 'dungluong', 'nguoitao', 'file_xuat', 'download_tu_do'),
			   'dependent'  => true
		 )
	);

//	var $hasMany = array( 'File' =>
//		 array('className'  => 'Uploader',
//			   'conditions' => array('File.controller' => 'lhop', 'File.trangthai <>' => 9),
//			   'order'      => 'File.name ASC',
//			   'foreignKey' => 'item_id','order' => 'File.id desc',
//			   'fields' => array('id', 'name', 'dungluong', 'nguoitao', 'file_xuat', 'download_tu_do'),
//			   'dependent'  => true
//		 )
// 	);
}
