<?php
class AppModel extends Model{
	var $actsAs = array('Containable');
	//var $cacheQueries = true;

	public function onError() {
	   $db = ConnectionManager::getDataSource('default');
	   $err = $db->lastError();
	   $this->log($err);
	   $this->log($this->data);
	}

	var $cache_query = true; // dùng cái này để check những chỗ ko cho cache
							 // CACHE_QUERY: check boostrap, tắt hoặc mở toàn bộ cache
	function find($type = 'first', $query = array() )
	{
		$params = $query;
		if( $type == 'first' && $this->name == 'User' )
		{
			return parent::find($type, $params);
		}
	  if ( $this->cache_query && $this->name != 'Session') {
		  $tag = isset($this->name) ? $this->name : 'appmodel';
		  $paramsHash = md5(serialize($params));
		  $version = (int)Cache::read($tag);
		  $fullTag = $tag . '_' . $type . '_' . $paramsHash;
		  if ($result = Cache::read($fullTag)) {
			  if ($result['version'] == $version)
				  return $result['data'];
		  }
		  $result = array('version' => $version, 'data' => parent::find($type, $params));
		  Cache::write($fullTag, $result);
		  Cache::write($tag, $version);

		  return $result['data'];
	  } else {
		  return parent::find($type, $params);
	  }
	}

	function updateCounter($tag = null)
	{
		if ($this->cache_query) {
			if( is_array($tag) )
			{
				foreach( $tag as $value )
				{
					Cache::write($value, 1 + (int)Cache::read($value));
				}
			}else{
				if(!isset($tag)){ $tag = isset($this->name) ? $this->name : 'appmodel'; }
				Cache::write($tag, 1 + (int)Cache::read($tag));
			}
		}

		// $this->log($tag);
	}

	function afterSave($created)
	{
		$this->updateCounter();
		parent::afterSave($created);
	}

	function beforeSave($options = array()) {

		if(isset($this->data['File']))
		{
			$this->updateCounter('Uploader');
		}

		// CHECK USER DANG NHAP DE ADD VAO TABLE SAVE LUON
		if( ( !isset($this->data['nguoitao']) && !isset($this->data[$this->name]['nguoitao']) )
				|| (isset($this->data['nguoitao']) && $this->data['nguoitao'] == 0)
				|| (isset($this->data[$this->name]['nguoitao']) && $this->data[$this->name]['nguoitao'] == 0)
		)
		{
			App::uses('AuthComponent', 'Controller/Component');
			$Auth = new AuthComponent(new ComponentCollection());
			$user_login_id = $Auth->user('id');
			if( !is_numeric($user_login_id) )$user_login_id = 0;

			$this->set(array('nguoisua' => $user_login_id));
			if (!isset($this->data[$this->name]['id']) && !isset($this->data['id']) && !$this->id) {

				$this->set(array('nguoitao' => $user_login_id));
			}
		}

		$check_them_trangthai = true;
		$arr_key = array_keys($this->data);
		if( !empty($arr_key) )
		{
			foreach( $arr_key as $value )
			{
				if( isset($this->data[$value]['trangthai']) || isset($this->data['trangthai']))
				{
					$check_them_trangthai = false;
					break;
				}
			}
		}

		//if( !isset($this->data['trangthai']) && !isset($this->data[$this->name]['trangthai']) )
		if( $check_them_trangthai )
		{
			$this->set(array('trangthai' => 4));
		}
		// END

		parent::beforeSave($options);
		return true;
	}
}
