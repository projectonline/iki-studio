<?php
class Congtrinh extends AppModel {
	var $name = 'Congtrinh';

	public $actsAs = array('Containable', 'Search.Searchable');

	public $filterArgs = array(
		array('name' => 'macongtrinh', 'type' => 'like', 'field' => 'Congtrinh.macongtrinh'),
		array('name' => 'id', 'type' => 'value', 'field' => 'Congtrinh.id'),
		array('name' => 'tieude', 'type' => 'fulltext', 'field' => 'Congtrinh.tieude'),
		array('name' => 'mota', 'type' => 'fulltext', 'field' => 'Congtrinh.mota')
	);

	var $hasMany = array(
		'Comment' => array(
			'className'  => 'Comment',
			'conditions' => array(
				'Comment.trangthai' => 4,
				'Comment.model' => 'Congtrinh'
			),
			'order'      => 'Comment.id DESC',
			'foreignKey' => 'item_id',
			'dependent'  => true
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'Nguoitao' => array(
			'className' => 'User',
			'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id'),
			'foreignKey' => 'nguoitao'
		),
		
		'Nguoisua' => array(
			'className' => 'User',
			'foreignKey' => 'nguoisua',
			'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id')
		)
	);
}
