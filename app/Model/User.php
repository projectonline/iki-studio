<?php
class User extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $name = 'User';
	var $actsAs = array('Search.Searchable');

	public $filterArgs = array(
		array('name' => 'realname', 'type' => 'fulltext', 'field' => 'User.realname'),
		array('name' => 'trangthai', 'type' => 'value', 'field' => 'User.trangthai'),
		array('name' => 'codetbl_id', 'type' => 'value', 'field' => 'User.codetbl_id'),
		array('name' => 'tel', 'type' => 'like', 'field' => 'User.tel'),
		array('name' => 'month', 'type' => 'query', 'method' => 'month_birthday'),
		array('name' => 'trinhdochuyenmon', 'type' => 'like', 'field' => 'User.trinhdochuyenmon')
	);

	function month_birthday( $data, $field )
	{
		$cond = array(
			'MONTH(User.birthday)' => $data['month'],
		);
		return $cond;
	}

	var $validate = array(
		'username' => array(
			'dk1' => array(
				'rule' => array('notEmpty'),

				 'message' => 'Vui lòng nhập tên đăng nhập'
			),
//			'dk2' => array(
//				 'rule' => array('isUnique'),
//
//				 'message' => 'Tên đăng nhập đã bị trùng'
//			),
			'dk3' => array(
				 'rule' => array('maxLength', 125 ),

				 'message' => 'Tên đăng nhập không quá 125 kí tự'
			),
			'dk4' => array(
				 'rule' => array('alphaNumeric'),

				 'message' => 'Tên đăng nhập chỉ bao gôm kí tự a->Z và số 0->9'
			),
			'tentrung' => array(
				 'rule' => array( 'check_exists' ),
				 'allowEmpty' => true,
				'message' => 'Tên đăng nhập đã tồn tại',
			)
		),
		'passwrd' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),
				 'message' => 'Vui lòng nhập mật khẩu'
			)
		),

		//diult dong code ngay 30/10/2013
		/*'realname' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng nhập tên thật'
			),
			'dk2' => array(
				 'rule' => array( 'Between' , 1, 125 ),

				 'message' => 'Tên thật không quá 125 kí tự'
			)
		),*/
		'group_id' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng chọn quyền hạn'
			),
			'dk2' => array(
				 'rule' => array( 'numeric'),

				 'message' => 'Quyền hạn chọn không đúng quy định'
			)
		),
		'id_congtythanhtoan' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng chọn công ty thanh toán lương'
			),
			'dk2' => array(
				 'rule' => array( 'numeric'),

				 'message' => 'Công ty thanh toán chọn không đúng quy định'
			)
		),
		'hinh' => array(
//			'dk1' => array(
//				'rule' => array('extension', array('gif', 'jpeg', 'png', 'jpg', 'bmp')),
//				'message' => 'Vui lòng chọn file đúng định dạng (gif, jpeg, png, jpg, bmp).'
//			),
			'dk1' => array(
				'rule' => array( 'isUploadedFile' ),
				 'on' => 'create',
				 'message' => 'Vui lòng chọn hình đại diện'
			)
		),
		// diult dong code ngay 30/10/2013
		/*'codetbl_id' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng chọn trực thuộc bộ phận nào'
			),
			'dk2' => array(
				 'rule' => array( 'numeric'),

				 'message' => 'Trực thuộc bộ phận không đúng quy định'
			)
		),*/
		'birthday_tmp' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng chọn ngày sinh'
			),
			'dk2' => array(
				 'rule' => array( 'date'),

				 'message' => 'Ngày sinh không hợp lệ'
			)
		),
		'ngayvaocty_tmp' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng chọn ngày vào công ty'
			),
			'dk2' => array(
				 'rule' => array( 'date'),

				 'message' => 'Ngày vào công ty không hợp lệ'
			)
		),
		'ngaytangluonggannhat_tmp' => array(
			'dk2' => array(
				 'rule' => array( 'date'),
				 'allowEmpty' => true,
				 'message' => 'Ngày tăng lương gần nhất không hợp lệ'
			)
		),
		'batdauhopdong_tmp' => array(
			'dk2' => array(
				 'rule' => 'date',
				 'allowEmpty' => true,
				 'message' => 'Ngày bắt đầu hợp đồng không hợp lệ'
			)
		),
		'ketthuchopdong_tmp' => array(
			'dk2' => array(
				 'rule' => 'date',
				 'allowEmpty' => true,
				 'message' => 'Ngày kết thúc hợp đồng không hợp lệ'
			)
		),
		'loaihopdongdaky' => array(
			'dk2' => array(
				 'rule' => array( 'maxLength' , 255 ),

				 'message' => 'Độ dài không quá 255 kí tự'
			)
		),
		'cmnd' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),
				 'message' => 'Vui lòng nhập CMND'
			)
		),

		// diult dong ngay 30/10/2013
		/*'trinhdochuyenmon' => array(
			'dk2' => array(
				 'rule' => array( 'maxLength' , 255 ),

				 'message' => 'Độ dài không quá 255 kí tự'
			)
		)*/
	);

	function isUploadedFile($params)
	{
		$val = array_shift($params);
		if ((isset($val['error']) && $val['error'] == 0) ||
		(!empty( $val['tmp_name']) && $val['tmp_name'] != 'none')) {
			return is_uploaded_file($val['tmp_name']);
		}
		return false;
	}

	function parentNode() {

		return null;

		if (!$this->id && empty($this->data)) {
			return null;
		}
		$data = $this->data;
		if (empty($this->data)) {
			$data = $this->read();
		}
		if (empty($data['User']['group_id'])) {
			return null;
		} else {
			return array('Group' => array('id' => $data['User']['group_id']));
		}
	}

	/**
	 * After save callback
	 *
	 * Update the aro for the user.
	 *
	 * @access public
	 * @return void
	 */
	function afterSave($created) {
		if (!$created) {
			// $parent = $this->parentNode();
			// $parent = $this->node($parent);
			// $node = $this->node();
			// $aro = $node[0];
			// $aro['Aro']['parent_id'] = $parent[0]['Aro']['id'];
			// $this->Aro->save($aro);
		}
		$this->updateCounter();
		parent::afterSave($created);
	}

	function check_pass( )
	{
		return $this->data['User']['passwrd'] == $this->data['User']['password_xacnhan'];
	}

	function check_passcu( )
	{
		return $this->data['User']['passcu'] == $this->data['User']['pass'];
	}

	function check_exists( )
	{
		if($this->data['User']['username'] == ''){
			return false;
		}
		$ret = $this->find('first', array('contain' => false,
			'conditions' => array('User.trangthai' => 4, 'User.username' => $this->data['User']['username'])) );
		if( isset( $ret['User'] ) > 0  )
		{
			if(isset($this->data['User']['id']) && $this->data['User']['id']== $ret['User']['id'])
			{
				return true;
			}
			return false;
		}
		return true;
	}

	// cac function use for controller ====== namnb 02/05/2012 ============

	function get_nhom()
	{
		return $this->Nhom->find('list',array(
			'conditions' => array(
				'code1' => 'usr',
				'code2' => 'grp',
				'trangthai' => 4
			),
			'fields' => array('id', 'name'),
			'contain' => false,
			'order' => 'name asc'
		));
	}
}

