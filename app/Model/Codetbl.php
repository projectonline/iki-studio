<?php
class Codetbl extends AppModel {
	var $name = 'Codetbl';

	public $actsAs = array('Containable', 'Search.Searchable');

	public $filterArgs = array(
		array('name' => 'code1', 'type' => 'value', 'field' => 'Codetbl.code1'),
		array('name' => 'code2', 'type' => 'value', 'field' => 'Codetbl.code2'),
		array('name' => 'name', 'type' => 'fulltext', 'field' => 'Codetbl.name'),
	);

	var $validate = array(
		'name' => array(
			'dk1' => array(
				'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng nhập tên'
			),
			'dk2' => array(
				 'rule' => array( 'maxLength', 255 ),

				 'message' => 'Tên không quá 255 kí tự'
			)
		)
	);

	var $belongsTo = array(
		'Truongbophan' =>
			array('className' => 'User',
					'foreignKey' => 'parent',
					'fields' => array('id','username', 'avatar', 'tel', 'line_dienthoai', 'realname')
			),
		'LoaiCha' =>
			array('className' => 'Codetbl',
					'foreignKey' => 'parent',
					'conditions' => array('LoaiCha.code1' => 'ctr', 'LoaiCha.code2' => 'tpy')
			),
		'Vitri_tinh' =>
			array('className' => 'Codetbl',
					'foreignKey' => 'parent',
					'conditions' => array('Vitri_tinh.code1' => 'ctr', 'Vitri_tinh.code2' => 'tin'),
					'fields' => array('id', 'name')
			),
	);

	var $hasMany = array(
		'User' =>
			array('className' => 'User',
					'foreignKey' => 'codetbl_id',
					'fields' => array('id','username', 'avatar', 'tel', 'line_dienthoai', 'realname'),
					'conditions' => array('User.trangthai' => 4)
			),
		'Vitri' =>
			array('className' => 'Codetbl',
					'foreignKey' => 'parent',
					'conditions' => array('Vitri.code1' => 'ctr', 'Vitri.code2' => 'tin')
			),
		'LoaiCon' =>
			array('className' => 'Codetbl',
					'foreignKey' => 'parent',
					'conditions' => array('LoaiCon.code1' => 'ctr', 'LoaiCon.code2' => 'tpy', 'LoaiCon.trangthai' => 4)
			),
		'Namtaikhoavt' =>
			array('className' => 'Namtaikhoavt',
					'foreignKey' => 'congty_id',
					'conditions' => array('Namtaikhoavt.trangthai' => 4)
			),
		'CongtrinhsTienvevt' =>
			array('className' => 'CongtrinhsTienvevt',
					'foreignKey' => 'congty_id',
					'conditions' => array('CongtrinhsTienvevt.trangthai' => 4)
			)
	);
}
