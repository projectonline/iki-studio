<?php
class Comment extends AppModel {
    var $name = 'Comment';
    var $validate = array(
        'noidung' => array(
            'dk1' => array(
                 'rule' => array( 'notEmpty' ),
                 'message' => 'Vui lòng nhập nội dung'
            ),
            'dk2' => array(
                'rule' => array( 'maxLength', 20000 ),'allowEmpty' => true,
                'message' => 'Nội dung không quá 20000 kí tự'
            )
        )
    );

    var $belongsTo = array(
        'Nguoitao' => array(
            'className' => 'User',
            'foreignKey' => 'nguoitao',
            'dependent' => false,
            'conditions' => '',
            'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id', 'created'),
            'order' => ''
        )
    );

    var $hasMany = array(
        'File' => array(
            'className'  => 'Uploader',
               'conditions' => array('File.controller' => 'comments', 'File.trangthai <>' => 9),
               'order'      => 'File.created DESC',
               'foreignKey' => 'item_id',
               'fields' => array('id', 'name', 'dungluong', 'nguoitao', 'file_xuat', 'download_tu_do'),
               'dependent'  => true
         )
    );

    public $actsAs = array('Containable', 'Search.Searchable');

    public $filterArgs = array(
        array('name' => 'q_noidung', 'type' => 'fulltext', 'field' => 'Comment.noidung'),
        array('name' => 'q_nguoitao', 'type' => 'like', 'field' => 'Nguoitao.username'),
        array('name' => 'q_searchall', 'type' => 'query', 'method' => 'q_searchall_f'),
    );

    function q_searchall_f( $data, $field )
    {
        if( $data['q_searchall'] )
        {
            $cond['no_model'] = 1;
        }else{
            $cond = array();
        }

        return $cond;
    }
}
