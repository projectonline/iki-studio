<?php //160712 Doan them
class Doitac extends AppModel {

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $name = 'Doitac';
    var $actsAs = array('Search.Searchable');

    public $filterArgs = array(
        array('name' => 'ten', 'type' => 'fulltext', 'field' => 'Doitac.ten'),
        array('name' => 'trangthai', 'type' => 'value', 'field' => 'Doitac.trangthai'),
        array('name' => 'dienthoai', 'type' => 'like', 'field' => 'Doitac.dienthoai'),
        array('name' => 'loai', 'type' => 'value', 'field' => 'Doitac.loai'), //1:client, 2:partner
        array('name' => 'nhan', 'type' => 'value', 'field' => 'Doitac.nhan') //1:cá nhân, 2:công ty
    );

    var $validate = array(
        'ten' => array(
            'dk1' => array(
                'rule' => array('notEmpty'),

                 'message' => 'Vui lòng nhập tên'
            ),
            'dk3' => array(
                 'rule' => array('maxLength', 125 ),

                 'message' => 'Tên không quá 125 kí tự'
            )/*,
            'dk4' => array(
                 'rule' => array('alphaNumeric'),

                 'message' => 'Tên chỉ bao gôm kí tự a->Z và số 0->9'
            )*/
        ),
        'dienthoai' => array(
            'dk1' => array(
                'rule' => array('notEmpty'),

                 'message' => 'Vui lòng nhập số điện thoại'
            )
        )
        /*'congtrinh_id' => array(
            'dk1' => array(
                'rule' => array( 'notEmpty' ),

                 'message' => 'Vui lòng chọn công trình'
            )
        )*/
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'Nguoitao' => array(
            'className' => 'User',
            'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id'),
            'foreignKey' => 'nguoitao'
        ),

        'Nguoisua' => array(
            'className' => 'User',
            'foreignKey' => 'nguoisua',
            'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id')
        ),
        'Congtrinh' => array(
            'className' => 'Congtrinh',
            'foreignKey' => 'congtrinh_id'
        )
    );
}

