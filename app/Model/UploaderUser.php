<?php class UploaderUser extends AppModel {
	var $name = 'UploaderUser';

	var $belongsTo = array(
		'Nguoitao' =>
			array('className' => 'User',
				'foreignKey' => 'nguoitao',
				'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id')
			),
		'User' =>
			array('className' => 'User',
				'foreignKey' => 'user_id',
				'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id')
			)
	);
}
