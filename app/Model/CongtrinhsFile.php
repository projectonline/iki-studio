<?php
class CongtrinhsFile extends AppModel {
	var $name = 'CongtrinhsFile';
	var $validate = array(
		'tieude' => array(
			'dk1' => array(
				 'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng nhập tiêu đề'
			),
			'dk2' => array(
				'rule' => array( 'maxLength', 255 ),
				'allowEmpty' => true,
				'message' => 'Tiêu đề không quá 255 kí tự'
			)
		),
		'mota' => array(
			'dk1' => array(
				 'rule' => array( 'notEmpty' ),

				 'message' => 'Vui lòng nhập mô tả'
			),
			'dk2' => array(
				'rule' => array( 'maxLength', 2000 ),
				'allowEmpty' => true,
				'message' => 'Nội dung không quá 2000 kí tự'
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Nguoitao' =>
				array('className' => 'User',
						'foreignKey' => 'nguoitao',
						'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id')
				),
			'Congtrinh' =>
				array('className' => 'Congtrinh',
					'foreignKey' => 'congtrinh_id',
					'counterCache' => true,
					'counterScope' => array('CongtrinhsFile.trangthai' => 4)
			 )
	);

	 var $hasMany = array(
		'File' => array(
			'className'  => 'Uploader',
			'conditions' => array(
				'File.controller' => 'congtrinhs_files',
				'File.trangthai' => 4
			),
			'order'      => 'File.name ASC',
			'foreignKey' => 'item_id',
			'fields' => array( 'id', 'name', 'dungluong', 'nguoitao', 'file_xuat', 'download_tu_do'),
			'dependent'  => true
		 )
	  );
}
