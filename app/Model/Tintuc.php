<?php
class Tintuc extends AppModel {
    var $name = 'Tintuc';
    var $validate = array(
        'tieude' => array(
            'require' => array(
                 'rule' => array( 'notEmpty' ),

                 'message' => 'Vui lòng nhập tiêu đề'
            ),
            'maxLength' => array(
                'rule' => array( 'maxLength', 255 ),
                'allowEmpty' => true,
                'message' => 'Tiêu đề không quá 255 kí tự'
            )
        ),
        'noidung' => array(
            'require' => array(
                 'rule' => array( 'notEmpty' ),

                 'message' => 'Vui lòng nhập nội dung'
            ),
            'maxLength' => array(
                'rule' => array( 'maxLength', 100000 ),
                'allowEmpty' => true,
                'message' => 'Nội dung không quá 100000 kí tự'
            )
        )
    );
    var $belongsTo = array(
            'Nguoitao' =>
                array('className' => 'User',
                    'foreignKey' => 'nguoitao',
                    'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id'),
                    'conditions' => '',
                    'order' => '',
                    'counterCache' => ''
                )
    );
}
