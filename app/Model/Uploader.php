<?php
class Uploader extends AppModel{
	var $name = 'Uploader';
	var $useTable = 'files';
	var $belongsTo = array(
		'Nguoitao' => array(
			'className'  => 'User',
			'foreignKey' => 'nguoitao',
			'fields'     => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id')
		),
		'CongtrinhsFile' => array(
			'className'  => 'CongtrinhsFile',
			'foreignKey' => 'item_id',
			'fields'     => array('id', 'loai', 'ngay_xuat')
		)
	);

	private function _generateRandomString($length = 6) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
}