<?php
class Loinhanphongit extends AppModel {
    var $name = 'Loinhanphongit';
    var $validate = array(
        'noidung' => array(
            'dk1' => array(
                 'rule' => array( 'notEmpty' ),
                 'message' => 'Vui lòng nhập nội dung'
            ),
            'dk2' => array(
                'rule' => array( 'maxLength', 4000 ),'allowEmpty' => true,
                'message' => 'Nội dung không quá 4000 kí tự'
            )
        )
    );

    var $belongsTo = array(
        'Nguoitao' => array(
            'className' => 'User',
            'foreignKey' => 'nguoitao',
            'fields' => array('id', 'username', 'avatar', 'skype', 'tel', 'line_dienthoai', 'realname', 'codetbl_id')
          )
    );

    var $hasMany = array(
        'Comment' => array(
            'className' => 'Comment',
            'conditions' => array(
                'Comment.trangthai' => 4,
                'Comment.model' => 'Loinhanphongit'
            ),
            'foreignKey' => 'item_id',
            'dependent' => true,
            'fields' => array('id', 'created', 'noidung', 'noidung_attach', 'nguoitao'),
            'order' => 'Comment.id desc',
            'limit' => 3
        )
    );
}
