<?php
/**
 * This is email configuration file.
 *
 * Use it to configure email transports of Cake.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 2.0.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * In this file you set up your send email details.
 *
 * @package       cake.config
 */
/**
 * Email configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * transport => The name of a supported transport; valid options are as follows:
 *		Mail 		- Send using PHP mail function
 *		Smtp		- Send using SMTP
 *		Debug		- Do not send the email, just return the result
 *
 * You can add custom transports (or override existing transports) by adding the
 * appropriate file to app/Network/Email.  Transports should be named 'YourTransport.php',
 * where 'Your' is the name of the transport.
 *
 * from =>
 * The origin email. See CakeEmail::from() about the valid values
 *
 */
class EmailConfig {

//		I ran across this error today after noticing a file_get_contents was not working. This was on a page that had been working fine for about a year.
//		“php_network_getaddresses: getaddrinfo failed: Name or service not known”
//
//		This issue is typically caused by the Apache/PHP host unable to contact the DNS server.
//
//		The first thing to check is to see if you can ping the remote host using console.
//
//		If no, then your primary name server on /etc/resolv.conf (Debian) is not working. Find a working DNS and restart Apache.
//		If yes, then Apache is still connecting to the broken DNS server. You should try restarting Apache to see if it refreshes the DNS server listing in /etc/resolv.conf.
//		Other suggestions to fixing this are:
//		1) Making sure there is an entry inside /etc/hosts for your localhost
//		2) Firewall rules
//		3) Manually entering the IP (last resort) You shouldn’t enter the IP since it may change, especially if its a host that you are not in control over.

	// Diult

	public $default = array(
		'transport' => 'Mail',
		'from' => 'you@localhost',
		//'charset' => 'utf-8',
		//'headerCharset' => 'utf-8',
	);
	//email vtco
	public $smtp = array(
		'transport' => 'Smtp',
		'from' => array('vantruong@vtco.com.vn' => 'CÔNG TY TNHH ĐẦU TƯ VTCO'),
		'host' => 'mail.vtco.com.vn',
		'port' => 25,
		'username' => 'vantruong@vtco.com.vn',
		'password' => 'vtco2469',
		'transport' => 'Smtp',
		'timeout' => 30,
		'client' => null,
		'log' => false,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8',
	);
//	public $smtp = array(
//		'transport' => 'Smtp',
//		'from' => array('lethidiu02@gmail.com' => 'Công ty Tư vấn Văn Trường'),
//		'host' => 'ssl://smtp.gmail.com',
//		'port' => 465,
//		'username' => 'lethidiu02@gmail.com',
//		'password' => 'huongtoimattroi',
//		'transport' => 'Smtp',
//		'timeout' => 30,
//		'client' => null,
//		'log' => false,
//		'charset' => 'utf-8',
//		'headerCharset' => 'utf-8',
//	);
	//email ddacons
	public $smtpddacons = array(
		'transport' => 'Smtp',
		'from' => array('info@ddacons.com' => 'CÔNG TY CỔ PHẦN KỸ THUẬT DDA'),
		'host' => 'mail.ddacons.com',
		'port' => 25,
		'username' => 'info@ddacons.com',
		'password' => 'vtvn123',
		'transport' => 'Smtp',
		'timeout' => 30,
		'client' => null,
		'log' => false,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8',
	);
	// public $smtpddacons= array(
	// 	'transport' => 'Smtp',
	// 	'from' => array('info@ddacons.com' => 'Công ty DDACONS'),
	// 	'host' => 'mail.ddacons.com',
	// 	'port' => 25,
	// 	'username' => 'info@ddacons.com',
	// 	'password' => 'vtvn123',
	// 	'transport' => 'Smtp',
	// 	'timeout' => 30,
	// 	'client' => null,
	// 	'log' => false,
	// 	'charset' => 'utf-8',
	// 	'headerCharset' => 'utf-8',
	// );


	//email scc
	public $smtpscc = array(
		'transport' => 'Smtp',
		'from' => array('info@sccsingapore.com' => 'Công Ty SCC'),
		'host' => 'ssl://smtp.gmail.com',
		'port' => 465,
		'username' => 'info@sccsingapore.com',
		'password' => 'space123',
		'sendas' => 'html',
		'transport' => 'Smtp',
		'timeout' => 30,
		'client' => null,
		'log' => false,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8',
	);

	//email las92
	public $smtplas92 = array(
		'transport' => 'Smtp',
		'from' => array('info@las92.com' => 'Công ty Cổ phần LAS92'),
		//'from' => array('lethidiu02@gmail.com' => 'Công Ty LAS92'),
		'host' => 'ssl://smtp.gmail.com',
		'port' => 465,
		'username' => 'info@las92.com',
		'password' => 'space123',
		'transport' => 'Smtp',
		'timeout' => 30,
		'client' => null,
		'log' => false,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8',
	);
	//email spaceaa
	public $smtpspaceaa = array(
		'transport' => 'Smtp',
		'from' => array('info@spaceaa.com' => 'Công Ty Đầu Tư Space'),
		'host' => 'ssl://smtp.gmail.com',
		'port' => 465,
		'username' => 'info@spaceaa.com',
		'password' => 'space123',
		'transport' => 'Smtp',
		'timeout' => 30,
		'client' => null,
		'log' => false,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8',
	);
	//email kidohu
	public $smtpskidohu = array(
		'transport' => 'Smtp',
		'from' => array('info@kidohu.com' => 'CÔNG TY CỔ PHẦN KI DO HU'),
		'host' => 'ssl://smtp.gmail.com',
		'port' => 465,
		'username' => 'info@kidohu.com',
		'password' => 'space123',
		'transport' => 'Smtp',
		'timeout' => 30,
		'client' => null,
		'log' => false,
		'charset' => 'utf-8',
		'headerCharset' => 'utf-8',
	);

	// public $smtp = array(
	// 	'transport' => 'Smtp',
	// 	'from' => array('services@aapple.com.vn' => 'Dich Vu Aapple'),
	// 	'host' => 'ssl://smtp.gmail.com',
	// 	'port' => 465,
	// 	'username' => 'services@aapple.com.vn',
	// 	'password' => 'kdvspace',
	// 	'transport' => 'Smtp',
	// 	'timeout' => 30,
	// 	'client' => null,
	// 	'log' => false,
	// 	'charset' => 'utf-8',
	// 	'headerCharset' => 'utf-8',
	// );


//	public $smtp = array(
//		'transport' => 'Smtp',
//		'from' => array('site@localhost' => 'My Site'),
//		'host' => 'localhost',
//		'port' => 25,
//		'timeout' => 30,
//		'username' => 'user',
//		'password' => 'secret',
//		'client' => null,
//		'log' => false
//		//'charset' => 'utf-8',
//		//'headerCharset' => 'utf-8',
//	);

	public $fast = array(
		'from' => 'you@localhost',
		'sender' => null,
		'to' => null,
		'cc' => null,
		'bcc' => null,
		'replyTo' => null,
		'readReceipt' => null,
		'returnPath' => null,
		'messageId' => true,
		'subject' => null,
		'message' => null,
		'headers' => null,
		'viewRender' => null,
		'template' => false,
		'layout' => false,
		'viewVars' => null,
		'attachments' => null,
		'emailFormat' => null,
		'transport' => 'Smtp',
		'host' => 'localhost',
		'port' => 25,
		'timeout' => 30,
		'username' => 'user',
		'password' => 'secret',
		'client' => null,
		'log' => true,
		//'charset' => 'utf-8',
		//'headerCharset' => 'utf-8',
	);

}
