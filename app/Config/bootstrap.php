<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));
/*Cache::config('default', array(
        'engine' => 'Memcache', //[required]
        'duration' => 3600, //[optional]
        'probability' => 100, //[optional]
        'prefix' => '_', //[optional]  prefix every cache file with this string
        'servers' => array(
                '127.0.0.1:11211' // localhost, default port 11211
        ), //[optional]
        'persistent' => true, // [optional] set this to false for non-persistent connections
        'compress' => false, // [optional] compress data in Memcache (slower, but uses less memory)
));*/
/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Plugin' => array('/full/path/to/plugins/', '/next/full/path/to/plugins/'),
 *     'Model' =>  array('/full/path/to/models/', '/next/full/path/to/models/'),
 *     'View' => array('/full/path/to/views/', '/next/full/path/to/views/'),
 *     'Controller' => array('/full/path/to/controllers/', '/next/full/path/to/controllers/'),
 *     'Model/Datasource' => array('/full/path/to/datasources/', '/next/full/path/to/datasources/'),
 *     'Model/Behavior' => array('/full/path/to/behaviors/', '/next/full/path/to/behaviors/'),
 *     'Controller/Component' => array('/full/path/to/components/', '/next/full/path/to/components/'),
 *     'View/Helper' => array('/full/path/to/helpers/', '/next/full/path/to/helpers/'),
 *     'Vendor' => array('/full/path/to/vendors/', '/next/full/path/to/vendors/'),
 *     'Console/Command' => array('/full/path/to/shells/', '/next/full/path/to/shells/'),
 *     'locales' => array('/full/path/to/locale/', '/next/full/path/to/locale/')
 * ));
 *
 */

/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */

 CakePlugin::load( array( 'Search', 'AjaxMultiUpload' )); //, 'Users' => array('routes' => true), 'Utils') );

// CakePlugin::load('mongodb');
 //CakePlugin::load('ReportManager',array('bootstrap' => true));

// http://www.cakephp.bee.pl/
// http://www.cakephp.4uk.pl/
// http://jquery.bassistance.de/autocomplete/demo/ jquery autocomplete
// http://jquery.bassistance.de/autocomplete/demo/

//	border-top-color: #E7E7E7;
//  border-top-style: solid;
//  border-top-width: 1px;

	 // Nhung dieu can chu y trong cake/libs
	define( 'TIME_CACHE_ELEMENT', '+1 day' );

	define( 'WEBSITE', 'http://qlda.spaceaa.com' );
	define( 'WEBSITE_HINH', 'http://hinh.spaceaa.com' );

	define( 'DIR_UPLOAD', APP.'upload/' );
	define( 'APP_DIR_UPLOAD', APP.'upload/' );

	define( 'W_UPLOAD', WWW_ROOT.'upload/' );

	// 2 CÁI DEFINE NÀY PHẢI ĐỒNG BỘ VỚI NHAU CÙNG THƯ MỤC
	define( 'DOWNLOAD_FROM_WEBROOT_SYMLINK', '/upload/tmp/symlink_download_file/' );
	define( 'SYMLINK_DOWNLOAD_FILE', W_UPLOAD.'tmp/symlink_download_file/' );

	define( 'LINK_MAIL_WEB_VTCO', 'http://mail.vtco.com.vn' );
	define( 'LINK_MAIL_WEB_DDACONS', 'http://mail.ddacons.com' );

	define( 'CACHE_QUERY', true );
	define( 'URL_BASE',   'http://'. env( 'SERVER_NAME' ).':'.env( 'SERVER_PORT' ) );
	define( 'MAX_RESULT_NUMBER', 100 );
	define( 'MIN_RESULT_NUMBER', 10 );
	define( 'UNCHOOSE', '----------' );
	define( 'MAX_SIZE_UPLOAD_HINH', 5000000 );
	define( 'SMALL', APP. 'webroot/upload/' );
	define( 'SMALLFILE', APP. 'upload/' );
	define( 'ALBUM_UPLOAD_PATH', DIR_UPLOAD . 'album/' );

	// Thumbnail of image
	define( 'IMAGE_THUMBNAIL', SMALL . 'thumbnails' );
	define( 'IMAGE_THUMBNAIL_WIDTH', '100' );
	define( 'IMAGE_THUMBNAIL_HEIGHT', '100' );
	define( 'IMAGE_THUMBNAIL_Q', '100' );
	define( 'IMAGE_THUMBNAIL_ZC', 'TL' );
	define( 'IMAGE_THUMBNAIL_FORMAT', 'png' );

	define( 'MAX_FILE_UPLOAD', 50 ); // files

	// namnb 19/04/2012
	define( 'MAX_SIZE_UPLOAD', (ini_get('upload_max_filesize') - 10) ); // MB
	define( 'POST_MAX_SIZE', (ini_get('post_max_size') - 10) ); // MB

	define( 'TMP_DIR_UPLOAD_AJAX', TMP.'upload_ajax/' ); // MB
	define( 'SYMBOL_TMP_URL_UPLOAD_AJAX', '_8_8_8_' ); // MB

	define( 'TGIAN_DUOC_PHEP_XOA_FILE', DAY*3 );

    define( 'COOKIE_MULTI_LOGIN', 'cookie_spaceaa' );
