<?php
class ChinhsuaShell extends AppShell {

	public $uses = array(
		'Phaplyvt',
		'Banvevt',
		'Bienban',
		'Khaosat',
		'Bantinh',
		'Giaodichvt',
		'Uploader',
		'CongtrinhsFile'
	);

	public function main() {

		$this->out('bat dau copy ....');

		$args = array(
			'Phaplyvt' => 'phaplyvts',
			'Banvevt' => 'banvevts',
			'Bienban' => 'bienbans',
			'Khaosat' => 'khaosats',
			'Bantinh' => 'bantinhs',
			'Giaodichvt' => 'giaodichvts'
		);

		$loai = array(
			'Phaplyvt' => 'phaply',
			'Banvevt' => 'banve',
			'Bienban' => 'bienban',
			'Khaosat' => 'khaosat',
			'Bantinh' => 'bantinh',
			'Giaodichvt' => 'giaodich'
		);

		foreach( $args as $key => $value )
		{

			$datas = $this->Uploader->find('list', array(
				'fields' => array('id', 'item_id'),
				'conditions' => array(
					'Uploader.controller' => $value,
					'Uploader.trangthai' => 4,
					'Uploader.created >' => '2012-12-20 00:00:00'
				),
				'contain' => false
			));

			$id_arr = array_values( array_unique($datas) );

			$datas = $this->$key->find('all', array(
				'conditions' => array(
					$key.'.id' => $id_arr,
					$key.'.trangthai' => 4
				),
				'contain' => false
			));

			foreach( $datas as $key1 => $value1 )
			{
				$save = $value1[$key];

				unset( $save['id'] );

				$save['loai'] = $loai[$key];

				$this->CongtrinhsFile->create();
				if( !$this->CongtrinhsFile->save( $save ) )
				{
					$this->out( 'Loi' );
					pr($value1);
				}else{

					$field = array('Uploader.controller' => '"congtrinhs_files"', 'Uploader.item_id' => $this->CongtrinhsFile->id );
					$cond = array('Uploader.controller' => $value, 'Uploader.item_id' => $value1[$key]['id']);

					// SAVE Chamcong 9
					$this->Uploader->belongsTo = array();
					if( !$this->Uploader->updateAll( $field, $cond ) )
					{
						$this->out('Update that bai.');
					}

					$this->out('ok');

				}

			}

			$this->out( 'ok -------------- ' . $key );

		}

		$this->out('Ket thuc.');

		exit;
	}

}
