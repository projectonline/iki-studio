<?php
class CopycongtyShell extends AppShell {

	public $uses = array(
		'Congtrinh',
		'CongtrinhsTienvevt'
	);

	public function main() {

		$this->out('bat dau copy ....');

		$datas = $this->Congtrinh->find('all', array(
			'fields' => array('id', 'congtythuchien_id'),
			'conditions' => array(
				'Congtrinh.trangthai' => 4,
				'Congtrinh.congtythuchien_id >' => 0
			),
			'contain' => false
		));

		foreach( $datas as $data )
		{
			$field = array(
				'CongtrinhsTienvevt.congty_id' => $data['Congtrinh']['congtythuchien_id']
			);
			$cond = array(
				'CongtrinhsTienvevt.congtrinh_id' => $data['Congtrinh']['id']
			);

			$this->CongtrinhsTienvevt->belongsTo = array();
			if( !$this->CongtrinhsTienvevt->updateAll( $field, $cond ) )
			{
				$this->out('Loi update id='.$data['Congtrinh']['id'].' & congtythuchien_id'.$data['Congtrinh']['congtythuchien_id']);
			}
		}

		$this->out('Ket thuc.');

		exit;
	}

}
