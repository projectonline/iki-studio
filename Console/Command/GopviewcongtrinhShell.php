<?php
class GopviewcongtrinhShell extends AppShell {

	public $uses = array(
		// 'Congvan',
		// 'Congvandens',
		// 'Congvandivt',
		// 'Nghiemthuvt',
		// 'Kiemdinhthinghiem',
		// 'Ddahinhcongtrinh',
		'CongtrinhsBct',
		'CongtrinhsBaocaotuan',
		'Uploader',
		'CongtrinhsFile'
	);

	public function main() {

		$this->out('bat dau copy ....');

		$args = array(
			'CongtrinhsBct',
			'CongtrinhsBaocaotuan'
		);

		$args_tmp = array(
			'CongtrinhsBct' => 'congtrinhs_bcts',
			'CongtrinhsBaocaotuan' => 'congtrinhs_baocaotuans'
		);


		foreach ($args as $key => $model)
		{
			# code...

			$this->out('Lay tat ca du lieu ....');

			$datas = $this->$model->find('all', array(
				'contain' => false
			));


			$this->out(' ==============================================');
			$this->out('Model = '.$model);

			foreach( $datas as $key => $data )
			{
				if( $key%50 == 0 )
				{
					$this->out($model.' + Qua moc '.$key);
				}

				$save = $data[$model];
				unset($save['id']);
				$save['loai'] = strtolower(str_replace('vt', '', $model));

				if( $model == "Congvan" )
				{
					$save['maso'] = $save['socongvan'];
					$save['tieude'] = $save['to'];
					$save['mota'] .= '<br><br><b>Nơi nhận</b>: '.$save['to'];
				}

				$this->CongtrinhsFile->create();
				if( $this->CongtrinhsFile->save( $save ) )
				{
					$field = array(
						'Uploader.controller' => '"congtrinhs_files"',
						'Uploader.item_id' => $this->CongtrinhsFile->id
					);
					$cond = array(
						'Uploader.controller' => $args_tmp[$model],
						'Uploader.item_id' =>  $data[$model]['id']
					);

					// SAVE Uploader 9
					$this->Uploader->belongsTo = array();
					if( !$this->Uploader->updateAll( $field, $cond ) )
					{
						$this->out($model.' ---- Uploader updateAll that bai: id='.$this->CongtrinhsFile->id);
					}


				}else{

					pr($this->CongtrinhsFile->validationErrors);;
					$this->out($model.' ---- CongtrinhsFile create that bai: id='.$data[$model]['id']);
				}
			}

		}


		// $datas = $this->$model->find('all', array(
		// 	'contain' => array(
		// 		'File' => array('id')
		// 	),
		// 	'limit' => 200
		// ));

		// $this->out(' ==============================================');
		// $this->out('Model = '.$model);

		// foreach( $datas as $key => $data )
		// {
		// 	if( $key%10 == 0 )
		// 	{
		// 		$this->out(' + Qua moc '.$key);
		// 	}

		// 	$save['CongtrinhsFile'] = $data[$model];
		// 	unset($save['CongtrinhsFile']['id']);
		// 	$save['CongtrinhsFile']['loai'] = strtolower(str_replace('vt', '', $model));
		// 	$save['CongtrinhsFile']['mota'] = $save['CongtrinhsFile']['mota'];

		// 	foreach( $data['File'] as $file )
		// 	{
		// 		unset($file['id']);
		// 		$save['File'][] = $file;
		// 	}

		// 	if( !$this->CongtrinhsFile->saveAll( $save, array('validate' => false) ) )
		// 	{
		// 		$this->out($model.' ---- save that bai: id='.$data['CongtrinhsFile']['id']);
		// 	}
		// }

		$this->out('Ket thuc.');

		exit;
	}

}
