<?php
class TaskcmcountShell extends AppShell {

	public $uses = array('Task', 'Comment');

	public function main() {

		$this->out('bat dau ....');

		$tasks = $this->Task->find('all', array(
			'fields' => array( 'id', 'modified' ),
			'conditions' => array('Task.trangthai' => 4),
			'contain' => false,
			'order' => 'Task.id desc'
		));

		foreach( $tasks as $task )
		{
			$this->out('thuc hien count comment ....');
			$comments = $this->Comment->find('all', array(
				'fields' => array('id', 'nguoitao', 'count(nguoitao) as count_nguoitao', 'model', 'item_id'),
				'conditions' => array(
					'Comment.trangthai' => 4,
					'Comment.item_id' => $task['Task']['id'],
					'Comment.model' => 'Task'
				),
				'contain' => array('Nguoitao'),
				'group' => 'nguoitao'
			));

			$tmps = array();
			foreach( $comments as $comment )
			{
				if(!isset($tmps[ $comment['Nguoitao']['codetbl_id']]))
				{

					$tmps[ $comment['Nguoitao']['codetbl_id']] = 0;
				}

				$tmps[ $comment['Nguoitao']['codetbl_id']] += $comment[0]['count_nguoitao'];
			}

			$save_to_task = ''; // style: phongban_countnumber_._phongban_countnumber_._phongban_countnumber_
			foreach( $tmps as $key => $tmp )
			{
				if( $save_to_task == '' )
				{
					$save_to_task .= $key.'_'.$tmp;
				}else{
					$save_to_task .= '_._'.$key.'_'.$tmp;
				}

			}

			$tmp_save = array(
				'id' => $task['Task']['id'],
				'num_cm_per_department' => $save_to_task,
				'modified' => $task['Task']['modified']
			);
			if( !$this->Task->save($tmp_save) )
			{
				$this->out('Error: Task.id = '.$this->Task->id.'......... save_to_task = '.$save_to_task);
			}
		}

		$this->out('Xong.');

		exit;
	}
}
