<?php
class HelloShell extends AppShell {

	public $uses = array(
		'Comment',
		'Traloiphongit',
		'Traloisinhnhat',
		'Traloitacphong',
		'Traloinhansuhanhchanh',
		'Traloiketoan',
		'Traloipkinhdoanh',
		'Traloipkhaosat',
		'Traloipthicong',
		'Kcstraloiloinhan',
		'Markettraloiloinhan',
		'Traloichinhanhhn',
		'Traloicnmt',
		'Daututraloiloinhan',
		'Traloinghiphep',
		'Traloitangcavt',
		'Traloidicongtac',
		'Traloiytuong',
		'Traloichiasekn',
		'Traloingoaikhoavt',
		'Traloigiaonhanhosovt',
		'Traloituyendung',
		'Traloitask',
		'Traloiddatask',
		'Traloitaskkientruc',
		'Traloitaskketcauvt',
		'Traloitaskthinghiem',
		'Thicongtraloitask',
		'Cnmttraloitask',
		'Hntraloitask',		

	);

	public function main() {

		$this->out('In this->args....');
		$this->out( print_r($this->args) );

		$arr = array(
			'Traloiphongit' => 'loinhanphongit_id',
			'Traloisinhnhat' => 'loinhansinhnhat_id',
			'Traloitacphong' => 'loinhantacphong_id',
			'Traloinhansuhanhchanh' => 'loinhannhansuhanhchanh_id',
			'Traloiketoan' => 'loinhanketoan_id',
			'Traloipkinhdoanh' => 'loinhanpkinhdoanh_id',
			'Traloipkhaosat' => 'loinhanpkhaosat_id',
			'Traloipthicong' => 'loinhanpthicong_id',
			'Kcstraloiloinhan' => 'kcsloinhan_id',
			'Traloibaotritaisan' => 'baotritaisan_id',
			'Markettraloiloinhan' => 'marketloinhan_id',
			'Traloipme' => 'loinhanpme_id',			
			'Traloivibim' => 'loinhanvibim_id',
			'Traloichinhanhhn' => 'loinhanchinhanhhn_id',
			'Traloicnmt' => 'loinhancnmt_id',
			'Daututraloiloinhan' => 'dautuloinhan_id',
			'Traloinghiphep' => 'loinhannghiphep_id',
			'Traloitangcavt' => 'loinhantangcavt_id',
			'Traloidicongtac' => 'loinhandicongtac_id',
			'Traloiytuong' => 'loinhanytuong_id',
			'Traloichiasekn' => 'loinhanchiasekn_id',
			'Traloingoaikhoavt' => 'loinhanngoaikhoavt_id',
			'Traloigiaonhanhosovt' => 'loinhangiaonhanhosovt_id',
			'Traloituyendung' => 'loinhantuyendung_id',
			'Traloitask' => 'task_id',
			'Traloiddatask' => 'ddatask_id',
			'Traloitaskkientruc' => 'taskkientruc_id',
			'Traloitaskketcauvt' => 'taskketcauvt_id',
			'Traloitaskthinghiem' => 'taskthinghiem_id',
			'Thicongtraloitask' => 'thicongtask_id',
			'Cnmttraloitask' => 'cnmttask_id',
			'Hntraloitask' => 'hntask_id'
		);

		$get_model_loinhan = array(
			'Traloiphongit' => 'Loinhanphongit',
			'Traloisinhnhat' => 'Loinhansinhnhat',
			'Traloitacphong' => 'Loinhantacphong',
			'Traloinhansuhanhchanh' => 'Loinhannhansuhanhchanh',
			'Traloiketoan' => 'Loinhanketoan',
			'Traloipkinhdoanh' => 'Loinhanpkinhdoanh',
			'Traloipkhaosat' => 'Loinhanpkhaosat',
			'Traloipme' => 'Loinhanpme',			
			'Traloivibim' => 'Loinhanvibim',
			'Traloipthicong' => 'Loinhanpthicong',
			'Kcstraloiloinhan' => 'Kcsloinhan',
			'Markettraloiloinhan' => 'Marketloinhan',
			'Traloichinhanhhn' => 'Loinhanchinhanhhn',
			'Traloicnmt' => 'Loinhancnmt',
			'Daututraloiloinhan' => 'Dautuloinhan',
			'Traloinghiphep' => 'Loinhannghiphep',
			'Traloitangcavt' => 'Loinhantangcavt',
			'Traloidicongtac' => 'Loinhandicongtac',
			'Traloiytuong' => 'Loinhanytuong',
			'Traloichiasekn' => 'Loinhanchiasekn',
			'Traloingoaikhoavt' => 'Loinhanngoaikhoavt',
			'Traloigiaonhanhosovt' => 'Loinhangiaonhanhosovt',
			'Traloituyendung' => 'Loinhantuyendung',
			'Traloitask' => 'Task',
			'Traloiddatask' => 'Ddatask',
			'Traloitaskkientruc' => 'Taskkientruc',
			'Traloitaskketcauvt' => 'Taskketcauvt',
			'Traloitaskthinghiem' => 'Taskthinghiem',
			'Thicongtraloitask' => 'Thicongtask',
			'Cnmttraloitask' => 'Cnmttask',
			'Hntraloitask' => 'Hntask',
			'Traloibaotritaisan' => 'Baotritaisan'

		foreach( $arr as $key => $value )
		{
			//$this->loadModel($key);
			$data = $this->{$key}->find('all', array(
				'contain' => false
			));

			$data = Set::classicExtract($data, '{n}.'.$key);
			foreach( $data as $key1 => $value1  )
			{
				$data[$key1]['model'] = $get_model_loinhan[$key];
				$data[$key1]['item_id'] = $data[$key1][$value];

				if( !is_numeric($data[$key1]['item_id']) || strlen($data[$key1]['noidung']) < 2)
				{
					//$this->out( print_r($data[$key1]) );
					unset($data[$key1]);
					continue;
				}

				$data[$key1]['tmp_model_traloi'] = $key;
				$data[$key1]['tmp_id_traloi'] = $data[$key1]['id'];

				unset($data[$key1][$value], $data[$key1]['id']);
			}

			$this->out('Bat dau saveAll...'.$key.'.....');
			if( $this->Comment->saveAll($data) )
			{
				$this->out( ' ...............----> xong ' );
			}else{
				$this->out( print_r($this->Comment->validationErrors) );
				$this->out( ' ...............----> ERROR ' );
			}
		}

		$this->out('Ket thuc.');

		exit;
	}
}
