<?php
class SmssendtolocalShell extends AppShell {

	public $uses = array(
		'Smstmp',
		'Smsoutbox'
	);

	public function main() {

		$this->out('bat dau vao ham ....');

		if( date('H:i') == '07:01' ){

			$save['PHONENUMBER'] = '0972509727';
			$save['CONTENT'] = "<SMS qlda> Hi you, I'm ok.";

			$this->Smsoutbox->save( $save );
		}

		$datas = $this->Smstmp->find('all', array(
			'conditions' => array(
				'Smstmp.trangthai' => 4
			),
			'contain' => false
		));

		foreach( $datas as $data )
		{
			$save['PHONENUMBER'] = $data['Smstmp']['sdt'];
			$save['CONTENT'] = $data['Smstmp']['noidung'];

			$this->Smsoutbox->create();
			if( $this->Smsoutbox->save( $save ) )
			{
				$this->Smstmp->id = $data['Smstmp']['id'];
				if( !$this->Smstmp->saveField('trangthai', 9) )
				{
					$this->log('Có lỗi khi saveField trangthai=9 bên Smstmp');
					break;
				}

			}else
			{
				$this->out('Error=');
				pr($save);
			}
		}

		$this->out('Ket thuc.');

		exit;
	}

}
