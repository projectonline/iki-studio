<?php
class ThongkethanhvienShell extends AppShell {

	public $uses = array(
		'Task','Taskkientruc',
		'Ddatask','Taskthinghiem',
		'Taskketcauvt','Thicongtask',
		'Hntask', 'Cnmttask',
		'Comment'
	);

	public function main() {

		$this->out('bat dau copy ....');

		foreach( $this->uses as $model )
		{
			if( $model == 'Comment' )continue;

			$datas = $this->{$model}->find('all', array(
				'fields' => array('id', 'comment_count'),
				'conditions' => array( $model.'.trangthai' => 4 ),
				'contain' => false
			));

			foreach( $datas as $data  )
			{
				if( $data[$model]['comment_count'] > 0 )
				{
					$tmp = $this->Comment->find('all', array(
						'fields' => array('id', 'nguoitao'),
						'conditions' => array(
							'Comment.item_id' => $data[$model]['id'],
							'Comment.trangthai' => 4,
							'Comment.model' => $model,
							'Nguoitao.trangthai' => 4
						),
						'contain' => array('Nguoitao' => array('id', 'username')),
						'group' => 'Comment.nguoitao'
					));

					if( isset($tmp[0]) )
					{
						$string = '';
						foreach( $tmp as $tmp_string )
						{
							if( $string == '' )
							{
								$string = $tmp_string['Nguoitao']['id'];
							}else{
								$string .= '_*_'.$tmp_string['Nguoitao']['id'];
							}
						}

						$this->{$model}->id = $data[$model]['id'];
						if( !$this->{$model}->saveField('user_id_traloi', $string) )
						{
							echo 'loi model='.$model.'  id='.$data[$model]['id'];
						}
					}
				}

			}
		}

		$this->out('Ket thuc.');

		exit;
	}

}
