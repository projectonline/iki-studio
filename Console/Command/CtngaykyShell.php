<?php
class CtngaykyShell extends AppShell {

	public $uses = array('Congtrinh');

	public function main() {

		$this->out('In this->args....');

		$array_tmp = array(
			'fields' => array('id', 'ngayky'),
			'conditions' => array(
				'Congtrinh.trangthai' => 4
			),
			'limit' => 1000,
			'contain' => array(
				'CongtrinhsHopdong' => array('id', 'tieude', 'ngayky')
			)
		);

		$datas = $this->Congtrinh->find('all',$array_tmp );

		foreach( $datas as $data )
		{

			if( isset($data['CongtrinhsHopdong'][0]) )
			{
				$this->out('Bat dau luu ....');
				$save = array();
				$save['id'] = $data['Congtrinh']['id'];
				$save['ngayky'] = $data['CongtrinhsHopdong'][0]['ngayky'];
				if( !$this->Congtrinh->save($save, false) )
				{
					$this->out('Error cong trinh: '.$save['id']);
				}else{
					//$this->out('thanh cong');
				}
			}
		}

		$this->out('Ket thuc.');

		exit;
	}
}
