<?php
class OnedrivedownloadShell extends AppShell {

	public $uses = array('Uploader', 'FilesOnedriverAuth', 'FilesOnedriverToken');

	public function main() {

		// load auth app info
		// $this->loadModel('FilesOnedriverAuth');
		$onedrive_auth = $this->FilesOnedriverAuth->find('first', array(
		 'conditions' => array('FilesOnedriverAuth.id' => 1),
		 'contain'    => false
		));
		$onedrive_auth = $onedrive_auth['FilesOnedriverAuth'];

		// Define security credentials for your app.
		define("client_id", $onedrive_auth['client_id']);
		define("client_secret", $onedrive_auth['client_secret']);
		define("callback_uri", $onedrive_auth['callback_uri']);
		define("onedrive_base_url", $onedrive_auth['onedrive_base_url']);
		require_once(APP.'Controller'.DS.'UploadersSkydrive.php');

		// get token for app access into account live.com
		// $this->loadModel('FilesOnedriverToken');
		$access_token = $this->FilesOnedriverToken->find('first', array(
		 'conditions' => array('FilesOnedriverToken.id' => 1),
		 'contain'    => false
		));
		$access_token = $access_token['FilesOnedriverToken'];

		// check token is expired or not
		if (time() > (int)$access_token['expires_in']) { // Token needs refreshing. Refresh it and then return the new one.
		 $refreshed = skydrive_auth::refresh_oauth_token($access_token['refresh_token']);
		 $refreshed['id']         = $access_token['id'];
		 $refreshed['expires_in'] = time() + (int)$refreshed['expires_in'];

		 // update old token into new token
		 if ($this->FilesOnedriverToken->save($refreshed))
			 $token = $refreshed['access_token'];
		 else{
			 echo '$this->FilesOnedriverToken->save failed, please copy this and send to IT. Thanks'; die;
		 }
		} else
		 $token = $access_token['access_token']; // Token currently valid. Return it.


		$sd = new skydrive($token);

		// -- check all files in db, if there are cloud -> download it into server
		$files = $this->Uploader->find('first', array(
			'conditions' => array(
				'Uploader.cloud_id <>'   => '',
				'Uploader.cloud_type <>' => '',
				'Uploader.cloud_time <'  => strtotime('now') - 20,
				'Uploader.trangthai'     => 4
			),
			'contain'    => false
		));
		foreach ($files as $key => $file) {

			$response = $sd->curl_get(onedrive_base_url.$file['cloud_id']."?access_token=".$token);
			if (@array_key_exists('error', $response)) {
				throw new Exception($response['error']." - ".$response['description']);
				exit;
			} else
				$props = Array(
					'id'              => $response['id'],
					'type'            => $response['type'],
					'name'            => $response['name'],
					'parent_id'       => $response['parent_id'],
					'size'            => $response['size'],
					'source'          => $response['source'],
					'created_time'    => $response['created_time'],
					'updated_time'    => $response['updated_time'],
					'link'            => $response['link'],
					'upload_location' => $response['upload_location'],
					'is_embeddable'   => $response['is_embeddable']
			 );

			$response = $sd->curl_get(onedrive_base_url.$file['cloud_id']."/content?access_token=".$token, "false", "HTTP/1.1 302 Found");
			$arraytoreturn = [];
			if (@array_key_exists('error', $response)) {
				echo 'error if (@array_key_exists(\'error\', $response)) {';
			}else
				array_push($arraytoreturn, Array('properties' => $props, 'data' => $response));


			if (isset($arraytoreturn[0])) {
				$type = $file['cloud_type'];
				if ($type == 'new') {
					$id       = $file['id'];
					$cloud_id = $file['cloud_id'];
					unset($file['id'], $file['cloud_id'], $file['cloud_source'], $file['cloud_time'], $file['cloud_type']);

					// create new file with the same info
					// App::uses('AppShell', 'Console/Command');
					// App::uses('Controller', 'Controller');

					// App::uses('ComponentCollection', 'Controller');
					// App::uses('AuthComponent', 'Controller/Component');
					// $collection = new ComponentCollection();
					// $AuthComponent = new AuthComponent($collection);

					$file['nguoitao'] = $file['nguoisua'] = $file['cloud_up_user_id'];
					$file['cloud_up_user_id'] = '';
					$this->Uploader->create();
					if ($this->Uploader->save($file)) {

						// download new file and update to current save file
						$file_path = DIR_UPLOAD.$file['file'];
						$file_path = dirname($file_path).DS.$this->Uploader->id.'_'.basename($file_path);
						file_put_contents($file_path, $arraytoreturn[0]['data']);
						chmod($file_path, 0777);

						$save         = [];
						$save['id']   = $this->Uploader->id;
						$save['name'] = $this->Uploader->id.'_'.$file['name'];
						$save['file'] = str_replace(DIR_UPLOAD, '', $file_path);

						if ($this->Uploader->save($save)){
							$this->out('Success '.$this->Uploader->id);

							// update old file : remove cloud id
							$save                     = [];
							$save['id']               = $id;
							$save['cloud_id']         = '';
							$save['cloud_source']     = '';
							$save['cloud_time']       = '';
							$save['cloud_type']       = '';
							if ($this->Uploader->save($save)) {
								$this->out('Update success' . $save['id']);
								$sd->delete_object($cloud_id);
								unlink(WWW_ROOT.'upload'.DS.'tmp'.DS.'view_office_online'.DS.'temp_view_'.$id.'.php');
							}
						}
						else{

							$this->out('Error: if ($this->Uploader->save($file))');
							// $this->Uploader->delete($save['id']); // if save failed
						}


					}else
						$this->out('Error: if ($this->Uploader->save($file)) {');

				}else{
					// update to old file
					$file_path = DIR_UPLOAD.$file['file'];
					file_put_contents($file_path, $arraytoreturn[0]['data']);
					$cloud_id = $file['cloud_id'];
					$file['cloud_id']         = '';
					$file['cloud_source']     = '';
					$file['cloud_time']       = '';
					$file['cloud_type']       = '';
					$sd->delete_object($cloud_id);
					unlink(WWW_ROOT.'upload'.DS.'tmp'.DS.'view_office_online'.DS.'temp_view_'.$file['id'].'.php');
					if ($this->Uploader->save($file))
						$this->out('Update success => ' . $file['id']);
				}
			}
		}
		exit;
	}
}