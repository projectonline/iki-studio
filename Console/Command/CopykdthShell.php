<?php
class CopykdthShell extends AppShell {

	public $uses = array(
		'Task',
		'Kdthtask',
		'Comment'
	);

	public function main() {

		$this->out('bat dau copy ....');

		$datas = $this->Kdthtask->find('all', array(
			'conditions' => array(
				'Kdthtask.trangthai' => 4
			),
			'contain' => false
		));

		foreach( $datas as $data )
		{

			$save = $data['Kdthtask'];
			$id_bk = $save['id'];
			unset( $save['id'] );
			$save['type'] = 8;

			$this->Task->create();
			if( $this->Task->save( $save, false ) )
			{

				$field = array(
					'Comment.model' => '"Task"',
					'Comment.item_id' => $this->Task->id
				);
				$cond = array(
					'Comment.model' => 'Kdthtask',
					'Comment.item_id' => $id_bk
				);

				$this->Comment->belongsTo = array();
				if( !$this->Comment->updateAll( $field, $cond ) )
				{
					$this->out('Loi update Comment item_id='.$id_bk);
				}

			}else{

				$this->out('Loi update Task, Kdthtask id_bk='.$id_bk);
			}

		}

		$this->out('Ket thuc.');

		exit;
	}

}
