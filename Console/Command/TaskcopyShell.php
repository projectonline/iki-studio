<?php
class TaskcopyShell extends AppShell {

	public $uses = array('Task', 'Comment', 'Taskkientruc', 'Ddatask', 'Taskthinghiem', 'Taskketcauvt', 'Thicongtask', 'Hntask', 'Cnmttask');

	public function main() {

		$this->out('bat dau ....');

		unset( $this->uses[0], $this->uses[1] );
		$type = 1;

		foreach( $this->uses as $model )
		{
			$this->out('Khoi tao model=' . $model);

			$array_tmp = array(
//				'fields' => array('id', 'ngayky'),
//				'conditions' => array(
//					'Congtrinh.trangthai' => 4
//				),
				'limit' => 10000,
				'maxLimit' => 10000,
				'contain' => false
			);

			$datas = $this->{$model}->find('all', $array_tmp );

			foreach( $datas as $data )
			{
				$save = $data[$model];
				$comments = $this->Comment->find('all', array(
					'conditions' => array(
						'Comment.model' => $model,
						'Comment.item_id' => $save['id']
					),
					'maxLimit' => 2000,
					'limit' => 1000,
					'contain' => false
				) );

				// LUU LOI NHAN TASK
				unset($save['id']);
				$save['type'] = $type;

				$this->Task->create();
				if( $this->Task->save( $save, false ) )
				{
					// LUU COMMENT
					if( isset($comments[0]) )
					{
						//$comments = Set::classicExtract($comments, '{n}.Comment');
						$tmp = array();
						foreach( $comments as $comment )
						{
							$comment = $comment['Comment'];
							unset($comment['id']);
							$comment['item_id'] = $this->Task->id;
							$comment['model'] = 'Task';
							$tmp[] = $comment;
						}

						if( !$this->Comment->saveAll($tmp) )
						{
							$this->out('Save error: Comment' );
						}
					}
				}else{
					$this->out('Save error: Task' );
				}
			}

			$type += 1;

			$this->out('Ket thuc');
		}

		$this->out('Xong.');

		exit;
	}
}
