<?php
class LactShell extends AppShell {

	public $uses = array(
		'Congtrinh',
		'CongtrinhsTienvevt',
		'CongtrinhsTongchivt'
	);

	public function main() {

		$this->out('bat dau copy ....');

		$congtrinh = $this->Congtrinh->find('list', array(
			'fields' => array('id', 'id'),
			'conditions' => array('Congtrinh.trangthai' => 4),
			'contain' => false,
		));

		$datas = $this->CongtrinhsTienvevt->find('all', array(
			'fields' => array('id', 'sum(giatri) as sum', 'congtrinh_id'),
			'conditions' => array('CongtrinhsTienvevt.trangthai' => 4),
			'contain' => false,
			'group' => 'congtrinh_id'
		));

		$tmp = $congtrinh;
		foreach( $datas as $data )
		{
			$this->Congtrinh->id = $data['CongtrinhsTienvevt']['congtrinh_id'];

			if( !$this->Congtrinh->saveField('giatritienve', $data[0]['sum']) )
			{
				$this->out('error_'.$data[0]['sum'].'_'.$data['CongtrinhsTienvevt']['congtrinh_id']);
			}else{
				unset($tmp[$data['CongtrinhsTienvevt']['congtrinh_id']]);
//				$this->out('success_'.number_format($data[0]['sum']).'_'.$data['CongtrinhsTienvevt']['congtrinh_id']);
			}
		}

		// updateAll
		$field = array('Congtrinh.giatritienve' => 0);
		$cond = array('Congtrinh.id' => $tmp);
		$this->Congtrinh->belongsTo = array();
		if( !$this->Congtrinh->updateAll( $field, $cond ) ){
			$this->out('error updateAll.');
		}


		// =====================================================================
		// CONG TRINH TIEN CHI
		$datas = $this->CongtrinhsTongchivt->find('all', array(
			'fields' => array('id', 'sum(giatri) as sum', 'congtrinh_id'),
			'conditions' => array('CongtrinhsTongchivt.trangthai' => 4),
			'contain' => false,
			'group' => 'congtrinh_id'
		));

		$tmp = $congtrinh;
		foreach( $datas as $data )
		{
			$this->Congtrinh->id = $data['CongtrinhsTongchivt']['congtrinh_id'];

			if( !$this->Congtrinh->saveField('giatridachi', $data[0]['sum']) )
			{
				$this->out('error_'.$data[0]['sum'].'_'.$data['CongtrinhsTongchivt']['congtrinh_id']);
			}else{
				unset($tmp[$data['CongtrinhsTongchivt']['congtrinh_id']]);
//				$this->out('success_'.number_format($data[0]['sum']).'_'.$data['CongtrinhsTienvevt']['congtrinh_id']);
			}
		}

		// updateAll
		$field = array('Congtrinh.giatridachi' => 0);
		$cond = array('Congtrinh.id' => $tmp);
		$this->Congtrinh->belongsTo = array();
		if( !$this->Congtrinh->updateAll( $field, $cond ) ){
			$this->out('error updateAll.');
		}

		$this->out('Ket thuc.');

		exit;
	}

}
