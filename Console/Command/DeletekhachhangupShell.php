<?php
class DeletekhachhangupShell extends AppShell {

	public $uses = array(
		'Uploader'
	);

	public function main() {

		$this->out('bat dau copy ....');

		$datas = $this->Uploader->find('all', array(
			'conditions' => array(
				'Uploader.file LIKE' => "khachhanguploads%"
			),
			'contain' => false
		));

		foreach( $datas as $data )
		{
			if( file_exists(APP_DIR_UPLOAD.$data['Uploader']['file']) )
			{
				$this->out('deleting file ....');
				$this->out('name: '.$data['Uploader']['name'].' ... link: '.$data['Uploader']['file']);
				if( unlink(APP_DIR_UPLOAD.$data['Uploader']['file']) )
				{
					$this->out('delete thanh cong');
				}else{

					$this->out('that bai');
				}
			}
		}

		$this->out('Ket thuc.');

		exit;
	}

}
