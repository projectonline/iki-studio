<?php
class TaskremoveShell extends AppShell {

	public $uses = array('Task', 'Comment', 'Taskkientruc', 'Ddatask', 'Taskthinghiem', 'Taskketcauvt', 'Thicongtask', 'Hntask', 'Cnmttask');

	public function main() {

		$this->out('bat dau ....');

		unset( $this->uses[0], $this->uses[1] );

		foreach( $this->uses as $model )
		{
			$this->out('Khoi tao model=' . $model);

			$array_tmp = array(
//				'fields' => array('id', 'ngayky'),
//				'conditions' => array(
//					'Congtrinh.trangthai' => 4
//				),
				'limit' => 10000,
				'maxLimit' => 10000,
				'contain' => false
			);

			$datas = $this->{$model}->find('all', $array_tmp );

			foreach( $datas as $data )
			{
				// save trangthai = 9;
				$tmp = $this->Task->find('first', array(
					'fields' => array('id', 'tieude'),
					'conditions' => array(
						'Task.type' => 0,
						'Task.trangthai' => 4,
						'Task.tieude' => $data[$model]['tieude']
					),
					'contain' => false
				));

				if(isset($tmp['Task']))
				{
					$update_tmp = array(
						'id' => $tmp['Task']['id'],
						'trangthai' => 9
					);

					if( $this->Task->save($update_tmp, false) )
					{
						$this->out('update trangthai id='.$tmp['Task']['id'].' ___ tieude='.$tmp['Task']['tieude']);
					}else{
						$this->out('update trangthai error');
					}
				}
			}

			$this->out('Ket thuc model');
		}

		$this->out('Xong.');

		exit;
	}
}
